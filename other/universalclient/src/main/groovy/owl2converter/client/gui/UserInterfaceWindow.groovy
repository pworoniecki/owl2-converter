package owl2converter.client.gui

import codetoowl2.CodeToOWL2TranslationEngine
import commoncodemetamodel.Element
import commoncodemetamodel.Folder
import commoncodemetamodel.tools.CommonCodeMetamodelReader
import commoncodemetamodel.tools.CommonCodeMetamodelWriter
import commoncodemetamodel.tools.exception.FileWriteException
import fromowl2builderapi.FromOWL2Translator
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.client.exception.ValidationException
import owl2metamodel.OntologyWriter
import owl2metamodel.OwlOntologyFormat
import owl2tocode.OWL2ToCodeTranslationEngine
import toowl2builderapi.ToOWL2Translator
import translatorapi.TranslatorAPI

import javax.swing.*
import javax.swing.filechooser.FileNameExtensionFilter
import java.awt.*
import java.util.List

@Component
class UserInterfaceWindow extends JFrame {

  private static final int ROWS = 3
  private static final int COLUMNS = 1
  private static final String FROM_OWL2_TRANSLATOR_SOURCE_LANGUAGE_NAME = 'OWL2'

  private JPanel mainPanel

  // file locations panel section
  private JPanel fileLocationsPanel
  private JTextField sourceFileLocationTextField
  private JButton chooseSourceFileButton
  private JTextField codeDestinationDirectoryTextField
  private JButton chooseGeneratedFilesDirectoryButton

  // code generation section
  private JButton generateCodeButton
  private JLabel statusLabel

  // converter translator section
  private JComboBox<String> fromTypeDropDownList = new JComboBox<>([] as String[])
  private JComboBox<String> toTypeDropDownList = new JComboBox<>([] as String[])

  private File chosenSourceFile
  private File chosenCodeDestinationDirectory


  // TODO too many dependencies
  private OWL2ToCodeTranslationEngine owl2ToCodeTranslationEngine
  private CodeToOWL2TranslationEngine codeToOWL2TranslationEngine
  private CommonCodeMetamodelWriter codeMetamodelWriter
  private List<FromOWL2Translator> fromOWL2Translators
  private List<ToOWL2Translator> toOWL2Translators
  private List<TranslatorAPI> translators
  private OntologyWriter ontologyWriter
  private CommonCodeMetamodelReader codeMetamodelReader

  @Autowired
  UserInterfaceWindow(OWL2ToCodeTranslationEngine owl2ToCodeTranslationEngine, CommonCodeMetamodelWriter codeMetamodelWriter,
                      Optional<List<FromOWL2Translator>> fromOWL2Translators, Optional<List<ToOWL2Translator>> toOWL2Translators,
                      CodeToOWL2TranslationEngine codeToOWL2TranslationEngine, CommonCodeMetamodelReader codeMetamodelReader,
                      OntologyWriter ontologyWriter) {
    super('OWL2<->Code converter')
    this.owl2ToCodeTranslationEngine = owl2ToCodeTranslationEngine
    this.codeMetamodelWriter = codeMetamodelWriter
    this.fromOWL2Translators = fromOWL2Translators.orElseGet { new ArrayList<>() }
    this.toOWL2Translators = toOWL2Translators.orElseGet { new ArrayList<>() }
    translators = [] + this.fromOWL2Translators + this.toOWL2Translators
    this.codeToOWL2TranslationEngine = codeToOWL2TranslationEngine
    this.codeMetamodelReader = codeMetamodelReader
    this.ontologyWriter = ontologyWriter
  }

  void showWindow() {
    this.mainPanel = new JPanel(new GridLayout(ROWS, COLUMNS))
    this.mainPanel.border = BorderFactory.createEmptyBorder(10, 10, 10, 10)
    setContentPane(mainPanel)
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
    setResizable(false)
    setSize(800, 200)

    initializeComponents()
    pack()
    setButtonActions()
    setVisible(true)
  }

  private void initializeComponents() {
    initializeFileLocationsPanel()
    initializeConversionTypePanel()
    initializeCodeGenerationSection()
    disableButtons()
  }

  private void initializeFileLocationsPanel() {
    def layout = new GridLayout(2, 3)
    layout.with {
      vgap = 10
      hgap = 10
    }
    this.fileLocationsPanel = new JPanel(layout)
    this.mainPanel.add(this.fileLocationsPanel)

    initializeFilesChooseSection()
  }

  private void initializeFilesChooseSection() {
    this.chooseSourceFileButton = new JButton('Choose source')
    this.chooseGeneratedFilesDirectoryButton = new JButton('Choose directory')
    updateFileLocationsPanel()
  }

  private void updateFileLocationsPanel() {
    this.fileLocationsPanel.clear()
    this.sourceFileLocationTextField = createNotEditableTextField()
    JLabel sourceFileDescriptionLabel = new JLabel('Source file/directory')
    if (isAnyConversionSelected()) {
      if (isConversionFromOWL2()) {
        sourceFileDescriptionLabel.text = 'OWL ontology file'
        this.chooseSourceFileButton.text = 'Choose OWL2 file'
      } else {
        sourceFileDescriptionLabel.text = 'Source code directory'
        this.chooseSourceFileButton.text = 'Choose code directory'
      }
    }
    this.fileLocationsPanel.with {
      add(sourceFileDescriptionLabel)
      add(this.sourceFileLocationTextField, 1)
      add(this.chooseSourceFileButton, 2)
    }
    initializeCodeDestinationSection()
    this.fileLocationsPanel.repaint()
    this.fileLocationsPanel.revalidate()
  }

  private boolean isAnyConversionSelected() {
    return fromTypeDropDownList.selectedIndex != -1
  }

  private static JTextField createNotEditableTextField() {
    return new JTextField(editable: false, focusable: false)
  }

  private void initializeCodeDestinationSection() {
    this.codeDestinationDirectoryTextField = createNotEditableTextField()
    this.fileLocationsPanel.with {
      add(new JLabel('Generated files destination'))
      add(this.codeDestinationDirectoryTextField)
      add(this.chooseGeneratedFilesDirectoryButton)
    }
  }

  private void initializeConversionTypePanel() {
    def conversionTypePanel = new JPanel(layout: new GridBagLayout())
    this.mainPanel.add(conversionTypePanel)

    JLabel conversionFromLabel = new JLabel('From')
    def conversionFromLabelConstraints = new GridBagConstraints(gridx:0, gridy: 0, insets: new Insets(0, 0, 0, 0))
    conversionTypePanel.add(conversionFromLabel, conversionFromLabelConstraints)

    String[] sourceLanguageNames = fromOWL2Translators*.sourceLanguageName + toOWL2Translators*.sourceLanguageName
    fromTypeDropDownList.setModel(new DefaultComboBoxModel<String>(sourceLanguageNames))
    fromTypeDropDownList.setSelectedIndex(-1)
    fromTypeDropDownList.addActionListener {
      if (isAnyConversionSelected()) {
        enableComponents()
      }
      String selectedSourceLanguage = fromTypeDropDownList.selectedItem.toString()
      List<String> targetLanguages = translators.findAll { it.sourceLanguageName == selectedSourceLanguage }*.targetLanguageName
      toTypeDropDownList.setModel(new DefaultComboBoxModel<String>(targetLanguages as String[]))
      updateFileLocationsPanel()
    }
    def fromTypeDropDownListConstraints = new GridBagConstraints(gridx: 1, gridy: 0, insets: new Insets(0, 5, 0, 0))
    conversionTypePanel.add(fromTypeDropDownList, fromTypeDropDownListConstraints)

    JLabel conversionToLabel = new JLabel('To')
    def conversionToLabelConstraints = new GridBagConstraints(gridx: 2, gridy: 0, insets: new Insets(0, 5, 0, 0))
    conversionTypePanel.add(conversionToLabel, conversionToLabelConstraints)

    def toTypeDropDownListConstraints = new GridBagConstraints(gridx: 3, gridy: 0, insets: new Insets(0, 5, 0, 0))
    conversionTypePanel.add(toTypeDropDownList, toTypeDropDownListConstraints)
  }

  private void initializeCodeGenerationSection() {
    def panel = new JPanel(layout: new GridBagLayout())
    this.mainPanel.add(panel)
    initializeCodeGenerationButton(panel)
    initializeCodeGenerationStatus(panel)
  }

  private void initializeCodeGenerationButton(JPanel panel) {
    this.generateCodeButton = new JButton('Generate code')
    panel.add(this.generateCodeButton, new GridBagConstraints(gridy: 0, ipadx: 100))
  }

  private void initializeCodeGenerationStatus(JPanel panel) {
    this.statusLabel = new JLabel('Choose conversion type, source file/directory and directory for generated files. Then click Convert button.')
    def constraints = new GridBagConstraints(gridy: 1, insets: new Insets(5, 0, 0, 0))
    panel.add(this.statusLabel, constraints)
  }

  private void setButtonActions() {
    chooseSourceFileButton.addActionListener { chooseSourceFile() }
    chooseGeneratedFilesDirectoryButton.addActionListener { chooseGeneratedFilesDirectory() }
    generateCodeButton.addActionListener { runConversion() }
  }

  private void chooseSourceFile() {
    if (isConversionFromOWL2()) {
      chooseOntologyFile()
    } else {
      chooseCodeDirectory()
    }
  }

  private void chooseOntologyFile() {
    def fileFilter = new FileNameExtensionFilter('OWL ontology file', 'owl', 'rdf')
    def fileChooser = new JFileChooser(fileFilter: fileFilter, approveButtonText: 'Choose selected file')
    int fileChooserReturnValue = fileChooser.showOpenDialog(this)

    if (fileChooserReturnValue == JFileChooser.APPROVE_OPTION) {
      this.chosenSourceFile = fileChooser.selectedFile
      this.sourceFileLocationTextField.text = this.chosenSourceFile.absolutePath
    }
  }

  private void chooseCodeDirectory() {
    def fileChooser = new JFileChooser(approveButtonText: 'Choose selected directory',
        fileSelectionMode: JFileChooser.DIRECTORIES_ONLY)
    int fileChooserReturnValue = fileChooser.showOpenDialog(this)

    if (fileChooserReturnValue == JFileChooser.APPROVE_OPTION) {
      this.chosenSourceFile = fileChooser.selectedFile
      this.sourceFileLocationTextField.text = this.chosenSourceFile.absolutePath
    }
  }

  private void chooseGeneratedFilesDirectory() {
    def directoryChooser = new JFileChooser(fileSelectionMode: JFileChooser.DIRECTORIES_ONLY,
        approveButtonText: 'Choose selected directory')
    int directoryChooserReturnValue = directoryChooser.showOpenDialog(this)

    if (directoryChooserReturnValue == JFileChooser.APPROVE_OPTION) {
      this.chosenCodeDestinationDirectory = directoryChooser.selectedFile
      this.codeDestinationDirectoryTextField.text = this.chosenCodeDestinationDirectory.absolutePath
    }
  }

  private void runConversion() {
    disableComponents()
    try {
      validateBeforeGeneratingCode()
    } catch (ValidationException e) {
      showErrorMessage(e.message)
      enableComponents()
      return
    }
    disableUnusedTranslators()
    if (isConversionFromOWL2()) {
      runCodeGeneration()
    } else {
      runOntologyGeneration()
    }
  }

  private void disableUnusedTranslators() {
    translators.each {
      if (fromTypeDropDownList.selectedItem.toString() == it.sourceLanguageName ||
          toTypeDropDownList.selectedItem.toString() == it.targetLanguageName) {
        it.enable()
      } else {
        it.disable()
      }
    }
  }

  private boolean isConversionFromOWL2() {
    return fromTypeDropDownList.selectedItem == FROM_OWL2_TRANSLATOR_SOURCE_LANGUAGE_NAME
  }

  private void runCodeGeneration() {
    def codeGenerationTask = {
      this.statusLabel.text = 'Please wait - conversion is in progress...'
      Optional<Set<Element>> sourceCode = convertChosenOntologyToCode()
      if (!sourceCode.isPresent()) {
        this.statusLabel.text = 'Failed to perform conversion'
        enableComponents()
        return
      }
      this.statusLabel.text = 'Please wait - storing generated code files...'
      TaskStatus codeFilesGenerationStatus = generateCodeFiles(sourceCode.get())
      if (codeFilesGenerationStatus == TaskStatus.ERROR) {
        this.statusLabel.text = 'Failed to save generated code files'
        enableComponents()
        return
      }
      this.statusLabel.text = 'Conversion successfully completed. ' +
          "Result files have been saved in directory: ${this.chosenCodeDestinationDirectory.absolutePath}"
      showCodeGenerationCompletedMessage()
      enableComponents()
    }
    new Thread(codeGenerationTask).start()
  }

  private Optional<Set<Element>> convertChosenOntologyToCode() {
    try {
      List<Set<Element>> ontologyTranslations = owl2ToCodeTranslationEngine.translate(this.chosenSourceFile)
      if (ontologyTranslations.size() != 1) {
        return Optional.empty()
      }
      return Optional.of(ontologyTranslations.first())
    } catch (Exception e) {
      e.printStackTrace()
      handleGenerationCodeError('An error occurred during ontology analyze. Please ensure ' +
          'it is provided in correct format (OWL/XML - e.g. generated in Protégé).')
      return Optional.empty()
    }
  }

  private void handleGenerationCodeError(String errorMessage) {
    showErrorMessage(errorMessage)
    this.statusLabel.text = errorMessage
    enableComponents()
  }

  private TaskStatus generateCodeFiles(Set<Element> codeMetamodel) {
    TaskStatus status = TaskStatus.SUCCESS
    try {
      codeMetamodelWriter.write(chosenCodeDestinationDirectory.toPath(), codeMetamodel)
    } catch (FileWriteException e) {
      e.printStackTrace()
      handleGenerationCodeError("Cannot save generated source in destination directory (${e.message}). " +
          'Please ensure if you have permissions to write files in the directory.')
      status = TaskStatus.ERROR
    } catch (Exception e) {
      e.printStackTrace()
      handleGenerationCodeError('Unexpected error occurred. Please try again or report this bug.')
      status = TaskStatus.ERROR
    }
    return status
  }

  private void runOntologyGeneration() {
    def ontologyGenerationTask = {
      this.statusLabel.text = 'Please wait - conversion is in progress...'
      Optional<OWLOntology> ontology = convertChosenCodeToOntology()
      if (!ontology.isPresent()) {
        this.statusLabel.text = 'Failed to perform conversion'
        enableComponents()
        return
      }
      this.statusLabel.text = 'Please wait - storing generated ontology file...'
      TaskStatus codeFilesGenerationStatus = generateOntologyFile(ontology.get())
      if (codeFilesGenerationStatus == TaskStatus.ERROR) {
        this.statusLabel.text = 'Failed to save generated ontology file'
        enableComponents()
        return
      }
      this.statusLabel.text = 'Conversion successfully completed. ' +
          "Result ontology file has been saved in directory: ${this.chosenCodeDestinationDirectory.absolutePath}"
      showCodeGenerationCompletedMessage()
      enableComponents()
    }
    new Thread(ontologyGenerationTask).start()
  }

  private Optional<OWLOntology> convertChosenCodeToOntology() {
    try {
      Folder codeFolder = codeMetamodelReader.parseFolder(this.chosenSourceFile.toPath())
      List<OWLOntology> generatedOntologies = codeToOWL2TranslationEngine.translate(codeFolder.elements)
      if (generatedOntologies.size() != 1) {
        return Optional.empty()
      }
      return Optional.of(generatedOntologies.first())
    } catch (Exception e) {
      e.printStackTrace()
      handleGenerationCodeError("An error occurred during code to ontology translation. Details: ${e.message}")
      return Optional.empty()
    }
  }

  private TaskStatus generateOntologyFile(OWLOntology ontology) {
    TaskStatus status = TaskStatus.SUCCESS
    try {
      // TODO let user to choose owl ontology format
      ontologyWriter.write(ontology, OwlOntologyFormat.FUNCTIONAL, chosenCodeDestinationDirectory)
    } catch (FileWriteException e) {
      e.printStackTrace()
      handleGenerationCodeError("Cannot save generated ontology in destination directory (details: ${e.message}). " +
          'Please ensure if you have permissions to write files in the directory.')
      status = TaskStatus.ERROR
    } catch (Exception e) {
      e.printStackTrace()
      handleGenerationCodeError('Unexpected error occurred. Please try again or report this bug.')
      status = TaskStatus.ERROR
    }
    return status
  }

  private void disableComponents() {
    disableButtons()
    this.fromTypeDropDownList.enabled = false
    this.toTypeDropDownList.enabled = false
  }

  private void disableButtons() {
    this.generateCodeButton.enabled = false
    this.chooseSourceFileButton.enabled = false
    this.chooseGeneratedFilesDirectoryButton.enabled = false
  }

  private void validateBeforeGeneratingCode() throws ValidationException {
    if (!this.chosenSourceFile) {
      throw new ValidationException('Input file has not been chosen. Choose it before conversion.')
    }
    if (!this.chosenSourceFile.exists()) {
      throw new ValidationException('Chosen input file does not exist or you do not have permissions to read the file. ' +
          'Choose correct file.')
    }
    if (!this.chosenCodeDestinationDirectory) {
      throw new ValidationException('You have not selected destination directory. Choose it before conversion.')
    }
  }

  private void showErrorMessage(String errorMessage) {
    JOptionPane.showMessageDialog(this, errorMessage, 'Cannot perform conversion', JOptionPane.ERROR_MESSAGE)
  }

  private void enableComponents() {
    this.generateCodeButton.enabled = true
    this.chooseSourceFileButton.enabled = true
    this.chooseGeneratedFilesDirectoryButton.enabled = true
    this.fromTypeDropDownList.enabled = true
    this.toTypeDropDownList.enabled = true
  }

  private void showCodeGenerationCompletedMessage() {
    JOptionPane.showMessageDialog(this, "Conversion from ${fromTypeDropDownList.selectedItem} to ${toTypeDropDownList.selectedItem} successfully completed. " +
        "Result files have been saved in directory: ${this.chosenCodeDestinationDirectory.absolutePath}",
        'Conversion completed', JOptionPane.INFORMATION_MESSAGE)
  }
}
