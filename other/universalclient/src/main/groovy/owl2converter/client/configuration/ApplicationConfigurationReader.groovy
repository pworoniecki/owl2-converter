package owl2converter.client.configuration

import org.yaml.snakeyaml.Yaml
import owl2converter.client.configuration.model.ApplicationConfiguration

class ApplicationConfigurationReader {

  static ApplicationConfiguration readConfiguration(String configurationFilePath) {
    def configurationFileStream = this.classLoader.getResourceAsStream(configurationFilePath)
    return new Yaml().loadAs(configurationFileStream, ApplicationConfiguration)
  }
}
