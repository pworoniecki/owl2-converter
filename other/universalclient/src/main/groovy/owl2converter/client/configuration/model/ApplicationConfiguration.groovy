package owl2converter.client.configuration.model

class ApplicationConfiguration {

  private GeneratorConfiguration generator

  GeneratorConfiguration getGenerator() {
    return generator
  }

  void setGenerator(GeneratorConfiguration generator) {
    this.generator = generator
  }
}
