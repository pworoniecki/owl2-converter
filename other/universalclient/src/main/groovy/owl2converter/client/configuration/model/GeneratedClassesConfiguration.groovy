package owl2converter.client.configuration.model

class GeneratedClassesConfiguration {

  private String basePackage
  private String modelSubPackage
  private String exceptionSubPackage
  private int indentationStep

  String getBasePackage() {
    return basePackage
  }

  void setBasePackage(String basePackage) {
    this.basePackage = basePackage
  }

  void setModelSubPackage(String modelSubPackage) {
    this.modelSubPackage = modelSubPackage
  }

  String getModelFullPackage() {
    return "$basePackage.$modelSubPackage"
  }

  void setExceptionSubPackage(String exceptionSubPackage) {
    this.exceptionSubPackage = exceptionSubPackage
  }

  String getExceptionFullPackage() {
    return "$basePackage.$exceptionSubPackage"
  }

  int getIndentationStep() {
    return indentationStep
  }

  void setIndentationStep(int indentation) {
    this.indentationStep = indentation
  }
}
