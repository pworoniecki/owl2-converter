package owl2converter.client.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class ValidationException extends Exception {
}
