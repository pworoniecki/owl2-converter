package owl2converter.client

import codetoowl2.CodeToOWL2Configuration
import commoncodemetamodel.CommonCodeMetamodelConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import owl2tocode.OWL2ToCodeConfiguration

@Configuration
@ComponentScan(basePackages = 'owl2converter')
@Import([OWL2ToCodeConfiguration, CodeToOWL2Configuration, CommonCodeMetamodelConfiguration])
class ApplicationSpringContextConfiguration {
}
