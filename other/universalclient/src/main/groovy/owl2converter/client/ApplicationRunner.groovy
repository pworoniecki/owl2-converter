package owl2converter.client

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import owl2converter.client.gui.UserInterfaceWindow
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext

import java.lang.reflect.Method
import java.nio.file.Paths

class ApplicationRunner {

  private static final String TRANSLATORS_DIRECTORY_NAME = 'translators'
  private static final Logger logger = LoggerFactory.getLogger(ApplicationRunner)

  static void main(String[] args) {
    loadTranslators()
    ApplicationContext applicationContext = initializeSpringContext()
    applicationContext.getBean(UserInterfaceWindow).showWindow()
  }

  private static void loadTranslators() {
    File translatorsDirectory = new File(TRANSLATORS_DIRECTORY_NAME)
    if (!translatorsDirectory.exists()) {
      logger.warn("Missing ${Paths.get(TRANSLATORS_DIRECTORY_NAME).toAbsolutePath()} directory - no translators will be loaded")
      return
    }
    translatorsDirectory.listFiles ({it.isFile()} as FileFilter).each { loadTranslator(it) }
  }

  // TODO better solution should be found in the future as this usage of reflection is a trick
  private static void loadTranslator(File translatorJarFile) {
    URLClassLoader urlClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader()
    Method method = URLClassLoader.getDeclaredMethod("addURL", [URL] as Class[])
    method.setAccessible(true)
    method.invoke(urlClassLoader, [translatorJarFile.toURI().toURL()] as Object[])
    logger.info("Loaded translators from ${translatorJarFile.toPath().toAbsolutePath()} file")
  }

  private static ApplicationContext initializeSpringContext() {
    def applicationContext = new AnnotationConfigApplicationContext()
    applicationContext.register(ApplicationSpringContextConfiguration)
    applicationContext.refresh()
    return applicationContext
  }
}
