package owl2converter.utils

/**
 * Utility class to check specific assertions.
 */
class Preconditions {

  /**
   * Asserts that given collection does not contain null.
   *
   * @param collection the collection to be checked
   * @throws IllegalArgumentException if collection contains null
   */
  static void assertNoNullInCollection(Collection<?> collection) {
    if (collection.any { it == null }) {
      throw new IllegalArgumentException("There is forbidden null element in collection: $collection")
    }
  }
}
