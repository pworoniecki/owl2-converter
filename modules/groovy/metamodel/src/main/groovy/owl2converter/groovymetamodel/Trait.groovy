package owl2converter.groovymetamodel

import groovy.transform.PackageScope
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method

/**
 * Class being a representation of Groovy trait
 */
class Trait {

  private String packageName
  private String name
  private List<Annotation> annotations
  private List<Trait> inheritedTraits
  private List<Attribute> attributes
  private List<Method> methods

  /**
   * Class constructor.
   *
   * @param packageName the package name of the trait
   * @param name the trait name
   * @param annotations the list of annotations used for the trait
   * @param inheritedTraits the list of traits inherited by the trait
   * @param attributes the list of trait attributes
   * @param methods the list of trait methods
   */
  @PackageScope Trait(String packageName, String name, List<Annotation> annotations, List<Trait> inheritedTraits,
                      List<Attribute> attributes, List<Method> methods) {
    this.packageName = packageName
    this.name = name
    this.annotations = annotations
    this.inheritedTraits = inheritedTraits
    this.attributes = attributes
    this.methods = methods
  }

  /**
   * Returns the trait package name.
   *
   * @return the package name of the trait
   */
  String getPackageName() {
    return packageName
  }

  /**
   * Returns the trait name.
   *
   * @return the trait name
   */
  String getName() {
    return name
  }

  /**
   * Returns the list of annotations used for the trait.
   *
   * @return the list of trait annotations
   */
  List<Annotation> getAnnotations() {
    return annotations
  }

  /**
   * Returns the list of traits inherited by the trait.
   *
   * @return the list of children traits
   */
  List<Trait> getInheritedTraits() {
    return inheritedTraits
  }

  /**
   * Returns the list of trait attributes.
   *
   * @return the list of trait attributes
   */
  List<Attribute> getAttributes() {
    return attributes
  }

  /**
   * Returns the list of trait methods.
   *
   * @return the list of trait methods
   */
  List<Method> getMethods() {
    return methods
  }

  /**
   * Returns the qualified name of the trait using the following pattern: {@code packageName.traitName}.
   *
   * @return the qualified name of the trait
   */
  String getFullName() {
    return "$packageName.$name"
  }

  /**
   * Checks if two traits are equal.
   *
   * @param o the other trait to be compared with
   * @return true if the package names are equal and the trait name are equal
   */
  boolean equals(o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false

    Trait that = (Trait) o
    return this.name == that.name && this.packageName == that.packageName
  }

  /**
   * Returns the hash code of the trait.
   *
   * @return the hash code calculated on the base of trait name.
   */
  int hashCode() {
    return name.hashCode()
  }

  /**
   * Runs a visitor for each attribute and each method.
   *
   * @param visitor the visitor to perform an operation on the trait
   */
  void accept(TraitElementVisitor visitor) {
    attributes.each { visitor.visit(it) }
    methods.each { visitor.visit(it) }
  }

  /**
   * A builder class to create a new trait.
   */
  static class Builder extends TraitBuilder<Builder, Trait> {

    /**
     * Class constructor.
     *
     * @param packageName a name of the package in which the trait is to be defined
     * @param name a name of the trait
     */
    Builder(String packageName, String name) {
      super(packageName, name)
    }

    /**
     * Returns the instance of the builder.
     *
     * @return the builder instance
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * A factory method to create a new trait.
     *
     * @return the new trait
     */
    @Override
    Trait buildTrait() {
      return new Trait(packageName, name, annotations, inheritedTraits, attributes, methods)
    }
  }
}
