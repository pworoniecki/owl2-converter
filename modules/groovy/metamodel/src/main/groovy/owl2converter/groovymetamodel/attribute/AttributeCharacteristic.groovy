package owl2converter.groovymetamodel.attribute

/**
 * Enumerator for attribute characteristics.
 */
enum AttributeCharacteristic {
  SYMMETRIC, ASYMMETRIC, TRANSITIVE, FUNCTIONAL, INVERSE_FUNCTIONAL, REFLEXIVE, IRREFLEXIVE
}
