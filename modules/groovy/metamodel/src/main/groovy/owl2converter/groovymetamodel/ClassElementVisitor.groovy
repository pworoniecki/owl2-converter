package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method

/**
 * An interface implemented by visitors performing the translation process.
 */
interface ClassElementVisitor {

  void visit(Constructor constructor, Class constructorOwner)

  void visit(Attribute attribute)

  void visit(Method method)
}
