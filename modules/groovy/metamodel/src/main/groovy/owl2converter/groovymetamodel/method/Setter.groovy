package owl2converter.groovymetamodel.method

import groovy.transform.InheritConstructors
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType

/**
 * An abstract class being a Groovy method meta-class with fixed name and parameters list.
 * It is designed to be an accessor method for an attribute that sets its value.<br>
 * Its name consists of "set" prefix and capitalized attribute's name. The parameters list of the method contains single parameter: the value to be set.<br>
 * Therefore its signature looks like: setAttribute(Attribute attribute)
 */
@InheritConstructors
class Setter extends Accessor {

  /**
   * Returns the setter parameter.
   *
   * @return the setter parameter
   */
  Parameter getParameter() {
    return parameters.first()
  }

  /**
   * Internal static builder to create setter methods.
   */
  static class Builder extends Accessor.Builder<Setter, Builder> {

    /**
     * Class constructor.
     *
     * @param attribute an attribute for which a setter will be created
     */
    Builder(Attribute attribute) {
      super(getName(attribute), attribute)
      this.parameters = [createParameter(attribute)]
      this.returnType = VoidType.INSTANCE
    }

    private static Parameter createParameter(Attribute attribute) {
      return new Parameter(attribute.valueType, attribute.name)
    }

    /**
     * Returns the builder instance.
     *
     * @return the builder instance
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * A factory method to create a new setter method.
     *
     * @return a new instance of setter method for given attribute
     */
    @Override
    Setter buildMethod() {
      return new Setter(this)
    }

    /**
     * Overridden for compatibility reasons.
     *
     * @param parameters
     * @return nothing
     * @throws UnsupportedOperationException always thrown as parameters list is fixed
     */
    @Override
    Builder withParameters(List<Parameter> parameters) {
      throw new UnsupportedOperationException('Parameters cannot be changed for setter.')
    }

    /**
     * Overridden for compatibility reasons.
     *
     * @param type
     * @return nothing
     * @throws UnsupportedOperationException always thrown as parameters list is fixed
     */
    @Override
    Builder withReturnType(Type type) {
      throw new UnsupportedOperationException('Return type cannot be changed for setter.')
    }
  }

  /**
   * Returns the name of the setter method in the form 'setAttribute_name' where 'Attribute_name' is the attribute's name.<br>
   * Example: if attribute's name is 'person', then the returned method's name is: {@code setPerson}
   *
   * @param attribute an attribute for which the method is to be created
   * @return the name of the setter method
   */
  static String getName(Attribute attribute) {
    return "set${attribute.name.capitalize()}"
  }
}
