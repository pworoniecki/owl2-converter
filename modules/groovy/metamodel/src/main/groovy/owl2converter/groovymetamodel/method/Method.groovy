package owl2converter.groovymetamodel.method

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType
import owl2converter.utils.Preconditions

import static com.google.common.base.Preconditions.checkNotNull

/**
 * Abstract class being a Groovy method meta-class.
 */
abstract class Method {

  private String name
  private AccessModifier accessModifier
  private boolean staticMethod = false
  private Type returnType
  private List<Parameter> parameters

  /**
   * Class constructor.
   *
   * @param builder the builder used for a method creation
   */
  protected Method(Builder builder) {
    this.name = builder.name
    this.accessModifier = builder.accessModifier
    this.staticMethod = builder.staticMethod
    this.returnType = builder.returnType
    this.parameters = builder.parameters
  }

  /**
   * Returns the name of the method.
   *
   * @return the method name
   */
  String getName() {
    return name
  }

  /**
   * Returns the access modifier of the method.
   *
   * @return the method access modifier
   */
  AccessModifier getAccessModifier() {
    return accessModifier
  }

  /**
   * Returns information if the method is static.
   *
   * @return true if the method is static
   */
  boolean isStatic() {
    return staticMethod
  }

  /**
   * Returns Type of the method.
   *
   * @return the method returned type
   */
  Type getReturnType() {
    return returnType
  }

  /**
   * Returns the list of method parameters.
   *
   * @return the list of parameters
   */
  List<Parameter> getParameters() {
    return parameters
  }

  /**
   * Checks if the method is the same as the other one given by the parameter.
   *
   * @param o the reference to the method to be compared
   * @return true if the method has the same name and parameters as the method passed by parameter
   */
  boolean equals(o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false

    Method method = (Method) o
    return method.name == this.name && method.parameters == this.parameters
  }

  /**
   * Returns the method hask code calculate on the base of method name.
   *
   * @return the method hash code
   */
  int hashCode() {
    return name.hashCode()
  }

  /**
   * Returns a string representation of the method meta-class. It is not a code representation but rather useful for debugging purposes.
   *
   * @return information about method's name, access modifier, type and parameters
   */
  @Override
  String toString() {
    return "Method{" +
        "name='" + name + '\'' +
        ", accessModifier=" + accessModifier +
        ", staticMethod=" + staticMethod +
        ", returnType=" + returnType.fullName +
        ", parameters=[" + parameters*.simpleDeclaration.join(', ') + ']' +
        '}';
  }

  /**
   * Inner static abstract class representing a builder to create a method.
   *
   * @param <S> a type of method to be built
   * @param <T> a specific builder used for method creation
   */
  protected static abstract class Builder<S, T extends Builder<S, T>> {

    protected String name
    protected AccessModifier accessModifier = AccessModifier.PUBLIC
    protected boolean staticMethod = false
    protected Type returnType = VoidType.INSTANCE
    protected List<Parameter> parameters = []

    /**
     * Class constructor.
     *
     * @param name a name of the method to be created
     * @throws NullPointerException if {@code name} is null
     */
    Builder(String name) {
      this.name = checkNotNull(name)
    }

    /**
     * Sets an access modifier for the method.
     *
     * @param accessModifier an access modifier (values of {@link AccessModifier} enumeration)
     * @return the instance of the builder
     */
    T withAccessModifier(AccessModifier accessModifier) {
      this.accessModifier = checkNotNull(accessModifier)
      return getThis()
    }

    /**
     * Sets a method to be static.
     *
     * @return an instance of the builder
     */
    T asStatic() {
      this.staticMethod = true
      return getThis()
    }

    /**
     * Sets a return type of the method.
     *
     * @param type a type to be used as return type in method signature
     * @return an instance of the builder
     * @throws NullPointerException if {@code type} is null
     */
    T withReturnType(Type type) {
      this.returnType = checkNotNull(type)
      return getThis()
    }

    /**
     * Sets a list of parameters for the method.
     *
     * @param parameters a list of method parameters (present in the method's signature)
     * @return an instance of builder
     * @throws NullPointerException if {@code parameters} is null
     * @throws IllegalArgumentException if {@code parameters} contains null
     */
    T withParameters(List<Parameter> parameters) {
      this.parameters = checkNotNull(parameters)
      Preconditions.assertNoNullInCollection(parameters)
      return getThis()
    }

    abstract T getThis()

    abstract S buildMethod()
  }
}
