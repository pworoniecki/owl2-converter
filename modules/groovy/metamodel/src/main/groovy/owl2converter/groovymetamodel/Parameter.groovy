package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.type.Type

import static com.google.common.base.Preconditions.checkNotNull

/**
 * Generic class representing a method parameter.
 *
 * @param <T> the type of the parameter
 */
class Parameter<T extends Type> {

  private T type
  private String name

  /**
   * Class constructor.
   *
   * @param type the type of parameter
   * @param name the name of parameter
   * @throws NullPointerException if {@code type} or {@code name} is null
   */
  Parameter(T type, String name) {
    this.type = checkNotNull(type)
    this.name = checkNotNull(name)
  }

  /**
   * Returns the type of parameter.
   *
   * @return the parameter type
   */
  T getType() {
    return type
  }

  /**
   * Returns the name of parameter.
   *
   * @return the parameter name
   */
  String getName() {
    return name
  }

  /**
   * Returns a string representing parameter declaration using the following pattern: type_name parameter_name.<br>
   * Example: for name 'person' and type 'com.example.Human', this methods returns 'Human&nbsp;person'
   *
   * @return the parameter declaration
   */
  String getSimpleDeclaration() {
    return "$type.simpleName $name"
  }
}
