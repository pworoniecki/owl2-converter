package owl2converter.groovymetamodel.type

import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Trait

import static com.google.common.base.Preconditions.checkNotNull

/**
 * A class representing a reference type in Groovy.
 */
class SimpleType extends Type {

  protected String packageName
  protected String className

  /**
   * Class constructor.
   *
   * @param basicType a Groovy class meta-model ({@link owl2converter.groovymetamodel.Class}) used as a template (name and package name of the class will be reused) for simple type creation
   */
  SimpleType(Class basicType) {
    this(basicType?.packageName, basicType?.name)
  }

  /**
   * Class constructor.
   *
   * @param basicType a Groovy trait meta-model ({@link owl2converter.groovymetamodel.Trait}) used as a template (name and package name of the trait will be reused) for simple type creation
   */
  SimpleType(Trait basicType) {
    this(basicType?.packageName, basicType?.name)
  }

  /**
   * Class constructor.
   *
   * @param basicType a class ({@link java.lang.Class}) used as a template (name and package name of the class will be reused) package for simple type creation
   */
  SimpleType(java.lang.Class<?> basicType) {
    this(basicType?.package?.name, basicType?.simpleName)
  }

  /**
   * Class constructor.
   *
   * @param packageName a package name of the class
   * @param className a class name
   */
  SimpleType(String packageName, String className) {
    this.packageName = checkNotNull(packageName)
    this.className = checkNotNull(className)
  }

  /**
   * Returns the qualified name (packageName.className) of the type, e.g. java.lang.Integer
   *
   * @return the qualified name of the type
   */
  @Override
  String getFullName() {
    return "$packageName.$className"
  }

  /**
   * Returns the package name of the type
   *
   * @return the package name of the type
   */
  @Override
  String getPackageName() {
    return packageName
  }

  /**
   * Returns the name of the type (without its package name), e.g. Integer
   *
   * @return the type name
   */
  @Override
  String getSimpleName() {
    return className
  }
}
