package owl2converter.groovymetamodel.method

import com.google.common.base.Preconditions
import owl2converter.groovymetamodel.attribute.Attribute

/**
 * An abstract class being a Groovy accessor method meta-class.
 */
abstract class Accessor extends Method {

  private Attribute attribute

  protected Accessor(Builder builder) {
    super(builder)
    this.attribute = builder.attribute
  }

  /**
   * Returns the attribute the accessor method is for.
   *
   * @return the attribute the method gives access
   */
  Attribute getAttribute() {
    return attribute
  }

  /**
   * An abstract builder class to create an accessor method for a specific attribute.

   * @param <S> a type of method to be built
   * @param <T> a specific builder used for method creation
   */
  protected static abstract class Builder<S, T extends Builder<S, T>> extends Method.Builder<S, T> {

    protected Attribute attribute

    /**
     * Class constructor.
     *
     * @param name a name of the method
     * @param attribute an attribute for which the method is to be built
     * @throws NullPointerException if {@code attribute} is null
     */
    Builder(String name, Attribute attribute) {
      super(name)
      this.attribute = Preconditions.checkNotNull(attribute)
    }
  }
}
