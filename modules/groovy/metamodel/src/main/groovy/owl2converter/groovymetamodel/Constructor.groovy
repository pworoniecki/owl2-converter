package owl2converter.groovymetamodel

import owl2converter.utils.Preconditions

import static com.google.common.base.Preconditions.checkNotNull

/**
 * A class representing a class constructor meta-model in Groovy.
 * Constructors are public, with no arguments and non-static by default.
 */
class Constructor {

  private AccessModifier accessModifier = AccessModifier.PUBLIC
  private List<Parameter> parameters = []
  private boolean staticConstructor = false

  /**
   * Returns constructor access modifier (e.g. public).
   *
   * @return the access modifier used for the constructor
   */
  AccessModifier getAccessModifier() {
    return accessModifier
  }

  /**
   * Sets an access modifier for the constructor.
   *
   * @param accessModifier an access modifier to be applied in the Groovy code
   */
  void setAccessModifier(AccessModifier accessModifier) {
    this.accessModifier = checkNotNull(accessModifier)
  }

  /**
   * Returns a list of constructor parameters.
   *
   * @return the list of constructor parameters.
   */
  List<Parameter> getParameters() {
    return parameters
  }

  /**
   * Sets a list of constructor parameters.
   *
   * @param parameters a list of constructor parameters
   * @throws NullPointerException if (@code parameters} is null
   * @throws IllegalArgumentException if (@code parameters} contains null
   */
  void setParameters(List<Parameter> parameters) {
    this.parameters = checkNotNull(parameters)
    Preconditions.assertNoNullInCollection(parameters)
  }

  /**
   * Returns information if the constructor is static or not.
   *
   * @return true if the constructor is static; false otherwise
   */
  boolean isStatic() {
    return staticConstructor
  }

  /**
   * Sets information if the constructor is static.
   *
   * @param isStatic true if the constructor should be static, false otherwise
   */
  void setIsStatic(boolean isStatic) {
    this.staticConstructor = isStatic
  }

  /**
   * Checks whether the constructors are equal.
   *
   * @param o the other constructor to be compared with
   * @return true if both constructors have the same access modifier, are static or non-static and have the same parameter lists
   */
  boolean equals(o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false

    Constructor that = (Constructor) o
    return accessModifier == that.accessModifier && parameters == that.parameters && staticConstructor == that.staticConstructor
  }

  int hashCode() {
    return 31 * accessModifier.hashCode() + parameters.hashCode() + (staticConstructor ? 1 : 0)
  }
}
