package owl2converter.groovymetamodel.type

import groovy.transform.InheritConstructors

/**
 * Class representing a parametrized list
 */
@InheritConstructors
class ListType extends CollectionType {

  /**
   * Class constructor.
   *
   * @param parameterizedType a type used for the list parametrization. Must not be null.
   */
  ListType(Type parameterizedType) {
    super(List, parameterizedType)
  }
}
