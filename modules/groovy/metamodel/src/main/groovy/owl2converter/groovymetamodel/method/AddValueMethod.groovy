package owl2converter.groovymetamodel.method

import groovy.transform.InheritConstructors
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType

/**
 * An abstract class being a Groovy method meta-class with fixed name and parameters list.
 * It is designed to be an accessor method for list-type attribute that adds single value to the list.<br>
 * Its name consists of "add" prefix and capitalized attribute's name. The parameters list of the method contains single parameter: the value to be added.<br>
 * Therefore its signature looks like: addAttribute(Attribute attribute)
 */
@InheritConstructors
class AddValueMethod extends Accessor {

  /**
   * Returns the first (the only one) parameter of the add value method.
   *
   * @return the parameter the accessor method deals with
   */
  Parameter getParameter() {
    return parameters.first()
  }

  /**
   * Internal static builder for the add value method
   */
  static class Builder extends Accessor.Builder<AddValueMethod, Builder> {

    /**
     * Class constructor. May throw an exception when the attribute is not a list.
     *
     * @param attribute an attribute for which the add method is to be generated
     * @throws IllegalArgumentException if {@code attribute}'s value type is not an instance of ListType
     */
    Builder(Attribute attribute) {
      super(getName(attribute), attribute)
      assertListTypeOfAttribute(attribute)
      this.parameters = [createParameter(attribute)]
      this.returnType = VoidType.INSTANCE
    }

    private static void assertListTypeOfAttribute(Attribute attribute) {
      if (!(attribute.valueType instanceof ListType)) {
        throw new IllegalArgumentException('Cannot create add value method for attribute which is not a list')
      }
    }

    private static Parameter createParameter(Attribute attribute) {
      return new Parameter((attribute.valueType as ListType).extractSimpleParameterizedType(), attribute.name)
    }

    /**
     * Returns a builder instance.
     *
     * @return the instance of builder
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * Returns a new instance of AddValueMethod.
     *
     * @return the new instance of AddValueMethod class
     */
    @Override
    AddValueMethod buildMethod() {
      return new AddValueMethod(this)
    }

    /**
     * Overridden only for compatibility purposes. Parameters cannot be changed so this method always throws an exception.
     *
     * @param parameters a list of methods' parameters
     * @return nothing
     * @throws UnsupportedOperationException always thrown as parameters list is fixed
     */
    @Override
    Builder withParameters(List<Parameter> parameters) {
      throw new UnsupportedOperationException('Parameters cannot be changed for add value method.')
    }

    /**
     * Overridden only for compatibility purposes. Parameters cannot be changed so this method always throws an exception.
     *
     * @param type a type to be returned by the method
     * @return nothing
     * @throws UnsupportedOperationException always thrown as parameters list is fixed
     */
    @Override
    Builder withReturnType(Type type) {
      throw new UnsupportedOperationException('Return type cannot be changed for add value method.')
    }
  }

  /**
   * Returns the name of the method in the form: 'addAttribute_name' where 'Attribute_name' is a name of the attribute.<br>
   * Example: if attribute's name is 'person', then the returned method's name is: {@code addPerson}
   *
   * @param attribute an attribute for which the method is generated
   * @return the name of the add value method
   */
  static String getName(Attribute attribute) {
    return "add${attribute.name.capitalize()}"
  }
}
