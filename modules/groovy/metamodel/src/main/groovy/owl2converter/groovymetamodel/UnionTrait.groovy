package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.utils.Preconditions

import static com.google.common.base.Preconditions.checkNotNull

/**
 * Class representing a union of traits in Groovy meta-model.
 *
 */
class UnionTrait extends Trait {

  private List<Trait> unionOf = []

  private UnionTrait(String packageName, String name, List<Annotation> annotations, List<Trait> inheritedTraits,
                     List<Attribute> attributes, List<Method> methods) {
    super(packageName, name, annotations, inheritedTraits, attributes, methods)
  }

  /**
   * Sets a list of traits being in union with the given trait.
   *
   * @param traits the list of traits being in union with the given trait
   * @throws NullPointerException if {@code traits} is null
   * @throws IllegalArgumentException if {@code traits} contains null element
   */
  void addUnionOf(List<Trait> traits) {
    Preconditions.assertNoNullInCollection(checkNotNull(traits))
    unionOf.addAll(traits)
  }

  /**
   * Checks whether a trait passed as a parameter is in union with {@code this} trait
   *
   * @param traitModel the trait to be checked
   * @return true if the {@code traitModel} is in union with {@code this}; false otherwise
   */
  boolean isUnionOf(Trait traitModel) {
    return traitModel in unionOf
  }

  /**
   * Returns the list of traits being in union with {@code this}.
   *
   * @return the list of traits being in union
   */
  List<Trait> getUnionOf() {
    return unionOf
  }

  /**
   * A builder used to create instance of {@link UnionTrait} class.
   */
  static class Builder extends TraitBuilder<Builder, UnionTrait> {

    /**
     * Class constructor.
     *
     * @param packageName the name of trait package
     * @param name the name of trait
     */
    Builder(String packageName, String name) {
      super(packageName, name)
    }

    /**
     * Returns the instance of the builder
     *
     * @return the builder instance
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * Creates a new {@link UnionTrait} with the properties set.
     *
     * @return a new instance of {@link UnionTrait}
     */
    @Override
    UnionTrait buildTrait() {
      return new UnionTrait(packageName, name, annotations, inheritedTraits, attributes, methods)
    }
  }
}
