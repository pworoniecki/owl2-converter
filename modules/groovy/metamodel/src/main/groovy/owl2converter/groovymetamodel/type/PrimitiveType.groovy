package owl2converter.groovymetamodel.type

/**
 * Class representing a primitive Groovy types.
 */
class PrimitiveType extends Type {

  static final PrimitiveType BOOLEAN = new PrimitiveType('boolean')

  private String name

  /**
   * Class constructor.
   *
   * @param name the name of primitive type
   */
  PrimitiveType(String name) {
    this.name = name
  }

  /**
   * Returns the full name of primitive type. As prmitivie type has no package, its name is returned.
   * So this method returns the same value as {@link PrimitiveType#getSimpleName}
   *
   * @return the name of primitive type
   */
  @Override
  String getFullName() {
    return name
  }

  /**
   * Returns the simple name of primitive type.
   *
   * @return the name of primitive type
   */
  @Override
  String getSimpleName() {
    return name
  }

  /**
   * Returns the package name of the primitive type. As primitive type has no package, null is returned.
   *
   * @return null
   */
  @Override
  String getPackageName() {
    return null
  }
}
