package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method

/**
 * An interface used by trait visitors.
 */
interface TraitElementVisitor {

  void visit(Method method)

  void visit(Attribute attribute)
}
