package owl2converter.groovymetamodel.method

import groovy.transform.InheritConstructors
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType

/**
 * An abstract class being a Groovy direct setter method meta-class with fixed name and parameters list.
 * It is designed to be an accessor method for an attribute to set its value without any validation.<br>
 * Its name consists of "set" prefix, capitalized attribute's name and 'Directly' suffix. The parameters list of the method contains single parameter: the value to be set.<br>
 * Therefore its signature looks like: setAttributeDirectly(Attribute attribute)
 *
 * It it used e.g. for inverse properties to avoid infinite loops
 */
@InheritConstructors
class DirectSetter extends Accessor {

  /**
   * Returns the first (the only one) parameter of the direct setter method.
   *
   * @return the parameter of the direct setter method
   */
  Parameter getParameter() {
    return parameters.first()
  }

  /**
   * A builder class to create a direct setter method
   */
  static class Builder extends Accessor.Builder<DirectSetter, Builder> {

    /**
     * Class constructor.
     *
     * @param attribute an attribute for which the direct setter is to be built
     */
    Builder(Attribute attribute) {
      super(getName(attribute), attribute)
      this.parameters = [createParameter(attribute)]
      this.returnType = VoidType.INSTANCE
    }

    private static Parameter createParameter(Attribute attribute) {
      return new Parameter(attribute.valueType, attribute.name)
    }

    /**
     * Returns an instance of the method builder.
     *
     * @return the instance of the builder
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * Returns a new instance of direct setter method.
     *
     * @return a new instance of direct setter method
     */
    @Override
    DirectSetter buildMethod() {
      return new DirectSetter(this)
    }

    /**
     * Overridden for compatibility purposes.
     *
     * @param parameters a list of method parameters
     * @return nothing
     * @throws UnsupportedOperationException always thrown as parameters list is fixed
     */
    @Override
    Builder withParameters(List<Parameter> parameters) {
      throw new UnsupportedOperationException('Parameters cannot be changed for setter.')
    }

    /**
     * Overridden for compatibility purposes.
     *
     * @param type a return type of the method
     * @return nothing
     * @throws UnsupportedOperationException always thrown as parameters list is fixed
     */
    @Override
    Builder withReturnType(Type type) {
      throw new UnsupportedOperationException('Return type cannot be changed for setter.')
    }
  }

  /**
   * Returns a name of the setter method in the form 'setAttribute_nameDirectly' where 'Attribute_name' is the attribute's name.<br>
   * Example: if attribute's name is 'person', then the returned method's name is: {@code setPersonDirectly}
   *
   * @param attribute an attribute for which the setter method is to be built
   * @return the name of the setter method
   */
  static String getName(Attribute attribute) {
    return "set${attribute.name.capitalize()}Directly"
  }
}
