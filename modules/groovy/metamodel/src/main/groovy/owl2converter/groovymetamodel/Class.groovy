package owl2converter.groovymetamodel

import groovy.transform.InheritConstructors
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method

/**
 * A representation of a Groovy class.
 */
class Class {

  private String packageName
  private String name
  private List<Annotation> annotations
  private Optional<Class> inheritedClass
  private List<Constructor> constructors
  private List<Attribute> attributes
  private List<Method> methods
  private List<Trait> implementedTraits

  /**
   * Class constructor.
   *
   * @param packageName a name of the package in which the class is to be defined
   * @param className a name of the class
   * @param annotations a list of annotations used for the class
   * @param inheritedClass a list of class parents (may be empty)
   * @param constructors a list of class constructors
   * @param attributes a list of class attributes
   * @param methods a list of class methods
   * @param implementedTraits a list of traits the class implements
   */
  protected Class(String packageName, String className, List<Annotation> annotations, Optional<Class> inheritedClass,
                  List<Constructor> constructors, List<Attribute> attributes, List<Method> methods,
                  List<Trait> implementedTraits) {
    this.packageName = packageName
    this.name = className
    this.annotations = annotations
    this.inheritedClass = inheritedClass
    this.constructors = constructors
    this.attributes = attributes
    this.methods = methods
    this.implementedTraits = implementedTraits
  }

  /**
   * Returns the class package name.
   *
   * @return the name of the class package
   */
  String getPackageName() {
    return packageName
  }

  /**
   * Returns the class name.
   *
   * @return the name of the class
   */
  String getName() {
    return name
  }

  /**
   * Returns the list of class annotations.
   *
   * @return the list of class annotations
   */
  List<Annotation> getAnnotations() {
    return annotations
  }

  /**
   * Returns the parent class if any.
   *
   * @return the parent class of the class if it exists
   */
  Optional<Class> getInheritedClass() {
    return inheritedClass
  }

  /**
   * Returns the list of class constructors.
   *
   * @return the list of constructors of the class
   */
  List<Constructor> getConstructors() {
    return constructors
  }

  /**
   * Returns the list of class attributes.
   *
   * @return the list of class attributes
   */
  List<Attribute> getAttributes() {
    return attributes
  }

  /**
   * Returns the list of class methods.
   *
   * @return the list of class methods
   */
  List<Method> getMethods() {
    return methods
  }

  /**
   * Returns the list of traits implemented by the class.
   *
   * @return the list of implemented traits
   */
  List<Trait> getImplementedTraits() {
    return implementedTraits
  }

  /**
   * Returns the qualified name of the class (package_name.class_name)
   *
   * @return the qualified name of the class
   */
  String getFullName() {
    return "$packageName.$name"
  }

  /**
   * Checks whether two classes are equal.
   *
   * @param o the other class to be compared with
   * @return true if the qualified name of both classes are the same
   */
  boolean equals(o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false

    Class that = (Class) o
    return this.name == that.name && this.packageName == that.packageName
  }

  /**
   * Returns the class hash code.
   *
   * @return the class hash code calculated on the base of name
   */
  int hashCode() {
    return name.hashCode()
  }

  /**
   * Runs proper visitor to translate class content (constructors, attributes, methods).
   *
   * @param visitor the {@link ClassElementVisitor} to process class content
   */
  void accept(ClassElementVisitor visitor) {
    constructors.each { visitor.visit(it, this) }
    attributes.each { visitor.visit(it) }
    methods.each { visitor.visit(it) }
  }

  /**
   * Internal static builder class to translate classes to Groovy source code.
   */
  @InheritConstructors
  static class Builder extends ClassBuilder<Builder, Class> {

    /**
     * Returns the builder instance.
     *
     * @return the builder instance
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * Creates a new instance of a class on the base of its properties.
     *
     * @return a new instance of class
     */
    @Override
    Class buildClass() {
      return new Class(packageName, className, annotations, inheritedClass, constructors, attributes, methods, implementedTraits)
    }
  }
}
