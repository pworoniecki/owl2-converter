package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.type.Type

import static com.google.common.base.Preconditions.checkNotNull

/**
 * Class representing annotation used for classes and traits.
 * It consists of type and parameters.<br>
 * Example: for {@code SuppressWarnings} type and {@code ['deprecation', 'unchecked']} parameters list, the following annotation is generated: {@code @SuppressWarnings(['deprecation', 'unchecked'])}
 */
class Annotation {

  private Type type
  private List<String> parameters

  /**
   * Class constructor
   *
   * @param type the annotation's type
   * @param parameters a list of annotation's parameters
   * @throws NullPointerException if {@code type} is null or {@code parameters} is null or any element of the parameters list is null
   */
  Annotation(Type type, List<String> parameters = []) {
    this.type = checkNotNull(type)
    this.parameters = checkNotNull(parameters)
    parameters.each { checkNotNull(it) }
  }

  /**
   * Returns the type of the annotation
   *
   * @return the type of the annotation
   */
  Type getType() {
    return type
  }

  /**
   * Returns annotation parameters.
   *
   * @return the list of annotation parameters.
   */
  List<String> getParameters() {
    return parameters
  }
}
