package owl2converter.groovymetamodel.attribute.relation

import owl2converter.groovymetamodel.attribute.Attribute

import static com.google.common.base.Preconditions.checkNotNull

/**
 * An abstract class for relations between attributes, e.g. equivalent property, sub property etc.
 */
abstract class AttributeRelation {

  private Attribute relatedAttribute

  /**
   * Class constructor.
   *
   * @param relatedAttribute an attribute that is related to the owner's attribute (owner is an attribute that holds the information about relation, i.e. {@code this}).
   * @throws NullPointerException if {@code relatedAttribute} is null
   */
  AttributeRelation(Attribute relatedAttribute) {
    this.relatedAttribute = checkNotNull(relatedAttribute)
  }

  /**
   * Returns the related attribute.
   * Owner of attribute relation is the attribute from which perspective the relation is defined.
   * The returned attribute is the second one ("child" attribute, destination-side attribute).
   *
   * @return the attribute representing the relation
   */
  Attribute getRelatedAttribute() {
    return relatedAttribute
  }

  /**
   * Checks whether two AttributeRelations are equal.
   *
   * @param o other attribute relation to be compared with
   * @return true if the related attributes in both relations are the same
   */
  boolean equals(Object o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false
    return relatedAttribute == ((AttributeRelation) o).relatedAttribute
  }

  /**
   * Returns the relation hash code.
   *
   * @return the hash code of the relation calculated on the basis of related attribute.
   */
  int hashCode() {
    return relatedAttribute.hashCode()
  }
}
