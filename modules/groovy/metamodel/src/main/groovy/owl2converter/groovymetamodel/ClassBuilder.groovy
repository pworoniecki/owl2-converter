package owl2converter.groovymetamodel

import groovy.transform.PackageScope
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.utils.Preconditions

import static com.google.common.base.Preconditions.checkNotNull

/**
 * An abstract generic builder to build Groovy class meta-model (an instance of {@link Class} class or its sub-class)
 *
 * @param <S> the type of specific class builder
 * @param <T> the type of specific class to be built
 */
abstract class ClassBuilder<S extends ClassBuilder, T extends Class> {

  @PackageScope String packageName
  @PackageScope String className
  @PackageScope List<Annotation> annotations = []
  @PackageScope Optional<Class> inheritedClass = Optional.empty()
  @PackageScope List<Constructor> constructors = []
  @PackageScope List<Attribute> attributes = []
  @PackageScope List<Method> methods = []
  @PackageScope List<Trait> implementedTraits = []

  /**
   * Class constructor.
   *
   * @param packageName a package of the class
   * @param className a name of the class
   */
  ClassBuilder(String packageName, String className) {
    this.packageName = checkNotNull(packageName)
    this.className = checkNotNull(className)
  }

  /**
   * Sets a list of annotations used for the class.
   *
   * @param annotations a list of class annotations
   * @return the builder instance
   */
  S withAnnotations(List<Annotation> annotations) {
    this.annotations = checkNotNull(annotations)
    Preconditions.assertNoNullInCollection(annotations)
    return getThis()
  }

  /**
   * Sets a parent class.
   *
   * @param inheritedClass a class to inherit from
   * @return builder instance
   * @throws NullPointerException if {@code inheritedClass} is null
   */
  S withInheritedClass(Class inheritedClass) {
    this.inheritedClass = Optional.of(checkNotNull(inheritedClass))
    return getThis()
  }

  /**
   * Sets a list of class constructors.
   *
   * @param constructors the list of class constructors
   * @return builder instance
   * @throws NullPointerException if {@code constructors} is null
   * @throws IllegalArgumentException if {@code constructors} contains null
   */
  S withConstructors(List<Constructor> constructors) {
    this.constructors = checkNotNull(constructors)
    Preconditions.assertNoNullInCollection(constructors)
    return getThis()
  }

  /**
   * Sets a list of class attributes.
   *
   * @param attributes the list of class attributes
   * @return the builder instance
   * @throws NullPointerException if {@code attributes} is null
   * @throws IllegalArgumentException if {@code attributes} contains null
   */
  S withAttributes(List<Attribute> attributes) {
    this.attributes = checkNotNull(attributes)
    Preconditions.assertNoNullInCollection(attributes)
    return getThis()
  }

  /**
   * Sets a list of class methods.
   *
   * @param methods the list of class methods
   * @return the builder instance
   * @throws NullPointerException if {@code methods} is null
   * @throws IllegalArgumentException if {@code methods} contains null
   */
  S withMethods(List<Method> methods) {
    this.methods = checkNotNull(methods)
    Preconditions.assertNoNullInCollection(methods)
    return getThis()
  }

  /**
   * Sets a list of traits implemented by the class.
   *
   * @param implementedTraits a list of traits implemented by the class
   * @return the builder instance
   * @throws NullPointerException if {@code implementedTraits} is null
   * @throws IllegalArgumentException if {@code implementedTraits} contains null
   */
  S withImplementedTraits(List<Trait> implementedTraits) {
    this.implementedTraits = checkNotNull(implementedTraits)
    Preconditions.assertNoNullInCollection(implementedTraits)
    return getThis()
  }

  /**
   * Returns the builder instance.
   *
   * @return the builder instance
   */
  abstract S getThis()

  /**
   * Creates a new instance of the class
   *
   * @return a new instance of the class
   */
  abstract T buildClass()
}
