package owl2converter.groovymetamodel

import groovy.transform.InheritConstructors

/**
 * A specialized class representing enums in Groovy meta-model.
 */
@InheritConstructors
class EnumClass extends Class {

  /**
   * A specialized builder to create enum class instances.
   */
  @InheritConstructors
  static class Builder extends ClassBuilder<Builder, EnumClass> {

    /**
     * Returns an instance of enum class builder.
     *
     * @return the builder instance
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * A factory method to create a new instance of EnumClass.
     *
     * @return a new instance of enum class
     */
    @Override
    EnumClass buildClass() {
      return new EnumClass(packageName, className, annotations, inheritedClass, constructors, attributes, methods,
          implementedTraits)
    }
  }
}
