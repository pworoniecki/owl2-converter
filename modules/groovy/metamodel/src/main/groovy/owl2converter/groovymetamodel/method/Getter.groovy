package owl2converter.groovymetamodel.method

import groovy.transform.InheritConstructors
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.Type

/**
 * An abstract class being a Groovy getter method meta-class with fixed name and parameters list.
 * It is designed to be an accessor method for an attribute to get its value.<br>
 * Its name consists of "get" prefix and capitalized attribute's name. The method has no parameters.<br>
 * Therefore its signature looks like: getAttribute()
 */
@InheritConstructors
class Getter extends Accessor {

  /**
   * Internal static class to build getter methods
   */
  static class Builder extends Accessor.Builder<Getter, Builder> {

    /**
     * Class constructor.
     *
     * @param attribute an attribute to build a getter method for
     */
    Builder(Attribute attribute) {
      super(getName(attribute), attribute)
      this.parameters = []
      this.returnType = attribute.valueType
    }

    /**
     * Returns the instance of builder instance.
     *
     * @return the builder instance
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * Creates a new instance of Getter method for an attribute.
     *
     * @return the getter method for an attribute
     */
    @Override
    Getter buildMethod() {
      return new Getter(this)
    }

    /**
     * Overridden for compatibility reasons.
     *
     * @param parameters list of parameters
     * @return nothing
     * @throws UnsupportedOperationException always thrown as parameters list is fixed
     */
    @Override
    Builder withParameters(List<Parameter> parameters) {
      throw new UnsupportedOperationException('Parameters cannot be changed for getter.')
    }

    /**
     * Overridden for compatibility reasons.
     *
     * @param type method's return type
     * @return nothing
     * @throws UnsupportedOperationException always thrown as parameters list is fixed
     */
    @Override
    Builder withReturnType(Type type) {
      throw new UnsupportedOperationException('Return type cannot be changed for getter.')
    }
  }

  /**
   * Returns the name of the getter method in the form 'getAttribute_name' where 'Attribute_name' is the attribute's name.<br>
   * Example: if attribute's name is 'person', then the returned method's name is: {@code getPerson}
   *
   * @param attribute an attribute the getter method is created for
   * @return the name of the getter method for the attribute
   */
  static String getName(Attribute attribute) {
    return "get${attribute.name.capitalize()}"
  }
}
