package owl2converter.groovymetamodel.method

import groovy.transform.InheritConstructors
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType

/**
 * An abstract class being a Groovy method meta-class with fixed name and parameters list.
 * It is designed to be an accessor method for list-type attribute that removes single value from the list.<br>
 * Its name consists of "remove" prefix and capitalized attribute's name. The parameters list of the method contains single parameter: the value to be removed.<br>
 * Therefore its signature looks like: removeAttribute(Attribute attribute)
 */
@InheritConstructors
class RemoveValueMethod extends Accessor {

  /**
   * Returns the first (the only one) parameter of the remove value method.
   *
   * @return the method parameter
   */
  Parameter getParameter() {
    return parameters.first()
  }

  /**
   * Internal static builder for the remove value method
   */
  static class Builder extends Accessor.Builder<RemoveValueMethod, Builder> {

    /**
     * Class constructor.
     *
     * @param attribute an attribute for which the method is to be built
     * @throws IllegalArgumentException if {@code attribute} is not of list type
     */
    Builder(Attribute attribute) {
      super(getName(attribute), attribute)
      assertListTypeOfAttribute(attribute)
      this.parameters = [createParameter(attribute)]
      this.returnType = VoidType.INSTANCE
    }

    private static void assertListTypeOfAttribute(Attribute attribute) {
      if (!(attribute.valueType instanceof ListType)) {
        throw new IllegalArgumentException('Cannot create remove value method for attribute which is not a list')
      }
    }

    private static Parameter createParameter(Attribute attribute) {
      return new Parameter((attribute.valueType as ListType).extractSimpleParameterizedType(), attribute.name)
    }

    /**
     * Returns builder instance.
     *
     * @return builder instance
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * Returns a new remove value method.
     *
     * @return the newly created remove value method.
     */
    @Override
    RemoveValueMethod buildMethod() {
      return new RemoveValueMethod(this)
    }

    /**
     * Overridden for compatibility purposes.
     *
     * @param parameters a list of method parameters
     * @return nothing
     * @throws UnsupportedOperationException as method parameters can not be changed
     */
    @Override
    Builder withParameters(List<Parameter> parameters) {
      throw new UnsupportedOperationException('Parameters cannot be changed for remove value method.')
    }

    /**
     * Overridden for compatibility purposes.
     *
     * @type return type of the method
     * @return nothing
     * @throws UnsupportedOperationException as method type cannot be changed
     */
    @Override
    Builder withReturnType(Type type) {
      throw new UnsupportedOperationException('Return type cannot be changed for remove value method.')
    }
  }

  /**
   * Returns the name of the method in the form: 'removeAttribute_name' where 'Attribute_name' is a name of the attribute.<br>
   * Example: if attribute's name is 'person', then the returned method's name is: {@code removePerson}
   *
   * @param attribute an attribute for which the method is to be created
   * @return the name of the remove value method
   */
  static String getName(Attribute attribute) {
    return "remove${attribute.name.capitalize()}"
  }
}
