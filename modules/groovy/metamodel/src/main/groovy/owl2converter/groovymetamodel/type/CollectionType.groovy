package owl2converter.groovymetamodel.type

import groovy.transform.InheritConstructors

import static com.google.common.base.Preconditions.checkNotNull

/**
 * Class representing a parametrized collection type
 */
@InheritConstructors
abstract class CollectionType extends Type {

  private final Class typeReferenceClass

  private Type parameterizedType

  /**
   * Class constructor.
   *
   * @param collectionTypeClass type of Collection, e.g. {@link List} or {@link Set}
   * @param parameterizedType a type used for the list parametrization. Mustn't be null.
   * @throws NullPointerException if {@code collectionTypeClass} or {@code parameterizedType} is null
   */
  CollectionType(Class collectionTypeClass, Type parameterizedType) {
    this.typeReferenceClass = checkNotNull(collectionTypeClass)
    this.parameterizedType = checkNotNull(parameterizedType)
  }

  /**
   * Returns the type used as an instance of generic parameter.
   *
   * @return the type used for list parametrization
   */
  Type getParameterizedType() {
    return parameterizedType
  }

  /**
   * Returns the qualified name of the parametrized list (e.g. java.util.List&lt;java.lang.Integer&gt;).
   *
   * @return the qualified name of the parametrized list
   */
  @Override
  String getFullName() {
    return "${typeReferenceClass.name}<${parameterizedType.fullName}>"
  }

  /**
   * Returns the simple name of the parametrized list (e.g. List&lt;Integer&gt;).
   *
   * @return the simple name of parametrized list
   */
  @Override
  String getSimpleName() {
    return "${typeReferenceClass.simpleName}<${parameterizedType.simpleName}>"
  }

  /**
   * Returns the fully qualified name of the package the type belongs to (package of {@code typeReferenceClass} passed to constructor)
   *
   * @return the fully qualified name of the package the {@code typeReferenceClass} belongs to
   */
  @Override
  String getPackageName() {
    return typeReferenceClass.package.name
  }

  /**
   * Returns the last actual parameter for recursive definitions of parametrized lists,
   * e.g. for List&lt;List&lt;List&lt;Integer&gt;&gt;&gt; it returns Integer.
   *
   * @return the last reference type used for Collection concretization
   */
  Type extractSimpleParameterizedType() {
    Type lastParametrizedType = parameterizedType
    while (lastParametrizedType instanceof CollectionType) {
      lastParametrizedType = (lastParametrizedType as CollectionType).parameterizedType
    }
    return lastParametrizedType
  }
}
