package owl2converter.groovymetamodel.type

/**
 * Class representing void type
 */
class VoidType extends Type {

  static final VoidType INSTANCE = new VoidType()

  private static final String TYPE_NAME = 'void'

  private VoidType() {}

  /**
   * Returns the full name of the type (returns the same value as {@link VoidType#getSimpleName} as void type has no package).
   *
   * @return void
   */
  @Override
  String getFullName() {
    return TYPE_NAME
  }

  /**
   * Returns the simple name of the type.
   *
   * @return void
   */
  @Override
  String getSimpleName() {
    return TYPE_NAME
  }

  /**
   * Returns the package name. It returns null as void type has no package.
   *
   * @return null
   */
  @Override
  String getPackageName() {
    return null
  }
}
