package owl2converter.groovymetamodel

import groovy.transform.PackageScope
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.utils.Preconditions

import static com.google.common.base.Preconditions.checkNotNull

/**
 * An abstract generic builder to build Groovy trait meta-model (an instance of {@link Trait} class or its sub-class)
 *
 * @param <S> the type of specific trait builder
 * @param <T> the type of specific trait to be built
 */
abstract class TraitBuilder<S extends TraitBuilder, T extends Trait> {

  @PackageScope String packageName
  @PackageScope String name
  @PackageScope List<Annotation> annotations = []
  @PackageScope List<Trait> inheritedTraits = []
  @PackageScope List<Attribute> attributes = []
  @PackageScope List<Method> methods = []

  /**
   * Class constructor.
   *
   * @param packageName a package name of trait
   * @param name a name of trait
   */
  TraitBuilder(String packageName, String name) {
    this.packageName = checkNotNull(packageName)
    this.name = checkNotNull(name)
  }

  /**
   * Sets a list of annotations.
   *
   * @param annotations the annotations to be set for the trait
   * @return the builder instance
   * @throws NullPointerException if {@code annotations} is a null
   * @throws IllegalArgumentException if {@code annotations} contains null element
   */
  S withAnnotations(List<Annotation> annotations) {
    this.annotations = checkNotNull(annotations)
    Preconditions.assertNoNullInCollection(annotations)
    return getThis()
  }

  /**
   * Sets a list of inherited traits.
   *
   * @param traits the list of traits inherited by the trait
   * @return the builder instance
   * @throws NullPointerException if {@code traits} is a null
   * @throws IllegalArgumentException if {@code traits} contains null element
   */
  S withInheritedTraits(List<Trait> traits) {
    this.inheritedTraits = checkNotNull(traits)
    Preconditions.assertNoNullInCollection(traits)
    return getThis()
  }

  /**
   * Sets a list of attributes.
   *
   * @param attributes the list of trait attributes
   * @return the builder instance
   * @throws NullPointerException if {@code attributes} is a null
   * @throws IllegalArgumentException if {@code attributes} contains null element or contains attribute with other than public or private access modifier
   */
  S withAttributes(List<Attribute> attributes) {
    this.attributes = checkNotNull(attributes)
    Preconditions.assertNoNullInCollection(attributes)
    assertPublicOrPrivateAttributes(attributes)
    return getThis()
  }

  /**
   * Checks whether the attributes in the list have only public or private access modifier.
   * Throws an uncontrolled IllegalArgumentException if the assertion is violated.
   *
   * @param attributes the list of attributes to be checked
   * @throws IllegalArgumentException if {@code attributes} contains instances with other than public or private access modifier
   */
  private static void assertPublicOrPrivateAttributes(List<Attribute> attributes) {
    List<Attribute> illegalAttributes = attributes.findAll {
      it.accessModifier != AccessModifier.PRIVATE && it.accessModifier != AccessModifier.PUBLIC
    }
    if (illegalAttributes) {
      throw new IllegalArgumentException('Trait may contain only private and public attributes. ' +
          "Illegal ones are: ${attributes.collect {"$it.valueType.simpleName $it.name"}}")
    }
  }

  /**
   * Sets a list of methods. Can throw uncontrolled exception if the list or any of its element is null
   * or if any of the methods are neither private nor public.
   *
   * @param methods the list of trait methods
   * @return the builder instance
   * @throws NullPointerException if {@code methods} is a null
   * @throws IllegalArgumentException if {@code methods} contains null element or contains method with other than public or private access modifier

   */
  S withMethods(List<Method> methods) {
    this.methods = checkNotNull(methods)
    Preconditions.assertNoNullInCollection(methods)
    assertPublicOrPrivateMethods(methods)
    return getThis()
  }

  /**
   * Checks whether the methods in the list have only public or private access modifier.
   * Throws an uncontrolled IllegalArgumentException if the assertion is violated.
   *
   * @param methods the list of methods to be checked
   * @throws IllegalArgumentException if {@code methods} contains instances with other than public or private access modifier
   */
  private static void assertPublicOrPrivateMethods(List<Method> methods) {
    List<Method> illegalMethods = methods.findAll {
      it.accessModifier != AccessModifier.PRIVATE && it.accessModifier != AccessModifier.PUBLIC
    }
    if (illegalMethods) {
      throw new IllegalArgumentException('Trait may contain only private and public methods. ' +
          "Illegal ones are: ${methods.collect {"$it.returnType.simpleName $it.name"}}")
    }
  }

  /**
   * Returns the builder instance.
   *
   * @return the builder instance
   */
  abstract S getThis()

  /**
   * Creates a new instance of the trait
   *
   * @return a new instance of the trait
   */
  abstract T buildTrait()
}
