package owl2converter.groovymetamodel.type

/**
 * Abstract superclass for all type meta-models, e.g. for primitive types, reference types, sets and lists
 */
abstract class Type {

  private static final List<String> DEFAULT_IMPORTED_PACKAGES = [
      'java.lang', 'java.util', 'java.io', 'java.net', 'groovy.lang', 'groovy.util'
  ]
  private static final List<String> DEFAULT_IMPORTED_CLASSES = ['java.math.BigInteger', 'java.math.BigDecimal']
  private static final List<String> PRIMITIVE_TYPES = ['byte', 'short', 'int', 'long', 'float', 'double', 'boolean', 'char']

  abstract String getFullName()

  abstract String getSimpleName()

  abstract String getPackageName()

  /**
   * Checks if the type is the same as the one passed by parameter.
   *
   * @param obj the reference to another type
   * @return true if the full names of two types are equal
   */
  @Override
  boolean equals(Object obj) {
    return obj != null && (obj as Type).fullName == fullName
  }

  @Override
  int hashCode() {
    return fullName.hashCode()
  }

  /**
   * Checks whether the type is imported by default in Groovy and therefore requires no explicit import statement.<br>
   * All types from the following packages are imported by default: 'java.lang', 'java.util', 'java.io', 'java.net', 'groovy.lang', 'groovy.util'.
   * Moreover, {@link java.math.BigInteger} and {@link java.math.BigDecimal} types as well as all primitive types are imported by default too.
   *
   * @return true if the type is imported by default in Groovy language
   */
  boolean isImportedByDefault() {
    return DEFAULT_IMPORTED_PACKAGES.any { packageName == it } || DEFAULT_IMPORTED_CLASSES.any { fullName == it } ||
        PRIMITIVE_TYPES.any { fullName == it }
  }
}
