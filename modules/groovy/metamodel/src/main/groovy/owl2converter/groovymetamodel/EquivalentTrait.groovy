package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method

/**
 * A specialized trait in Groovy meta-model used for representation of equivalent traits.
 * In translator OWL equivalent classes are translated into equivalent traits.
 */
class EquivalentTrait extends Trait {

  private EquivalentTrait(String packageName, String name, List<Annotation> annotations, List<Trait> inheritedTraits,
                          List<Attribute> attributes, List<Method> methods) {
    super(packageName, name, annotations, inheritedTraits, attributes, methods)
  }

  /**
   * A builder for creation of EquivalentTrait instances
   */
  static class Builder extends TraitBuilder<Builder, EquivalentTrait> {

    /**
     * Class constructor.
     *
     * @param packageName a name of the package in which the trait is to be defined
     * @param name a name of the trait
     */
    Builder(String packageName, String name) {
      super(packageName, name)
    }

    /**
     * Returns the builder instance.
     *
     * @return the builder instance
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * A factory method to create an equivalent trait.
     *
     * @return a new instance of equivalent trait
     */
    @Override
    EquivalentTrait buildTrait() {
      return new EquivalentTrait(packageName, name, annotations, inheritedTraits, attributes, methods)
    }
  }
}
