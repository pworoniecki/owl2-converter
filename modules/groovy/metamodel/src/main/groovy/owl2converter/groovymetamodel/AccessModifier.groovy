package owl2converter.groovymetamodel

/**
 * Enumeration representing Groovy access modifiers
 */
enum AccessModifier {

  PRIVATE('private'), PROTECTED('protected'), PACKAGE_PROTECTED('@PackageScope'), PUBLIC('')

  private String groovyCode

  private AccessModifier(String groovyCode) {
    this.groovyCode = groovyCode
  }

  String toGroovyCode() {
    return groovyCode
  }
}
