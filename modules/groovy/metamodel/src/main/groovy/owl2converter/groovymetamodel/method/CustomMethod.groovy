package owl2converter.groovymetamodel.method

import com.google.common.base.Preconditions
import groovy.transform.InheritConstructors

/**
 * Representation of a custom method meta-model that allows to set all its attributes
 */
@InheritConstructors
class CustomMethod extends Method {

  private String bodyCode

  /**
   * Class constructor.
   *
   * @param builder the builder used for method instance creation
   */
  private CustomMethod(Builder builder) {
    super(builder)
    this.bodyCode = builder.bodyCode
  }

  /**
   * Returns the body string of the method.
   *
   * @return method body source code.
   */
  String getBodyCode() {
    return bodyCode
  }

  /**
   * Internal static builder for custom method creation.
   */
  static class Builder extends Method.Builder<CustomMethod, Builder> {

    protected String bodyCode

    /**
     * Class constructor.
     *
     * @param name a name of method
     * @param bodyCode a string with method body; mustn't be null
     * @throws NullPointerException if {@code bodyCode} is null
     */
    Builder(String name, String bodyCode) {
      super(name)
      this.bodyCode = Preconditions.checkNotNull(bodyCode)
    }

    /**
     * Returns the builder instance.
     *
     * @return the builder instance
     */
    @Override
    Builder getThis() {
      return this
    }

    /**
     * Creates a new instance of method.
     *
     * @return a new instance of method
     */
    @Override
    CustomMethod buildMethod() {
      return new CustomMethod(this)
    }
  }
}
