package owl2converter.groovymetamodel.attribute

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.attribute.relation.AttributeRelation
import owl2converter.groovymetamodel.attribute.relation.EquivalentToAttributeRelation
import owl2converter.groovymetamodel.attribute.relation.InverseAttributeRelation
import owl2converter.groovymetamodel.attribute.relation.SuperAttributeOfRelation
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType
import owl2converter.utils.Preconditions

import static com.google.common.base.Preconditions.checkNotNull

/**
 * Class represening a Groovy meta-attribute
 */
class Attribute {

  private String name
  private AccessModifier accessModifier
  private boolean staticAttribute
  private Type valueType
  private String defaultValue
  private List<AttributeCharacteristic> characteristics
  private List<AttributeRelation> relations

  private Attribute(String name, AccessModifier accessModifier, boolean staticAttribute, Type valueType, String defaultValue,
                    List<AttributeCharacteristic> characteristics, List<AttributeRelation> relations) {
    this.name = name
    this.accessModifier = accessModifier
    this.staticAttribute = staticAttribute
    this.valueType = valueType
    this.defaultValue = defaultValue
    this.characteristics = characteristics
    this.relations = relations
  }

  /**
   * Returns the attribute name.
   *
   * @return the name of attribute
   */
  String getName() {
    return name
  }

  /**
   * Returns the attribute access modifier.
   *
   * @return the access modifier
   */
  AccessModifier getAccessModifier() {
    return accessModifier
  }

  /**
   * Returns information if the attribute is static.
   *
   * @return true if attribute is static
   */
  boolean isStatic() {
    return staticAttribute
  }

  /**
   * Returns the attribute type (for reference and primitive types).
   *
   * @return the type of attribute
   */
  Type getValueType() {
    return valueType
  }

  /**
   * Extracts a parameter type for a list type or does nothing.
   *
   * @return the paremeter type for a list type (e.g. List<T>) or the type unchanged
   */
  Type extractSimpleValueType() {
    return valueType instanceof ListType ? valueType.extractSimpleParameterizedType() : valueType
  }

  /**
   * Returns a string representing the default value of the the attribute.
   *
   * @return the default value of attribute
   */
  String getDefaultValue() {
    return defaultValue
  }

  /**
   * Returns attribute characteristics, e.g. symmetric, transitive
   *
   * @return the list of attribute characteristics
   */
  List<AttributeCharacteristic> getCharacteristics() {
    return characteristics
  }

  /**
   *
   * @return the list of relations the attribute
   */
  List<AttributeRelation> getRelations() {
    return relations
  }

  boolean isEquivalentOwnedAttribute() {
    return findAnyEquivalentAttribute().isPresent()
  }

  Optional<Attribute> findAnyEquivalentAttribute() {
    return Optional.ofNullable(relations.find { it instanceof EquivalentToAttributeRelation }?.relatedAttribute)
  }

  boolean isSuperAttribute() {
    return !findSubAttributes().isEmpty()
  }

  List<Attribute> findSubAttributes() {
    return relations.findAll { it instanceof SuperAttributeOfRelation }*.relatedAttribute
  }

  boolean isInverseAttribute() {
    return !findInverseAttributes().isEmpty()
  }

  List<Attribute> findInverseAttributes() {
    return relations.findAll { it instanceof InverseAttributeRelation }*.relatedAttribute
  }

  boolean equals(Object o) {
    if (this.is(o)) return true
    if (!o || getClass() != o.class) return false

    Attribute that = (Attribute) o
    return that.accessModifier == this.accessModifier && that.name == this.name
  }

  int hashCode() {
    return name.hashCode()
  }

  static class AttributeBuilder {

    private String name
    private AccessModifier accessModifier = AccessModifier.PUBLIC
    private boolean staticAttribute = false
    private Type valueType
    private String defaultValue
    private List<AttributeCharacteristic> characteristics = []
    private List<AttributeRelation> relations = []

    AttributeBuilder(String name, Type valueType) {
      this.name = checkNotNull(name)
      this.valueType = checkNotNull(valueType)
      assertCorrectType(valueType)
    }

    private static void assertCorrectType(Type type) {
      if (type instanceof VoidType) {
        throw new IllegalArgumentException('Cannot set void type as type of an attribute')
      }
    }

    AttributeBuilder withAccessModifier(AccessModifier modifier) {
      this.accessModifier = checkNotNull(modifier)
      return this
    }

    AttributeBuilder asStatic() {
      this.staticAttribute = true
      return this
    }

    AttributeBuilder withDefaultValue(String value) {
      this.defaultValue = checkNotNull(value)
      return this
    }

    AttributeBuilder withCharacteristics(List<AttributeCharacteristic> characteristics) {
      this.characteristics = checkNotNull(characteristics)
      Preconditions.assertNoNullInCollection(characteristics)
      return this
    }

    AttributeBuilder withRelations(List<AttributeRelation> relations) {
      this.relations = checkNotNull(relations)
      Preconditions.assertNoNullInCollection(relations)
      return this
    }

    Attribute buildAttribute() {
      return new Attribute(name, accessModifier, staticAttribute, valueType, defaultValue, characteristics, relations)
    }
  }
}
