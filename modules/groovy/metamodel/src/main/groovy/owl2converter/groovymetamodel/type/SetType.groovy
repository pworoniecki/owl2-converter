package owl2converter.groovymetamodel.type

import groovy.transform.InheritConstructors

/**
 * Class representing a parametrized set
 */
@InheritConstructors
class SetType extends CollectionType {

  /**
   * Class constructor.
   *
   * @param parameterizedType a type used for the set parametrization. Must not be null.
   */
  SetType(Type parameterizedType) {
    super(Set, parameterizedType)
  }
}
