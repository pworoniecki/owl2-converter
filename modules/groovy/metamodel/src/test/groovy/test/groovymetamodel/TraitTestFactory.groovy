package test.groovymetamodel

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.type.SimpleType

import static test.groovymetamodel.AttributeTestFactory.createAttribute
import static test.groovymetamodel.MethodTestFactory.createMethod

class TraitTestFactory {

  static Trait createTrait(String traitName = 'TestTrait', String packageName = 'org.test') {
    return new Trait.Builder(packageName, traitName)
        .withAttributes([createAttribute('test', new SimpleType(String), AccessModifier.PUBLIC)])
        .withInheritedTraits([createEmptyTrait()])
        .withMethods([createMethod('test', AccessModifier.PRIVATE)])
        .buildTrait()
  }

  static Trait createEmptyTrait(String name = 'EmptyTrait') {
    return new Trait.Builder('org.test', name).buildTrait()
  }
}
