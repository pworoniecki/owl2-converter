package test.groovymetamodel

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Getter

import static test.groovymetamodel.AttributeTestFactory.createAttribute

class GetterTestFactory {

  static Getter createMethod(Attribute attribute = createAttribute()) {
    return new Getter.Builder(attribute).withAccessModifier(AccessModifier.PROTECTED).asStatic()
        .buildMethod()
  }

  static Getter createMethod(Attribute attribute, AccessModifier modifier, boolean isStatic) {
    def builder = new Getter.Builder(attribute).withAccessModifier(modifier)
    return isStatic ? builder.asStatic().buildMethod() : builder.buildMethod()
  }
}
