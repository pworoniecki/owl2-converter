package test.groovymetamodel

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.RemoveValueMethod

import static test.groovymetamodel.AttributeTestFactory.createAttribute

class RemoveValueMethodTestFactory {

  static RemoveValueMethod createRemoveValueMethod(Attribute attribute) {
    return new RemoveValueMethod.Builder(attribute).buildMethod()
  }

  static RemoveValueMethod createRemoveValueMethod(String attributeName = 'sample') {
    return new RemoveValueMethod.Builder(createAttribute(attributeName, Types.STRING_LIST)).buildMethod()
  }
}
