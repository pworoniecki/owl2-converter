package test.groovymetamodel

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.type.SimpleType

import static owl2converter.groovymetamodel.AccessModifier.PACKAGE_PROTECTED

class ConstructorTestFactory {

  static Constructor createConstructor(AccessModifier accessModifier = PACKAGE_PROTECTED,
                                       List<Parameter> parameters = [createTestParameter()], boolean isStatic = false) {
    return new Constructor(accessModifier: accessModifier, parameters: parameters, isStatic: isStatic)
  }

  private static Parameter createTestParameter() {
    return new Parameter(new SimpleType(Integer), 'testParameter')
  }
}
