package test.groovymetamodel

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.AttributeCharacteristic
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.SYMMETRIC

class AttributeTestFactory {

  static Attribute createAttribute(String name = 'testAttribute', Type type = new SimpleType(String),
                                   AccessModifier accessModifier = AccessModifier.PROTECTED) {
    return new Attribute.AttributeBuilder(name, type)
        .withCharacteristics([SYMMETRIC, REFLEXIVE])
        .withAccessModifier(accessModifier)
        .withRelations([])
        .asStatic()
        .withDefaultValue('DefaultAttributeValue')
        .buildAttribute()
  }

  static Attribute createAttribute(List<AttributeCharacteristic> characteristics, String name = 'testAttribute',
                                   Type type = new SimpleType(String)) {
    return new Attribute.AttributeBuilder(name, type)
        .withCharacteristics(characteristics)
        .withAccessModifier(AccessModifier.PROTECTED)
        .withRelations([])
        .asStatic()
        .withDefaultValue('DefaultAttributeValue')
        .buildAttribute()
  }

  static Attribute createEmptyStaticAttribute(String name = 'testAttribute', Type type = new SimpleType(String)) {
    return new Attribute.AttributeBuilder(name, type).asStatic().buildAttribute()
  }

  static Attribute createEmptyNonStaticAttribute(String name = 'testAttribute', Type type = new SimpleType(String)) {
    return new Attribute.AttributeBuilder(name, type).buildAttribute()
  }
}
