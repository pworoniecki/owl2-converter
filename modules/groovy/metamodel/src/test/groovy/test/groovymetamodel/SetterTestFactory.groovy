package test.groovymetamodel

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Setter

import static test.groovymetamodel.AttributeTestFactory.createAttribute

class SetterTestFactory {

  static Setter createMethod(Attribute attribute = createAttribute()) {
    return new Setter.Builder(attribute).withAccessModifier(AccessModifier.PROTECTED).asStatic()
        .buildMethod()
  }
}
