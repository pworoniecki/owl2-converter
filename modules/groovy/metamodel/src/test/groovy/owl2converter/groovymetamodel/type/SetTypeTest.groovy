package owl2converter.groovymetamodel.type

import spock.lang.Specification
import spock.lang.Unroll

class SetTypeTest extends Specification {

  private static final SimpleType SIMPLE_PARAMETERIZED_TYPE = new SimpleType(String)

  def 'should return simple set type name with simple name of its parametrized type'() {
    given:
    def parametrizedType = SIMPLE_PARAMETERIZED_TYPE

    when:
    def type = new SetType(parametrizedType)

    then:
    type.simpleName == "${Set.simpleName}<$parametrizedType.simpleName>".toString()
  }

  def 'should return full set type name with full name of its parametrized type'() {
    given:
    def parametrizedType = SIMPLE_PARAMETERIZED_TYPE

    when:
    def type = new SetType(parametrizedType)

    then:
    type.fullName == "${Set.name}<${parametrizedType.fullName}>".toString()
  }

  def 'should return package name of Set type'() {
    given:
    def parametrizedType = SIMPLE_PARAMETERIZED_TYPE

    when:
    def type = new SetType(parametrizedType)

    then:
    type.packageName == Set.package.name
  }

  @Unroll
  def 'should extract nested simple parameterized type'() {
    when:
    def type = new SetType(parametrizedType)

    then:
    type.extractSimpleParameterizedType() == SIMPLE_PARAMETERIZED_TYPE

    where:
    parametrizedType << [
        SIMPLE_PARAMETERIZED_TYPE,
        new SetType(SIMPLE_PARAMETERIZED_TYPE),
        new SetType(new SetType(new SetType(SIMPLE_PARAMETERIZED_TYPE)))
    ]
  }
}
