package owl2converter.groovymetamodel.type

import spock.lang.Specification
import spock.lang.Subject

@Subject(PrimitiveType)
class PrimitiveTypeTest extends Specification {

  private static final String BOOLEAN_TYPE_NAME = 'boolean'

  def 'should return boolean as simple name of type'() {
    expect:
    PrimitiveType.BOOLEAN.simpleName == BOOLEAN_TYPE_NAME
  }

  def 'should return boolean as full name of type'() {
    expect:
    PrimitiveType.BOOLEAN.fullName == BOOLEAN_TYPE_NAME
  }

  def 'should return no package'() {
    expect:
    PrimitiveType.BOOLEAN.packageName == null
  }
}
