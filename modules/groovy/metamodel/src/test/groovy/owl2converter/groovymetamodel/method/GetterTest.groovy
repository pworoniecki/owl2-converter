package owl2converter.groovymetamodel.method

import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.VoidType
import test.groovymetamodel.AttributeTestFactory
import spock.lang.Specification

class GetterTest extends Specification {

  def 'should automatically set name of getter'() {
    given:
    def attributeName = 'testAttribute'
    def attribute = AttributeTestFactory.createAttribute(attributeName)

    when:
    def getter = new Getter.Builder(attribute).buildMethod()

    then:
    getter.name == "get${attributeName.capitalize()}".toString()
  }

  def 'should automatically set parameters of getter'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    def getter = new Getter.Builder(attribute).buildMethod()

    then:
    getter.parameters.isEmpty()
  }

  def 'should automatically set return type of getter'() {
    given:
    def attributeName = 'testAttribute'
    def attributeType = new SimpleType(Integer)
    def attribute = AttributeTestFactory.createAttribute(attributeName, attributeType)

    when:
    def getter = new Getter.Builder(attribute).buildMethod()

    then:
    getter.returnType == attributeType
  }

  def 'should not allow to set parameters'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    new Getter.Builder(attribute).withParameters([])

    then:
    thrown UnsupportedOperationException
  }

  def 'should not allow to set return type'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    new Getter.Builder(attribute).withReturnType(VoidType.INSTANCE)

    then:
    thrown UnsupportedOperationException
  }
}
