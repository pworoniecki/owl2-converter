package owl2converter.groovymetamodel.type

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class VoidTypeTest extends Specification {

  private static final String VOID_TYPE_NAME = 'void'

  @Subject
  @Shared
  private VoidType type = VoidType.INSTANCE

  def 'should return void as simple name of type'() {
    expect:
    type.simpleName == VOID_TYPE_NAME
  }

  def 'should return void as full name of type'() {
    expect:
    type.fullName == VOID_TYPE_NAME
  }

  def 'should return no package'() {
    expect:
    type.packageName == null
  }
}
