package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.type.SimpleType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static owl2converter.groovymetamodel.AccessModifier.PACKAGE_PROTECTED
import static owl2converter.groovymetamodel.AccessModifier.PRIVATE

class ConstructorTest extends Specification {

  private static final AccessModifier ACCESS_MODIFIER = PACKAGE_PROTECTED
  private static final List<Parameter> PARAMETERS = [new Parameter(new SimpleType(Integer), 'testParameter')]
  private static final boolean IS_STATIC = true

  @Subject
  @Shared
  private Constructor testConstructor

  def setupSpec() {
    testConstructor = new Constructor(accessModifier: ACCESS_MODIFIER, parameters: PARAMETERS, isStatic: IS_STATIC)
  }

  def 'should return constructor parameters used to instantiate constructor'() {
    expect:
    with(testConstructor) {
      accessModifier == ACCESS_MODIFIER
      parameters == PARAMETERS
      isStatic() == IS_STATIC
    }
  }

  @Unroll
  def 'should not allow to create constructor with any non-primitive field defined as null'() {
    when:
    new Constructor(accessModifier: modifier, parameters: parameters, isStatic: IS_STATIC)

    then:
    thrown NullPointerException

    where:
    modifier        | parameters
    null            | PARAMETERS
    ACCESS_MODIFIER | null
  }

  @Unroll
  def 'should assert that two constructors are equal when all their properties are equal'() {
    expect:
    new Constructor(accessModifier: ACCESS_MODIFIER, parameters: PARAMETERS, isStatic: IS_STATIC) == testConstructor
  }

  @Unroll
  def 'should assert that two constructors are not equal when any of their properties are not equal'() {
    expect:
    new Constructor(accessModifier: modifier, parameters: parameters, isStatic: isStatic) != testConstructor

    where:
    modifier        | parameters | isStatic
    PRIVATE         | PARAMETERS | IS_STATIC
    ACCESS_MODIFIER | []         | IS_STATIC
    ACCESS_MODIFIER | PARAMETERS | !IS_STATIC
  }

  @Unroll
  def 'should assert that two constructors have equal hashcode when all their properties are equal'() {
    expect:
    new Constructor(accessModifier: ACCESS_MODIFIER, parameters: PARAMETERS, isStatic: IS_STATIC).hashCode() ==
        testConstructor.hashCode()
  }

  def 'should assert that two constructors have different hashcode when they have different properties'() {
    expect:
    new Constructor(accessModifier: modifier, parameters: parameters, isStatic: isStatic).hashCode() !=
        testConstructor.hashCode()

    where:
    modifier        | parameters | isStatic
    PRIVATE         | PARAMETERS | IS_STATIC
    ACCESS_MODIFIER | []         | IS_STATIC
    ACCESS_MODIFIER | PARAMETERS | !IS_STATIC
  }

  def 'should assert that two constructors are not equal when one of them is null'() {
    expect:
    testConstructor != null
  }

  def 'should assert that a constructor is equal to itself'() {
    expect:
    testConstructor == testConstructor
  }

  def 'should assert that two constructors are not equal when one of them is instance of another class'() {
    expect:
    testConstructor != 5
  }
}
