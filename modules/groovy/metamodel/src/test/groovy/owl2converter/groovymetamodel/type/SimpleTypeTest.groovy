package owl2converter.groovymetamodel.type

import owl2converter.groovymetamodel.Class
import spock.lang.Specification

import static test.groovymetamodel.ClassTestFactory.createClass

class SimpleTypeTest extends Specification {

  def 'should return class name of class model passed to constructor'() {
    given:
    def sampleClass = createClass()

    when:
    def type = new SimpleType(sampleClass)

    then:
    type.simpleName == sampleClass.name
  }

  def 'should return class name with package name of class model passed to constructor'() {
    given:
    def sampleClass = createClass()

    when:
    def type = new SimpleType(sampleClass)

    then:
    type.fullName == "${sampleClass.packageName}.${sampleClass.name}".toString()
  }

  def 'should return package name of class model passed to constructor'() {
    given:
    def sampleClass = createClass()

    when:
    def type = new SimpleType(sampleClass)

    then:
    type.packageName == sampleClass.packageName
  }

  def 'should return class name of real class passed to constructor'() {
    given:
    def realClassType = String

    when:
    def type = new SimpleType(realClassType)

    then:
    type.simpleName == realClassType.simpleName
  }

  def 'should return class name with package name of real class passed to constructor'() {
    given:
    def realClassType = String

    when:
    def type = new SimpleType(realClassType)

    then:
    type.fullName == realClassType.name
  }

  def 'should return package name of real class passed to constructor'() {
    given:
    def realClassType = String

    when:
    def type = new SimpleType(realClassType)

    then:
    type.packageName == realClassType.package.name
  }

  def 'should not allow to create type when empty class model was provided'() {
    when:
    new SimpleType((Class) null)

    then:
    thrown NullPointerException
  }

  def 'should not allow to create type when empty real class was provided'() {
    when:
    new SimpleType((java.lang.Class) null)

    then:
    thrown NullPointerException
  }
}
