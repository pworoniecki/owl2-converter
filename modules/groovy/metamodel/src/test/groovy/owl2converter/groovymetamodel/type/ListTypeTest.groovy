package owl2converter.groovymetamodel.type

import spock.lang.Specification
import spock.lang.Unroll

class ListTypeTest extends Specification {

  private static final SimpleType SIMPLE_PARAMETERIZED_TYPE = new SimpleType(String)

  def 'should return simple list type name with simple name of its parametrized type'() {
    given:
    def parametrizedType = SIMPLE_PARAMETERIZED_TYPE

    when:
    def type = new ListType(parametrizedType)

    then:
    type.simpleName == "${List.simpleName}<$parametrizedType.simpleName>".toString()
  }

  def 'should return full list type name with full name of its parametrized type'() {
    given:
    def parametrizedType = SIMPLE_PARAMETERIZED_TYPE

    when:
    def type = new ListType(parametrizedType)

    then:
    type.fullName == "${List.name}<${parametrizedType.fullName}>".toString()
  }

  def 'should return package name of List type'() {
    given:
    def parametrizedType = SIMPLE_PARAMETERIZED_TYPE

    when:
    def type = new ListType(parametrizedType)

    then:
    type.packageName == List.package.name
  }

  @Unroll
  def 'should extract nested simple parameterized type'() {
    when:
    def type = new ListType(parametrizedType)

    then:
    type.extractSimpleParameterizedType() == SIMPLE_PARAMETERIZED_TYPE

    where:
    parametrizedType << [
        SIMPLE_PARAMETERIZED_TYPE,
        new ListType(SIMPLE_PARAMETERIZED_TYPE),
        new ListType(new ListType(new ListType(SIMPLE_PARAMETERIZED_TYPE)))
    ]
  }
}
