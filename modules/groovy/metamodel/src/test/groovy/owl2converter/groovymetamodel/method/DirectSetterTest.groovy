package owl2converter.groovymetamodel.method

import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.VoidType
import test.groovymetamodel.AttributeTestFactory
import spock.lang.Specification

class DirectSetterTest extends Specification {

  def 'should set name of direct setter basing on its related attribute'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    def method = new DirectSetter.Builder(attribute).buildMethod()

    then:
    method.name == DirectSetter.getName(attribute)
  }

  def 'should set parameters during initialization'() {
    given:
    def attributeName = 'testAttribute'
    def attributeType = new SimpleType(Integer)
    def attribute = AttributeTestFactory.createAttribute(attributeName, attributeType)

    when:
    def method = new DirectSetter.Builder(attribute).buildMethod()

    then:
    method.parameters.size() == 1
    with(method.parameters.first()) {
      name == attributeName
      type == attributeType
    }
  }

  def 'should set return type during initialization to void'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    def method = new DirectSetter.Builder(attribute).buildMethod()

    then:
    method.returnType == VoidType.INSTANCE
  }

  def 'should not allow to set parameters'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    new DirectSetter.Builder(attribute).withParameters([])

    then:
    thrown UnsupportedOperationException
  }

  def 'should not allow to set return type'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    new DirectSetter.Builder(attribute).withReturnType(VoidType.INSTANCE)

    then:
    thrown UnsupportedOperationException
  }

  def 'should return single parameter'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    def method = new DirectSetter.Builder(attribute).buildMethod()

    then:
    method.parameter == method.parameters.first()
  }
}
