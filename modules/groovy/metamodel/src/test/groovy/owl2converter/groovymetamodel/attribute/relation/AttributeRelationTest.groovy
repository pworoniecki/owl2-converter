package owl2converter.groovymetamodel.attribute.relation

import owl2converter.groovymetamodel.attribute.Attribute
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static test.groovymetamodel.AttributeTestFactory.createAttribute

class AttributeRelationTest extends Specification {

  private static final Attribute ATTRIBUTE = createAttribute()

  @Shared
  @Subject
  private TestAttributeRelation testRelation = new TestAttributeRelation(ATTRIBUTE)

  def 'should not allow to create attribute relation without related attribute'() {
    given:
    def relatedAttribute = null

    when:
    new TestAttributeRelation(relatedAttribute)

    then:
    thrown NullPointerException
  }

  def 'should assert that two relations are equal when they have the same related attribute'() {
    expect:
    new TestAttributeRelation(ATTRIBUTE) == testRelation
  }

  def 'should assert that two relations are not equal when they have different related attributes'() {
    expect:
    new TestAttributeRelation(createAttribute('anotherAttribute')) != testRelation
  }

  def 'should assert that two relations have equal hashcode when they have the same related attribute'() {
    expect:
    new TestAttributeRelation(ATTRIBUTE).hashCode() == testRelation.hashCode()
  }

  def 'should assert that two relations have different hashcode when they have different related attributes'() {
    expect:
    new TestAttributeRelation(createAttribute('anotherAttribute')).hashCode() != testRelation.hashCode()
  }

  def 'should assert that two relations are not equal when one of them is null'() {
    expect:
    testRelation != null
  }

  def 'should assert that a relation between attributes is equal to itself'() {
    expect:
    testRelation == testRelation
  }

  def 'should assert that two attribute relations are not equal when one of them is instance of another class'() {
    expect:
    testRelation != 5
  }

  private static class TestAttributeRelation extends AttributeRelation {
    TestAttributeRelation(Attribute relatedAttribute) {
      super(relatedAttribute)
    }
  }
}
