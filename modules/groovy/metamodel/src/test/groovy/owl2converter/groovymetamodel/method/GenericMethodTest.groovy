package owl2converter.groovymetamodel.method

import spock.lang.Specification

class GenericMethodTest extends Specification {

  def 'should set body of custom method'() {
    given:
    def bodyCode = 'testMethodCode'

    when:
    def method = new CustomMethod.Builder('testMethod', bodyCode).buildMethod()

    then:
    method.bodyCode == bodyCode
  }

  def 'should not allow to create custom method without body code'() {
    given:
    def bodyCode = null

    when:
    new CustomMethod.Builder('testMethod', bodyCode).buildMethod()

    then:
    thrown NullPointerException
  }
}
