package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.SimpleType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static test.groovymetamodel.ConstructorTestFactory.createConstructor
import static test.groovymetamodel.AttributeTestFactory.createAttribute
import static test.groovymetamodel.ClassTestFactory.createEmptyClass
import static test.groovymetamodel.MethodTestFactory.createMethod
import static test.groovymetamodel.TraitTestFactory.createTrait

class ClassTest extends Specification {

  private static final String PACKAGE_NAME = 'org.test'
  private static final String CLASS_NAME = 'TestClass'
  private static final List<Annotation> ANNOTATIONS = [new Annotation(new SimpleType(Override))]
  private static final List<Attribute> ATTRIBUTES = [createAttribute('attr1'), createAttribute('attr2')]
  private static final List<Constructor> CONSTRUCTORS = [createConstructor()]
  private static final Class INHERITED_CLASS = new Class.Builder('org.inherited', 'InheritedClass').buildClass()
  private static final List<Method> METHODS = [createMethod('method1'), createMethod('method2')]
  private static final List<Trait> IMPLEMENTED_TRAITS = [createTrait()]

  @Subject
  @Shared
  private Class testClass

  def setupSpec() {
    testClass = createClass(PACKAGE_NAME, CLASS_NAME, ANNOTATIONS, INHERITED_CLASS, CONSTRUCTORS, ATTRIBUTES, METHODS,
        IMPLEMENTED_TRAITS)
  }

  def 'should return class parameters passed to class builder'() {
    expect:
    with(testClass) {
      packageName == PACKAGE_NAME
      name == CLASS_NAME
      annotations == ANNOTATIONS
      attributes == ATTRIBUTES
      constructors == CONSTRUCTORS
      inheritedClass.isPresent()
      inheritedClass.get() == INHERITED_CLASS
      methods == METHODS
    }
  }

  @Unroll
  def 'should not allow to create class with any field defined as null'() {
    when:
    createClass(packageName, className, annotations, inheritedClass, constructors, attributes, methods,
        implementedTraits)

    then:
    thrown NullPointerException

    where:
    packageName  | className  | inheritedClass  | constructors | attributes | methods | implementedTraits  | annotations
    null         | CLASS_NAME | INHERITED_CLASS | CONSTRUCTORS | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME | null       | INHERITED_CLASS | CONSTRUCTORS | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | null            | CONSTRUCTORS | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | INHERITED_CLASS | null         | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | INHERITED_CLASS | CONSTRUCTORS | null       | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | INHERITED_CLASS | CONSTRUCTORS | ATTRIBUTES | null    | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | INHERITED_CLASS | CONSTRUCTORS | ATTRIBUTES | METHODS | null               | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | INHERITED_CLASS | CONSTRUCTORS | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | null
  }

  def 'should not allow to create class with any annotation defined as null'() {
    given:
    def annotations = ANNOTATIONS + null

    when:
    createClass(PACKAGE_NAME, CLASS_NAME, annotations, INHERITED_CLASS, CONSTRUCTORS, ATTRIBUTES, METHODS,
        IMPLEMENTED_TRAITS)

    then:
    thrown IllegalArgumentException
  }

  def 'should not allow to create class with any constructor defined as null'() {
    given:
    def constructors = CONSTRUCTORS + null

    when:
    createClass(PACKAGE_NAME, CLASS_NAME, ANNOTATIONS, INHERITED_CLASS, constructors, ATTRIBUTES, METHODS,
        IMPLEMENTED_TRAITS)

    then:
    thrown IllegalArgumentException
  }

  def 'should not allow to create class with any attribute defined as null'() {
    given:
    def attributes = ATTRIBUTES + null

    when:
    createClass(PACKAGE_NAME, CLASS_NAME, ANNOTATIONS, INHERITED_CLASS, CONSTRUCTORS, attributes, METHODS,
        IMPLEMENTED_TRAITS)

    then:
    thrown IllegalArgumentException
  }

  def 'should not allow to create class with any method defined as null'() {
    given:
    def methods = METHODS + null

    when:
    createClass(PACKAGE_NAME, CLASS_NAME, ANNOTATIONS, INHERITED_CLASS, CONSTRUCTORS, ATTRIBUTES, methods,
        IMPLEMENTED_TRAITS)

    then:
    thrown IllegalArgumentException
  }

  def 'should not allow to create class with any implemented trait defined as null'() {
    given:
    def implementedTraits = IMPLEMENTED_TRAITS + null

    when:
    createClass(PACKAGE_NAME, CLASS_NAME, ANNOTATIONS, INHERITED_CLASS, CONSTRUCTORS, ATTRIBUTES, METHODS,
        implementedTraits)

    then:
    thrown IllegalArgumentException
  }

  @Unroll
  def 'should assert that two classes are equal when their class names and package names are equal'() {
    expect:
    createClass(packageName, className, annotations, inheritedClass, constructors, attributes, methods, implementedTraits) ==
        testClass

    where:
    packageName  | className  | inheritedClass     | constructors | attributes | methods | implementedTraits  | annotations
    PACKAGE_NAME | CLASS_NAME | createEmptyClass() | CONSTRUCTORS | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | INHERITED_CLASS    | []           | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | INHERITED_CLASS    | CONSTRUCTORS | []         | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | INHERITED_CLASS    | CONSTRUCTORS | ATTRIBUTES | []      | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | INHERITED_CLASS    | CONSTRUCTORS | ATTRIBUTES | METHODS | []                 | ANNOTATIONS
    PACKAGE_NAME | CLASS_NAME | INHERITED_CLASS    | CONSTRUCTORS | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | []
  }

  @Unroll
  def 'should assert that two classes are not equal when their class names or package names are not equal'() {
    expect:
    createClass(packageName, className, ANNOTATIONS, INHERITED_CLASS, CONSTRUCTORS, ATTRIBUTES, METHODS,
        IMPLEMENTED_TRAITS) != testClass

    where:
    packageName      | className
    'anotherPackage' | CLASS_NAME
    PACKAGE_NAME     | 'AnotherClass'
  }

  @Unroll
  def 'should assert that two classes have equal hashcode when their class names are equal'() {
    expect:
    createClass(packageName, CLASS_NAME, annotations, inheritedClass, constructors, attributes, methods, implementedTraits)
        .hashCode() == testClass.hashCode()

    where:
    packageName      | inheritedClass     | constructors | attributes | methods | implementedTraits  | annotations
    'anotherPackage' | INHERITED_CLASS    | CONSTRUCTORS | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME     | createEmptyClass() | CONSTRUCTORS | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME     | INHERITED_CLASS    | []           | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME     | INHERITED_CLASS    | CONSTRUCTORS | []         | METHODS | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME     | INHERITED_CLASS    | CONSTRUCTORS | ATTRIBUTES | []      | IMPLEMENTED_TRAITS | ANNOTATIONS
    PACKAGE_NAME     | INHERITED_CLASS    | CONSTRUCTORS | ATTRIBUTES | METHODS | []                 | ANNOTATIONS
    PACKAGE_NAME     | INHERITED_CLASS    | CONSTRUCTORS | ATTRIBUTES | METHODS | IMPLEMENTED_TRAITS | []
  }

  def 'should assert that two classes have different hashcode when they have different class names'() {
    when:
    def anotherClass = createClass(PACKAGE_NAME, 'AnotherClass', ANNOTATIONS, INHERITED_CLASS, CONSTRUCTORS, ATTRIBUTES,
        METHODS, IMPLEMENTED_TRAITS)

    then:
    anotherClass.hashCode() != testClass.hashCode()
  }

  def 'should assert that a class is equal to itself'() {
    expect:
    testClass == testClass
  }

  def 'should assert that two classes are not equal when one of them is null'() {
    expect:
    testClass != null
  }

  def 'should assert that two class models are not equal when one of them is instance of another class'() {
    expect:
    testClass != 5
  }

  def 'all constructors, attributes and methods should be visited by class element visitor'() {
    given:
    def visitor = Mock(ClassElementVisitor)

    when:
    testClass.accept(visitor)

    then:
    with(testClass) {
      constructors.each { 1 * visitor.visit(it, testClass) }
      attributes.each { 1 * visitor.visit(it) }
      methods.each { 1 * visitor.visit(it) }
    }
  }

  def 'should return name composed of package name and class name'() {
    expect:
    testClass.fullName == "${testClass.packageName}.$testClass.name".toString()
  }

  private static Class createClass(String packageName, String className, List<Annotation> annotations,
                                             Class inheritedClass, List<Constructor> constructors, List<Attribute> attributes,
                                             List<Method> methods, List<Trait> implementedTraits) {
    return new Class.Builder(packageName, className).withAnnotations(annotations).withAttributes(attributes)
        .withConstructors(constructors).withMethods(methods).withInheritedClass(inheritedClass)
        .withImplementedTraits(implementedTraits).buildClass()
  }
}
