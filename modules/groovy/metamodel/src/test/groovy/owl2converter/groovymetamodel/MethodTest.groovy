package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static owl2converter.groovymetamodel.AccessModifier.PACKAGE_PROTECTED
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static test.groovymetamodel.MethodTestFactory.createMethod

class MethodTest extends Specification {

  private static final String METHOD_NAME = 'testMethod'
  private static final AccessModifier ACCESS_MODIFIER = PACKAGE_PROTECTED
  private static final boolean STATIC_METHOD = true
  private static final Type TYPE = new SimpleType(String)
  private static final List<Parameter> PARAMETERS = [new Parameter(new SimpleType(Integer), 'testParameter')]

  @Subject
  @Shared
  private Method testMethod

  def setupSpec() {
    testMethod = createMethod(METHOD_NAME, ACCESS_MODIFIER, STATIC_METHOD, TYPE, PARAMETERS)
  }

  def 'should return method parameters passed to method builder'() {
    expect:
    with(testMethod) {
      name == METHOD_NAME
      accessModifier == ACCESS_MODIFIER
      isStatic() == STATIC_METHOD
      returnType == TYPE
      parameters == PARAMETERS
    }
  }

  @Unroll
  def 'should not allow to create method with any field defined as null'() {
    when:
    createMethod(name, modifier, STATIC_METHOD, returnType, parameters)

    then:
    thrown NullPointerException

    where:
    name        | modifier        | returnType | parameters
    null        | ACCESS_MODIFIER | TYPE       | PARAMETERS
    METHOD_NAME | null            | TYPE       | PARAMETERS
    METHOD_NAME | ACCESS_MODIFIER | null       | PARAMETERS
    METHOD_NAME | ACCESS_MODIFIER | TYPE       | null
  }

  def 'should not allow to create method with any parameter defined as null'() {
    given:
    def parameters = PARAMETERS + null

    when:
    createMethod(METHOD_NAME, ACCESS_MODIFIER, STATIC_METHOD, TYPE, parameters)

    then:
    thrown IllegalArgumentException
  }

  @Unroll
  def 'should assert that two methods are equal when their names and parameters are equal'() {
    expect:
    createMethod(METHOD_NAME, modifier, isStatic, returnType, PARAMETERS) == testMethod

    where:
    modifier        | isStatic       | returnType
    PUBLIC          | STATIC_METHOD  | TYPE
    ACCESS_MODIFIER | !STATIC_METHOD | TYPE
    ACCESS_MODIFIER | STATIC_METHOD  | new ListType(new SimpleType(BigInteger))

  }

  @Unroll
  def 'should assert that two methods are not equal when their names or parameters are not equal'() {
    expect:
    createMethod(name, ACCESS_MODIFIER, STATIC_METHOD, TYPE, parameters) != testMethod

    where:
    name          | parameters
    'anotherName' | PARAMETERS
    METHOD_NAME   | []
  }

  @Unroll
  def 'should assert that two methods have equal hashcode when their names are equal'() {
    expect:
    createMethod(METHOD_NAME, modifier, isStatic as boolean, returnType, parameters).hashCode() == testMethod.hashCode()

    where:
    modifier        | isStatic       | returnType                               | parameters
    PUBLIC          | STATIC_METHOD  | TYPE                                     | PARAMETERS
    ACCESS_MODIFIER | !STATIC_METHOD | TYPE                                     | PARAMETERS
    ACCESS_MODIFIER | STATIC_METHOD  | new ListType(new SimpleType(BigInteger)) | PARAMETERS
    ACCESS_MODIFIER | STATIC_METHOD  | TYPE                                     | []
  }

  def 'should assert that two methods have different hashcode when they have different names'() {
    when:
    def anotherMethod = createMethod('anotherMethod', ACCESS_MODIFIER, STATIC_METHOD, TYPE, PARAMETERS)

    then:
    anotherMethod.hashCode() != testMethod.hashCode()
  }

  def 'should assert that two methods are not equal when one of them is null'() {
    expect:
    testMethod != null
  }

  def 'should assert that a method is equal to itself'() {
    expect:
    testMethod == testMethod
  }

  def 'should assert that two methods are not equal when one of them is instance of another class'() {
    expect:
    testMethod != 5
  }
}
