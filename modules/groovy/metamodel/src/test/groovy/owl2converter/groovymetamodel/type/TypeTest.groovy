package owl2converter.groovymetamodel.type

import spock.lang.Specification

import static test.groovymetamodel.ClassTestFactory.createEmptyClass

class TypeTest extends Specification {

  def 'should indicate types as equal when their full names are equal'() {
    given:
    def fullname = 'test'

    when:
    Type type1 = new TestType(fullname)
    Type type2 = new TestType(fullname)

    then:
    type1 == type2
  }

  def 'should not indicate types as equal when their full names are not equal'() {
    when:
    Type type1 = new TestType('test1')
    Type type2 = new TestType('test2')

    then:
    type1 != type2
  }

  def 'should not indicate types as equal when one of them is null'() {
    when:
    Type type1 = new TestType('test')
    Type type2 = null

    then:
    type1 != type2
  }

  def 'should calculate equal hashcodes for types with equal full names'() {
    given:
    def fullname = 'test'

    when:
    Type type1 = new TestType(fullname)
    Type type2 = new TestType(fullname)

    then:
    type1.hashCode() == type2.hashCode()
  }

  def 'should calculate different hashcodes for types with different full names'() {
    when:
    Type type1 = new TestType('test1')
    Type type2 = new TestType('test2')

    then:
    type1.hashCode() != type2.hashCode()
  }

  def 'should indicate that object type is imported by default'() {
    expect:
    type.isImportedByDefault()

    where:
    type << ['java.lang', 'java.util', 'java.io', 'java.net', 'groovy.lang', 'groovy.util']
        .collect { new SimpleType(createEmptyClass(it, 'TestClass')) } +
        [new SimpleType(BigInteger), new SimpleType(BigDecimal)]
  }

  def 'should indicate that primitive type is imported by default'() {
    expect:
    new PrimitiveType(type).isImportedByDefault()

    where:
    type << ['byte', 'short', 'int', 'long', 'float', 'double', 'boolean', 'char']
  }

  class TestType extends Type {

    private String fullname

    TestType(String fullname) {
      this.fullname = fullname
    }

    @Override
    String getFullName() {
      return fullname
    }

    @Override
    String getSimpleName() {
      return null
    }

    @Override
    String getPackageName() {
      return null
    }
  }
}
