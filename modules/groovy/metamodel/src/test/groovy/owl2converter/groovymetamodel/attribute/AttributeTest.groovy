package owl2converter.groovymetamodel.attribute

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.attribute.relation.AttributeRelation
import owl2converter.groovymetamodel.attribute.relation.EquivalentToAttributeRelation
import owl2converter.groovymetamodel.attribute.relation.InverseAttributeRelation
import owl2converter.groovymetamodel.attribute.relation.SuperAttributeOfRelation
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.VoidType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static owl2converter.groovymetamodel.AccessModifier.PROTECTED
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.SYMMETRIC
import static test.groovymetamodel.AttributeTestFactory.createAttribute
import static test.groovymetamodel.AttributeTestFactory.createEmptyNonStaticAttribute

class AttributeTest extends Specification {

  private static final String ATTRIBUTE_NAME = 'testAttribute'
  private static final boolean STATIC_ATTRIBUTE = true
  private static final SimpleType TYPE = new SimpleType(String)
  private static final Object DEFAULT_VALUE = 'DefaultValue'
  private static final List<AttributeRelation> RELATIONS = [new EquivalentToAttributeRelation(createAttribute())]
  private static final AccessModifier ACCESS_MODIFIER = PROTECTED
  private static final List<AttributeCharacteristic> CHARACTERISTICS = [REFLEXIVE, SYMMETRIC]

  @Subject
  @Shared
  private Attribute testAttribute

  def setupSpec() {
    testAttribute = createAttribute(ATTRIBUTE_NAME, ACCESS_MODIFIER, STATIC_ATTRIBUTE, TYPE, DEFAULT_VALUE,
        CHARACTERISTICS, RELATIONS)
  }

  def 'should return attribute parameters passed to attribute builder'() {
    expect:
    with(testAttribute) {
      name == ATTRIBUTE_NAME
      isStatic() == STATIC_ATTRIBUTE
      valueType == TYPE
      relations == RELATIONS
      accessModifier == ACCESS_MODIFIER
      characteristics == CHARACTERISTICS
    }
  }

  @Unroll
  def 'should not allow to create attribute with any field defined as null'() {
    when:
    createAttribute(name, modifier, STATIC_ATTRIBUTE, valueType, defaultValue, characteristics, relations)

    then:
    thrown NullPointerException

    where:
    name           | modifier        | valueType | defaultValue  | characteristics | relations
    null           | ACCESS_MODIFIER | TYPE      | DEFAULT_VALUE | CHARACTERISTICS | RELATIONS
    ATTRIBUTE_NAME | null            | TYPE      | DEFAULT_VALUE | CHARACTERISTICS | RELATIONS
    ATTRIBUTE_NAME | ACCESS_MODIFIER | null      | DEFAULT_VALUE | CHARACTERISTICS | RELATIONS
    ATTRIBUTE_NAME | ACCESS_MODIFIER | TYPE      | null          | CHARACTERISTICS | RELATIONS
    ATTRIBUTE_NAME | ACCESS_MODIFIER | TYPE      | DEFAULT_VALUE | null            | RELATIONS
    ATTRIBUTE_NAME | ACCESS_MODIFIER | TYPE      | DEFAULT_VALUE | CHARACTERISTICS | null
  }

  def 'should not allow to create attribute with any characteristic defined as null'() {
    given:
    def characteristics = CHARACTERISTICS + null

    when:
    createAttribute(ATTRIBUTE_NAME, ACCESS_MODIFIER, STATIC_ATTRIBUTE, TYPE, DEFAULT_VALUE, characteristics, RELATIONS)

    then:
    thrown IllegalArgumentException
  }

  def 'should not allow to create attribute with any relation defined as null'() {
    given:
    def relations = RELATIONS + null

    when:
    createAttribute(ATTRIBUTE_NAME, ACCESS_MODIFIER, STATIC_ATTRIBUTE, TYPE, DEFAULT_VALUE, CHARACTERISTICS, relations)

    then:
    thrown IllegalArgumentException
  }

  @Unroll
  def 'should assert that two attributes are equal when their names and access modifiers are equal'() {
    expect:
    createAttribute(ATTRIBUTE_NAME, ACCESS_MODIFIER, isStatic.asBoolean(), valueType, DEFAULT_VALUE, characteristics,
        relations) == testAttribute

    where:
    valueType               | characteristics | relations | isStatic
    new SimpleType(Integer) | CHARACTERISTICS | RELATIONS | STATIC_ATTRIBUTE
    TYPE                    | []              | RELATIONS | STATIC_ATTRIBUTE
    TYPE                    | CHARACTERISTICS | []        | STATIC_ATTRIBUTE
    TYPE                    | CHARACTERISTICS | RELATIONS | !STATIC_ATTRIBUTE
  }

  @Unroll
  def 'should assert that two attributes are not equal when their names or access modifiers are not equal'() {
    expect:
    createAttribute(name, modifier, STATIC_ATTRIBUTE, TYPE, DEFAULT_VALUE, CHARACTERISTICS, RELATIONS) != testAttribute

    where:
    name               | modifier
    'anotherAttribute' | ACCESS_MODIFIER
    ATTRIBUTE_NAME     | PUBLIC
  }

  @Unroll
  def 'should assert that two attributes have equal hashcode when they have equal names'() {
    expect:
    createAttribute(ATTRIBUTE_NAME, modifier, isStatic.asBoolean(), valueType, defaultValue, characteristics,
        relations).hashCode() == testAttribute.hashCode()

    where:
    modifier        | valueType               | defaultValue   | characteristics | relations | isStatic
    PUBLIC          | TYPE                    | DEFAULT_VALUE  | CHARACTERISTICS | RELATIONS | STATIC_ATTRIBUTE
    ACCESS_MODIFIER | new SimpleType(Integer) | DEFAULT_VALUE  | CHARACTERISTICS | RELATIONS | STATIC_ATTRIBUTE
    ACCESS_MODIFIER | TYPE                    | 'AnotherValue' | CHARACTERISTICS | RELATIONS | STATIC_ATTRIBUTE
    ACCESS_MODIFIER | TYPE                    | DEFAULT_VALUE  | []              | RELATIONS | STATIC_ATTRIBUTE
    ACCESS_MODIFIER | TYPE                    | DEFAULT_VALUE  | CHARACTERISTICS | []        | STATIC_ATTRIBUTE
    ACCESS_MODIFIER | TYPE                    | DEFAULT_VALUE  | CHARACTERISTICS | RELATIONS | !STATIC_ATTRIBUTE
  }

  def 'should assert that two attributes have different hashcode when theirs names are different'() {
    when:
    def anotherAttribute = createAttribute('anotherAttribute', ACCESS_MODIFIER, STATIC_ATTRIBUTE, TYPE, DEFAULT_VALUE,
        CHARACTERISTICS, RELATIONS)

    then:
    anotherAttribute.hashCode() != testAttribute.hashCode()
  }

  def 'should assert that an attribute is equal to itself'() {
    expect:
    testAttribute == testAttribute
  }

  def 'should assert that two attributes are not equal when one of them is null'() {
    expect:
    testAttribute != null
  }

  def 'should assert that two attributes are not equal when one of them is instance of another class'() {
    expect:
    testAttribute != 5
  }

  def 'should assert that attribute is equivalent to another attribute'() {
    given:
    def equivalentAttribute = createAttribute()

    when:
    def attribute = new Attribute.AttributeBuilder('test', new SimpleType(String))
        .withRelations([new EquivalentToAttributeRelation(equivalentAttribute)]).buildAttribute()

    then:
    attribute.isEquivalentOwnedAttribute()
    with(attribute.findAnyEquivalentAttribute()) {
      isPresent()
      get() == equivalentAttribute
    }
  }

  def 'should assert that attribute is not equivalent to any another attribute'() {
    expect:
    !createEmptyNonStaticAttribute().isEquivalentOwnedAttribute()
  }

  def 'should assert that attribute is super of another attribute'() {
    given:
    def subAttributes = [createAttribute('first'), createAttribute('second')]

    when:
    def attribute = new Attribute.AttributeBuilder('test', new SimpleType(String))
        .withRelations(subAttributes.collect { new SuperAttributeOfRelation(it) })
        .buildAttribute()

    then:
    with(attribute) {
      isSuperAttribute()
      findSubAttributes() == subAttributes
    }
  }

  def 'should assert that attribute has no subproperties'() {
    expect:
    !createEmptyNonStaticAttribute().isSuperAttribute()
  }

  def 'should assert that attribute is inverse of another attribute'() {
    given:
    def inverseAttributes = [createAttribute('first'), createAttribute('second')]

    when:
    def attribute = new Attribute.AttributeBuilder('test', new SimpleType(String))
        .withRelations(inverseAttributes.collect { new InverseAttributeRelation(it) })
        .buildAttribute()

    then:
    with(attribute) {
      isInverseAttribute()
      findInverseAttributes() == inverseAttributes
    }
  }

  def 'should assert that attribute has no inverse properties'() {
    expect:
    !createEmptyNonStaticAttribute().isInverseAttribute()
  }

  def 'should throw an exception when trying to create attribute with void type'() {
    when:
    new Attribute.AttributeBuilder('test', VoidType.INSTANCE).buildAttribute()

    then:
    thrown IllegalArgumentException
  }

  def 'should return simple type of attribute'() {
    when:
    def attribute = new Attribute.AttributeBuilder('test', type).buildAttribute()

    then:
    attribute.extractSimpleValueType() == expectedType

    where:
    type                                                | expectedType
    new SimpleType(Integer)                             | new SimpleType(Integer)
    new ListType(new SimpleType(Integer))               | new SimpleType(Integer)
    new ListType(new ListType(new SimpleType(Integer))) | new SimpleType(Integer)
  }

  private static Attribute createAttribute(String name, AccessModifier modifier, boolean isStatic,
                                           SimpleType valueType, Object defaultValue,
                                           List<AttributeCharacteristic> characteristics,
                                           List<AttributeRelation> relations) {
    def builder = new Attribute.AttributeBuilder(name, valueType).withAccessModifier(modifier)
        .withCharacteristics(characteristics).withRelations(relations).withDefaultValue(defaultValue)
    return isStatic ? builder.asStatic().buildAttribute() : builder.buildAttribute()
  }
}
