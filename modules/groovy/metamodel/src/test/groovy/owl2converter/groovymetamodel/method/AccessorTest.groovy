package owl2converter.groovymetamodel.method

import test.groovymetamodel.AccessorTestFactory
import test.groovymetamodel.AttributeTestFactory
import spock.lang.Specification

class AccessorTest extends Specification {

  def 'should link an attribute to accessor'() {
    given:
    def name = 'testAccessor'
    def relatedAttribute = AttributeTestFactory.createAttribute()

    when:
    def accessor = AccessorTestFactory.createAccessor(name, relatedAttribute)

    then:
    accessor.attribute == relatedAttribute
  }

  def 'should not allow to create accessor without linked attribute'() {
    given:
    def name = 'testAccessor'
    def relatedAttribute = null

    when:
    AccessorTestFactory.createAccessor(name, relatedAttribute)

    then:
    thrown NullPointerException
  }
}
