package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import spock.lang.Specification

class AnnotationTest extends Specification {

  private static final Type TYPE = new SimpleType(Override)

  def 'should not allow to create annotation without type'() {
    given:
    Type type = null

    when:
    new Annotation(type)

    then:
    thrown NullPointerException
  }

  def 'should not allow to create annotation with null parameters'() {
    given:
    List<String> parameters = null

    when:
    new Annotation(TYPE, parameters)

    then:
    thrown NullPointerException
  }

  def 'should not allow to create annotation with any parameter defined as null'() {
    given:
    def parameters = ['annotationParameter', null]

    when:
    new Annotation(TYPE, parameters)

    then:
    thrown NullPointerException
  }
}
