package owl2converter.groovymetamodel.method

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.VoidType
import spock.lang.Specification

import static test.groovymetamodel.AttributeTestFactory.createAttribute

class RemoveValueMethodTest extends Specification {

  private static final Attribute ATTRIBUTE = createAttribute('testAttribute', new ListType(new SimpleType(Integer)))

  def 'should set name of remove value method basing on its related attribute'() {
    when:
    def method = new RemoveValueMethod.Builder(ATTRIBUTE).buildMethod()

    then:
    method.name == RemoveValueMethod.getName(ATTRIBUTE)
  }

  def 'should set parameters during initialization'() {
    given:
    def attributeName = 'testAttribute'
    def attributeType = new ListType(new SimpleType(Integer))
    def attribute = createAttribute(attributeName, attributeType)

    when:
    def method = new RemoveValueMethod.Builder(attribute).buildMethod()

    then:
    method.parameters.size() == 1
    def parameter = method.parameters.first()
    parameter.name == attributeName
    parameter.type == attributeType.extractSimpleParameterizedType()
  }

  def 'should set return type during initialization to void'() {
    when:
    def method = new RemoveValueMethod.Builder(ATTRIBUTE).buildMethod()

    then:
    method.returnType == VoidType.INSTANCE
  }

  def 'should not allow to set parameters'() {
    when:
    new RemoveValueMethod.Builder(ATTRIBUTE).withParameters([])

    then:
    thrown UnsupportedOperationException
  }

  def 'should not allow to set return type'() {
    when:
    new RemoveValueMethod.Builder(ATTRIBUTE).withReturnType(VoidType.INSTANCE)

    then:
    thrown UnsupportedOperationException
  }

  def 'should return single parameter'() {
    when:
    def method = new RemoveValueMethod.Builder(ATTRIBUTE).buildMethod()

    then:
    method.parameter == method.parameters.first()
  }

  def 'should throw an exception when trying to create remove value method using non-list attribute'() {
    given:
    def attribute = createAttribute('test', new SimpleType(Integer))

    when:
    new RemoveValueMethod.Builder(attribute)

    then:
    thrown IllegalArgumentException
  }
}
