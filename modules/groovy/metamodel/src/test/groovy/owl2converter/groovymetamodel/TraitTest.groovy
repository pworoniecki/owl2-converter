package owl2converter.groovymetamodel

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.SimpleType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static test.groovymetamodel.AttributeTestFactory.createAttribute
import static test.groovymetamodel.MethodTestFactory.createMethod

class TraitTest extends Specification {

  private static final String PACKAGE_NAME = 'org.test'
  private static final String TRAIT_NAME = 'TestTrait'
  private static final List<Annotation> ANNOTATIONS = [new Annotation(new SimpleType(Override))]
  private static final List<Attribute> ATTRIBUTES = [
      createAttribute('attr1', new SimpleType(String), AccessModifier.PRIVATE),
      createAttribute('attr2', new SimpleType(Integer), AccessModifier.PUBLIC)
  ]
  private static final List<Trait> INHERITED_TRAITS =
      [new Trait.Builder('org.inherited', 'inheritedTraits').buildTrait()]
  private static final List<Method> METHODS = [
      createMethod('method1', AccessModifier.PRIVATE),
      createMethod('method2', AccessModifier.PUBLIC)
  ]

  @Subject
  @Shared
  private Trait testTrait

  def setupSpec() {
    testTrait = createTrait(PACKAGE_NAME, TRAIT_NAME, ANNOTATIONS, INHERITED_TRAITS, ATTRIBUTES, METHODS)
  }

  def 'should return trait parameters passed to trait builder'() {
    expect:
    with(testTrait) {
      packageName == PACKAGE_NAME
      name == TRAIT_NAME
      attributes == ATTRIBUTES
      inheritedTraits == INHERITED_TRAITS
      methods == METHODS
    }
  }

  @Unroll
  def 'should not allow to create trait with any field defined as null'() {
    when:
    createTrait(packageName, traitName, annotations, inheritedTraits, attributes, methods)

    then:
    thrown NullPointerException

    where:
    packageName  | traitName  | annotations | inheritedTraits  | attributes | methods
    null         | TRAIT_NAME | ANNOTATIONS | INHERITED_TRAITS | ATTRIBUTES | METHODS
    PACKAGE_NAME | null       | ANNOTATIONS | INHERITED_TRAITS | ATTRIBUTES | METHODS
    PACKAGE_NAME | TRAIT_NAME | null        | INHERITED_TRAITS | ATTRIBUTES | METHODS
    PACKAGE_NAME | TRAIT_NAME | ANNOTATIONS | null             | ATTRIBUTES | METHODS
    PACKAGE_NAME | TRAIT_NAME | ANNOTATIONS | INHERITED_TRAITS | null       | METHODS
    PACKAGE_NAME | TRAIT_NAME | ANNOTATIONS | INHERITED_TRAITS | ATTRIBUTES | null
  }

  def 'should not allow to create trait with any annotation defined as null'() {
    given:
    def annotations = ANNOTATIONS + null

    when:
    createTrait(PACKAGE_NAME, TRAIT_NAME, annotations, INHERITED_TRAITS, ATTRIBUTES, METHODS)

    then:
    thrown IllegalArgumentException
  }

  def 'should not allow to create trait with any inherited trait defined as null'() {
    given:
    def inheritedTraits = INHERITED_TRAITS + null

    when:
    createTrait(PACKAGE_NAME, TRAIT_NAME, ANNOTATIONS, inheritedTraits, ATTRIBUTES, METHODS)

    then:
    thrown IllegalArgumentException
  }

  def 'should not allow to create trait with any attribute defined as null'() {
    given:
    def attributes = ATTRIBUTES + null

    when:
    createTrait(PACKAGE_NAME, TRAIT_NAME, ANNOTATIONS, INHERITED_TRAITS, attributes, METHODS)

    then:
    thrown IllegalArgumentException
  }

  def 'should not allow to create trait with any method defined as null'() {
    given:
    def methods = METHODS + null

    when:
    createTrait(PACKAGE_NAME, TRAIT_NAME, ANNOTATIONS, INHERITED_TRAITS, ATTRIBUTES, methods)

    then:
    thrown IllegalArgumentException
  }

  @Unroll
  def 'should assert that two traits are equal when their trait names and package names are equal'() {
    expect:
    createTrait(PACKAGE_NAME, TRAIT_NAME, annotations, inheritedTraits, attributes, methods) == testTrait

    where:
    annotations | inheritedTraits      | attributes | methods
    []          | INHERITED_TRAITS     | ATTRIBUTES | METHODS
    ANNOTATIONS | [createEmptyTrait()] | ATTRIBUTES | METHODS
    ANNOTATIONS | INHERITED_TRAITS     | ATTRIBUTES | METHODS
    ANNOTATIONS | INHERITED_TRAITS     | []         | METHODS
    ANNOTATIONS | INHERITED_TRAITS     | ATTRIBUTES | []
  }

  @Unroll
  def 'should assert that two traits are not equal when their trait names or package names are not equal'() {
    expect:
    createTrait(packageName, traitName, ANNOTATIONS, INHERITED_TRAITS, ATTRIBUTES, METHODS) != testTrait

    where:
    packageName      | traitName
    'anotherPackage' | TRAIT_NAME
    PACKAGE_NAME     | 'AnotherTrait'
  }

  @Unroll
  def 'should assert that two traits have equal hashcode when their trait names are equal'() {
    expect:
    createTrait(packageName, TRAIT_NAME, annotations, inheritedTraits, attributes, methods).hashCode() ==
        testTrait.hashCode()

    where:
    packageName      | annotations | inheritedTraits      | attributes | methods
    'anotherPackage' | ANNOTATIONS | INHERITED_TRAITS     | ATTRIBUTES | METHODS
    PACKAGE_NAME     | []          | INHERITED_TRAITS     | ATTRIBUTES | METHODS
    PACKAGE_NAME     | ANNOTATIONS | [createEmptyTrait()] | ATTRIBUTES | METHODS
    PACKAGE_NAME     | ANNOTATIONS | INHERITED_TRAITS     | ATTRIBUTES | METHODS
    PACKAGE_NAME     | ANNOTATIONS | INHERITED_TRAITS     | []         | METHODS
    PACKAGE_NAME     | ANNOTATIONS | INHERITED_TRAITS     | ATTRIBUTES | []
  }

  def 'should assert that two traits have different hashcode when they have different trait names'() {
    when:
    def anotherTrait = createTrait(PACKAGE_NAME, 'AnotherTrait', ANNOTATIONS, INHERITED_TRAITS, ATTRIBUTES, METHODS)

    then:
    anotherTrait.hashCode() != testTrait.hashCode()
  }

  def 'should assert that a trait is equal to itself'() {
    expect:
    testTrait == testTrait
  }

  def 'should assert that two traits are not equal when one of them is null'() {
    expect:
    testTrait != null
  }

  def 'should assert that two trait models are not equal when one of them is instance of another trait'() {
    expect:
    testTrait != 5
  }

  def 'all attributes and methods should be visited by trait element visitor'() {
    given:
    def visitor = Mock(TraitElementVisitor)

    when:
    testTrait.accept(visitor)

    then:
    with(testTrait) {
      attributes.each { 1 * visitor.visit(it) }
      methods.each { 1 * visitor.visit(it) }
    }
  }

  def 'should return name composed of package name and trait name'() {
    expect:
    testTrait.fullName == "${testTrait.packageName}.$testTrait.name".toString()
  }

  @Unroll
  def 'should throw an exception when any attribute is neither private not public'() {
    when:
    new Trait.Builder('test.package', 'TestTrait').withAttributes([attribute])

    then:
    thrown IllegalArgumentException

    where:
    attribute << [
        createAttribute('attr1', new SimpleType(String), AccessModifier.PACKAGE_PROTECTED),
        createAttribute('attr2', new SimpleType(String), AccessModifier.PROTECTED)
    ]
  }

  @Unroll
  def 'should throw an exception when any method is neither private not public'() {
    when:
    new Trait.Builder('test.package', 'TestTrait').withMethods([method])

    then:
    thrown IllegalArgumentException

    where:
    method << [
        createMethod('method1', AccessModifier.PACKAGE_PROTECTED),
        createMethod('method2', AccessModifier.PROTECTED)
    ]
  }

  private static Trait createTrait(String packageName, String traitName, List<Annotation> annotations,
                                   List<Trait> inheritedTraits, List<Attribute> attributes, List<Method> methods) {
    return new Trait.Builder(packageName, traitName).withAnnotations(annotations).withAttributes(attributes)
        .withMethods(methods).withInheritedTraits(inheritedTraits).buildTrait()
  }

  private static Trait createEmptyTrait() {
    return new Trait.Builder('org.test', 'EmptyTrait').buildTrait()
  }
}
