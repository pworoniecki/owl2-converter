package owl2converter.groovymetamodel.method

import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.VoidType
import test.groovymetamodel.AttributeTestFactory
import spock.lang.Specification

class SetterTest extends Specification {

  def 'should set name during initialization'() {
    given:
    def attributeName = 'testAttribute'
    def attribute = AttributeTestFactory.createAttribute(attributeName)

    when:
    def setter = new Setter.Builder(attribute).buildMethod()

    then:
    setter.name == "set${attributeName.capitalize()}".toString()
  }

  def 'should set parameters during initialization'() {
    given:
    def attributeName = 'testAttribute'
    def attributeType = new SimpleType(Integer)
    def attribute = AttributeTestFactory.createAttribute(attributeName, attributeType)

    when:
    def setter = new Setter.Builder(attribute).buildMethod()

    then:
    setter.parameters.size() == 1
    def parameter = setter.parameters.first()
    parameter.name == attributeName
    parameter.type == attributeType
  }

  def 'should set return type during initialization to void'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    def setter = new Setter.Builder(attribute).buildMethod()

    then:
    setter.returnType == VoidType.INSTANCE
  }

  def 'should not allow to set parameters'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    new Setter.Builder(attribute).withParameters([])

    then:
    thrown UnsupportedOperationException
  }

  def 'should not allow to set return type'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    new Setter.Builder(attribute).withReturnType(VoidType.INSTANCE)

    then:
    thrown UnsupportedOperationException
  }

  def 'should return single parameter'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    def setter = new Setter.Builder(attribute).buildMethod()

    then:
    setter.parameter == setter.parameters.first()
  }
}
