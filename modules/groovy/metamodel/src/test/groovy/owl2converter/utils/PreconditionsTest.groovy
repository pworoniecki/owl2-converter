package owl2converter.utils

import spock.lang.Specification

class PreconditionsTest extends Specification {

  def 'should throw an exception when there is at least one null element in collection'() {
    when:
    Preconditions.assertNoNullInCollection(collection)

    then:
    thrown IllegalArgumentException

    where:
    collection << [
        [1, 2, null, 3],
        [1, 2, null, 3, null, 4]
    ]
  }

  def 'should not throw any exception when there is no null element in collection'() {
    given:
    def collection = [1, 2, 3]

    when:
    Preconditions.assertNoNullInCollection(collection)

    then:
    noExceptionThrown()
  }

  def 'should not throw any exception when collection is empty'() {
    given:
    def collection = []

    when:
    Preconditions.assertNoNullInCollection(collection)

    then:
    noExceptionThrown()
  }
}
