package owl2converter.owl2groovy.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class ValidationException extends Exception {
}
