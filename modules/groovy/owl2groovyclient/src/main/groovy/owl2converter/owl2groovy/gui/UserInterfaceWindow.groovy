package owl2converter.owl2groovy.gui

import commoncodemetamodel.Element
import commoncodemetamodel.tools.CommonCodeMetamodelWriter
import commoncodemetamodel.tools.exception.FileWriteException
import owl2converter.owl2groovy.exception.ValidationException
import owl2tocode.OWL2ToCodeTranslationEngine

import javax.swing.*
import javax.swing.filechooser.FileNameExtensionFilter
import java.awt.*
import java.util.List

class UserInterfaceWindow extends JFrame {

  private static final int ROWS = 2
  private static final int COLUMNS = 1

  private JPanel mainPanel

  // file locations panel section
  private JPanel fileLocationsPanel
  private JTextField ontologyFileLocationTextField
  private JButton chooseOntologyFileButton
  private JTextField codeDestinationDirectoryTextField
  private JButton chooseCodeDestinationDirectoryButton

  // code generation section
  private JButton generateCodeButton
  private JLabel statusLabel

  private File chosenOntologyFile
  private File chosenCodeDestinationDirectory
  private OWL2ToCodeTranslationEngine translationEngine
  private CommonCodeMetamodelWriter codeMetamodelWriter

  UserInterfaceWindow(OWL2ToCodeTranslationEngine translationEngine, CommonCodeMetamodelWriter codeMetamodelWriter) {
    super('OWL2 to Groovy converter')
    this.translationEngine = translationEngine
    this.codeMetamodelWriter = codeMetamodelWriter
    initializeFrame()
  }

  private void initializeFrame() {
    this.mainPanel = new JPanel(new GridLayout(ROWS, COLUMNS))
    this.mainPanel.border = BorderFactory.createEmptyBorder(10, 10, 10, 10)
    setContentPane(mainPanel)
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
    setResizable(false)
    setSize(800, 200)

    initializeComponents()
    pack()
    setButtonActions()
    setVisible(true)
  }

  private void initializeComponents() {
    initializeFileLocationsPanel()
    initializeCodeGenerationSection()
  }

  private void initializeFileLocationsPanel() {
    def layout = new GridLayout(2, 3)
    layout.with {
      vgap = 10
      hgap = 10
    }
    this.fileLocationsPanel = new JPanel(layout)
    this.mainPanel.add(this.fileLocationsPanel)

    initializeOntologyFileSection()
    initializeCodeDestinationSection()
  }

  private void initializeOntologyFileSection() {
    this.ontologyFileLocationTextField = createNotEditableTextField()
    this.chooseOntologyFileButton = new JButton('Choose file')
    this.fileLocationsPanel.with {
      add(new JLabel('OWL ontology file'))
      add(this.ontologyFileLocationTextField)
      add(this.chooseOntologyFileButton)
    }
  }

  private static JTextField createNotEditableTextField() {
    return new JTextField(editable: false, focusable: false)
  }

  private void initializeCodeDestinationSection() {
    this.codeDestinationDirectoryTextField = createNotEditableTextField()
    this.chooseCodeDestinationDirectoryButton = new JButton('Choose directory')
    this.fileLocationsPanel.with {
      add(new JLabel('Generated source code destination'))
      add(this.codeDestinationDirectoryTextField)
      add(this.chooseCodeDestinationDirectoryButton)
    }
  }

  private void initializeCodeGenerationSection() {
    def panel = new JPanel(layout: new GridBagLayout())
    this.mainPanel.add(panel)
    initializeCodeGenerationButton(panel)
    initializeCodeGenerationStatus(panel)
  }

  private void initializeCodeGenerationButton(JPanel panel) {
    this.generateCodeButton = new JButton('Generate code')
    panel.add(this.generateCodeButton, new GridBagConstraints(gridy: 0, ipadx: 100))
  }

  private void initializeCodeGenerationStatus(JPanel panel) {
    this.statusLabel = new JLabel('Choose ontology and destination directory for generated source code. Then click Generate code.')
    def constraints = new GridBagConstraints(gridy: 1, insets: new Insets(5, 0, 0, 0))
    panel.add(this.statusLabel, constraints)
  }

  private void setButtonActions() {
    chooseOntologyFileButton.addActionListener { chooseOntologyFile() }
    chooseCodeDestinationDirectoryButton.addActionListener { chooseCodeDestinationDirectory() }
    generateCodeButton.addActionListener { generateCode() }
  }

  private void chooseOntologyFile() {
    def fileFilter = new FileNameExtensionFilter('OWL ontology file', 'owl', 'rdf')
    def fileChooser = new JFileChooser(fileFilter: fileFilter, approveButtonText: 'Choose selected file')
    int fileChooserReturnValue = fileChooser.showOpenDialog(this)

    if (fileChooserReturnValue == JFileChooser.APPROVE_OPTION) {
      this.chosenOntologyFile = fileChooser.selectedFile
      this.ontologyFileLocationTextField.text = this.chosenOntologyFile.absolutePath
    }
  }

  private void chooseCodeDestinationDirectory() {
    def directoryChooser = new JFileChooser(fileSelectionMode: JFileChooser.DIRECTORIES_ONLY,
        approveButtonText: 'Choose selected directory')
    int directoryChooserReturnValue = directoryChooser.showOpenDialog(this)

    if (directoryChooserReturnValue == JFileChooser.APPROVE_OPTION) {
      this.chosenCodeDestinationDirectory = directoryChooser.selectedFile
      this.codeDestinationDirectoryTextField.text = this.chosenCodeDestinationDirectory.absolutePath
    }
  }

  private void generateCode() {
    disableButtons()
    try {
      validateBeforeGeneratingCode()
    } catch (ValidationException e) {
      showErrorMessage(e.message)
      enableButtons()
      return
    }
    runCodeGeneration()
  }

  private void runCodeGeneration() {
    def codeGenerationTask = {
      this.statusLabel.text = 'Please wait. Ontology is being analyzed...'
      Optional<Set<Element>> sourceCode = convertChosenOntologyToCode()
      if (!sourceCode.isPresent()) {
        return
      }
      this.statusLabel.text = 'Ontology analyze completed. Please wait, Groovy source code is being generated...'
      TaskStatus codeFilesGenerationStatus = generateCodeFiles(sourceCode.get())
      if (codeFilesGenerationStatus == TaskStatus.ERROR) {
        return
      }
      this.statusLabel.text = 'Source code has been generated. ' +
          "It has been saved in directory: ${this.chosenCodeDestinationDirectory.absolutePath}"
      showCodeGenerationCompletedMessage()
      enableButtons()
    }
    new Thread(codeGenerationTask).start()
  }

  private Optional<Set<Element>> convertChosenOntologyToCode() {
    try {
      List<Set<Element>> ontologyTranslations = translationEngine.translate(this.chosenOntologyFile)
      if (ontologyTranslations.size() > 1) {
        return Optional.empty()
      }
      return Optional.of(ontologyTranslations.first())
    } catch (Exception e) {
      e.printStackTrace()
      handleGenerationCodeError('An error occurred during ontology analyze. Please ensure ' +
          'it is provided in correct format (OWL/XML - e.g. generated in Protégé).')
      return Optional.empty()
    }
  }

  private void handleGenerationCodeError(String errorMessage) {
    showErrorMessage(errorMessage)
    this.statusLabel.text = errorMessage
    enableButtons()
  }

  private TaskStatus generateCodeFiles(Set<Element> codeMetamodel) {
    TaskStatus status = TaskStatus.SUCCESS
    try {
      codeMetamodelWriter.write(chosenCodeDestinationDirectory.toPath(), codeMetamodel)
    } catch (FileWriteException e) {
      e.printStackTrace()
      handleGenerationCodeError("Cannot save generated source in destination directory (${e.message}). " +
          'Please ensure if you have permissions to write files in the directory.')
      status = TaskStatus.ERROR
    } catch (Exception e) {
      e.printStackTrace()
      handleGenerationCodeError('Unexpected error occurred. Please try again or report this bug.')
      status = TaskStatus.ERROR
    }
    return status
  }

  private void disableButtons() {
    this.generateCodeButton.enabled = false
    this.chooseOntologyFileButton.enabled = false
    this.chooseCodeDestinationDirectoryButton.enabled = false
  }

  private void validateBeforeGeneratingCode() throws ValidationException {
    if (!this.chosenOntologyFile) {
      throw new ValidationException('OWL file was not chosen. Choose it before generating source code.')
    }
    if (!this.chosenOntologyFile.exists()) {
      throw new ValidationException('Chosen OWL file does not exist or you do not have permissions to read the file. ' +
          'Choose correct file.')
    }
    if (!this.chosenCodeDestinationDirectory) {
      throw new ValidationException('You have not selected destination directory. Choose it before generating source code.')
    }
  }

  private void showErrorMessage(String errorMessage) {
    JOptionPane.showMessageDialog(this, errorMessage, 'Cannot generate source code', JOptionPane.ERROR_MESSAGE)
  }

  private void enableButtons() {
    this.generateCodeButton.enabled = true
    this.chooseOntologyFileButton.enabled = true
    this.chooseCodeDestinationDirectoryButton.enabled = true
  }

  private void showCodeGenerationCompletedMessage() {
    JOptionPane.showMessageDialog(this, 'OWL ontology has been successfully converted to Groovy source code. ' +
        "The code has been saved in directory: ${this.chosenCodeDestinationDirectory.absolutePath}",
        'Code has been successfully generated', JOptionPane.INFORMATION_MESSAGE)
  }
}
