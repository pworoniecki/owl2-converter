package owl2converter.owl2groovy

import commoncodemetamodel.tools.CommonCodeMetamodelWriter
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import owl2converter.owl2groovy.configuration.ApplicationSpringContextConfiguration
import owl2converter.owl2groovy.gui.UserInterfaceWindow
import owl2tocode.OWL2ToCodeTranslationEngine

class ApplicationRunner {

  static void main(String[] args) {
    ApplicationContext applicationContext = initializeSpringContext()
    OWL2ToCodeTranslationEngine translationEngine = applicationContext.getBean(OWL2ToCodeTranslationEngine)
    CommonCodeMetamodelWriter codeMetamodelWriter = applicationContext.getBean(CommonCodeMetamodelWriter)
    new UserInterfaceWindow(translationEngine, codeMetamodelWriter)
  }

  private static ApplicationContext initializeSpringContext() {
    def applicationContext = new AnnotationConfigApplicationContext()
    applicationContext.register(ApplicationSpringContextConfiguration)
    applicationContext.refresh()
    return applicationContext
  }
}
