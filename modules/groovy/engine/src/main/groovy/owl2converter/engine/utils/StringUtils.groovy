package owl2converter.engine.utils

/**
 * Utility class for String manipulation.
 */
class StringUtils {

  /**
   * Adds a single space character for non empty string.
   *
   * @param str the string to be processed
   * @return the {@code str} string with single space added when it was not empty; an empty string otherwise
   */
  static String emptyOrAddSpace(String str) {
    return str.isEmpty() ? str : "$str "
  }
}
