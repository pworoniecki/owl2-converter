package owl2converter.engine.generator.method.addvalue

import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.AddValueMethod
import owl2converter.groovymetamodel.method.DirectSetter
import owl2converter.groovymetamodel.type.ListType
import org.springframework.stereotype.Component

/**
 * A code generation strategy class supporting add value methods of inverse attributes.
 */
@Component
class InverseAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  /**
   * Checks whether the generator can be used for the method.
   *
   * @param method a method for which the code is to be generated
   * @return true if the method is defined for an inverse attribute
   */
  @Override
  boolean supports(AddValueMethod method) {
    return method.attribute.isInverseAttribute()
  }

  /**
   * Returns the string representing validation part of code.
   *
   * @param method a method for which the code is to be generated
   * @return no code (empty {@link Optional})
   */
  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    return Optional.empty()
  }

  /**
   * Returns the string representing specific part of method's code (calls the direct setter of the inverse attribute).
   *
   * @param method a method for which the code is to be generated
   * @return a string with specific method code
   */
  @Override
  Optional<String> generateSpecificCode(AddValueMethod method) {
    List<Attribute> inverseAttributes = method.attribute.findInverseAttributes()
    return Optional.of(generateCode(addSelfToAttributes(inverseAttributes, method.parameter)))
  }

  private static List<String> addSelfToAttributes(List<Attribute> attributes, Parameter methodParameter) {
    return attributes.collect { addSelfToAttribute(it, methodParameter) }
  }

  private static String addSelfToAttribute(Attribute attribute, Parameter methodParameter) {
    if (attribute.valueType instanceof ListType) {
      return "${methodParameter.name}.add${attribute.name.capitalize()}(this)"
    }
    return "${methodParameter.name}.${DirectSetter.getName(attribute)}(this)"
  }

  /**
   * Returns the relative order of generator within the group
   *
   * @return 8
   */
  @Override
  int getOrderWithinPriorityGroup() {
    return 8
  }
}
