package owl2converter.engine.generator.method

import owl2converter.groovymetamodel.method.Method

/**
 * A trait with abstract methods representing trait's code generation strategy for a method
 */
trait MethodBodyCodeGeneratorStrategy<T extends Method> {

  /**
   * Checks whether the class supports generation of code for given {@link T} instance.
   *
   * @param method a method for which the code is to be generated
   * @return true if the class implementing the trait supports {@code method} generation
   */
  abstract boolean supports(T method)

  /**
   * Returns the validation part of generated code for given {@link T} instance.
   *
   * @param method a method for which the code is to be generated
   * @return a string with validation code to be used in the method (if any)
   */
  abstract Optional<String> generateValidationCode(T method)

  /**
   * Returns the specific part of generated code for given {@link T} instance.
   *
   * @param method a method for which the code is to be generated
   * @return a string with specific add value code (if any)
   */
  abstract Optional<String> generateSpecificCode(T method)

  /**
   * Converts list of source code lines into single multi-line text
   *
   * @param codeLines source code lines collected in a list
   * @return the result source code text
   */
  String generateCode(List<String> codeLines) {
    return codeLines.join('\n')
  }

  /**
   * Returns the priority of the specific strategy. Lower value means higher priority.
   *
   * @return the priority of the specific strategy ({@link Integer#MAX_VALUE} by default).
   */
  int getPriority() {
    return Integer.MAX_VALUE
  }

  /**
   * Returns the priority of the specific strategy in group. Lower value means higher priority.
   *
   * @return the priority of the specific strategy within a group ({@link Integer#MAX_VALUE} by default).
   */
  int getOrderWithinPriorityGroup() {
    return Integer.MAX_VALUE
  }
}
