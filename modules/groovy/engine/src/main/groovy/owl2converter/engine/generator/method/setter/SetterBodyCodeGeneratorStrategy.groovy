package owl2converter.engine.generator.method.setter

import owl2converter.engine.generator.method.MethodBodyCodeGeneratorStrategy
import owl2converter.groovymetamodel.method.Setter

/**
 * A trait with abstract methods representing trait's code generation strategy for setter
 */
trait SetterBodyCodeGeneratorStrategy extends MethodBodyCodeGeneratorStrategy<Setter> {
}
