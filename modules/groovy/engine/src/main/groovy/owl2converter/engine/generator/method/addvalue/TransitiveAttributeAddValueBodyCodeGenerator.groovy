package owl2converter.engine.generator.method.addvalue

import owl2converter.groovymetamodel.method.AddValueMethod
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE

/**
 * A code generation strategy class supporting add value methods of transitive attributes.
 */
@Component
class TransitiveAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  /**
   * Checks whether the generator can be used for the method.
   *
   * @param method a method for which the code is to be generated
   * @return true if the method is defined for a transitive attribute
   */
  @Override
  boolean supports(AddValueMethod method) {
    return TRANSITIVE in method.attribute.characteristics
  }

  /**
   * Returns the string representing validation part of code
   *
   * @param method a method for which the code is to be generated
   * @return no code (empty {@link Optional})
   */
  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    return Optional.empty()
  }

  /**
   * Returns the string representing specific part of method's code
   *
   * @param method a method for which the code is to be generated
   * @return no code (empty {@link Optional})
   */
  @Override
  Optional<String> generateSpecificCode(AddValueMethod method) {
    return Optional.empty()
  }

  /**
   * Returns the relative order of generator within the group
   *
   * @return 4
   */
  @Override
  int getOrderWithinPriorityGroup() {
    return 4
  }
}
