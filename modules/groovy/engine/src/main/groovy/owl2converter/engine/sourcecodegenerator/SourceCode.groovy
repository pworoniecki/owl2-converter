package owl2converter.engine.sourcecodegenerator

interface SourceCode {

  /**
   * Returns name of the element for which the source code is stored (e.g. class or trait).
   *
   * @return element's name
   */
  String getName()

  /**
   * Returns source code of an element (e.g. class or trait)
   *
   * @return element's source code
   */
  String getSourceCode()
}