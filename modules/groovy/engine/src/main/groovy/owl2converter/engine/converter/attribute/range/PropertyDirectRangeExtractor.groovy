package owl2converter.engine.converter.attribute.range

import org.semanticweb.owlapi.model.AxiomType
import org.semanticweb.owlapi.model.DataRangeType
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom
import org.semanticweb.owlapi.model.OWLDataRange
import org.semanticweb.owlapi.model.OWLDatatype
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.slf4j.Logger

/**
 * A class responsible for finding information about OWL property range in an OWL ontology using OWL property range axiom.
 * Information about the range is returned as an instance of {@link OWLClass}.
 */
@Component
class PropertyDirectRangeExtractor {

  private Logger logger

  /**
   * Class constructor.
   *
   * @param logger a logger to write warnings about not supported ontology features
   */
  @Autowired
  PropertyDirectRangeExtractor(Logger logger) {
    this.logger = logger
  }

  /**
   * Extracts range of OWL object property by scanning OWL object property range axioms and returns it as an instance of {@link OWLClass}.
   * Writes warnings to the log if range extraction was impossible.
   *
   * @param property an OWL object property the range is to be found
   * @param ontology an OWL ontology in which the property is defined
   * @return {@link OWLClass} instance wrapped by {@link Optional} being the representation of property range if the range has been defined in the ontology,
   * {@link Optional#EMPTY} otherwise
   */
  // TODO support more than 1 range declarations - it means intersections of classes
  // TODO support other types of range declarations, e.g. union of classes
  Optional<OWLClass> extractSingleDirectRange(OWLObjectProperty property, OWLOntology ontology) {
    List<OWLClassExpression> rangeDeclarations = ontology.axioms(property).findAll { it.axiomType == AxiomType.OBJECT_PROPERTY_RANGE }
        .collect { (it as OWLObjectPropertyRangeAxiom).range } as List<OWLClassExpression>
    if (rangeDeclarations.isEmpty()) {
      return Optional.empty()
    }
    if (rangeDeclarations.size() > 1) {
      logger.warn("More than 1 range declarations found for property $property but only 1 is supported at the time. " +
          "Range will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found range declarations: $rangeDeclarations")
      return Optional.empty()
    }
    if (rangeDeclarations.first() && !(rangeDeclarations.first() instanceof OWLClass)) {
      logger.warn("Unsupported type of range declaration found for property $property. " +
          "Range will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found range declaration: ${rangeDeclarations.first()}")
      return Optional.empty()
    }
    return Optional.ofNullable(rangeDeclarations.first() as OWLClass)
  }

  /**
   * Extracts range of OWL data property by scanning OWL object data range axioms and returns it as an instance of {@link OWLDatatype}.
   * Writes warnings to the log if range extraction was impossible.
   *
   * @param property an OWL data property the range is to be found
   * @param ontology an OWL ontology in which the property is defined
   * @return {@link OWLDatatype} instance wrapped by {@link Optional} being the representation of property range if the range has been defined in the ontology,
   * {@link Optional#EMPTY} otherwise
   */
  Optional<OWLDatatype> extractSingleDirectRange(OWLDataProperty property, OWLOntology ontology) {
    List<OWLDataRange> rangeDeclarations = ontology.axioms(property).findAll { it.axiomType == AxiomType.DATA_PROPERTY_RANGE }
        .collect { (it as OWLDataPropertyRangeAxiom).range } as List<OWLDataRange>
    if (rangeDeclarations.isEmpty()) {
      return Optional.empty()
    }
    if (rangeDeclarations.size() > 1) {
      logger.warn("More than 1 range declarations found for property $property but only 1 is supported at the time. " +
          "Range will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found range declarations: $rangeDeclarations")
      return Optional.empty()
    }
    if (rangeDeclarations.first() && rangeDeclarations.first().dataRangeType != DataRangeType.DATATYPE) {
      logger.warn("Unsupported type of range declaration found for property $property. " +
          "Range will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found range declaration: ${rangeDeclarations.first()}")
      return Optional.empty()
    }
    return Optional.ofNullable(rangeDeclarations.first().asOWLDatatype())
  }
}
