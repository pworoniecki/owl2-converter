package owl2converter.engine.generator

import owl2converter.groovymetamodel.Trait
import org.springframework.stereotype.Component

/**
 * Signature generator of the trait.
 */
@Component
class TraitSignatureCodeGenerator {

  /**
   * Generates the signature (the first line) of the trait (i.e. 'trait ... [implements ...] {').
   *
   * @param traitModel a trait the signature is to be generated
   * @return the string with trait signature
   */
  String generateTraitSignature(Trait traitModel) {
    def implementedTraitsSignaturePart = traitModel.inheritedTraits.any() ?
        " implements ${traitModel.inheritedTraits*.name.join(', ')}" : ''
    return "trait ${traitModel.name}" + implementedTraitsSignaturePart + ' {'
  }
}
