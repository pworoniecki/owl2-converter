package owl2converter.engine.converter.trait

import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.UnionTrait
import owl2converter.groovymetamodel.attribute.Attribute
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.stereotype.Component
import owl2converter.engine.converter.attribute.PredefinedTypes

/**
 * A class responsible for information extraction about classes from OWL ontology and for their mapping into Traits being part of Groovy meta-model.
 */
@Component
class BaseTraitsExtractor {

  private SimpleBaseTraitExtractor simpleBaseTraitsExtractor
  private UnionSubclassesExtractor unionSubclassesExtractor
  private Trait thingTrait

  /**
   * Class constructor.
   *
   * @param simpleBaseTraitsExtractor
   * @param unionSubclassesExtractor
   * @param predefinedTypes
   */
  BaseTraitsExtractor(SimpleBaseTraitExtractor simpleBaseTraitsExtractor, UnionSubclassesExtractor unionSubclassesExtractor,
                      PredefinedTypes predefinedTypes) {
    thingTrait = predefinedTypes.thingTrait
    this.unionSubclassesExtractor = unionSubclassesExtractor
    this.simpleBaseTraitsExtractor = simpleBaseTraitsExtractor
  }

  /**
   * Extracts information about classes from OWL ontology and maps them into Traits from Groovy meta-model.
   *
   * @param ontology an ontology from which OWL classes are extracted
   * @param attributes a mapping between OWL properties and Groovy attribute metamodels
   * @return the map containing a base trait (with potential children traits) for each OWL class
   */
  Map<OWLClass, Trait> extract(OWLOntology ontology, Map<OWLProperty, Attribute> attributes) {
    Map<OWLClass, Trait> traits = ontology.classesInSignature().findAll()
        .collectEntries { [(it): simpleBaseTraitsExtractor.extract(ontology, it as OWLClass, attributes)] }
    Map<OWLClass, UnionTrait> unionTraits = traits.findAll { it.value instanceof UnionTrait } as Map<OWLClass, UnionTrait>
    unionTraits.each { owlClass, unionTrait ->
      unionTrait.addUnionOf(unionSubclassesExtractor.extract(ontology, owlClass).collect { traits[it] })
    }
    traits.values().each { it.inheritedTraits.addAll(extractInheritedTraits(it, traits, unionTraits.values(), ontology)) }
    return traits + [(ontology.getOWLOntologyManager().getOWLDataFactory().getOWLThing()): thingTrait]
  }

  private List<Trait> extractInheritedTraits(Trait baseTrait, Map<OWLClass, Trait> traits,
                                             Collection<UnionTrait> unionTraits, OWLOntology ontology) {
    OWLClass baseOwlClass = traits.find { it.value == baseTrait }.key
    List<Trait> subTraits = findSubclasses(ontology, baseOwlClass).collect { traits[it] }
    List<UnionTrait> ownerUnionTraits = findOwnerUnionTrait(unionTraits, baseTrait)
    return subTraits.isEmpty() && ownerUnionTraits.isEmpty() ? [thingTrait] : subTraits + ownerUnionTraits
  }

  private static List<OWLClass> findSubclasses(OWLOntology ontology, OWLClass owlClass) {
    return ontology.subClassAxiomsForSubClass(owlClass).collect { it.superClass }
        .findAll { it instanceof OWLClass } as List<OWLClass>
  }

  private static List<UnionTrait> findOwnerUnionTrait(Collection<UnionTrait> unionTraits, Trait baseTrait) {
    return unionTraits.findAll { it.isUnionOf(baseTrait) }
  }
}
