package owl2converter.engine.codebuilder

import owl2converter.engine.codebuilder.common.CodeBuilder
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.converter.IndividualsDataSourceClassExtractor
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.type.Type

/**
 * Builder responsible for generation of Groovy class's source code
 */
class ClassCodeBuilder extends CodeBuilder {

  private Class classModel

  /**
   * Class constructor.
   *
   * @param classModel the class meta-model to be translated to the source code.
   */
  ClassCodeBuilder(Class classModel) {
    this.classModel = classModel
  }

  /**
   * Appends the "}" to the class's source code.
   */
  void appendClassSignatureEnd() {
    appendCode('}')
  }

  /**
   * Appends the package declaration of the class to be translated.
   */
  void appendPackage() {
    appendCode("package ${classModel.packageName}")
  }

  /**
   * Appends all imports required by class definition, caused by parent classes, implemented traits, attributes, methods, annotations etc.
   */
  @Override
  void appendSpecificImports(GeneratorConfiguration generatorConfiguration) {
    if (classModel.name == IndividualsDataSourceClassExtractor.GENERATED_CLASS_NAME) {
      appendPackageImport(generatorConfiguration.generatedClasses.modelFullPackage)
      appendPackageImport("${generatorConfiguration.generatedClasses.basePackage}.individual")
    }
    appendInheritedClassImportIfNecessary()
    appendImplementedTraitImports(classModel.implementedTraits, classModel.packageName)
    appendConstructorParameterImports(classModel.constructors)
    appendMethodParameterImports(classModel.methods)
    appendMethodReturnTypeImports(classModel.methods)
    appendAttributeTypeImports(classModel.attributes)
    appendAnnotationTypeImports(classModel.annotations)
  }

  private void appendInheritedClassImportIfNecessary() {
    classModel.inheritedClass.ifPresent({ inheritedClass ->
      if (inheritedClass.packageName != classModel.packageName) {
        appendClassImport(inheritedClass.fullName)
      }
    })
  }

  private void appendConstructorParameterImports(List<Constructor> constructors) {
    List<Type> uniqueParameterTypes = constructors*.parameters*.type.flatten().unique() as List<Type>
    appendImports(uniqueParameterTypes, getOwnerPackageName())
  }

  /**
   * Appends static constructors to the class's source code where constructors are separated by one empty line.
   *
   * @param constructorsToCode the map containing source code for each constructor
   */
  void appendStaticConstructorsWithEmptyLines(Map<Constructor, String> constructorsToCode) {
    constructorsToCode.each { constructor, code ->
      if (constructor.isStatic()) {
        appendCode(code)
        appendEmptyLine()
      }
    }
  }

  /**
   * Appends non-static constructors to the class's source code where constructors are separated by one empty line.
   *
   * @param constructorsToCode the map containing source code for each constructor
   */
  void appendNonStaticConstructorsWithEmptyLines(Map<Constructor, String> constructorsToCode) {
    constructorsToCode.each { constructor, code ->
      if (!constructor.isStatic()) {
        appendCode(code)
        appendEmptyLine()
      }
    }
  }

  /**
   * Returns the name of the package the class is defined in.
   *
   * @return the name of the package
   */
  @Override
  String getOwnerPackageName() {
    return classModel.packageName
  }
}
