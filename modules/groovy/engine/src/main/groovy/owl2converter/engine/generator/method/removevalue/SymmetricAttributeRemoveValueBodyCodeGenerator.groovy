package owl2converter.engine.generator.method.removevalue

import owl2converter.groovymetamodel.method.RemoveValueMethod
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.SYMMETRIC

@Component
class SymmetricAttributeRemoveValueBodyCodeGenerator implements RemoveValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(RemoveValueMethod method) {
    return SYMMETRIC in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificCode(RemoveValueMethod method) {
    return Optional.of("${method.parameter.name}.remove${method.attribute.name.capitalize()}(this)")
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 7
  }
}
