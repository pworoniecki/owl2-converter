package owl2converter.engine.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class MissingIRIRemainderException extends Exception {
}
