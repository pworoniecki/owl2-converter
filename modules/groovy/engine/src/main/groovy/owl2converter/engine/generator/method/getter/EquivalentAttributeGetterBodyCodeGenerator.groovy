package owl2converter.engine.generator.method.getter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Getter
import org.springframework.stereotype.Component

@Component
class EquivalentAttributeGetterBodyCodeGenerator implements GetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Getter getter) {
    return getter.attribute.isEquivalentOwnedAttribute()
  }

  @Override
  String generateCode(Getter getter) {
    Attribute equivalentOwnerAttribute = getter.attribute.findAnyEquivalentAttribute().get()
    return "return get${equivalentOwnerAttribute.name.capitalize()}()"
  }

  @Override
  int getPriority() {
    return 1
  }
}
