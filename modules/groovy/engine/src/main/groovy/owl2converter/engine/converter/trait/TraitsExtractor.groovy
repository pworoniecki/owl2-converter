package owl2converter.engine.converter.trait

import owl2converter.groovymetamodel.EquivalentTrait
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.UnionTrait
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.SetType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.stereotype.Component
import owl2converter.engine.converter.TraitInstanceMethodsFactory

import static owl2converter.groovymetamodel.AccessModifier.PRIVATE

/**
 * Class responsible for mapping OWL classes from an OWL ontology to Trait Groovy metamodel instances
 */
@Component
class TraitsExtractor {

  static final String TRAIT_NAME_SUFFIX = 'Trait'
  static final String ALL_INSTANCES_ATTRIBUTE_NAME = 'allInstances'

  private BaseTraitsExtractor baseTraitsExtractor
  private TraitInstanceMethodsFactory traitInstanceMethodsFactory
  private EquivalentTraitsExtractor equivalentTraitsExtractor

  /**
   * Class constructor.
   *
   * @param baseTraitsExtractor
   * @param traitInstanceMethodsFactory
   * @param equivalentTraitsExtractor
   */
  TraitsExtractor(BaseTraitsExtractor baseTraitsExtractor, TraitInstanceMethodsFactory traitInstanceMethodsFactory,
                  EquivalentTraitsExtractor equivalentTraitsExtractor) {
    this.baseTraitsExtractor = baseTraitsExtractor
    this.traitInstanceMethodsFactory = traitInstanceMethodsFactory
    this.equivalentTraitsExtractor = equivalentTraitsExtractor
  }

  /**
   * Map an OWL class to a list of traits representing the class (being its Groovy equivalent) have to implement.
   *
   * @param ontology an OWL ontology being source of OWL classes information
   * @param attributes a mapping between OWL properties and their Groovy meta-model attributes representation
   * @return the mapping between all OWL classes read from the ontology to the list of traits created for the class
   * (containing trait created directly for the class and optionally trait created for its equivalent class - if there is such a class)
   */
  Map<OWLClass, List<Trait>> extract(OWLOntology ontology, Map<OWLProperty, Attribute> attributes) {
    Map<OWLClass, Trait> baseTraits = baseTraitsExtractor.extract(ontology, attributes)
    Map<OWLClass, Optional<EquivalentTrait>> equivalentTraits = equivalentTraitsExtractor.extract(baseTraits, ontology)
    addAllInstancesAttributeToTraits(baseTraits, equivalentTraits)
    addInstanceMethodsToBaseTraits(baseTraits.values(), equivalentTraits.values().findAll { it.isPresent() }*.get())
    return baseTraits.collectEntries {
      [(it.key): extractTraitsForOwlClass(it.key, baseTraits, equivalentTraits)]
    } as Map<OWLClass, List<Trait>>
  }

  private static Map<OWLClass, Trait> addAllInstancesAttributeToTraits(
      Map<OWLClass, Trait> baseTraits, Map<OWLClass, Optional<EquivalentTrait>> equivalentTraits) {
    return baseTraits.findAll { owlClass, traitModel -> !(traitModel instanceof UnionTrait) }.each { owlClass, traitModel ->
      traitModel.attributes.add(0, createAllClassInstancesAttribute(traitModel, equivalentTraits[owlClass]))
    }
  }

  private static Attribute createAllClassInstancesAttribute(Trait traitModel, Optional<EquivalentTrait> equivalentTrait) {
    Trait attributeTypeTrait = equivalentTrait.map { it as Trait }.orElse(traitModel)
    Type type = new SetType(new SimpleType(attributeTypeTrait))
    return new Attribute.AttributeBuilder(ALL_INSTANCES_ATTRIBUTE_NAME, type).asStatic().withAccessModifier(PRIVATE)
        .withDefaultValue('[].toSet()').buildAttribute()
  }

  private void addInstanceMethodsToBaseTraits(Collection<Trait> baseTraits, List<EquivalentTrait> equivalentTraits) {
    baseTraits.each { it.methods.addAll(traitInstanceMethodsFactory.create(it, equivalentTraits)) }
  }

  private static List<Trait> extractTraitsForOwlClass(OWLClass owlClass, Map<OWLClass, Trait> baseTraits,
                                                      Map<OWLClass, Optional<EquivalentTrait>> equivalentTraits) {
    Trait classTrait = baseTraits[owlClass]
    Optional<Trait> equivalentTrait = equivalentTraits[owlClass]
    return equivalentTrait.isPresent() ? [equivalentTrait.get(), classTrait] : [classTrait]
  }
}
