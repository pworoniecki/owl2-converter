package owl2converter.engine.generator.method.removevalue

import owl2converter.groovymetamodel.method.RemoveValueMethod
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.IRREFLEXIVE

@Component
class IrreflexiveAttributeRemoveValueBodyCodeGenerator implements RemoveValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(RemoveValueMethod method) {
    return IRREFLEXIVE in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 2
  }
}
