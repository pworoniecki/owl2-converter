package owl2converter.engine.converter.attribute

import org.slf4j.Logger
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.relation.AttributeRelation
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * A class which translates OWL properties (data and object) into Groovy meta-model attributes.
 * The result of translation is returned as a map which keys are OWL properties, and values are Attributes.
 */
@Component
class AttributesExtractor {

  private Logger logger
  private PropertyToAttributeConverter propertyToAttributeConverter
  private AttributeRelationsExtractor relationsExtractor

  /**
   * Class constructor. Requires objects necessary to perform attribute extraction process.
   *
   * @param logger a logger to write warnings about not transformed elements
   * @param propertyToAttributeConverter
   * @param relationsExtractor
   */
  @Autowired
  AttributesExtractor(Logger logger, PropertyToAttributeConverter propertyToAttributeConverter,
                      AttributeRelationsExtractor relationsExtractor) {
    this.logger = logger
    this.propertyToAttributeConverter = propertyToAttributeConverter
    this.relationsExtractor = relationsExtractor
  }

  /**
   * Extracts object and data properties from an ontology and maps them into instances of {@link Attribute} class from Groovy meta-model
   *
   * @param ontology an ontology from which attributes will be extracted
   * @return a mapping between extracted OWL properties and their {@link Attribute} instance equivalent
   */
  Map<OWLProperty, Attribute> extract(OWLOntology ontology) {
    return (extractObjectPropertyAttributes(ontology) as Map<OWLProperty, Attribute>) +
        (extractDataPropertyAttributes(ontology) as Map<OWLProperty, Attribute>)
  }

  private Map<OWLObjectProperty, Attribute> extractObjectPropertyAttributes(OWLOntology ontology) {
    List<OWLObjectProperty> objectPropertiesDefinedInOntology = extractObjectPropertiesOfOntology(ontology)
    List<OWLObjectProperty> objectPropertiesDefinedOutsideOntology = ontology.objectPropertiesInSignature().findAll() -
        objectPropertiesDefinedInOntology
    warnAboutPropertiesFromOtherOntologies(objectPropertiesDefinedOutsideOntology)
    Map<OWLObjectProperty, Attribute> propertyToAttribute = objectPropertiesDefinedInOntology
        .collectEntries { [(it): propertyToAttributeConverter.convertWithoutRelations(it, ontology)] }
    propertyToAttribute.each { property, attribute -> addRelations(property, attribute, propertyToAttribute, ontology) }
    return propertyToAttribute
  }

  private static List<OWLObjectProperty> extractObjectPropertiesOfOntology(OWLOntology ontology) {
    return ontology.objectPropertiesInSignature().findAll {
      it.getIRI().namespace == ontology.ontologyID.ontologyIRI.get().toString() + '#'
    }
  }

  private void warnAboutPropertiesFromOtherOntologies(List<OWLProperty> properties) {
    properties.each {
      logger.warn("Property '$it' is from another ontology. " +
          "Such properties are not yet supported so this property will not be parsed.")
    }
  }

  private void addRelations(OWLObjectProperty property, Attribute attribute, Map<OWLObjectProperty, Attribute> attributes,
                            OWLOntology ontology) {
    List<AttributeRelation> attributeRelations = relationsExtractor.extract(property, ontology, attributes)
    attribute.relations.addAll(attributeRelations)
  }

  private Map<OWLDataProperty, Attribute> extractDataPropertyAttributes(OWLOntology ontology) {
    List<OWLDataProperty> dataPropertiesDefinedInOntology = extractDataPropertiesOfOntology(ontology)
    List<OWLDataProperty> dataPropertiesDefinedOutsideOntology = ontology.dataPropertiesInSignature().findAll() -
        dataPropertiesDefinedInOntology
    warnAboutPropertiesFromOtherOntologies(dataPropertiesDefinedOutsideOntology)
    Map<OWLDataProperty, Attribute> propertyToAttribute = dataPropertiesDefinedInOntology
        .collectEntries { [(it): propertyToAttributeConverter.convertWithoutRelations(it, ontology)] }
    propertyToAttribute.each { property, attribute -> addRelations(property, attribute, propertyToAttribute, ontology) }
    return propertyToAttribute
  }

  private static List<OWLDataProperty> extractDataPropertiesOfOntology(OWLOntology ontology) {
    return ontology.dataPropertiesInSignature().findAll {
      it.getIRI().namespace == ontology.ontologyID.ontologyIRI.get().toString() + '#'
    }
  }

  private void addRelations(OWLDataProperty property, Attribute attribute, Map<OWLDataProperty, Attribute> attributes,
                            OWLOntology ontology) {
    List<AttributeRelation> attributeRelations = relationsExtractor.extract(property, ontology, attributes)
    attribute.relations.addAll(attributeRelations)
  }
}
