package owl2converter.engine.generator.constructor

import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor

/**
 * An interface to be implemented by constructor body generators.
 */
interface ConstructorBodyCodeGenerator {

  /**
   * Indicates if class is supported by generator (i.e. generator can generate code for constructor of the class)
   *
   * @param ownerClass
   * @return true if generator supports given class, false otherwise
   */
  boolean supports(Class ownerClass)

  /**
   * Returns the generated string with constructor body.
   *
   * @param constructor an constructor for which the body is to be generated
   * @return the string with constructor body (without the signature and enclosing brackets { body })
   */
  String generateCode(Constructor constructor)
}
