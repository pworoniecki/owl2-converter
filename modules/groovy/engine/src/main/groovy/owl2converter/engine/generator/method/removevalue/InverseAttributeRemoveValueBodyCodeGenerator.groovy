package owl2converter.engine.generator.method.removevalue

import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.DirectSetter
import owl2converter.groovymetamodel.method.RemoveValueMethod
import owl2converter.groovymetamodel.type.ListType
import org.springframework.stereotype.Component

@Component
class InverseAttributeRemoveValueBodyCodeGenerator implements RemoveValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(RemoveValueMethod method) {
    return method.attribute.isInverseAttribute()
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificCode(RemoveValueMethod method) {
    List<Attribute> inverseAttributes = method.attribute.findInverseAttributes()
    return Optional.of(generateCode(removeSelfFromAttributes(inverseAttributes, method.parameter)))
  }

  private static List<String> removeSelfFromAttributes(List<Attribute> attributes, Parameter methodParameter) {
    return attributes.collect { return removeSelfFromAttribute(it, methodParameter) }
  }

  private static String removeSelfFromAttribute(Attribute attribute, Parameter methodParameter) {
    if (attribute.valueType instanceof ListType) {
      return "${methodParameter.name}.remove${attribute.name.capitalize()}(this)"
    }
    return "${methodParameter.name}.${DirectSetter.getName(attribute)}(null)"
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 8
  }
}
