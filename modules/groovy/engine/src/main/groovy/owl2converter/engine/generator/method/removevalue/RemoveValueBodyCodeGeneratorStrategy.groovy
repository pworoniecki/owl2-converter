package owl2converter.engine.generator.method.removevalue

import owl2converter.engine.generator.method.MethodBodyCodeGeneratorStrategy
import owl2converter.groovymetamodel.method.RemoveValueMethod

/**
 * A trait with abstract methods representing trait's code generation strategy for remove value method
 */
trait RemoveValueBodyCodeGeneratorStrategy extends MethodBodyCodeGeneratorStrategy<RemoveValueMethod> {
}
