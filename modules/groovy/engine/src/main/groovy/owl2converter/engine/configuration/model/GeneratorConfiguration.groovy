package owl2converter.engine.configuration.model

/**
 * A wrapper class for configuration of generated classes.
 */
class GeneratorConfiguration {

  private GeneratedClassesConfiguration generatedClasses

  /**
   * Returns the configuration for class generation.
   *
   * @return the configuration for class generation
   */
  GeneratedClassesConfiguration getGeneratedClasses() {
    return generatedClasses
  }

  /**
   * Sets a configuration for class generation.
   *
   * @param generatedClass a configuration for class generation
   */
  void setGeneratedClasses(GeneratedClassesConfiguration generatedClass) {
    this.generatedClasses = generatedClass
  }
}
