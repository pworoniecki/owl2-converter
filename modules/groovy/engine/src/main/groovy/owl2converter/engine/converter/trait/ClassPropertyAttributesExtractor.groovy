package owl2converter.engine.converter.trait

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.Type
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.stereotype.Component
import owl2converter.engine.converter.attribute.domain.PropertyDomainExtractor
import owl2converter.engine.exception.MissingIRIRemainderException

/**
 * A class responsible for allocation of attributes from Groovy meta-model to a proper OWL class.
 * The allocation is done based on the information of property domain (the attribute belongs to the class being the property domain).
 * It is assumed that the extraction process which maps OWL properties to Groovy attributes is completed.
 */
@Component
class ClassPropertyAttributesExtractor {

  private PropertyDomainExtractor propertyDomainExtractor

  /**
   * Class constructor.
   *
   * @param propertyDomainExtractor an instance of property domain extractor
   */
  ClassPropertyAttributesExtractor(PropertyDomainExtractor propertyDomainExtractor) {
    this.propertyDomainExtractor = propertyDomainExtractor
  }

  /**
   * Selects attributes belonging to the specific OWL class basing on the mapping between all owl properties and attributes created from them
   *
   * @param ontology an OWL ontology the OWL class is defined in
   * @param owlClass an OWL class for which attributes are returned
   * @param attributes a mapping between OWL properties their related Groovy attribute metamodels
   * @return the list of attributes belonging to the OWL class
   * @throws MissingIRIRemainderException if {@code owlClass}'s name is missing in {@code ontology}
   */
  List<Attribute> extractClassAttributes(OWLOntology ontology, OWLClass owlClass, Map<OWLProperty, Attribute> attributes) {
    return attributes.findAll { OWLProperty property, Attribute attribute ->
      belongsPropertyToClass(property, owlClass, ontology)
    }.values().toList()
  }

  private boolean belongsPropertyToClass(OWLProperty property, OWLClass owlClass, OWLOntology ontology) {
    Type propertyDomain = propertyDomainExtractor.extractDomain(property, ontology)
    return isTypeCreatedFromOwlClass(propertyDomain, owlClass)
  }

  private static boolean isTypeCreatedFromOwlClass(Type type, OWLClass owlClass) {
    Type extractedType = type instanceof ListType ? type.extractSimpleParameterizedType() : type
    return extractedType.simpleName == extractName(owlClass)
  }

  private static String extractName(OWLClass owlClass) {
    return owlClass.getIRI().remainder.orElseThrow {
      throw new MissingIRIRemainderException("Cannot extract name of OWL class $owlClass")
    }
  }
}
