package owl2converter.engine.converter

import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.exception.MissingIRIRemainderException

/**
 * Class responsible for mapping OWL classes defined in an OWL ontology to Class Groovy metamodel.
 */
@Component
class ClassesExtractor {

  private String classPackage
  private ConstructorsExtractor constructorsExtractor

  /**
   * Class constructor.
   *
   * @param configuration
   * @param constructorsExtractor
   */
  @Autowired
  ClassesExtractor(ApplicationConfiguration configuration, ConstructorsExtractor constructorsExtractor) {
    classPackage = configuration.generator.generatedClasses.modelFullPackage
    this.constructorsExtractor = constructorsExtractor
  }

  /**
   * Extracts information about OWL classes and map them into Groovy class metamodels
   *
   * @param traits a mapping between OWL classes and lists of trait metamodels created for them
   * @return the list of Groovy class metamodels created containing information extracted from OWL classes contained in {@code traits}
   * @throws MissingIRIRemainderException if an OWL class has no name
   */
  List<Class> extract(Map<OWLClass, List<Trait>> traits) {
    return traits.keySet().findAll().collect { extractClass(it as OWLClass, traits) }
  }

  private Class extractClass(OWLClass owlClass, Map<OWLClass, List<Trait>> traits) {
    String className = owlClass.getIRI().remainder
        .orElseThrow { new MissingIRIRemainderException("Missing OWL class name: $owlClass") }
    Class.Builder classBuilder = new Class.Builder(classPackage, className)
        .withAnnotations([])
        .withAttributes(extractAttributes())
        .withConstructors(extractConstructors())
        .withImplementedTraits(traits[owlClass])
        .withMethods(extractMethods())
    Optional<Class> inheritedClass = extractInheritedClass()
    return (inheritedClass.isPresent() ? classBuilder.withInheritedClass(inheritedClass.get()) : classBuilder).buildClass()
  }

  private static List<Attribute> extractAttributes() {
    return []
  }

  List<Constructor> extractConstructors() {
    return constructorsExtractor.extract()
  }

  private static Optional<Class> extractInheritedClass() {
    return Optional.empty()
  }

  private static List<Method> extractMethods() {
    return []
  }
}
