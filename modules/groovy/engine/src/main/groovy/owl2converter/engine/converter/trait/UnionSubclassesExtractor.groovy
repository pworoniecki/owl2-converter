package owl2converter.engine.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom
import org.semanticweb.owlapi.model.OWLObjectUnionOf
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component

import static org.semanticweb.owlapi.model.ClassExpressionType.OBJECT_UNION_OF

/**
 * Class responsible for extraction of subclasses of an OWL class being part of union class axioms related to the class
 */
@Component
class UnionSubclassesExtractor {

  /**
   * Extracts information about other OWL classes being part of union class axioms (related to given OWL class).
   * Union class axioms are: combination of equivalent class axiom (where equivalent class is defined by ObjectUnionOf axiom)
   * and DisjointUnion axiom.
   *
   * @param ontology an OWL ontology containing the OWL class and checked union class axioms
   * @param owlClass an OWL class for which information about union subclasses are to be extracted
   * @return the list of OWL classes being part of classes union where {@code owlClass} is part of the union too
   */
  List<OWLClass> extract(OWLOntology ontology, OWLClass owlClass) {
    if (ontology.equivalentClassesAxioms(owlClass).count() > 0) {
      return findUnionSubclassesInEquivalentUnionAxiom(ontology, owlClass)
    }
    return findUnionSubclassesInDisjointUnionAxiom(ontology, owlClass)
  }

  private static List<OWLClass> findUnionSubclassesInEquivalentUnionAxiom(OWLOntology ontology, OWLClass owlClass) {
    OWLEquivalentClassesAxiom equivalentClassesAxiom = ontology.equivalentClassesAxioms(owlClass)
        .find { it.classExpressions().any { it.classExpressionType == OBJECT_UNION_OF } } as OWLEquivalentClassesAxiom
    OWLObjectUnionOf objectUnionOf = equivalentClassesAxiom.classExpressions()
        .find { it.classExpressionType == OBJECT_UNION_OF } as OWLObjectUnionOf
    return objectUnionOf.classesInSignature().findAll { it != owlClass }
  }

  private static List<OWLClass> findUnionSubclassesInDisjointUnionAxiom(OWLOntology ontology, OWLClass owlClass) {
    List<OWLDisjointUnionAxiom> disjointUnionAxioms = ontology.disjointUnionAxioms(owlClass).findAll()
    return disjointUnionAxioms.collect { it.classesInSignature().findAll { it != owlClass } }.flatten() as List<OWLClass>
  }
}
