package owl2converter.engine.converter.attribute.range

import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.converter.attribute.PredefinedTypes
import owl2converter.engine.converter.attribute.domain.PropertyDirectDomainExtractor
import owl2converter.engine.converter.trait.TraitsExtractor
import owl2converter.engine.exception.MissingIRIRemainderException
import owl2converter.engine.utils.OptionalTransformer

/**
 * A class responsible for translation of an object property range into its Groovy meta-model type.
 */
@Component
class ObjectPropertyRangeExtractor {

  private String generatedClassesPackage
  private PredefinedTypes predefinedTypes
  private PropertyDirectDomainExtractor directDomainExtractor
  private PropertyDirectRangeExtractor directRangeExtractor

  /**
   * Class constructor.
   *
   * @param applicationConfiguration an instance of application configuration (probably not used)
   * @param predefinedTypes an instance of predefined types class
   * @param directDomainExtractor an instance of DirectDomainExtractor
   * @param directRangeExtractor an instance of RangeExtractor
   */
  @Autowired
  ObjectPropertyRangeExtractor(ApplicationConfiguration applicationConfiguration, PredefinedTypes predefinedTypes,
                               PropertyDirectDomainExtractor directDomainExtractor,
                               PropertyDirectRangeExtractor directRangeExtractor) {
    this.generatedClassesPackage = applicationConfiguration.generator.generatedClasses.modelFullPackage
    this.predefinedTypes = predefinedTypes
    this.directDomainExtractor = directDomainExtractor
    this.directRangeExtractor = directRangeExtractor
  }

  /**
   * Returns a type from Groovy meta-model being a representation of OWL object property range.
   * If there is no explicit range declaration, then range is reasoned using equivalent property's range,
   * super property's range or inverse property's domain.
   *
   * @param property an OWL object property range of which is to be translated to a Groovy meta-model type
   * @param ontology an OWL ontology in which the property is defined
   * @return the type being a Groovy meta-model type representation of the OWL object property range
   */
  Type extractRange(OWLObjectProperty property, OWLOntology ontology) {
    Optional<Type> simpleType = OptionalTransformer.of(directRangeExtractor.extractSingleDirectRange(property, ontology))
        .or { extractRangeOfEquivalentProperty(property, ontology) }
        .or { extractRangeOfSuperProperty(property, ontology) }
        .or { extracDomainOfInverseProperty(property, ontology) }
        .getResult()
        .map { convertToTraitType(it) }
    if (hasSingleType(property, ontology)) {
      return simpleType.orElse(predefinedTypes.thingTraitType)
    }
    return simpleType.isPresent() ? new ListType(simpleType.get()) : predefinedTypes.thingTraitListType
  }

  private static boolean hasSingleType(OWLObjectProperty property, OWLOntology ontology) {
    return ontology.functionalObjectPropertyAxioms(property).count() > 0
  }

  private Type convertToTraitType(OWLClass owlClass) {
    String className = owlClass.getIRI().remainder
        .orElseThrow { throw new MissingIRIRemainderException("Missing class name in ontology for: $owlClass") }
    return new SimpleType(generatedClassesPackage, className + TraitsExtractor.TRAIT_NAME_SUFFIX)
  }

  private Optional<OWLClassExpression> extractRangeOfEquivalentProperty(OWLObjectProperty property, OWLOntology ontology) {
    return findEquivalentProperty(property, ontology).flatMap { directRangeExtractor.extractSingleDirectRange(it, ontology) }
  }

  private static Optional<OWLObjectProperty> findEquivalentProperty(OWLObjectProperty property, OWLOntology ontology) {
    def equivalentPropertyAxiom =
        ontology.equivalentObjectPropertiesAxioms(property).find() as OWLEquivalentObjectPropertiesAxiom
    return Optional.ofNullable(equivalentPropertyAxiom?.properties()?.find { !it.is(property) } as OWLObjectProperty)
  }

  private Optional<OWLClassExpression> extractRangeOfSuperProperty(OWLObjectProperty property,
                                                                   OWLOntology ontology) {
    return findSuperProperty(property, ontology).flatMap { directRangeExtractor.extractSingleDirectRange(it, ontology) }
  }

  private static Optional<OWLObjectProperty> findSuperProperty(OWLObjectProperty property, OWLOntology ontology) {
    def subPropertyAxiom = ontology.objectSubPropertyAxiomsForSubProperty(property).find() as OWLSubObjectPropertyOfAxiom
    return Optional.ofNullable(subPropertyAxiom?.superProperty as OWLObjectProperty)
  }

  private Optional<OWLClassExpression> extracDomainOfInverseProperty(OWLObjectProperty property, OWLOntology ontology) {
    return findInverseProperty(property, ontology).flatMap { directDomainExtractor.extractSingleDirectDomain(it, ontology) }
  }

  private static Optional<OWLObjectProperty> findInverseProperty(OWLObjectProperty property, OWLOntology ontology) {
    def inversePropertyAxiom = ontology.inverseObjectPropertyAxioms(property).find() as OWLInverseObjectPropertiesAxiom
    return Optional.ofNullable(inversePropertyAxiom?.properties()?.find { !it.is(property) } as OWLObjectProperty)
  }
}
