package owl2converter.engine.generator.method.getter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Getter
import owl2converter.groovymetamodel.type.ListType
import org.springframework.stereotype.Component

@Component
class SuperAttributeGetterBodyCodeGenerator implements GetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Getter getter) {
    return getter.attribute.isSuperAttribute()
  }

  @Override
  String generateCode(Getter getter) {
    List<Attribute> subAttributes = getter.attribute.findSubAttributes()
    if (getter.attribute.valueType instanceof ListType) {
      return generateCodeForListAttribute(getter, subAttributes)
    }
    return generateCodeForSingleAttribute(getter, subAttributes)
  }

  private static String generateCodeForListAttribute(Getter getter, List<Attribute> subAttributes) {
    return "return this.${getter.attribute.name}" + subAttributes.collect { " + this.${it.name}" }.join('')
  }

  private static String generateCodeForSingleAttribute(Getter getter, List<Attribute> subAttributes) {
    String values = "this.${getter.attribute.name}" + subAttributes.collect { ", this.${it.name}" }.join('')
    return [
        "def values = [$values]",
        'if (values.count { it != null } == 0) {',
        'return null',
        '}',
        'if (values.count { it != null } == 1) {',
        'return values.find { it != null }',
        '}',
        "throw new IllegalValuesException('More than one value is set across subattributes and current attribute')"
    ].join('\n')
  }

  @Override
  int getPriority() {
    return 1
  }
}
