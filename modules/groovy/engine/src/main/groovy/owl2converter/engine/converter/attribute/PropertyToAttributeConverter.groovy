package owl2converter.engine.converter.attribute

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.Type
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.converter.attribute.range.PropertyRangeExtractor
import owl2converter.engine.exception.MissingIRIRemainderException

/**
 * A class responsible for conversion of OWL property into an instance of {@link Attribute} class form Groovy meta-model.
 */
@Component
class PropertyToAttributeConverter {

  private PropertyRangeExtractor propertyRangeExtractor
  private AttributeCharacteristicsExtractor characteristicsExtractor
  private AttributeDefaultValueExtractor defaultValueExtractor

  /**
   * Class constructor.
   *
   * @param propertyRangeExtractor an instance of PropertyRangeExtractor class
   * @param characteristicsExtractor an instance of AttributeCharacteristicsExtractor class
   * @param defaultValueExtractor an instance of AttributeDefaultValueExtractor class
   */
  @Autowired
  PropertyToAttributeConverter(PropertyRangeExtractor propertyRangeExtractor,
                               AttributeCharacteristicsExtractor characteristicsExtractor,
                               AttributeDefaultValueExtractor defaultValueExtractor) {
    this.propertyRangeExtractor = propertyRangeExtractor
    this.characteristicsExtractor = characteristicsExtractor
    this.defaultValueExtractor = defaultValueExtractor
  }

  /**
   * Returns the attribute being the Groovy representation of OWL object property
   * without information about attribute relations (e.g. inverse attributes, equivalent attributes etc.)
   *
   * @param property an OWL object property to be converted into Groovy meta-model equivalent
   * @param ontology an OWL ontology in which the property is defined
   * @return an instance of @{link Attribute} being the Groovy meta-model representation of OWL object property
   */
  Attribute convertWithoutRelations(OWLObjectProperty property, OWLOntology ontology) {
    Type range = propertyRangeExtractor.extractRange(property, ontology)
    Optional<String> defaultValue = defaultValueExtractor.extract(property, ontology)
    def builder = new Attribute.AttributeBuilder(extractName(property), range)
        .withCharacteristics(characteristicsExtractor.extract(property, ontology))
        .withAccessModifier(AccessModifier.PRIVATE)
    return defaultValue.isPresent() ? builder.withDefaultValue(defaultValue.get()).buildAttribute() : builder.buildAttribute()
  }

  private static String extractName(OWLProperty property) {
    return property.getIRI().remainder
        .orElseThrow { new MissingIRIRemainderException("Invalid property's IRI: ${property.getIRI()}") }
  }

  /**
   * Returns the attribute being the Groovy representation of OWL data property
   * without information about attribute relations (e.g. inverse attributes, equivalent attributes etc.)
   *
   * @param property an OWL data property to be converted into Groovy meta-model equivalent
   * @param ontology an OWL ontology in which the property is defined
   * @return an instance of @{link Attribute} being the Groovy meta-model representation of OWL object property
   */
  Attribute convertWithoutRelations(OWLDataProperty property, OWLOntology ontology) {
    Type range = propertyRangeExtractor.extractRange(property, ontology)
    Optional<String> defaultValue = defaultValueExtractor.extract(property, ontology)
    def builder = new Attribute.AttributeBuilder(extractName(property), range)
        .withCharacteristics(characteristicsExtractor.extract(property, ontology))
        .withAccessModifier(AccessModifier.PRIVATE)
    return defaultValue.isPresent() ? builder.withDefaultValue(defaultValue.get()).buildAttribute() : builder.buildAttribute()
  }
}
