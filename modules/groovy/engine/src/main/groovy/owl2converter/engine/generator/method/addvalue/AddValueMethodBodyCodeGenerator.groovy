package owl2converter.engine.generator.method.addvalue

import owl2converter.groovymetamodel.method.AddValueMethod
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.generator.method.MethodBodyCodeGenerator

/**
 * The concrete code generator implementation supporting add value method. Coordinates the construction process.
 */
@Component
class AddValueMethodBodyCodeGenerator implements MethodBodyCodeGenerator<AddValueMethod> {

  private List<AddValueBodyCodeGeneratorStrategy> generators

  /**
   * Class constructor.
   *
   * @param generators a list of specific strategies for code generation
   */
  @Autowired
  AddValueMethodBodyCodeGenerator(List<AddValueBodyCodeGeneratorStrategy> generators) {
    this.generators = generators
  }

  /**
   * Generates the whole body of the {@link AddValueMethod} method
   *
   * @param method a method for which the code is to be generated
   * @return the generated body for the method
   */
  // TODO add equivalent property and subproperty and inverse property and datatype property, and all property restrictions
  @Override
  String generateCode(AddValueMethod method) {
    List<AddValueBodyCodeGeneratorStrategy> orderedGenerators =
        findGenerators(method).sort { it.getOrderWithinPriorityGroup() }
    List<String> validationCode = [generateGeneralValidationCode(method)] +
        generateSpecificValidationCode(orderedGenerators, method)
    List<String> addValueCode = [generateGeneralAddValueCode(method)] +
        generateSpecificCode(orderedGenerators, method)
    return (validationCode + addValueCode).join('\n')
  }

  private List<AddValueBodyCodeGeneratorStrategy> findGenerators(AddValueMethod method) {
    def possibleGenerators = generators.findAll { it.supports(method) }
    def minGeneratorPriority = possibleGenerators*.priority.min()
    return possibleGenerators.findAll { it.priority == minGeneratorPriority }
  }

  private static String generateGeneralValidationCode(AddValueMethod method) {
    return "if (${method.parameter.name} == null || this.${method.attribute.name}.contains(${method.parameter.name})) { return }"
  }

  private static List<String> generateSpecificValidationCode(List<AddValueBodyCodeGeneratorStrategy> orderedGenerators,
                                                             AddValueMethod method) {
    return orderedGenerators.collect { it.generateValidationCode(method) }
        .findAll { it.isPresent() }.collect { it.get() }
  }

  private static List<String> generateSpecificCode(List<AddValueBodyCodeGeneratorStrategy> orderedGenerators,
                                                           AddValueMethod method) {
    return orderedGenerators.collect { it.generateSpecificCode(method) }.findAll { it.isPresent() }.collect { it.get() }
  }

  private static String generateGeneralAddValueCode(AddValueMethod method) {
    return "this.${method.attribute.name}.add(${method.parameter.name})"
  }

  @Override
  Class<AddValueMethod> getSupportedClass() {
    return AddValueMethod
  }
}
