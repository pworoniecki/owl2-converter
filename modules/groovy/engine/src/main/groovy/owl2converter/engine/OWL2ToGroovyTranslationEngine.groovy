package owl2converter.engine

import commoncodemetamodel.Element
import commoncodemetamodel.File
import commoncodemetamodel.Folder
import commoncodemetamodel.TextFile
import fromowl2builderapi.FromOWL2Translator
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.converter.ObjectOrientedCodeModel
import owl2converter.engine.converter.OntologyToCodeModelConverter
import owl2converter.engine.generator.ClassCodeGenerator
import owl2converter.engine.generator.TraitCodeGenerator
import owl2converter.engine.sourcecodegenerator.ClassSourceCode
import owl2converter.engine.sourcecodegenerator.SourceCode
import owl2converter.engine.sourcecodegenerator.TraitSourceCode
import owl2converter.engine.utils.GroovyCodeFormatter
import owl2converter.groovymetamodel.Class

import java.nio.charset.StandardCharsets

/**
 * The main class responsible for OWL2 to Groovy translation.
 */
@Component
class OWL2ToGroovyTranslationEngine extends FromOWL2Translator {

  private static final String GROOVY_SOURCE_FILE_EXTENSION = 'groovy'

  private OntologyToCodeModelConverter ontologyToCodeModelConverter
  private ClassCodeGenerator classCodeGenerator
  private TraitCodeGenerator traitCodeGenerator
  private GroovyCodeFormatter codeFormatter
  private GeneratorConfiguration generatorConfiguration
  private ResultCodeBaseMetamodelExtractor resultCodeBaseMetamodelExtractor

  /**
   * Class constructor. Requires many components responsible for translation.
   *
   * @param ontologyToCodeModelConverter an object
   * @param classCodeGenerator
   * @param traitCodeGenerator
   * @param codeFormatter a code formatter object
   * @param applicationConfiguration an object with application configuration data
   * @param resultCodeBaseMetamodelExtractor
   */
  @Autowired
  OWL2ToGroovyTranslationEngine(OntologyToCodeModelConverter ontologyToCodeModelConverter, ClassCodeGenerator classCodeGenerator,
                                TraitCodeGenerator traitCodeGenerator, GroovyCodeFormatter codeFormatter, ApplicationConfiguration applicationConfiguration,
                                ResultCodeBaseMetamodelExtractor resultCodeBaseMetamodelExtractor) {
    this.ontologyToCodeModelConverter = ontologyToCodeModelConverter
    this.classCodeGenerator = classCodeGenerator
    this.traitCodeGenerator = traitCodeGenerator
    this.codeFormatter = codeFormatter
    this.generatorConfiguration = applicationConfiguration.generator
    this.resultCodeBaseMetamodelExtractor = resultCodeBaseMetamodelExtractor
  }

  /**
   * Translates OWL ontology into a set of elements (folder and files written in Groovy).
   *
   * @param ontology an ontology to be translated
   * @return the result of translation; a set of folder and files written mostly in Groovy
   */
  @Override
  Set<Element> translateOWL2Ontology(OWLOntology ontology) {
    ObjectOrientedCodeModel codeModel = ontologyToCodeModelConverter.convert(ontology)
    List<ClassSourceCode> classSourceCodes = generateClassesSourceCode(codeModel)
    List<TraitSourceCode> traitSourceCodes = generateTraitsSourceCode(codeModel)
    Class individualsDataSourceClass = codeModel.individualsDataSourceClass
    ClassSourceCode individualDataSourceClassModel = new ClassSourceCode(individualsDataSourceClass,
            classCodeGenerator.generateCode(individualsDataSourceClass))
    return generateCodeMetamodel(classSourceCodes, traitSourceCodes, individualDataSourceClassModel)
  }

  private List<ClassSourceCode> generateClassesSourceCode(ObjectOrientedCodeModel codeModel) {
    return codeModel.classes.collect {
      new ClassSourceCode(it, classCodeGenerator.generateCode(it))
    }
  }

  private List<TraitSourceCode> generateTraitsSourceCode(ObjectOrientedCodeModel codeModel) {
    return codeModel.traits.collect {
      new TraitSourceCode(it, traitCodeGenerator.generateCode(it))
    }
  }

  private Set<Element> generateCodeMetamodel(List<ClassSourceCode> classSourceCodes, List<TraitSourceCode> traitSourceCodes,
                                             ClassSourceCode individualDataSourceClassModel) {
    Folder modelFolder = createModelFolder(classSourceCodes, traitSourceCodes)
    return createGeneratedCodeFolder(modelFolder, individualDataSourceClassModel)
  }

  private Folder createModelFolder(List<ClassSourceCode> classSourceCodes, List<TraitSourceCode> traitSourceCodes) {
    Set<File> sourceCodeFiles = createSourceCodeFiles(classSourceCodes) + createSourceCodeFiles(traitSourceCodes)
    return new Folder(generatorConfiguration.generatedClasses.modelSubPackage, sourceCodeFiles)
  }

  private Set<Element> createGeneratedCodeFolder(Folder modelFolder, ClassSourceCode individualDataSourceClassModel) {
    Set<Element> generatedCodeElements = resultCodeBaseMetamodelExtractor.extractResultCodeMetamodel()
    Folder srcFolder = generatedCodeElements.find { it.name == 'src' && it instanceof Folder } as Folder
    Folder groovySrcFolder = getNestedSubfolder(srcFolder, ['main', 'groovy'].toArray(new String[0]))
    String[] basePackageFolderHierarchyInSrcFolder = generatorConfiguration.generatedClasses.basePackage.split('\\.')
    Folder groovySrcBasePackageFolder = getNestedSubfolder(groovySrcFolder, basePackageFolderHierarchyInSrcFolder)
    String[] modelFolderHierarchyInSrcFolder = generatorConfiguration.generatedClasses.modelFullPackage.split('\\.')
    Folder codeBaseModelFolder = getNestedSubfolder(groovySrcFolder, modelFolderHierarchyInSrcFolder)
    codeBaseModelFolder.addElements(modelFolder.files)
    groovySrcBasePackageFolder.addElements(createSourceCodeFiles([individualDataSourceClassModel]))
    return generatedCodeElements
  }

  private static Folder getNestedSubfolder(Folder folder, String[] consecutiveSubfolders) {
    Folder currentFolder = folder
    for (String subfolderName: consecutiveSubfolders) {
      Optional<Folder> subfolder = currentFolder.getSubfolder(subfolderName)
      if (!subfolder.isPresent()) {
        throw new IllegalArgumentException("Missing $subfolderName subfolder in $currentFolder.name folder")
      }
      currentFolder = subfolder.get()
    }
    return currentFolder
  }

  private Set<File> createSourceCodeFiles(List<? extends SourceCode> sourceCodes) {
    return sourceCodes.collect {
      String formattedSourceCode = codeFormatter.format(it.sourceCode)
      return new TextFile(it.name, GROOVY_SOURCE_FILE_EXTENSION, formattedSourceCode, StandardCharsets.UTF_8)
    }
  }

  /**
   * Returns the target translation language.
   *
   * @return 'Groovy' as the target translation language
   */
  @Override
  String getTargetLanguageName() {
    return 'Groovy'
  }
}
