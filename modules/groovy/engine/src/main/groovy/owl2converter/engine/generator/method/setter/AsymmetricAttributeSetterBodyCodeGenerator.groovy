package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.method.Setter
import owl2converter.groovymetamodel.type.ListType
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.ASYMMETRIC

@Component
class AsymmetricAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    return ASYMMETRIC in setter.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    String parameterName = setter.parameter.name
    List<String> validationCodeLines = setter.attribute.valueType instanceof ListType ?
        generateListAttributeValidationCodeLines(parameterName) : generateNonListAttributeValidationCodeLines(parameterName)
    return Optional.of(generateCode(validationCodeLines))
  }

  private static List<String> generateListAttributeValidationCodeLines(String parameterName) {
    return [
        "def forbiddenValues = ${parameterName}.findAll { it.${parameterName}.contains(this) }",
        "if (forbiddenValues) {",
        "throw new AsymmetricAttributeException(\"Cannot set values '\$forbiddenValues' - it would break asymmetric attribute rule\")",
        '}'
    ]
  }

  private static List<String> generateNonListAttributeValidationCodeLines(String parameterName) {
    return [
        "if (${parameterName}.${parameterName} == this) {",
        "throw new AsymmetricAttributeException(\"Cannot set value '\$$parameterName' - it would break asymmetric attribute rule\")",
        '}'
    ]
  }

  @Override
  Optional<String> generateSpecificCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 3
  }
}
