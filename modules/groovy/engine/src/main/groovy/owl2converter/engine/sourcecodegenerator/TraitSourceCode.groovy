package owl2converter.engine.sourcecodegenerator

import owl2converter.groovymetamodel.Trait

/**
 * A wrapper class to keep together a trait meta-model instance and its source code representation (string).
 */
class TraitSourceCode implements SourceCode {

  private Trait traitModel
  private String sourceCode

  /**
   * Class constructor.
   *
   * @param traitModel a trait meta-class instance
   * @param sourceCode a string being a result of source code generation for the traitModel
   */
  TraitSourceCode(Trait traitModel, String sourceCode) {
    this.traitModel = traitModel
    this.sourceCode = sourceCode
  }

  /**
   * Returns the trait meta-model instance
   *
   * @return the trait model
   */
  Trait getTraitModel() {
    return traitModel
  }

  /**
   * Returns the name of the trait.
   *
   * @return the trait name
   */
  @Override
  String getName() {
    return traitModel.name
  }

  /**
   * Returns the source code of the trait model.
   *
   * @return the source code
   */
  @Override
  String getSourceCode() {
    return sourceCode
  }
}
