package owl2converter.engine.codebuilder.common

import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.Type

/**
 * A trait responsible for appending methods, both static and instance ones with required imports.
 */
trait ContainingMethods implements ContainingCode, ContainingImports {

  /**
   * Appends imports required by a method list return values.
   *
   * @param methods a list of methods which return value types are to be imported
   */
  void appendMethodReturnTypeImports(List<Method> methods) {
    List<Type> uniqueParameterTypes = methods*.returnType.unique()
    appendImports(uniqueParameterTypes, getOwnerPackageName())
  }

  /**
   * Appends imports required by a method list parameters.
   *
   * @param methods a list of methods which parameter types are to be imported
   */
  void appendMethodParameterImports(List<Method> methods) {
    List<Type> uniqueParameterTypes = methods*.parameters*.type.flatten().unique() as List<Type>
    appendImports(uniqueParameterTypes, getOwnerPackageName())
  }

  /**
   * Appends source code declarations from {@code attributesToCode} for all static methods.
   * Adds an empty line after each appended method.
   *
   * @param methodsToCode a mapping between method meta-model instances and their source code representations
   */
  void appendStaticMethodsWithEmptyLines(Map<Method, String> methodsToCode) {
    methodsToCode.each { method, code ->
      if (method.isStatic()) {
        appendCode(code)
        appendEmptyLine()
      }
    }
  }

  /**
   * Appends source code declarations from {@code attributesToCode} for all non-static methods.
   * Adds an empty line after each appended method.
   *
   * @param methodsToCode a mapping between method meta-model instances and their source code representations
   */
  void appendNonStaticMethodsWithEmptyLines(Map<Method, String> methodsToCode) {
    methodsToCode.each { method, code ->
      if (!method.isStatic()) {
        appendCode(code)
        appendEmptyLine()
      }
    }
  }
}
