package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.method.Setter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.generator.method.MethodBodyCodeGenerator

@Component
class SetterBodyCodeGenerator implements MethodBodyCodeGenerator<Setter> {

  private List<SetterBodyCodeGeneratorStrategy> generators

  @Autowired
  SetterBodyCodeGenerator(List<SetterBodyCodeGeneratorStrategy> generators) {
    this.generators = generators
  }

  // TODO add equivalent property and subproperty and inverse property
  // and datatype property, and all property restrictions
  @Override
  String generateCode(Setter setter) {
    List<SetterBodyCodeGeneratorStrategy> orderedGenerators =
        findGenerators(setter).sort { it.getOrderWithinPriorityGroup() }
    List<String> validationCode = [generateGeneralValidationCode(setter)] +
        generateSpecificValidationCode(orderedGenerators, setter)
    List<String> settingValueCode = generateSpecificCode(orderedGenerators, setter) ?:
        [generateDefaultSettingValueCode(setter)]
    return (validationCode + settingValueCode).join('\n')
  }

  private List<SetterBodyCodeGeneratorStrategy> findGenerators(Setter setter) {
    def possibleGenerators = generators.findAll { it.supports(setter) }
    def minGeneratorPriority = possibleGenerators*.priority.min()
    return possibleGenerators.findAll { it.priority == minGeneratorPriority }
  }

  private static String generateGeneralValidationCode(Setter setter) {
    return "if (this.${setter.attribute.name} == ${setter.parameter.name}) { return }"
  }

  private static List<String> generateSpecificValidationCode(List<SetterBodyCodeGeneratorStrategy> orderedGenerators,
                                                             Setter setter) {
    return orderedGenerators.collect { it.generateValidationCode(setter) }
        .findAll { it.isPresent() }.collect { it.get() }
  }

  private static List<String> generateSpecificCode(List<SetterBodyCodeGeneratorStrategy> orderedGenerators,
                                                               Setter setter) {
    return orderedGenerators.collect { it.generateSpecificCode(setter) }
        .findAll { it.isPresent() }.collect { it.get() }
  }

  private static String generateDefaultSettingValueCode(Setter setter) {
    return "this.${setter.attribute.name} = ${setter.parameter.name}"
  }

  @Override
  Class<Setter> getSupportedClass() {
    return Setter
  }
}
