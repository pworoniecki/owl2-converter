package owl2converter.engine.generator

import owl2converter.groovymetamodel.Class
import org.springframework.stereotype.Component

/**
 * Signature generator of the class.
 */
@Component
class ClassSignatureCodeGenerator {

  /**
   * Generates the signature (the first line) of the class (i.e. 'class ... extends ... implements ... {'.
   *
   * @param classModel a class for which the signature code is to be generated
   * @return the string with class signature
   */
  String generateClassSignature(Class classModel) {
    def inheritedClassSignaturePart = classModel.inheritedClass.isPresent() ?
        " extends ${classModel.inheritedClass.get().name}" : ''
    def implementedTraitsSignaturePart = classModel.implementedTraits.any() ?
        " implements ${classModel.implementedTraits*.name.join(', ')}" : ''
    return "class ${classModel.name}" + inheritedClassSignaturePart + implementedTraitsSignaturePart + ' {'
  }
}
