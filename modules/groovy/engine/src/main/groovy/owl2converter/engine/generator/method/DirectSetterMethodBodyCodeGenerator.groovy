package owl2converter.engine.generator.method

import owl2converter.groovymetamodel.method.DirectSetter
import org.springframework.stereotype.Component

/**
 * A body source code generator for direct setter.
 */
@Component
class DirectSetterMethodBodyCodeGenerator implements MethodBodyCodeGenerator<DirectSetter> {

  /**
   * Generates the body of direct setter method.
   *
   * @param setter a method for which the code is to be generated
   * @return the string contained in method definition (i.e. source code of method without its signature)
   */
  @Override
  String generateCode(DirectSetter setter) {
    return "this.${setter.attribute.name} = ${setter.parameter.name}"
  }

  /**
   * Returns {@link DirectSetter} as a method for which the generator can generate code
   *
   * @return DirectSetter meta-class
   */
  @Override
  Class<DirectSetter> getSupportedClass() {
    return DirectSetter
  }
}
