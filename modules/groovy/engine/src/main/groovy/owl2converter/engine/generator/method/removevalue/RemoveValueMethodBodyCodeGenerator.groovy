package owl2converter.engine.generator.method.removevalue

import owl2converter.groovymetamodel.method.Getter
import owl2converter.groovymetamodel.method.RemoveValueMethod
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.generator.method.MethodBodyCodeGenerator

@Component
class RemoveValueMethodBodyCodeGenerator implements MethodBodyCodeGenerator<RemoveValueMethod> {

  private List<RemoveValueBodyCodeGeneratorStrategy> generators

  @Autowired
  RemoveValueMethodBodyCodeGenerator(List<RemoveValueBodyCodeGeneratorStrategy> generators) {
    this.generators = generators
  }

  // TODO add equivalent property and subproperty and inverse property
  // and datatype property, and all property restrictions
  @Override
  String generateCode(RemoveValueMethod method) {
    List<RemoveValueBodyCodeGeneratorStrategy> orderedGenerators =
        findGenerators(method).sort { it.getOrderWithinPriorityGroup() }
    List<String> validationCode = [generateGeneralValidationCode(method)] +
        generateSpecificValidationCode(orderedGenerators, method)
    List<String> removeValueCode = [generateGeneralRemoveValueCode(method)] +
        generateSpecificCode(orderedGenerators, method)
    return (validationCode + removeValueCode).join('\n')
  }

  private List<RemoveValueBodyCodeGeneratorStrategy> findGenerators(RemoveValueMethod method) {
    def possibleGenerators = generators.findAll { it.supports(method) }
    def minGeneratorPriority = possibleGenerators*.priority.min()
    return possibleGenerators.findAll { it.priority == minGeneratorPriority }
  }

  private static String generateGeneralValidationCode(RemoveValueMethod method) {
    String attributeGetterName = Getter.getName(method.attribute)
    return "if (${method.parameter.name} == null || !this.${attributeGetterName}().contains(${method.parameter.name})) { return }"
  }

  private static List<String> generateSpecificValidationCode(List<RemoveValueBodyCodeGeneratorStrategy> orderedGenerators,
                                                             RemoveValueMethod method) {
    return orderedGenerators.collect { it.generateValidationCode(method) }
        .findAll { it.isPresent() }.collect { it.get() }
  }

  private static List<String> generateSpecificCode(List<RemoveValueBodyCodeGeneratorStrategy> orderedGenerators,
                                                           RemoveValueMethod method) {
    return orderedGenerators.collect { it.generateSpecificCode(method) }.findAll { it.isPresent() }.collect { it.get() }
  }

  private static String generateGeneralRemoveValueCode(RemoveValueMethod method) {
    return "this.${method.attribute.name}.remove(${method.parameter.name})"
  }

  @Override
  Class<RemoveValueMethod> getSupportedClass() {
    return RemoveValueMethod
  }
}
