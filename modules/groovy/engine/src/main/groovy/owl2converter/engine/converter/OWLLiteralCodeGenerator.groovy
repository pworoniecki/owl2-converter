package owl2converter.engine.converter

import org.semanticweb.owlapi.model.IRI
import org.semanticweb.owlapi.model.OWLLiteral
import org.springframework.stereotype.Component
import owl2converter.engine.exception.MissingIRIRemainderException

@Component
class OWLLiteralCodeGenerator {

    private static final String XML_SCHEMA_NAMESPACE = 'http://www.w3.org/2001/XMLSchema'
    private static final Map<String, LiteralToCodeConverter> TYPE_TO_CODE_MAPPING = [
            string            : { literal -> "'$literal'" },
            normalizedString  : { literal -> "new NormalizedString('$literal')" },
            token             : { literal -> "new Token('$literal')" },
            language          : { literal -> "new Language('$literal')" },
            NMTOKEN           : { literal -> "new NMTOKEN('$literal')" },
            Name              : { literal -> "new Name('$literal')" },
            NCName            : { literal -> "new NCName('$literal')" },
            boolean           : { literal -> "Boolean.valueOf('$literal')" },
            decimal           : { literal -> "new BigDecimal('$literal')" },
            float             : { literal -> "Float.valueOf('$literal')" },
            double            : { literal -> "Double.valueOf('$literal')" },
            integer           : { literal -> "new BigInteger('$literal')" },
            nonPositiveInteger: { literal -> "new NonPositiveInteger(new BigInteger('$literal'))" },
            negativeInteger   : { literal -> "new NegativeInteger(new BigInteger('$literal'))" },
            long              : { literal -> "Long.valueOf('$literal')" },
            int               : { literal -> "Integer.valueOf('$literal')" },
            short             : { literal -> "Short.valueOf('$literal')" },
            byte              : { literal -> "Byte.valueOf('$literal')" },
            nonNegativeInteger: { literal -> "new NonNegativeInteger(new BigInteger('$literal'))" },
            unsignedLong      : { literal -> "new UnsignedLong(new BigInteger('$literal'))" },
            unsignedInt       : { literal -> "new UnsignedInt(new BigInteger('$literal'))" },
            unsignedShort     : { literal -> "new UnsignedShort(new BigInteger('$literal'))" },
            unsignedByte      : { literal -> "new UnsignedByte(new BigInteger('$literal'))" },
            positiveInteger   : { literal -> "new PositiveInteger(new BigInteger('$literal'))" },
            dateTime          : { literal -> "DatatypeFactory.newInstance().newXMLGregorianCalendar('$literal')" },
            time              : { literal -> "DatatypeFactory.newInstance().newXMLGregorianCalendar('$literal')" },
            date              : { literal -> "DatatypeFactory.newInstance().newXMLGregorianCalendar('$literal')" },
            gYearMonth        : { literal -> "DatatypeFactory.newInstance().newXMLGregorianCalendar('$literal')" },
            gYear             : { literal -> "DatatypeFactory.newInstance().newXMLGregorianCalendar('$literal')" },
            gMonthDay         : { literal -> "DatatypeFactory.newInstance().newXMLGregorianCalendar('$literal')" },
            gDay              : { literal -> "DatatypeFactory.newInstance().newXMLGregorianCalendar('$literal')" },
            gMonth            : { literal -> "DatatypeFactory.newInstance().newXMLGregorianCalendar('$literal')" }
    ].collectEntries { [(it.key): it.value as LiteralToCodeConverter] } as Map<String, LiteralToCodeConverter>

    /**
     * Converts an OWL data type into its Groovy meta-model equivalent.
     *
     * @param datatype an OWL data type to be converted
     * @return the Groovy meta-model type equivalent (representation) of the OWL data type
     * @throws owl2converter.engine.exception.MissingIRIRemainderException if the name of the OWL data type is absent in the ontology
     * @throws IllegalArgumentException if the namespace of the OWL data type is different than http://www.w3.org/2001/XMLSchema
     */
    String generateCode(OWLLiteral literal) {
        IRI typeIdentifier = literal.datatype.getIRI()
        assertCorrectTypeNamespace(typeIdentifier)
        String name = typeIdentifier.remainder.orElseThrow {
            new MissingIRIRemainderException("Missing data type of literal: $literal")
        }
        return TYPE_TO_CODE_MAPPING.get(name).generateCode(literal.literal).toString()
    }

    private static void assertCorrectTypeNamespace(IRI typeIdentifier) {
        if (typeIdentifier.namespace != "$XML_SCHEMA_NAMESPACE#") {
            throw new IllegalArgumentException("Datatype must be a value from XML Schema namespace " +
                    "(http://www.w3.org/2001/XMLSchema). Provided value ($typeIdentifier) is from another namespace: " +
                    "(${typeIdentifier.namespace}).")
        }
    }

    @FunctionalInterface
    private static interface LiteralToCodeConverter {
        GString generateCode(String literal)
    }
}
