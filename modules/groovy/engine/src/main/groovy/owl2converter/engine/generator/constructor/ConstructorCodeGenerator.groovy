package owl2converter.engine.generator.constructor

import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.generator.exception.ConstructorBodyCodeGeneratorNotFoundException
import owl2converter.engine.utils.StringUtils

/**
 * Class responsible for constructor's source code generation
 */
@Component
class ConstructorCodeGenerator {

  private List<ConstructorBodyCodeGenerator> bodyCodeGenerators

  @Autowired
  ConstructorCodeGenerator(List<ConstructorBodyCodeGenerator> bodyCodeGenerators) {
    this.bodyCodeGenerators = bodyCodeGenerators
  }

  /**
   * Generates a code for the constructor in the class being the constructor owner.
   *
   * @param constructor a constructor meta-model instance which code is to be generated
   * @param constructorOwner a class meta-model instance to which the constructor belongs to
   * @return the string with the full code (public [static] ClassName(){ ... }) of the constructor
   * @throws ConstructorBodyCodeGeneratorNotFoundException if there is no generator supporting constructor's owner (i.e. proper generator bean is not present)
   */
  String generateCode(Constructor constructor, Class constructorOwner) {
    def codeBuilder = new StringBuilder()
    codeBuilder.with {
      append(constructorSignature(constructor, constructorOwner) + '\n')
      append(constructorBody(constructor, constructorOwner) + '\n')
      append('}')
    }
    return codeBuilder.toString()
  }

  private static String constructorSignature(Constructor constructor, Class constructorOwner) {
    def accessModifier = StringUtils.emptyOrAddSpace(constructor.accessModifier.toGroovyCode())
    def staticModifier = constructor.isStatic() ? 'static ' : ''
    def parameters = constructor.parameters.collect { it.simpleDeclaration }.join(', ')

    return "$accessModifier$staticModifier$constructorOwner.name($parameters) {"
  }

  private String constructorBody(Constructor constructor, Class constructorOwner) {
    def bodyCodeGenerator = bodyCodeGenerators.find { it.supports(constructorOwner) }
    if (bodyCodeGenerator == null) {
      throw new ConstructorBodyCodeGeneratorNotFoundException(constructorOwner)
    }
    return bodyCodeGenerator.generateCode(constructor)
  }
}
