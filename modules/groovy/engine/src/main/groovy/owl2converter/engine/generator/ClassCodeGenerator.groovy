package owl2converter.engine.generator

import owl2converter.groovymetamodel.Class
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.codebuilder.ClassCodeBuilder
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration

/**
 * Generates the whole code for a class with the use of visitor pattern.
 * Coordinates the construction process.
 */
@Component
class ClassCodeGenerator {

  private ClassBodyCodeGenerator classBodyGenerator
  private ClassSignatureCodeGenerator classSignatureGenerator
  private GeneratorConfiguration generatorConfiguration

  /**
   * Class constructor.
   *
   * @param classBodyGenerator
   * @param classSignatureGenerator
   * @param applicationConfiguration
   */
  @Autowired
  ClassCodeGenerator(ClassBodyCodeGenerator classBodyGenerator, ClassSignatureCodeGenerator classSignatureGenerator,
                     ApplicationConfiguration applicationConfiguration) {
    this.classBodyGenerator = classBodyGenerator
    this.classSignatureGenerator = classSignatureGenerator
    this.generatorConfiguration = applicationConfiguration.generator
  }

  /**
   * Generates the full definition of the class (starting from package declaration).
   *
   * @param classModel a class for which the whole source code is to be generated
   * @return the string with full class implementation
   */
  String generateCode(Class classModel) {
    classModel.accept(classBodyGenerator)

    def codeBuilder = new ClassCodeBuilder(classModel)
    codeBuilder.with {
      appendPackage()
      appendEmptyLine()
      appendImports(generatorConfiguration)
      appendEmptyLine()
      appendAnnotations(classModel.annotations)
      appendCode(classSignatureGenerator.generateClassSignature(classModel))
      appendEmptyLine()
      appendStaticAttributes(classBodyGenerator.attributeCodes)
      appendStaticConstructorsWithEmptyLines(classBodyGenerator.constructorCodes)
      appendNonStaticAttributes(classBodyGenerator.attributeCodes)
      appendNonStaticConstructorsWithEmptyLines(classBodyGenerator.constructorCodes)
      appendStaticMethodsWithEmptyLines(classBodyGenerator.methodCodes)
      appendNonStaticMethodsWithEmptyLines(classBodyGenerator.methodCodes)
      appendClassSignatureEnd()
      appendEmptyLine()
    }

    return codeBuilder.code
  }
}
