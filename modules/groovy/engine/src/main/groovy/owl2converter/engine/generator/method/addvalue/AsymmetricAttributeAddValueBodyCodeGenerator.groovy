package owl2converter.engine.generator.method.addvalue

import owl2converter.groovymetamodel.method.AddValueMethod
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.ASYMMETRIC

/**
 * A code generation strategy class supporting add value methods of asymmetric attributes.
 */
@Component
class AsymmetricAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  /**
   * Checks whether the generator can be used for the method.
   *
   * @param method a method for which the code is to be generated
   * @return true if the method is defined for an asymmetric attribute
   */
  @Override
  boolean supports(AddValueMethod method) {
    return ASYMMETRIC in method.attribute.characteristics
  }

  /**
   * Returns the string representing validation part of method's source code.
   *
   * @param method a method for which the code is to be generated
   * @return a string with validation code
   */
  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    def parameterName = method.parameter.name
    def validationCode = generateCode([
        "if (${parameterName}.${parameterName}.contains(this)) {",
        "throw new AsymmetricAttributeException(\"Cannot add value '\$$parameterName' - " +
            "it would break asymmetric attribute rule\")",
        '}'
    ])
    return Optional.of(validationCode)
  }

  /**
   * Returns the string representing specific part of method's code.
   *
   * @param method a method for which the code is to be generated
   * @return no code (empty {@link Optional})
   */
  @Override
  Optional<String> generateSpecificCode(AddValueMethod method) {
    return Optional.empty()
  }

  /**
   * Returns the relative order of generator within the group
   *
   * @return 3
   */
  @Override
  int getOrderWithinPriorityGroup() {
    return 3
  }
}
