package owl2converter.engine.generator.method.getter

import owl2converter.groovymetamodel.method.Getter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.generator.method.MethodBodyCodeGenerator

@Component
class GetterBodyCodeGenerator implements MethodBodyCodeGenerator<Getter> {

  private List<GetterBodyCodeGeneratorStrategy> generators

  @Autowired
  GetterBodyCodeGenerator(List<GetterBodyCodeGeneratorStrategy> generators) {
    this.generators = generators
  }

  @Override
  String generateCode(Getter method) {
    GetterBodyCodeGeneratorStrategy generator = generators.findAll { it.supports(method) }.min { it.priority }
    if (!generator) {
      throw new IllegalStateException("Missing body code generator that supports getter $method.name")
    }
    return generator.generateCode(method)
  }

  @Override
  Class<Getter> getSupportedClass() {
    return Getter
  }
}
