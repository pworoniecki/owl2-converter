package owl2converter.engine.codebuilder.common

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.Type

/**
 * A trait responsible for appending attributes, both static and instance ones with reuqired imports.
 */
trait ContainingAttributes implements ContainingCode, ContainingImports {

  /**
   * Appends imports of types required by attributes.
   *
   * @param attributes a list of attributes
   */
  void appendAttributeTypeImports(List<Attribute> attributes) {
    List<Type> uniqueParameterTypes = attributes*.valueType.unique()
    appendImports(uniqueParameterTypes, getOwnerPackageName())
  }

  /**
   * Appends source code declarations from {@code attributesToCode} for all static attributes.
   * Adds an empty line if any static attribute has been appended.
   *
   * @param attributesToCode a mapping between attribute meta-model instances and their source code representations
   */
  void appendStaticAttributes(Map<Attribute, String> attributesToCode) {
    boolean isAnyStaticAttributePresent = false
    attributesToCode.each { attribute, code ->
      if (attribute.isStatic()) {
        appendCode(code)
        isAnyStaticAttributePresent = true
      }
    }
    if (isAnyStaticAttributePresent) {
      appendEmptyLine()
    }
  }

  /**
   * Appends source code declarations from {@code attributesToCode} for all non-static attributes
   * Adds an empty line if any static attribute has been appended.
   *
   * @param attributesToCode a mapping between attribute meta-model instances and their source code representations
   */
  void appendNonStaticAttributes(Map<Attribute, String> attributesToCode) {
    boolean isAnyNonStaticAttributePresent = false
    attributesToCode.each { attribute, code ->
      if (!attribute.isStatic()) {
        appendCode(code)
        isAnyNonStaticAttributePresent = true
      }
    }
    if (isAnyNonStaticAttributePresent) {
      appendEmptyLine()
    }
  }
}
