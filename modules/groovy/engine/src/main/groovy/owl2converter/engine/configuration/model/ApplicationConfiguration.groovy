package owl2converter.engine.configuration.model

/**
 * A wrapper class containing generator configuration.
 */
class ApplicationConfiguration {

  private GeneratorConfiguration generator

  GeneratorConfiguration getGenerator() {
    return generator
  }

  void setGenerator(GeneratorConfiguration generator) {
    this.generator = generator
  }
}
