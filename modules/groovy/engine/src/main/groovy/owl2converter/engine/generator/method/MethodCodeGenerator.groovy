package owl2converter.engine.generator.method

import owl2converter.groovymetamodel.method.Method
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.generator.exception.MethodBodyCodeGeneratorNotFoundException
import owl2converter.engine.utils.StringUtils

/**
 * Class generating the source code of methods
 */
@Component
class MethodCodeGenerator {

  private List<MethodBodyCodeGenerator> bodyCodeGenerators

  /**
   * Class constructors.
   *
   * @param bodyCodeGenerators
   */
  MethodCodeGenerator(List<MethodBodyCodeGenerator> bodyCodeGenerators) {
    this.bodyCodeGenerators = bodyCodeGenerators
  }

  /**
   * Generates the source code for the method.
   *
   * @param method a method for which the source code is to be generated
   * @return the string with the full definition of the method
   * @throws MethodBodyCodeGeneratorNotFoundException if the class instantiated with an empty
   *         list of MethodBodyCodeGenerator or none of them is appropriate for the method.
   */
  String generateCode(Method method) {
    def codeBuilder = new StringBuilder()
    codeBuilder.with {
      append(methodSignature(method) + '\n')
      append(methodBody(method) + '\n')
      append('}')
    }
    return codeBuilder.toString()
  }

  private static String methodSignature(Method method) {
    def accessModifier = StringUtils.emptyOrAddSpace(method.accessModifier.toGroovyCode())
    def staticModifier = method.isStatic() ? 'static ' : ''
    def returnType = "${method.returnType.simpleName} "
    def parameters = method.parameters.collect { it.simpleDeclaration }.join(', ')

    return "$accessModifier$staticModifier$returnType$method.name($parameters) {"
  }

  private String methodBody(Method method) {
    def bodyCodeGenerator = bodyCodeGenerators.find { it.supports(method) }
    if (!bodyCodeGenerator) {
      throw new MethodBodyCodeGeneratorNotFoundException(method)
    }
    return bodyCodeGenerator.generateCode(method)
  }
}
