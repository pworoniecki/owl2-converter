package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.method.Setter
import owl2converter.groovymetamodel.type.ListType
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE

@Component
class FunctionalReflexiveAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    return setter.attribute.characteristics.containsAll([FUNCTIONAL, REFLEXIVE])
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificCode(Setter setter) {
    String setterParameterName = setter.parameter.name
    if (setter.parameter.type instanceof ListType) {
      throw new IllegalStateException("Setter of functional attribute cannot has list type")
    }
    return Optional.of("throw new ReflexiveAttributeException(\"Cannot set \$${setterParameterName} " +
        "as it is not 'this' instance - it would break reflexive attribute rule\")",)
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 10
  }
}
