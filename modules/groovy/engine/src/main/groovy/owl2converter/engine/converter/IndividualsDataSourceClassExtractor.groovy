package owl2converter.engine.converter

import org.semanticweb.owlapi.model.*
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.exception.MissingIRIRemainderException
import owl2converter.engine.exception.NotSupportedOwlElementException
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.CustomMethod
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.CollectionType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType

import static owl2converter.groovymetamodel.AccessModifier.PRIVATE
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC

/**
 * Class responsible for mapping OWL individuals defined in an OWL ontology to Class Groovy metamodel.
 */
@Component
class IndividualsDataSourceClassExtractor {

  static final String GENERATED_CLASS_NAME = 'IndividualsDataSource'

  private static final String INITIALIZE_INDIVIDUALS_METHOD_NAME = 'initializeIndividualsRepository'
  private static final String INDIVIDUAL_WRAPPER_CLASS_NAME = 'IndividualWrapper'

  private Logger logger
  private String basePackage
  private String classPackage
  private String individualWrapperClassPackage
  private OWLLiteralCodeGenerator owlLiteralCodeGenerator
  private IndividualsTopologicalSorter individualsTopologicalSorter

  /**
   * Class constructor.
   *
   * @param configuration
   * @param constructorsExtractor
   */
  @Autowired
  IndividualsDataSourceClassExtractor(Logger logger, ApplicationConfiguration configuration,
                                      OWLLiteralCodeGenerator owlLiteralCodeGenerator,
                                      IndividualsTopologicalSorter individualsTopologicalSorter) {
    this.logger = logger
    this.basePackage = configuration.generator.generatedClasses.basePackage
    this.classPackage = basePackage
    this.individualWrapperClassPackage = basePackage + '.individual'
    this.owlLiteralCodeGenerator = owlLiteralCodeGenerator
    this.individualsTopologicalSorter = individualsTopologicalSorter
  }

  /**
   * Extracts information about OWL classes and map them into Groovy class metamodels
   *
   * @param traits a mapping between OWL classes and lists of trait metamodels created for them
   * @return the list of Groovy class metamodels created containing information extracted from OWL classes contained in {@code traits}
   * @throws MissingIRIRemainderException if an OWL class has no name
   */
  Class extract(OWLOntology ontology, List<Class> classes) {
    List<OWLNamedIndividual> individuals = ontology.individualsInSignature().findAll()
    List<Method> individualFactoryMethods = individualsTopologicalSorter.sort(ontology, individuals).collect {
      createIndividualFactoryMethod(ontology, it, classes)
    }
    Method getIndividualsMethod = createInitializeIndividualsMethod(individualFactoryMethods)
    return new Class.Builder(classPackage, GENERATED_CLASS_NAME)
            .withConstructors([new Constructor(accessModifier: PRIVATE, isStatic: false, parameters: [])])
            .withMethods(individualFactoryMethods + getIndividualsMethod)
            .buildClass()
  }

  private static Method createInitializeIndividualsMethod(List<Method> individualFactoryMethods) {
    String methodCode = individualFactoryMethods.collect {
      "IndividualRepository.addIndividual(${it.name}())"
    }.join('\n')
    return new CustomMethod.Builder(INITIALIZE_INDIVIDUALS_METHOD_NAME, methodCode)
            .withAccessModifier(PUBLIC)
            .asStatic()
            .withReturnType(VoidType.INSTANCE)
            .buildMethod()
  }

  private Type getIndividualWrapperClassType() {
    return new SimpleType(individualWrapperClassPackage, INDIVIDUAL_WRAPPER_CLASS_NAME)
  }

  private Method createIndividualFactoryMethod(OWLOntology ontology, OWLNamedIndividual individual, List<Class> classes) {
    String individualName = getIndividualName(individual)
    String methodName = "create${individualName.capitalize()}"
    Class individualClass = findIndividualClass(ontology, individual, classes)
    String methodCode = createIndividualFactoryMethodCode(ontology, individual, individualName, individualClass)
    return new CustomMethod.Builder(methodName, methodCode)
            .withAccessModifier(PRIVATE)
            .asStatic()
            .withParameters([])
            .withReturnType(getIndividualWrapperClassType())
            .buildMethod()
  }

  private String createIndividualFactoryMethodCode(OWLOntology ontology, OWLNamedIndividual individual,
                                                          String individualName, Class individualClass) {
    Trait anyImplementedTrait = individualClass.implementedTraits.first()
    String individualInstanceVariable = 'individualInstance'
    Map<String, List<String>> propertyValues = getPropertyValues(ontology, individual, anyImplementedTrait)
    List<String> propertyValuesCodeLines = propertyValues.collect {
      propertyValue -> propertyValue.value.collect { "set${propertyValue.key.capitalize()}($it)" }
    }.flatten() as List<String>
    return ([
            "${individualClass.name} $individualInstanceVariable = ${individualClass.name}.createInstance()".toString()
    ] + (propertyValuesCodeLines.isEmpty() ? [] :
            ( ["${individualInstanceVariable}.with {".toString()] + propertyValuesCodeLines + '}')) +
    "return new ${INDIVIDUAL_WRAPPER_CLASS_NAME}('$individualName', $individualInstanceVariable)")
            .join('\n')
  }

  private Map<String, List<String>> getPropertyValues(OWLOntology ontology, OWLNamedIndividual individual,
                                                             Trait individualType) {
    Map<String, List<String>> propertyValues = [:]
    fillObjectPropertyValues(ontology, individual, individualType, propertyValues)
    fillDataPropertyValues(ontology, individual, individualType, propertyValues)
    return propertyValues
  }

  private static void fillObjectPropertyValues(OWLOntology ontology, OWLNamedIndividual individual, Trait individualType,
                                               Map<String, List<String>> propertyValues) {
    List<OWLObjectPropertyAssertionAxiom> objectPropertyAssertionAxioms =
            ontology.objectPropertyAssertionAxioms(individual).findAll() as List<OWLObjectPropertyAssertionAxiom>
    Map<Attribute, List<OWLNamedIndividual>> objectPropertyValues = objectPropertyAssertionAxioms.groupBy { it.property }
            .collectEntries {[(findIndividualAttribute(it.key as OWLObjectProperty, individual, individualType)): it.value*.object]}
    objectPropertyValues.forEach { attribute, individuals ->
      propertyValues.put(attribute.name, individuals.collect {
        String valueCode = generateCodeReferringToIndividual(it)
        return attribute.valueType instanceof CollectionType ? "[$valueCode]" : valueCode
      })
    }
  }

  private static Attribute findIndividualAttribute(OWLProperty property, OWLNamedIndividual individual, Trait individualType) {
    String attributeName = property.getIRI().remainder
            .orElseThrow { new MissingIRIRemainderException("Missing name of OWL object property: $property") }
    Attribute attribute = individualType.attributes.find { it.name == attributeName }
    if (attribute == null) {
      throw new NotSupportedOwlElementException("Individual refers to attribute not declared directly for its type. " +
              "Referred property: $property for individual: $individual")
    }
    return attribute
  }

  private static String generateCodeReferringToIndividual(OWLNamedIndividual individual) {
    String individualName = getIndividualName(individual)
    return "IndividualRepository.getIndividual('$individualName').individualInstance"
  }

  private static String getIndividualName(OWLNamedIndividual individual) {
    individual.getIRI().remainder.orElseThrow { new MissingIRIRemainderException("Missing OWL individual name: $individual") }
  }

  private void fillDataPropertyValues(OWLOntology ontology, OWLNamedIndividual individual, Trait individualType,
                                             Map<String, List<String>> propertyValues) {
    List<OWLDataPropertyAssertionAxiom> dataPropertyAssertionAxioms =
            ontology.dataPropertyAssertionAxioms(individual).findAll() as List<OWLDataPropertyAssertionAxiom>
    Map<Attribute, List<OWLLiteral>> dataPropertyValues = dataPropertyAssertionAxioms.groupBy { it.property }
            .collectEntries {[(findIndividualAttribute(it.key as OWLDataProperty, individual, individualType)): it.value*.object]}
    dataPropertyValues.forEach { attribute, literals ->
      propertyValues.put(attribute.name, literals.collect {
        String valueCode = owlLiteralCodeGenerator.generateCode(it)
        return attribute.valueType instanceof CollectionType ? "[$valueCode]" : valueCode
      })
    }
  }

  private Class findIndividualClass(OWLOntology ontology, OWLIndividual individual, List<Class> classes) {
    OWLClassAssertionAxiom individualClassAxiom = findIndividualClassAxiom(ontology, individual)
    OWLClassExpression individualClass = Optional.ofNullable(individualClassAxiom.getClassExpression())
            .orElseThrow { new NotSupportedOwlElementException("Individual '$individual' cannot be converted because its class has not been declared") }
    if (!(individualClass instanceof OWLClass)) {
      throw new NotSupportedOwlElementException("Cannot parse class of individual '$individual'. " +
              "Its type '${individualClass.class.simpleName}' is not supported")
    }
    return classes.find { it.name == extractClassName(individualClass as OWLClass) }
  }

  private static String extractClassName(OWLClass owlClass) {
    return owlClass.getIRI().remainder
            .orElseThrow { new MissingIRIRemainderException("Missing OWL class name: $owlClass") }
  }

  private OWLClassAssertionAxiom findIndividualClassAxiom(OWLOntology ontology, OWLIndividual individual) {
    def classAssertionAxioms = ontology.classAssertionAxioms(individual).findAll() as List<OWLClassAssertionAxiom>
    if (classAssertionAxioms.size() == 1) {
      return classAssertionAxioms.first()
    }
    List<OWLClassAssertionAxiom> classAssertionAxiomsWithoutThing =
            classAssertionAxioms.findAll { !it.classExpression.isOWLThing() }
    if (classAssertionAxiomsWithoutThing.isEmpty()) {
      throw new NotSupportedOwlElementException("No ClassAssertion axiom found for individual '$individual'.")
    }
    if (classAssertionAxiomsWithoutThing.size() > 1) {
      logger.warn("More than one ClassAssertion axiom found for individual '$individual'. " +
              "This is not supported at the moment so only the first one will be used as individual's class.")
    }
    return classAssertionAxiomsWithoutThing.first()
  }
}
