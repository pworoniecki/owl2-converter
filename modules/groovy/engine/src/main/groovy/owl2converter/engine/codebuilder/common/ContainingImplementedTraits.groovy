package owl2converter.engine.codebuilder.common

import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.type.SimpleType

/**
 * A trait responsible for imports required to support implemented trait types.
 */
trait ContainingImplementedTraits implements ContainingImports {

  /**
   * Appends imports of implemented trait types. Does not append imports for traits that are not required to be imported
   * (traits from {@code ownerPackageName} package or imported by default in Groovy language).
   *
   * @param implementedTraits a list of traits to be imported (if the import is necessary)
   * @param ownerPackageName a name of package with import clause
   */
  void appendImplementedTraitImports(List<Trait> implementedTraits, String ownerPackageName) {
    implementedTraits.findAll { it.packageName != ownerPackageName && !new SimpleType(it).isImportedByDefault() }
        .each { appendClassImport(it.fullName) }
  }
}
