package owl2converter.engine.codebuilder.common

import owl2converter.groovymetamodel.Annotation
import owl2converter.groovymetamodel.type.Type

/**
 * A trait for appending annotations with necessary imports
 */
trait ContainingAnnotations implements ContainingCode, ContainingImports {

  /**
   * Appends imports of types required by annotations
   *
   * @param annotations a list of annotations to be appended
   */
  void appendAnnotationTypeImports(List<Annotation> annotations) {
    List<Type> uniqueParameterTypes = annotations*.type.unique()
    appendImports(uniqueParameterTypes, getOwnerPackageName())
  }

  /**
   * Appends a list of annotations with their parameters
   *
   * @param annotations a list of annotations to be appended
   */
  void appendAnnotations(List<Annotation> annotations) {
    annotations.each { appendAnnotation(it) }
  }

  /**
   * Appends an annotation with its parameters
   *
   * @param annotation an annotation to be appended
   */
  void appendAnnotation(Annotation annotation) {
    def parametersCodePart = annotation.parameters ? "([${annotation.parameters*.toString().join(', ')}])" : ''
    appendCode("@${annotation.type.simpleName}$parametersCodePart")
  }
}
