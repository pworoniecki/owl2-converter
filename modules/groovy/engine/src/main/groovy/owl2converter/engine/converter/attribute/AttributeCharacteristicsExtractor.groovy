package owl2converter.engine.converter.attribute

import org.semanticweb.owlapi.model.OWLAsymmetricObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLFunctionalObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLInverseFunctionalObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLIrreflexiveObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLReflexiveObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLTransitiveObjectPropertyAxiom
import owl2converter.groovymetamodel.attribute.AttributeCharacteristic
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLSymmetricObjectPropertyAxiom
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.ASYMMETRIC
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.INVERSE_FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.IRREFLEXIVE
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.SYMMETRIC
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE

/**
 * A class responsible for extraction of property characteristics from OWL ontology and their translation to Groovy meta-model.
 * OWL property characteristics are represented as instances of {@link AttributeCharacteristic} in Groovy meta-model.
 */
@Component
class AttributeCharacteristicsExtractor {

  private static Map<AttributeCharacteristic, AxiomsExtractor> CHARACTERISTIC_TO_OBJECT_PROPERTY_AXIOMS_EXTRACTOR = [
      (SYMMETRIC)         : AttributeCharacteristicsExtractor.&extractSymmetricPropertyAxioms,
      (ASYMMETRIC)        : AttributeCharacteristicsExtractor.&extractAsymmetricPropertyAxioms,
      (TRANSITIVE)        : AttributeCharacteristicsExtractor.&extractTransitivePropertyAxioms,
      (FUNCTIONAL)        : AttributeCharacteristicsExtractor.&extractFunctionalPropertyAxioms,
      (INVERSE_FUNCTIONAL): AttributeCharacteristicsExtractor.&extractInverseFunctionalPropertyAxioms,
      (REFLEXIVE)         : AttributeCharacteristicsExtractor.&extractReflexivePropertyAxioms,
      (IRREFLEXIVE)       : AttributeCharacteristicsExtractor.&extractIrreflexivePropertyAxioms
  ].collectEntries { key, value -> [(key): value as AxiomsExtractor] } as Map<AttributeCharacteristic, AxiomsExtractor>

  /**
   * Returns a list of OWL object property characteristics.
   *
   * @param property an object property for which the characteristics are to be found
   * @param ontology an OWL ontology in which the property is defined
   * @return the list of property characteristics defined in the ontology for the property
   */
  List<AttributeCharacteristic> extract(OWLObjectProperty property, OWLOntology ontology) {
    return CHARACTERISTIC_TO_OBJECT_PROPERTY_AXIOMS_EXTRACTOR
        .findAll { characteristic, extractor -> extractor.hasAnyAxioms(property, ontology) }
        .collect { characteristic, extractor -> characteristic }
  }

  /**
   * Returns a list of OWL data property characteristics.
   * It may be either single-element list (with functional characteristic) or empty one
   * as there are no other possible characteristics of OWL data properties.
   *
   * @param property an OWL data property to be checked against functionality characteristic
   * @param ontology an OWL ontology in which the property is defined
   * @return the empty list if the property is not functional; [{@link AttributeCharacteristic#FUNCTIONAL}] list otherwise
   */
  List<AttributeCharacteristic> extract(OWLDataProperty property, OWLOntology ontology) {
    return ontology.functionalDataPropertyAxioms(property).count() > 0 ? [FUNCTIONAL] : []
  }

  private static List<OWLSymmetricObjectPropertyAxiom> extractSymmetricPropertyAxioms(OWLObjectProperty property,
                                                                                      OWLOntology ontology) {
    return ontology.symmetricObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLAsymmetricObjectPropertyAxiom> extractAsymmetricPropertyAxioms(OWLObjectProperty property,
                                                                                        OWLOntology ontology) {
    return ontology.asymmetricObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLTransitiveObjectPropertyAxiom> extractTransitivePropertyAxioms(OWLObjectProperty property,
                                                                                        OWLOntology ontology) {
    return ontology.transitiveObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLFunctionalObjectPropertyAxiom> extractFunctionalPropertyAxioms(OWLObjectProperty property,
                                                                                        OWLOntology ontology) {
    return ontology.functionalObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLInverseFunctionalObjectPropertyAxiom> extractInverseFunctionalPropertyAxioms(OWLObjectProperty property,
                                                                                                      OWLOntology ontology) {
    return ontology.inverseFunctionalObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLReflexiveObjectPropertyAxiom> extractReflexivePropertyAxioms(OWLObjectProperty property,
                                                                                      OWLOntology ontology) {
    return ontology.reflexiveObjectPropertyAxioms(property).findAll()
  }

  private static List<OWLIrreflexiveObjectPropertyAxiom> extractIrreflexivePropertyAxioms(OWLObjectProperty property,
                                                                                                                                OWLOntology ontology) {
    return ontology.irreflexiveObjectPropertyAxioms(property).findAll()
  }

  private trait AxiomsExtractor {
    abstract List<OWLObjectPropertyAxiom> extractAxioms(OWLObjectProperty property, OWLOntology ontology)

    boolean hasAnyAxioms(OWLObjectProperty property, OWLOntology ontology) {
      return !extractAxioms(property, ontology).isEmpty()
    }
  }
}
