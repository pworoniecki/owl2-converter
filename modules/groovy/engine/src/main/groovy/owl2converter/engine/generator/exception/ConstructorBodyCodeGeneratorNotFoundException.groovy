package owl2converter.engine.generator.exception

import owl2converter.groovymetamodel.Class

class ConstructorBodyCodeGeneratorNotFoundException extends Exception {

  ConstructorBodyCodeGeneratorNotFoundException(Class constructorOwner) {
    super("Cannot generate body code of constructor because there is no code generator " +
        "that supports constructor defined for class $constructorOwner")
  }
}
