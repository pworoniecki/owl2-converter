package owl2converter.engine.generator.method

import owl2converter.groovymetamodel.method.CustomMethod
import org.springframework.stereotype.Component

/**
 * Generates body for a custom method.
 */
@Component
class GenericMethodBodyCodeGenerator implements MethodBodyCodeGenerator<CustomMethod> {

  /**
   * Generates the body of a custom method.
   *
   * @param method a custom method for which the code is to be generated
   * @return the string contained in method definition (i.e. source code of method without its signature)
   */
  @Override
  String generateCode(CustomMethod method) {
    return method.bodyCode
  }

  /**
   * Returns {@link CustomMethod} as a method for which the generator can be used
   *
   * @return GenericMethod meta-class
   */
  @Override
  Class<CustomMethod> getSupportedClass() {
    return CustomMethod
  }
}
