package owl2converter.engine

import commoncodemetamodel.BinaryFile
import commoncodemetamodel.Element
import commoncodemetamodel.Folder
import commoncodemetamodel.TextFile
import commoncodemetamodel.tools.CommonCodeMetamodelReader
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.exception.FileAccessException
import net.lingala.zip4j.core.ZipFile
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import java.nio.file.Files
import java.nio.file.Path

/**
 * A class that reads folders and files structure of translation result code base (i.e. constant set of files present in each translation result) into common code metamodel format (i.e. set of {@link Element} class instances).
 */
@Component
class ResultCodeBaseMetamodelExtractor {

  private static final String RESULT_CODE_BASE_FILE = 'generatedCodeBase.zip'
  private static final String RESULT_CODE_BASE_PACKAGE_PLACEHOLDER = '[PACKAGE_PLACEHOLDER]'
  private static final Set<String> TEXT_FILE_EXTENSIONS = ['groovy', 'gradle']

  private String generatedCodePackage
  private CommonCodeMetamodelReader codeMetamodelReader

  @Autowired
  ResultCodeBaseMetamodelExtractor(ApplicationConfiguration applicationConfiguration, CommonCodeMetamodelReader codeMetamodelReader) {
    this.generatedCodePackage = applicationConfiguration.generator.generatedClasses.basePackage
    this.codeMetamodelReader = codeMetamodelReader
  }

  /**
   * Reads folders and files structure of translation result code base (i.e. constant set of files present in each translation result) into common code metamodel format (i.e. set of {@link Element} class instances).
   *
   * @return translation result code base files in form of common code metamodel
   */
  Set<Element> extractResultCodeMetamodel() {
    Path codeBaseFilesDirectory = extractCodeBaseFilesFromZip()
    Folder codeBaseFilesCodeMetamodel = codeMetamodelReader.parseFolder(codeBaseFilesDirectory, TEXT_FILE_EXTENSIONS)
    return codeBaseFilesCodeMetamodel.elements.collect { resolvePackagePlaceholder(it) }
  }

  private Path extractCodeBaseFilesFromZip() {
    Path tempDirectory = Files.createTempDirectory('codebase')
    File tempZipCodeBaseFile = saveCodeBaseZipFile(tempDirectory)
    def zipFile = new ZipFile(tempZipCodeBaseFile)
    zipFile.extractAll(tempDirectory.toFile().absolutePath)
    zipFile.file.delete()
    return tempDirectory
  }

  private File saveCodeBaseZipFile(Path tempDirectory) {
    InputStream fileStream = getClass().getResourceAsStream("/$RESULT_CODE_BASE_FILE")
    Path tempZipCodeBaseFile = tempDirectory.resolve(RESULT_CODE_BASE_FILE)
    if (!Files.isWritable(tempZipCodeBaseFile) && !Files.createFile(tempZipCodeBaseFile)) {
      throw new FileAccessException("Cannot create file: $tempZipCodeBaseFile")
    }
    tempZipCodeBaseFile.bytes = fileStream.bytes
    return tempZipCodeBaseFile.toFile()
  }

  private Element resolvePackagePlaceholder(Element element) {
    return element instanceof commoncodemetamodel.File ? resolveFilePackagePlaceholder(element) :
        resolveFolderPackagePlaceholder(element as Folder)
  }

  private Element resolveFilePackagePlaceholder(commoncodemetamodel.File file) {
    if (file instanceof BinaryFile) {
      return file
    }
    TextFile textFile = file as TextFile
    String newContent = textFile.content.replace(RESULT_CODE_BASE_PACKAGE_PLACEHOLDER, generatedCodePackage)
    return new TextFile(textFile.name, textFile.extension, newContent, textFile.encoding)
  }

  private Element resolveFolderPackagePlaceholder(Folder folder) {
    Set<Element> newElements = folder.elements.collect { resolvePackagePlaceholder(it) }
    if (folder.name == RESULT_CODE_BASE_PACKAGE_PLACEHOLDER) {
      String[] consecutiveFolderNames = generatedCodePackage.split('\\.')
      return createFolderHierarchy(consecutiveFolderNames, newElements)
    }
    return new Folder(folder.name, newElements)
  }

  private static Folder createFolderHierarchy(String[] consecutiveFolderNames, Set<Element> deepestFolderElements) {
    Folder lastFolder = null
    consecutiveFolderNames.reverse().each {
      lastFolder = lastFolder == null ? new Folder(it, deepestFolderElements) : new Folder(it, [lastFolder].toSet())
    }
    return lastFolder
  }
}
