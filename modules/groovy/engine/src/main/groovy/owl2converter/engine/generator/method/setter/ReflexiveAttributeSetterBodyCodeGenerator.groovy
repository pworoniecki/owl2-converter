package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.AttributeCharacteristic
import owl2converter.groovymetamodel.method.Setter
import owl2converter.groovymetamodel.type.ListType
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE

@Component
class ReflexiveAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    List<AttributeCharacteristic> characteristics = setter.attribute.characteristics
    return REFLEXIVE in characteristics && !(FUNCTIONAL in characteristics)
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    String setterParameterName = setter.parameter.name
    List<String> codeLines = setter.attribute.valueType instanceof ListType ?
        generateListAttributeValidationCodeLines(setterParameterName) :
        generateNonListAttributeValidationCodeLines(setterParameterName)
    return Optional.of(generateCode(codeLines))
  }

  private static List<String> generateListAttributeValidationCodeLines(String setterParameterName) {
    return [
        "if (!${setterParameterName}.contains(this)) {",
        "throw new ReflexiveAttributeException(\"Cannot set \$${setterParameterName} as it does not contain " +
            "'this' instance - it would break reflexive attribute rule\")",
        '}',
    ]
  }

  private static List<String> generateNonListAttributeValidationCodeLines(String setterParameterName) {
    return [
        "if (${setterParameterName} != this) {",
        "throw new ReflexiveAttributeException(\"Cannot set \$${setterParameterName} as it is not 'this' instance - " +
            "it would break reflexive attribute rule\")",
        '}',
    ]
  }

  @Override
  Optional<String> generateSpecificCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 1
  }
}
