package owl2converter.engine.generator.constructor

import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.EnumClass
import org.springframework.stereotype.Component

@Component
class EnumClassConstructorBodyCodeGenerator implements ConstructorBodyCodeGenerator {

  static final String INDIVIDUAL_NAME_PARAMETER = 'individualName'

  @Override
  boolean supports(Class constructorOwner) {
    return constructorOwner?.class == EnumClass
  }

  @Override
  String generateCode(Constructor constructor) {
    def parameterSettersInvocations = constructor.parameters.findAll { it.name != INDIVIDUAL_NAME_PARAMETER }
        .collect { "set${it.name.capitalize()}(${it.name})" }.join('\n')

    return """addInstance(this)
             |IndividualRepository.addIndividual($INDIVIDUAL_NAME_PARAMETER, this)
             |$parameterSettersInvocations""".stripMargin()
  }
}
