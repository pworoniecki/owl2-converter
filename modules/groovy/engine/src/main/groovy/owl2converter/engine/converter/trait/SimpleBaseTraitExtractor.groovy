package owl2converter.engine.converter.trait

import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.stereotype.Component
import owl2converter.engine.converter.attribute.AttributeMethodsFactory

/**
 * A class responsible for
 */
@Component
class SimpleBaseTraitExtractor {

  private AttributeMethodsFactory attributeMethodsFactory
  private ClassPropertyAttributesExtractor classPropertyAttributesExtractor
  private TraitBuilderFactory traitBuilderFactory
  private AnnotationsExtractor annotationsExtractor

  /**
   * Class constructor.
   *
   * @param attributeMethodsFactory
   * @param classPropertyAttributesExtractor
   * @param traitBuilderFactory
   * @param annotationsExtractor
   */
  SimpleBaseTraitExtractor(AttributeMethodsFactory attributeMethodsFactory,
                           ClassPropertyAttributesExtractor classPropertyAttributesExtractor,
                           TraitBuilderFactory traitBuilderFactory, AnnotationsExtractor annotationsExtractor) {
    this.attributeMethodsFactory = attributeMethodsFactory
    this.traitBuilderFactory = traitBuilderFactory
    this.classPropertyAttributesExtractor = classPropertyAttributesExtractor
    this.annotationsExtractor = annotationsExtractor
  }

  /**
   * Maps an OWL class from an OWL ontology into a base trait Groovy meta-model.
   *
   * @param ontology an ontology in which the OWL class is defined
   * @param owlClass an OWL class to be mapped to a trait
   * @param attributes a mapping between OWL properties from OWL ontology and their Groovy attributes (part of Groovy metamodel)
   * @return the trait being the representation of OWL class filled with basic information: name, annotations, attributes and methods
   */
  Trait extract(OWLOntology ontology, OWLClass owlClass, Map<OWLProperty, Attribute> attributes) {
    List<Attribute> propertyAttributes = classPropertyAttributesExtractor.extractClassAttributes(ontology, owlClass, attributes)
    return traitBuilderFactory.createTraitBuilder(owlClass, ontology)
        .withAnnotations(annotationsExtractor.extract(owlClass, ontology))
        .withAttributes(propertyAttributes)
        .withMethods(extractPropertyMethods(propertyAttributes)).buildTrait()
  }

  private List<Method> extractPropertyMethods(List<Attribute> attributes) {
    return attributes.collect { attributeMethodsFactory.createMethods(it) }.flatten() as List<Method>
  }
}
