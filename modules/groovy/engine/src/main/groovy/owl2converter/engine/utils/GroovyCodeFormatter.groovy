package owl2converter.engine.utils

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.configuration.model.ApplicationConfiguration

/**
 * Utility class used for groovy code formatting according to the indentation settings kept by
 * {@link owl2converter.engine.configuration.model.ApplicationConfiguration}
 */
@Component
class GroovyCodeFormatter {

  private final int indentationStep
  private int indentationLevel

  /**
   * Class constructor
   * @param applicationConfiguration the object containing application configuration
   */
  @Autowired
  GroovyCodeFormatter(ApplicationConfiguration applicationConfiguration) {
    this.indentationStep = applicationConfiguration.generator.generatedClasses.indentationStep
  }

  /**
   * Formats a source code given by parameter using the indentation level from application configuration
   *
   * @param sourceCode the source code to be formatted
   * @return the formatted source code
   */
  String format(String sourceCode) {
    this.indentationLevel = 0
    def codeLines = sourceCode.readLines()
    return appendIndentation(codeLines).join('\n')
  }

  private List<String> appendIndentation(List<String> lines) {
    lines.eachWithIndex {
      line, index ->
        if (line.endsWith('}') && !line.contains('{')) {
          this.indentationLevel--
        }
        lines[index] = appendIndentation(line)
        if (line.endsWith('{')) {
          this.indentationLevel++
        }
    }
    return lines
  }

  private String appendIndentation(String line) {
    def indentation = ' ' * this.indentationLevel * this.indentationStep
    return indentation + line
  }
}
