package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.DirectSetter
import owl2converter.groovymetamodel.method.Setter
import owl2converter.groovymetamodel.type.ListType
import org.springframework.stereotype.Component

@Component
class InverseAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    return setter.attribute.isInverseAttribute()
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificCode(Setter setter) {
    String attributeName = setter.attribute.name
    String capitalizedAttributeName = attributeName.capitalize()
    String setterParameterName = setter.parameters.first().name
    List<Attribute> inverseAttributes = setter.attribute.findInverseAttributes()
    List<String> codeLines = setter.attribute.valueType instanceof ListType ?
        generateCodeLinesForListAttribute(capitalizedAttributeName, attributeName, setterParameterName, inverseAttributes) :
        generateCodeLinesForNonListAttribute(attributeName, setterParameterName, inverseAttributes
    )
    return Optional.of(generateCode(codeLines))
  }

  private static List<String> generateCodeLinesForListAttribute(String capitalizedAttributeName, String attributeName,
                                                                String setterParameterName, List<Attribute> inverseAttributes) {
    return [
        "def old$capitalizedAttributeName = this.$attributeName ?: []",
        "def new$capitalizedAttributeName = $setterParameterName ?: []",
        "def removed$capitalizedAttributeName = old$capitalizedAttributeName - new$capitalizedAttributeName",
        "def added$capitalizedAttributeName = new$capitalizedAttributeName - old$capitalizedAttributeName"
    ] + removeSelfFromListAttributes("removed$capitalizedAttributeName", inverseAttributes) + [
        "added${capitalizedAttributeName}.each {",
    ] + addSelfToAttributes(inverseAttributes) + [
        '}',
        "this.$attributeName = $setterParameterName"
    ] as List<String>
  }

  private static List<String> removeSelfFromListAttributes(String removedValuesVariableName, List<Attribute> attributes) {
    List<Attribute> listTypeAttributes = attributes.findAll { it.valueType instanceof ListType }
    if (listTypeAttributes.isEmpty()) {
      return []
    }
    return ["${removedValuesVariableName}.each {"] +
        listTypeAttributes.collect { attribute -> "it.${attribute.name}.remove(this)" } + '}'
  }

  private static List<String> addSelfToAttributes(List<Attribute> attributes) {
    return attributes.collect { attribute ->
      def attributeDirectSetterName = DirectSetter.getName(attribute)
      return "it.$attributeDirectSetterName(${getDirectSetterParametersInvokedFromListAttribute(attribute)})"
    }
  }

  private static String getDirectSetterParametersInvokedFromListAttribute(Attribute attribute) {
    if (attribute.valueType instanceof ListType) {
      String attributeName = attribute.name
      return "it.$attributeName ? it.$attributeName + this : [this]"
    }
    return 'this'
  }

  private static List<String> generateCodeLinesForNonListAttribute(String attributeName, String setterParameterName,
                                                                   List<Attribute> inverseAttributes) {
    return inverseAttributes.collect {
      "${setterParameterName}.${DirectSetter.getName(it)}(${getDirectSetterParametersInvokedFromNonListAttribute(it)})"
    } + "this.$attributeName = $setterParameterName"
  }

  private static String getDirectSetterParametersInvokedFromNonListAttribute(Attribute attribute) {
    return attribute.valueType instanceof ListType ? '[this]' : 'this'
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 8
  }
}
