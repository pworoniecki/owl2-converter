package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.AttributeCharacteristic
import owl2converter.groovymetamodel.method.Setter
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE

@Component
class FunctionalAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    List<AttributeCharacteristic> characteristics = setter.attribute.characteristics
    return FUNCTIONAL in characteristics && !(REFLEXIVE in characteristics)
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 5
  }
}
