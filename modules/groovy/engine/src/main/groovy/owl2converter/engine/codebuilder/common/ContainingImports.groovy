package owl2converter.engine.codebuilder.common

import owl2converter.groovymetamodel.type.Type

interface ContainingImports {

  /**
   * Appends imports for all types from given list to a class/trait defined in given owner package.
   *
   * @param types a list of types to be imported
   * @param ownerPackage a package name of import's owner (class or trait where imports are to be added)
   */
  abstract void appendImports(List<Type> types, String ownerPackage)

  /**
   * Appends imports of a class.
   *
   * @param classFullName a qualified name of class to be imported
   */
  abstract void appendClassImport(String classFullName)

  /**
   * Appends imports of a package in the form packageName.*.
   *
   * @param packageName a package which content is to be imported
   */
  abstract void appendPackageImport(String packageName)

  /**
   * Returns a name of an owner package.
   *
   * @return the package name
   */
  abstract String getOwnerPackageName()
}
