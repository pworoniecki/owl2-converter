package owl2converter.engine.configuration

import org.yaml.snakeyaml.Yaml
import owl2converter.engine.configuration.model.ApplicationConfiguration


class ApplicationConfigurationReader {

  /**
   * Utility method which reads a configuration file in yml format and returns a corresponding object structure.
   *
   * @param configurationFilePath an absolute path to a configuration file
   * @return application configuration object storing all configuration parameters
   */
  static ApplicationConfiguration readConfiguration(String configurationFilePath) {
    def configurationFileStream = this.classLoader.getResourceAsStream(configurationFilePath)
    return new Yaml().loadAs(configurationFileStream, ApplicationConfiguration)
  }
}
