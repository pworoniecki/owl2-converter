package owl2converter.engine.generator.method.removevalue

import owl2converter.groovymetamodel.method.RemoveValueMethod
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE

@Component
class ReflexiveAttributeRemoveValueBodyCodeGenerator implements RemoveValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(RemoveValueMethod method) {
    return REFLEXIVE in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod method) {
    def methodParameterName = method.parameter.name
    def validationCode = generateCode([
        "if ($methodParameterName == this) {",
        "throw new ReflexiveAttributeException(\"Cannot remove value 'this' - " +
            "it would break reflexivity of attribute\")",
        '}',
    ])
    return Optional.of(validationCode)
  }

  @Override
  Optional<String> generateSpecificCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 1
  }
}
