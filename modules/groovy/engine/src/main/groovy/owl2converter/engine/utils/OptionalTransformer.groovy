package owl2converter.engine.utils

import java.util.function.Supplier

import static com.google.common.base.Preconditions.checkNotNull

/**
 * Utility class performing some operations on {@link Optional} class instances (combining multiple of them into single result)
 * @param <T> the parameterized type of {@link Optional} supported by this class, i.e. Optional&lt;T&gt; instances are accepted/returned by methods in this class
 */
class OptionalTransformer<T> {

  private Optional<T> wrappedOptional

  private OptionalTransformer(Optional<T> optional) {
    this.wrappedOptional = optional
  }

  /**
   * Factory method of {@link OptionalTransformer} accepting the first {@link Optional} instance that can be transformed
   *
   * @param optional {@link Optional} instance to be transformed
   * @return {@link OptionalTransformer} instance with the {@code optional} as the element to be transformed
   */
  static <T> OptionalTransformer<T> of(Optional<T> optional) {
    return new OptionalTransformer<T>(checkNotNull(optional))
  }

  /**
   * Transforms wrapped {@link Optional} instance by replacing it with passed {@link Optional} instance if the current one does not contain any value (i.e. it is an empty Optional).
   *
   * @param alternativeOptionalSupplier supplier of {@link Optional} value that will be used if transformer's current {@link Optional} instance is empty
   * @return current transformer instance after applying the specified transformation
   */
  OptionalTransformer<T> or(Supplier<Optional<T>> alternativeOptionalSupplier) {
    if (!wrappedOptional.isPresent()) {
      this.wrappedOptional = alternativeOptionalSupplier.get()
    }
    return this
  }

  /**
   * Returns the result of performed transformations.
   *
   * @return wrapped {@link Optional} instance after all invoked transformations
   */
  Optional<T> getResult() {
    return wrappedOptional
  }
}
