package owl2converter.engine.converter.attribute.domain

import owl2converter.groovymetamodel.type.Type
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * A class responsible for translation of an OWL property domain (data, object) to its equivalent Groovy meta-model type.
 */
@Component
class PropertyDomainExtractor {

  private ObjectPropertyDomainExtractor objectPropertyDomainExtractor
  private DataPropertyDomainExtractor dataPropertyDomainExtractor

  /**
   * Class constructor.
   *
   * @param objectPropertyDomainExtractor an instance of ObjectPropertyDomainExtractor (used for translation of OWL object property's domain)
   * @param dataPropertyDomainExtractor an instance of DataPropertyDomainExtractor (used for translation of OWL data property's domain)
   */
  @Autowired
  PropertyDomainExtractor(ObjectPropertyDomainExtractor objectPropertyDomainExtractor, DataPropertyDomainExtractor dataPropertyDomainExtractor) {
    this.objectPropertyDomainExtractor = objectPropertyDomainExtractor
    this.dataPropertyDomainExtractor = dataPropertyDomainExtractor
  }

  /**
   * Translates an OWL property (either object or data type) domain into its Groovy meta-model type equivalent.
   *
   * @param property an OWL property which is to be translated
   * @param ontology an OWL ontology in which the property is defined in
   * @return the type from Groovy meta-model being a representation of the domain of the OWL property
   * @throws IllegalArgumentException when property is neither OWL object property nor OWL data property
   */
  Type extractDomain(OWLProperty property, OWLOntology ontology) {
    if (property instanceof OWLObjectProperty) {
      return objectPropertyDomainExtractor.extract(property as OWLObjectProperty, ontology)
    }
    if (property instanceof OWLDataProperty) {
      return dataPropertyDomainExtractor.extract(property as OWLDataProperty, ontology)
    }
    throw new IllegalArgumentException("Cannot extract domain of property '$property' because it has unsupported type")
  }
}
