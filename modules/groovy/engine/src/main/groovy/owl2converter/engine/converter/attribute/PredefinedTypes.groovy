package owl2converter.engine.converter.attribute

import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.converter.trait.TraitsExtractor

/**
 * A class storing instances of Groovy meta-model types reused in multiple translations.
 */
@Component
class PredefinedTypes {

  private static final String THING_CLASS_NAME = 'Thing'

  private String packageName
  private Class thingClass
  private Trait thingTrait
  private Type thingTraitType
  private Type thingTraitListType

  /**
   * Class constructor.
   *
   * @param applicationConfiguration an application configuration
   */
  @Autowired
  PredefinedTypes(ApplicationConfiguration applicationConfiguration) {
    packageName = applicationConfiguration.generator.generatedClasses.modelFullPackage
    thingTrait = new Trait.Builder(packageName, THING_CLASS_NAME + TraitsExtractor.TRAIT_NAME_SUFFIX).buildTrait()
    thingClass = new Class.Builder(packageName, THING_CLASS_NAME).withImplementedTraits([thingTrait]).buildClass()
    thingTraitType = new SimpleType(thingTrait)
    thingTraitListType = new ListType(thingTraitType)
  }

  /**
   * Returns Thing class.
   *
   * @return the Thing class
   */
  Class getThingClass() {
    return thingClass
  }

  /**
   * Returns Thing trait.
   *
   * @return the Thing trait
   */
  Trait getThingTrait() {
    return thingTrait
  }

  /**
   * Returns {@link Type} instance representing type of Thing trait returned by {@link PredefinedTypes#getThingTrait} method.
   *
   * @return the type of Thing trait, being equivalent of ThingTrait
   */
  Type getThingTraitType() {
    return thingTraitType
  }

  /**
   * Returns {@link ListType} instance representing list type parametrized by Thing trait type (returned by {@link PredefinedTypes#getThingTrait} method).
   *
   * @return the type being equivalent of List<ThingTrait>
   */
  Type getThingTraitListType() {
    return thingTraitListType
  }
}
