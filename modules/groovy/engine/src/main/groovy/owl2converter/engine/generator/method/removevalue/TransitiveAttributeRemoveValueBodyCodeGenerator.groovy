package owl2converter.engine.generator.method.removevalue

import owl2converter.groovymetamodel.method.RemoveValueMethod
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class TransitiveAttributeRemoveValueBodyCodeGenerator implements RemoveValueBodyCodeGeneratorStrategy {

  @Override
  boolean supports(RemoveValueMethod method) {
    return TRANSITIVE in method.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(RemoveValueMethod method) {
    List<String> codeLines = generateListAttributeCodeLines(method)
    return Optional.of(codeLines.join('\n'))
  }

  private static List<String> generateListAttributeCodeLines(RemoveValueMethod method) {
    String parameterName = method.parameter.name
    List<String> codeLines = [
        "if (!this.${method.attribute.name}.contains($parameterName)) {",
        "throw new TransitiveAttributeException(\"Cannot remove \$$parameterName because it would break transitivity rule\")",
        '}'
    ]
    return codeLines
  }

  @Override
  Optional<String> generateSpecificCode(RemoveValueMethod method) {
    return Optional.empty()
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 4
  }
}
