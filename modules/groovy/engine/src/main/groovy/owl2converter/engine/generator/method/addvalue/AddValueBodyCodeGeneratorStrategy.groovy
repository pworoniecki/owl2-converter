package owl2converter.engine.generator.method.addvalue

import owl2converter.engine.generator.method.MethodBodyCodeGeneratorStrategy
import owl2converter.groovymetamodel.method.AddValueMethod

/**
 * A trait with abstract methods representing trait's code generation strategy for add value method
 */
trait AddValueBodyCodeGeneratorStrategy extends MethodBodyCodeGeneratorStrategy<AddValueMethod> {
}
