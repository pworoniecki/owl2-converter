package owl2converter.engine.generator.constructor

import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import org.springframework.stereotype.Component

/**
 * Code generator that generates empty body of default class's constructor
 */
@Component
class GenericClassConstructorBodyCodeGenerator implements ConstructorBodyCodeGenerator {

  private static String EMPTY_CODE = ''

  /**
   * Checks whether the constructor belongs to a class.
   *
   * @param constructorOwner a class for which
   * @return true if {@code constructorOvner} is an instance of {@link Class} type but not an instance of its subtype
   * (e.g. {@link owl2converter.groovymetamodel.EnumClass} is not supported); false otherwise
   */
  @Override
  boolean supports(Class constructorOwner) {
    return constructorOwner?.class == Class
  }

  /**
   * Returns the body of the default class constructor (empty string)
   * @param constructor a constructor for which the body is to be generated
   * @return the body of {@code constructor}
   */
  @Override
  String generateCode(Constructor constructor) {
    return EMPTY_CODE
  }
}
