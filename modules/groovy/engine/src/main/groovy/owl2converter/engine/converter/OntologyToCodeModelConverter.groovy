package owl2converter.engine.converter

import owl2converter.engine.converter.attribute.AttributesExtractor
import owl2converter.engine.converter.trait.TraitsExtractor
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.attribute.Attribute
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * A class responsible for conversion of OWL ontology into Groovy code model.
 */
@Component
class OntologyToCodeModelConverter {

  private AttributesExtractor attributesExtractor
  private ClassesExtractor classesExtractor
  private TraitsExtractor traitsExtractor
  private IndividualsDataSourceClassExtractor individualsDataSourceClassExtractor

  /**
   * Class constructor. Requires components necessary to perform conversion process.
   *
   * @param attributesExtractor
   * @param classesExtractor
   * @param traitsExtractor
   */
  @Autowired
  OntologyToCodeModelConverter(AttributesExtractor attributesExtractor,
                               ClassesExtractor classesExtractor, TraitsExtractor traitsExtractor, IndividualsDataSourceClassExtractor individualsDataSourceClassExtractor) {
    this.attributesExtractor = attributesExtractor
    this.classesExtractor = classesExtractor
    this.traitsExtractor = traitsExtractor
    this.individualsDataSourceClassExtractor = individualsDataSourceClassExtractor
  }

  /**
   * Converts an OWL ontology into a set of Groovy classes and traits wrapped by {@link ObjectOrientedCodeModel}.
   *
   * @param ontology an OWL ontology to be converted to Groovy code model
   * @return an instance of {@link ObjectOrientedCodeModel} keeping together all Groovy classes and traits
   */
  ObjectOrientedCodeModel convert(OWLOntology ontology) {
    Map<OWLProperty, Attribute> attributes = attributesExtractor.extract(ontology)
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, attributes)
    List<Class> classes = classesExtractor.extract(traits)
    Class individualsDataSourceClass = individualsDataSourceClassExtractor.extract(ontology, classes)
    return new ObjectOrientedCodeModel(classes, traits.values().flatten().toList(), individualsDataSourceClass)
  }
}
