package owl2converter.engine.converter.attribute.range

import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.exception.MissingIRIRemainderException
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import org.semanticweb.owlapi.model.IRI
import org.semanticweb.owlapi.model.OWLDatatype
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.xml.datatype.XMLGregorianCalendar

/**
 * A class responsible for conversion of OWL data types (e.g. string, boolean) to their Groovy meta-model representations.
 */
@Component
class OWLDatatypeToTypeConverter {

  private static final String OWL_CUSTOM_TYPE_SUBPACKAGE = 'owlSimpleType'
  private static final String OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER = '[PACKAGE_PLACEHOLDER]'
  private static final String XML_SCHEMA_NAMESPACE = 'http://www.w3.org/2001/XMLSchema'
  private static final Map<String, Type> TYPE_MAPPING = [
      string            : new SimpleType(String),
      normalizedString  : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'NormalizedString'),
      token             : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'Token'),
      language          : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'Language'),
      NMTOKEN           : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'NMTOKEN'),
      Name              : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'Name'),
      NCName            : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'NCName'),
      boolean           : new SimpleType(Boolean),
      decimal           : new SimpleType(BigDecimal),
      float             : new SimpleType(Float),
      double            : new SimpleType(Double),
      integer           : new SimpleType(BigInteger),
      nonPositiveInteger: new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'NonPositiveInteger'),
      negativeInteger   : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'NegativeInteger'),
      long              : new SimpleType(Long),
      int               : new SimpleType(Integer),
      short             : new SimpleType(Short),
      byte              : new SimpleType(Byte),
      nonNegativeInteger: new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'NonNegativeInteger'),
      unsignedLong      : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'UnsignedLong'),
      unsignedInt       : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'UnsignedInt'),
      unsignedShort     : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'UnsignedShort'),
      unsignedByte      : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'UnsignedByte'),
      positiveInteger   : new SimpleType(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, 'PositiveInteger'),
      dateTime          : new SimpleType(XMLGregorianCalendar),
      time              : new SimpleType(XMLGregorianCalendar),
      date              : new SimpleType(XMLGregorianCalendar),
      gYearMonth        : new SimpleType(XMLGregorianCalendar),
      gYear             : new SimpleType(XMLGregorianCalendar),
      gMonthDay         : new SimpleType(XMLGregorianCalendar),
      gDay              : new SimpleType(XMLGregorianCalendar),
      gMonth            : new SimpleType(XMLGregorianCalendar)
  ]

  private String generatedCodePackage

  /**
   * Class constructor.
   *
   * @param applicationConfiguration an application configuration instance
   */
  @Autowired
  OWLDatatypeToTypeConverter(ApplicationConfiguration applicationConfiguration) {
    this.generatedCodePackage = applicationConfiguration.generator.generatedClasses.basePackage
  }

  /**
   * Converts an OWL data type into its Groovy meta-model equivalent.
   *
   * @param datatype an OWL data type to be converted
   * @return the Groovy meta-model type equivalent (representation) of the OWL data type
   * @throws MissingIRIRemainderException if the name of the OWL data type is absent in the ontology
   * @throws IllegalArgumentException if the namespace of the OWL data type is different than http://www.w3.org/2001/XMLSchema
   */
  Type convert(OWLDatatype datatype) {
    IRI typeIdentifier = datatype.getIRI()
    assertCorrectTypeNamespace(typeIdentifier)
    String name = typeIdentifier.remainder.orElseThrow {
      new MissingIRIRemainderException("Missing remainder in identifier of datatype: $datatype")
    }
    return getType(name)
  }

  private static void assertCorrectTypeNamespace(IRI typeIdentifier) {
    if (typeIdentifier.namespace != "$XML_SCHEMA_NAMESPACE#") {
      throw new IllegalArgumentException("Datatype must be value from XML Schema namespace " +
          "(http://www.w3.org/2001/XMLSchema). Provided value ($typeIdentifier) is from another namespace: " +
          "(${typeIdentifier.namespace}).")
    }
  }

  private Type getType(String name) {
    Type type = TYPE_MAPPING[name]
    if (type.packageName == OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER) {
      String packageName = type.packageName.replace(OWL_CUSTOM_TYPE_PACKAGE_PLACEHOLDER, generatedCodePackage) +
          '.' + OWL_CUSTOM_TYPE_SUBPACKAGE
      return new SimpleType(packageName, type.simpleName)
    }
    return type
  }
}
