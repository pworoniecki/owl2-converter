package owl2converter.engine.generator.method.common

import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.CustomMethod
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.VoidType
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class TransitiveAttributeHelpersGenerator {

  private static final String GETTER_HELPER_PARAMETER_NAME = 'result'

  /**
   * A factory method to create a custom method instance (with the source code) for a specific attribute.
   * The method creates a helper method only for transitive attributes which sets transitively the value.
   *
   * @param attribute an attribute for which the helper method is to be built
   * @return a helper method as {@link CustomMethod} instance.
   * @throws {@link IllegalArgumentException} if the attribute is not trasitive or is not of list type.
   */
  CustomMethod generateGetterHelperMethod(Attribute attribute) {
    assertSupportedAttribute(attribute)
    def methodCode = generateGetterHelperMethodCode(attribute)
    return new CustomMethod.Builder(getGetterHelperMethodName(attribute), methodCode)
        .withParameters([new Parameter(attribute.valueType, GETTER_HELPER_PARAMETER_NAME)])
        .withReturnType(VoidType.INSTANCE)
        .withAccessModifier(PUBLIC)
        .buildMethod()
  }

  private static void assertSupportedAttribute(Attribute attribute) {
    if (!attribute.characteristics.contains(TRANSITIVE)) {
      throw new IllegalArgumentException('Attribute must be transitive')
    }
    if (!(attribute.valueType instanceof ListType)) {
      throw new IllegalArgumentException('Attribute must be type of list')
    }
  }
  /**
   * Returns the name of a helper method to serve of transitive attributes in the form: '_fillTransitiveAttribute_name'.
   *
   * @param attribute an attribute for which the helper method is to be generated
   * @return the name of the helper method for the attribute
   */
  static String getGetterHelperMethodName(Attribute attribute) {
    return "_fillTransitive${attribute.name.capitalize()}"
  }

  private static String generateGetterHelperMethodCode(Attribute attribute) {
    return [
        "(this.${attribute.name} - $GETTER_HELPER_PARAMETER_NAME).each {",
        "if (!${GETTER_HELPER_PARAMETER_NAME}.contains(it)) {",
        "${GETTER_HELPER_PARAMETER_NAME}.add(it)",
        "it.${getGetterHelperMethodName(attribute)}($GETTER_HELPER_PARAMETER_NAME)",
        '}',
        '}'
    ].join('\n')
  }
}
