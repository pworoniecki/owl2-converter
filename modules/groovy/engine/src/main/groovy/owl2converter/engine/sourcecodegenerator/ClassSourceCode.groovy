package owl2converter.engine.sourcecodegenerator

import owl2converter.groovymetamodel.Class

/**
 * A wrapper class storing together class meta-model instance together with its code translation.
 */
class ClassSourceCode implements SourceCode {

  private Class classModel
  private String sourceCode

  /**
   * Class constructor.
   *
   * @param classModel a class meta-model instance
   * @param sourceCode a string with a source code for classModel
   */
  ClassSourceCode(Class classModel, String sourceCode) {
    this.classModel = classModel
    this.sourceCode = sourceCode
  }

  /**
   * Returns the instance of class meta-model.
   *
   * @return the class meta-model instance.
   */
  Class getClassModel() {
    return classModel
  }

  /**
   * Returns the name of class
   *
   * @return the name of class
   */
  @Override
  String getName() {
    return classModel.name
  }

  /**
   * Returns source code representation of class meta-model instance.
   *
   * @return a string with source code for class meta-model instance
   */
  @Override
  String getSourceCode() {
    return sourceCode
  }
}
