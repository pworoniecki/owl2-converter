package owl2converter.engine.converter.trait

import owl2converter.groovymetamodel.EquivalentTrait
import owl2converter.groovymetamodel.Trait
import org.semanticweb.owlapi.model.ClassExpressionType
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.exception.MissingIRIRemainderException
import owl2converter.engine.exception.NotSupportedOwlElementException

import static owl2converter.engine.converter.trait.TraitsExtractor.TRAIT_NAME_SUFFIX

/**
 * A class responsible for extraction of equivalent traits basing on equivalent class axioms defined in OWL ontology
 */
@Component
class EquivalentTraitsExtractor {

  static final String EQUIVALENT_TRAIT_NAME_SUFFIX = 'Equivalent'

  private String traitPackage

  /**
   * Class constructor.
   *
   * @param configuration an instance of application configuration
   */
  @Autowired
  EquivalentTraitsExtractor(ApplicationConfiguration configuration) {
    traitPackage = configuration.generator.generatedClasses.modelFullPackage
  }

  /**
   * Iterates over all OWL classes in the ontology, extracts for them equivalent OWL classes
   * and for each such equivalent class the method creates an instance of {@link EquivalentTrait}.
   *
   * @param baseTraits a mapping between OWL ontology classes and traits
   * @param ontology an OWL ontology in which the OWL classes are defined
   * @return the mapping between OWL classes and their equivalent traits; if no equivalent class is found for given class, then the equivalent trait is specified as empty {@link Optional}
   * @throws NotSupportedOwlElementException if more than 1 equivalent class is defined for a specific OWL class
   * @throws MissingIRIRemainderException if any OWL class has no name defined in the OWL ontology
   */
  Map<OWLClass, Optional<EquivalentTrait>> extract(Map<OWLClass, Trait> baseTraits, OWLOntology ontology) {
    Set<EquivalentTrait> allEquivalentTraits = []
    return baseTraits.collectEntries {
      OWLClass currentClass = it.key
      Optional<EquivalentTrait> equivalentTrait = extractEquivalentTraits(currentClass, baseTraits, ontology, allEquivalentTraits)
      equivalentTrait.ifPresent { allEquivalentTraits.add(it) }
      return [(currentClass): equivalentTrait]
    } as Map<OWLClass, Optional<EquivalentTrait>>
  }

  private Optional<EquivalentTrait> extractEquivalentTraits(OWLClass owlClass, Map<OWLClass, Trait> baseTraits,
                                                            OWLOntology ontology, Set<EquivalentTrait> existingEquivalentTraits) {
    List<OWLEquivalentClassesAxiom> equivalentClassesAxioms = ontology.equivalentClassesAxioms(owlClass)
        .findAll { it.classExpressions().every { it.classExpressionType == ClassExpressionType.OWL_CLASS } }
    if (equivalentClassesAxioms.isEmpty()) {
      return Optional.empty()
    }
    if (equivalentClassesAxioms.size() > 1) {
      throw new NotSupportedOwlElementException("More than one class is equivalent to class $owlClass. It is not supported")
    }
    OWLClass equivalentClass = equivalentClassesAxioms.first().namedClasses().find { it != owlClass } as OWLClass
    return Optional.of(extractEquivalentTrait(owlClass, equivalentClass, baseTraits, existingEquivalentTraits))
  }

  private EquivalentTrait extractEquivalentTrait(OWLClass class1, OWLClass class2, Map<OWLClass, Trait> baseTraits,
                                                 Set<EquivalentTrait> existingEquivalentTraits) {
    EquivalentTrait existingEquivalentTrait =
        existingEquivalentTraits.find { it.inheritedTraits.sort() == [baseTraits[class1], baseTraits[class2]].sort() }
    return existingEquivalentTrait ?: createEquivalentTrait(class1, class2, baseTraits)
  }

  private EquivalentTrait createEquivalentTrait(OWLClass class1, OWLClass class2, Map<OWLClass, Trait> baseTraits) {
    String equivalentTraitNamePrefix = [extractName(class1), extractName(class2)].sort().join('')
    String equivalentTraitName = equivalentTraitNamePrefix + EQUIVALENT_TRAIT_NAME_SUFFIX + TRAIT_NAME_SUFFIX
    return new EquivalentTrait.Builder(traitPackage, equivalentTraitName)
        .withInheritedTraits([baseTraits[class1], baseTraits[class2]]).buildTrait()
  }

  private static String extractName(OWLClass owlClass) {
    return owlClass.getIRI().remainder.orElseThrow {
      throw new MissingIRIRemainderException("Cannot extract name of OWL class $owlClass")
    }
  }
}
