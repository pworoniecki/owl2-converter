package owl2converter.engine

import commoncodemetamodel.CommonCodeMetamodelConfiguration
import logger.LoggerConfiguration
import owl2converter.engine.configuration.ApplicationConfigurationReader
import owl2converter.engine.configuration.model.ApplicationConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import owl2metamodel.OWL2MetamodelConfiguration

/**
 * Spring context configuration class responsible for providing beans necessary to translate OWL2 to Groovy source code
 */

@Configuration
@ComponentScan(basePackageClasses = OWL2ToGroovyTranslationEngine)
@Import([OWL2MetamodelConfiguration, CommonCodeMetamodelConfiguration, LoggerConfiguration])
class OWL2ToGroovyEngineConfiguration {

  private static final String APPLICATION_CONFIGURATION_FILE_NAME = 'configuration.yml'

  /**
   * Reads a configuration file and creates spring bean from read settings (as an application configuration object).
   *
   * @return application configuration object
   */
  @Bean
  ApplicationConfiguration applicationConfiguration() {
    return ApplicationConfigurationReader.readConfiguration(APPLICATION_CONFIGURATION_FILE_NAME)
  }
}
