package owl2converter.engine.converter

import owl2converter.groovymetamodel.EquivalentTrait
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.UnionTrait
import owl2converter.groovymetamodel.method.CustomMethod
import owl2converter.groovymetamodel.type.PrimitiveType
import owl2converter.groovymetamodel.type.SetType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static owl2converter.engine.converter.trait.TraitsExtractor.ALL_INSTANCES_ATTRIBUTE_NAME
import static owl2converter.engine.converter.trait.TraitsExtractor.TRAIT_NAME_SUFFIX

/**
 * A factory class used for creation of methods for generated traits.
 */
@Component
class TraitInstanceMethodsFactory {

  static final String CREATE_INSTANCE_METHOD_NAME = 'createInstance'
  static final String ADD_INSTANCE_METHOD_NAME = 'addInstance'
  static final String REMOVE_INSTANCE_METHOD_NAME = 'removeInstance'
  static final String CONTAINS_INSTANCE_METHOD_NAME = 'containsInstance'
  static final String GET_INSTANCES_METHOD_NAME = 'getInstances'
  static final String INSTANCE_PARAMETER_NAME = 'classInstance'
  static final String REMOVE_INSTANCE_RESULT_VARIABLE_NAME = 'removalResult'

  /**
   * A factory method creating a list of custom methods for a trait.
   *
   * @param traitModel a trait meta-model instance for which methods should be created
   * @param equivalentTraits a list of equivalent traits for a trait model (the list can be null)
   * @return the list of custom methods for a trait meta-model instance
   */
  List<CustomMethod> create(Trait traitModel, List<EquivalentTrait> equivalentTraits) {
    Optional<Trait> equivalentTrait = findEquivalentTrait(traitModel, equivalentTraits)
    Optional<String> equivalentTraitName = equivalentTrait.map { it.name }
    return [createCreateInstanceMethod(traitModel, equivalentTrait)] +
        createAddInstanceMethod(traitModel, equivalentTrait) +
        createRemoveInstanceMethod(traitModel, equivalentTrait) +
        createContainsInstanceMethod(traitModel, equivalentTraitName) +
        createGetInstancesMethod(traitModel, equivalentTraitName)
  }

  private static Optional<Trait> findEquivalentTrait(Trait traitModel, List<EquivalentTrait> equivalentTraits) {
    Trait equivalentTrait = equivalentTraits.find { it.inheritedTraits.contains(traitModel) }
    return Optional.ofNullable(equivalentTrait)
  }

  private static CustomMethod createCreateInstanceMethod(Trait traitModel, Optional<Trait> equivalentTrait) {
    String traitName = traitModel.name
    Optional<String> equivalentClassTraitName = equivalentTrait.map { it.inheritedTraits.find { it.name != traitName }.name }
    String methodCode = isUnionTrait(traitModel) ? generateUnsupportedInUnionClassMethodCode() :
        generateCreateInstanceMethodCode(traitModel, equivalentClassTraitName)
    return new CustomMethod.Builder(CREATE_INSTANCE_METHOD_NAME, methodCode).withParameters([]).withAccessModifier(PUBLIC)
        .asStatic().withReturnType(new SimpleType(traitModel.packageName, getClassName(traitName))).buildMethod()
  }

  private static boolean isUnionTrait(Trait traitModel) {
    return traitModel instanceof UnionTrait
  }

  private static String generateUnsupportedInUnionClassMethodCode() {
    return "throw new ${UnsupportedOperationException.simpleName}('Method not available in union class')"
  }

  private static String generateCreateInstanceMethodCode(Trait traitModel, Optional<String> equivalentTraitName) {
    String className = getClassName(traitModel.name)
    List<String> codeLines = [
        "def $INSTANCE_PARAMETER_NAME = new $className()",
        "${ALL_INSTANCES_ATTRIBUTE_NAME}.add($INSTANCE_PARAMETER_NAME)",
    ] + traitModel.inheritedTraits.findAll { !isUnionTrait(it) }
        .collect { "${getClassName(it.name)}.$ADD_INSTANCE_METHOD_NAME($INSTANCE_PARAMETER_NAME)" }
    equivalentTraitName.ifPresent { codeLines.add("${getClassName(it)}.${ADD_INSTANCE_METHOD_NAME}($INSTANCE_PARAMETER_NAME)") }
    codeLines.add("return $INSTANCE_PARAMETER_NAME")
    return generateCode(codeLines)
  }

  private static String generateCode(List<String> codeLines) {
    return codeLines.join('\n')
  }

  private static CustomMethod createAddInstanceMethod(Trait traitModel, Optional<Trait> equivalentTrait) {
    String traitName = traitModel.name
    String traitPackage = traitModel.packageName
    Optional<String> equivalentClassTraitName = equivalentTrait.map { it.inheritedTraits.find { it.name != traitName }.name }
    Optional<String> equivalentTraitName = equivalentTrait.map { it.name }
    String methodCode = isUnionTrait(traitModel) ?
        generateUnsupportedInUnionClassMethodCode(): generateAddInstanceMethodCode(traitModel, equivalentClassTraitName)
    return new CustomMethod.Builder(ADD_INSTANCE_METHOD_NAME, methodCode)
        .withParameters([new Parameter(new SimpleType(traitPackage, equivalentTraitName.orElse(traitName)),
        INSTANCE_PARAMETER_NAME)])
        .withAccessModifier(PUBLIC).asStatic().withReturnType(VoidType.INSTANCE).buildMethod()
  }

  private static List<String> generateThrowUnsupportedInstanceTypeExceptionCode() {
    return [
        'else {',
        "throw new ${IllegalArgumentException.simpleName}('Unsupported instance type')",
        '}'
    ]
  }

  private static List<String> generateAddInstanceToInheritedClassesCode(Trait traitModel) {
    return traitModel.inheritedTraits.findAll { !isUnionTrait(it) }
        .collect { "${getClassName(it.name)}.$ADD_INSTANCE_METHOD_NAME($INSTANCE_PARAMETER_NAME)" }
  }

  private static String generateAddInstanceMethodCode(Trait traitModel, Optional<String> equivalentTraitName) {
    List<String> codeLines = [
        "if(${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)) {",
        'return',
        '}',
        "${ALL_INSTANCES_ATTRIBUTE_NAME}.add($INSTANCE_PARAMETER_NAME)"
    ] + generateAddInstanceToInheritedClassesCode(traitModel)
    equivalentTraitName.ifPresent { codeLines.add("${getClassName(it)}.${ADD_INSTANCE_METHOD_NAME}($INSTANCE_PARAMETER_NAME)") }
    return codeLines.join('\n')
  }

  private static CustomMethod createRemoveInstanceMethod(Trait traitModel, Optional<Trait> equivalentTrait) {
    String traitName = traitModel.name
    String traitPackage = traitModel.packageName
    Optional<String> equivalentClassTraitName = equivalentTrait.map { it.inheritedTraits.find { it.name != traitName }.name }
    Optional<String> equivalentTraitName = equivalentTrait.map { it.name }
    String methodCode = isUnionTrait(traitModel) ?
        generateRemoveInstanceMethodCodeInUnionTrait(traitModel as UnionTrait, equivalentTraitName) :
        generateRemoveInstanceMethodCode(traitModel, equivalentClassTraitName)
    return new CustomMethod.Builder(REMOVE_INSTANCE_METHOD_NAME, methodCode)
        .withParameters([new Parameter(new SimpleType(traitPackage, equivalentTraitName.orElse(traitName)),INSTANCE_PARAMETER_NAME)])
        .withAccessModifier(PUBLIC).asStatic().withReturnType(PrimitiveType.BOOLEAN).buildMethod()
  }

  private static String generateRemoveInstanceMethodCodeInUnionTrait(UnionTrait unionTrait, Optional<String> equivalentTraitName) {
    List<String> codeLines = []
    equivalentTraitName.ifPresent { codeLines.add("${getClassName(it)}.${REMOVE_INSTANCE_METHOD_NAME}($INSTANCE_PARAMETER_NAME)") }
    if (unionTrait.unionOf.size() > 0) {
      codeLines.add("boolean $REMOVE_INSTANCE_RESULT_VARIABLE_NAME")
      codeLines.addAll(generateRemoveInstanceFromFirstUnionClassCode(unionTrait))
    }
    if (unionTrait.unionOf.size() > 1) {
      codeLines.addAll(generateRemoveInstanceFromNonFirstUnionClassCode(unionTrait))
    }
    if (unionTrait.unionOf.size() > 0) {
      codeLines.addAll(generateThrowUnsupportedInstanceTypeExceptionCode())
    }
    codeLines.addAll(generateRemoveInstanceFromInheritedClassesCode(unionTrait))
    codeLines.add("return $REMOVE_INSTANCE_RESULT_VARIABLE_NAME")
    return codeLines.join('\n')
  }

  private static List<String> generateRemoveInstanceFromFirstUnionClassCode(UnionTrait unionTrait) {
    String firstUnionTraitName = unionTrait.unionOf.first().name
    String firstUnionClassName = getClassName(firstUnionTraitName)
    return [
        "if(${INSTANCE_PARAMETER_NAME}.class == $firstUnionClassName) {",
        "$REMOVE_INSTANCE_RESULT_VARIABLE_NAME = ${firstUnionClassName}.${REMOVE_INSTANCE_METHOD_NAME}($INSTANCE_PARAMETER_NAME as $firstUnionTraitName)",
        '}'
    ]
  }

  private static List<String> generateRemoveInstanceFromNonFirstUnionClassCode(UnionTrait unionTrait) {
    return unionTrait.unionOf[1..unionTrait.unionOf.size() - 1].collect {
      String className = getClassName(it.name)
      return [
          "else if(${INSTANCE_PARAMETER_NAME}.class == $className) {",
          "$REMOVE_INSTANCE_RESULT_VARIABLE_NAME = ${className}.${REMOVE_INSTANCE_METHOD_NAME}($INSTANCE_PARAMETER_NAME as ${it.name})",
          '}'
      ]
    }.flatten() as List<String>
  }

  private static List<String> generateRemoveInstanceFromInheritedClassesCode(Trait traitModel) {
    return traitModel.inheritedTraits.findAll { !isUnionTrait(it) }
        .collect { "${getClassName(it.name)}.$REMOVE_INSTANCE_METHOD_NAME($INSTANCE_PARAMETER_NAME)" }
  }

  private static String generateRemoveInstanceMethodCode(Trait traitModel, Optional<String> equivalentTraitName) {
    List<String> codeLines = [
        "if(!${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)) {",
        'return false',
        '}',
        "${ALL_INSTANCES_ATTRIBUTE_NAME}.remove($INSTANCE_PARAMETER_NAME)"
    ] + generateRemoveInstanceFromInheritedClassesCode(traitModel)
    equivalentTraitName.ifPresent { codeLines.add("${getClassName(it)}.${REMOVE_INSTANCE_METHOD_NAME}($INSTANCE_PARAMETER_NAME)") }
    codeLines.add('return true')
    return codeLines.join('\n')
  }

  private static CustomMethod createContainsInstanceMethod(Trait traitModel, Optional<String> equivalentTraitName) {
    String traitName = traitModel.name
    String traitPackage = traitModel.packageName
    String methodCode = generateContainsInstanceCode(traitModel)
    Type parameterType = new SimpleType(traitPackage, equivalentTraitName.orElse(traitName))
    return new CustomMethod.Builder(CONTAINS_INSTANCE_METHOD_NAME, methodCode)
        .withParameters([new Parameter(parameterType, INSTANCE_PARAMETER_NAME)])
        .withAccessModifier(PUBLIC).asStatic().withReturnType(PrimitiveType.BOOLEAN).buildMethod()
  }

  private static String generateContainsInstanceCode(Trait traitModel) {
    if (isUnionTrait(traitModel)) {
      return generateContainsInstanceCodeForUnionTrait(traitModel as UnionTrait)
    }
    return "${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)"
  }

  private static String generateContainsInstanceCodeForUnionTrait(UnionTrait unionTrait) {
    return 'return ' + unionTrait.unionOf.collect { "${getClassName(it.name)}.containsInstance($INSTANCE_PARAMETER_NAME as ${it.name})" }
        .join(' || ')
  }

  private static CustomMethod createGetInstancesMethod(Trait traitModel, Optional<String> equivalentTraitName) {
    String traitName = traitModel.name
    String traitPackage = traitModel.packageName
    String methodCode = generateGetInstancesCode(traitModel)
    Type methodReturnType = new SetType(new SimpleType(traitPackage, equivalentTraitName.orElse(traitName)))
    return new CustomMethod.Builder(GET_INSTANCES_METHOD_NAME, methodCode).withParameters([]).withAccessModifier(PUBLIC)
        .asStatic().withReturnType(methodReturnType).buildMethod()
  }

  private static String generateGetInstancesCode(Trait traitModel) {
    if (isUnionTrait(traitModel)) {
      return generateGetInstancesCodeForUnionTrait(traitModel as UnionTrait)
    }
    return "return new HashSet<>($ALL_INSTANCES_ATTRIBUTE_NAME)"
  }

  private static String generateGetInstancesCodeForUnionTrait(UnionTrait unionTrait) {
    return 'return [] + ' + unionTrait.unionOf.collect { getClassName(it.name) + '.getInstances()' }.join(' + ')
  }

  private static String getClassName(String traitName) {
    return traitName - TRAIT_NAME_SUFFIX
  }
}
