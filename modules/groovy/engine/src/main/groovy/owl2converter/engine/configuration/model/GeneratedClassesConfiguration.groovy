package owl2converter.engine.configuration.model

/**
 * Class keeping configuration for generated source code
 */
class GeneratedClassesConfiguration {

  private String basePackage
  private String modelSubPackage
  private String exceptionSubPackage
  private int indentationStep

  /**
   * Returns the name of the folder (absolute path), in which the source code will be generated.
   *
   * @return the name of the parent folder
   */
  String getBasePackage() {
    return basePackage
  }

  /**
   * Sets a name of the parent folder, in which the source code will be generated.
   *
   * @param basePackage the name of the parent folder
   */
  void setBasePackage(String basePackage) {
    this.basePackage = basePackage
  }

  /**
   * Sets a name of the subfolder to keep model elements.
   *
   * @param modelSubPackage the name of the subfolder to keep model elements
   */
  void setModelSubPackage(String modelSubPackage) {
    this.modelSubPackage = modelSubPackage
  }

  /**
   * Returns the name of the subfolder to keep model elements
   *
   * @return the name of the model subfolder
   */
  String getModelSubPackage() {
    return modelSubPackage
  }

  /**
   * Returns the absolute path to the model subfolder.
   *
   * @return the absolute path to the model subfolder
   */
  String getModelFullPackage() {
    return "$basePackage.$modelSubPackage"
  }

  /**
   * Sets a name of the subfolder to keep exceptions.
   *
   * @param exceptionSubPackage the name of exceptions' subfolder
   */
  void setExceptionSubPackage(String exceptionSubPackage) {
    this.exceptionSubPackage = exceptionSubPackage
  }

  /**
   * Returns absolute path to the exceptions' subfolder.
   *
   * @return the absolute path to the exceptions' subfolder
   */
  String getExceptionFullPackage() {
    return "$basePackage.$exceptionSubPackage"
  }

  /**
   * Returns the number of spaces used for indentation.
   *
   * @return the indentation level
   */
  int getIndentationStep() {
    return indentationStep
  }

  /**
   * Sets the number of spaces used for indentation.
   *
   * @param indentation the number of spaces
   */
  void setIndentationStep(int indentation) {
    this.indentationStep = indentation
  }
}
