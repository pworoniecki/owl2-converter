package owl2converter.engine.generator.method.addvalue

import owl2converter.groovymetamodel.method.AddValueMethod
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.INVERSE_FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE
import static owl2converter.engine.converter.trait.TraitsExtractor.ALL_INSTANCES_ATTRIBUTE_NAME

/**
 * A code generation strategy class supporting add value methods of inverse functional attributes.
 */
@Component
class InverseFunctionalAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  /**
   * Checks whether the generator can be used for the method.
   *
   * @param method a method for which the code is to be generated
   * @return true if the method is defined for an inverse functional attribute
   */
  @Override
  boolean supports(AddValueMethod method) {
    return INVERSE_FUNCTIONAL in method.attribute.characteristics
  }

  /**
   * Returns the string representing validation part of code (throws an exception if the method parameter used in other instances).
   * Add additional code for inverse functional and transitive attributes.
   *
   * @param method a method for which the code is to be generated
   * @return the string with validation code
   */
  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    def attributeName = method.attribute.name
    def methodParameterName = method.parameter.name
    def codeLines = [
        "def ${attributeName}OfOtherInstances = (${ALL_INSTANCES_ATTRIBUTE_NAME} - this)*.${attributeName}.flatten()",
        "if (${methodParameterName} in ${attributeName}OfOtherInstances) {",
        "throw new InverseFunctionalAttributeException(\"Cannot add value '\$${methodParameterName}' - " +
            "it would break inverse functional attribute rule\")",
        '}',
    ]
    if (TRANSITIVE in method.attribute.characteristics) {
      codeLines.addAll([
          "if (!${methodParameterName}.${attributeName}.isEmpty()) {",
          "throw new InverseFunctionalAttributeException(\"Cannot add value \$${methodParameterName} because it would break " +
              "inverse functional attribute rule - some values would belong to more than one class instance due to transitivity.\")",
          '}',
      ])
    }
    return Optional.of(generateCode(codeLines))
  }

  /**
   * Returns the string representing specific part of method's code
   *
   * @param method a method for which the code is to be generated
   * @return no code (empty {@link Optional})
   */
  @Override
  Optional<String> generateSpecificCode(AddValueMethod method) {
    return Optional.empty()
  }

  /**
   * Returns the relative order of generator within the group
   *
   * @return 6
   */
  @Override
  int getOrderWithinPriorityGroup() {
    return 6
  }
}