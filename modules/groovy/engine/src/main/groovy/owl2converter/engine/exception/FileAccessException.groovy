package owl2converter.engine.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class FileAccessException extends Exception {
}
