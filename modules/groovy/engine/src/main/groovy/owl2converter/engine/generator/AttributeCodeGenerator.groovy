package owl2converter.engine.generator

import owl2converter.groovymetamodel.attribute.Attribute
import org.springframework.stereotype.Component
import owl2converter.engine.utils.StringUtils

/**
 * Generator generating a source code of an attribute
 */
@Component
class AttributeCodeGenerator {

  /**
   * Generates definition of the attribute (e.g. private String name = 'John')
   *
   * @param attribute an attribute for which the code is to be generated
   * @return the string with attribute definition
   */
  String generateCode(Attribute attribute) {
    def accessModifier = StringUtils.emptyOrAddSpace(attribute.accessModifier.toGroovyCode())
    def staticModifier = attribute.isStatic() ? 'static ' : ''
    def defaultValue = attribute.defaultValue ? " = ${attribute.defaultValue}" : ''

    return "$accessModifier$staticModifier${attribute.valueType.simpleName} ${attribute.name}$defaultValue"
  }
}
