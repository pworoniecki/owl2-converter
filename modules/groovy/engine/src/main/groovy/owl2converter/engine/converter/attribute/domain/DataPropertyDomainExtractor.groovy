package owl2converter.engine.converter.attribute.domain

import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLSubDataPropertyOfAxiom
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.converter.attribute.PredefinedTypes
import owl2converter.engine.exception.MissingIRIRemainderException
import owl2converter.engine.utils.OptionalTransformer

/**
 * A class responsible for translation of an OWL data property domain to its equivalent type in Groovy meta-model.
 */
@Component
class DataPropertyDomainExtractor {

  private String generatedClassesPackage
  private PredefinedTypes predefinedTypes
  private PropertyDirectDomainExtractor directDomainExtractor

  /**
   * Class constructor.
   *
   * @param applicationConfiguration an instance of ApplicationConfiguration class
   * @param predefinedTypes an instance of PredefinedTypes class
   * @param directDomainExtractor an instance of PropertyDirectDomainExtractor class
   */
  @Autowired
  DataPropertyDomainExtractor(ApplicationConfiguration applicationConfiguration, PredefinedTypes predefinedTypes,
                              PropertyDirectDomainExtractor directDomainExtractor) {
    this.generatedClassesPackage = applicationConfiguration.generator.generatedClasses.modelFullPackage
    this.predefinedTypes = predefinedTypes
    this.directDomainExtractor = directDomainExtractor
  }

  /**
   * Returns the type from Groovy meta-model being a representation of OWL data property domain.
   * If there is no explicit domain declaration, then domain is reasoned using equivalent property's domain,
   * or super property's domain.
   *
   * @param property an OWL data property which domain has to be translated to Groovy meta-model type
   * @param ontology an OWL ontology in which the property is defined
   * @return the type from Groovy meta-model being the representation of the OWL data property domain
   */
  Type extract(OWLDataProperty property, OWLOntology ontology) {
    return OptionalTransformer.of(directDomainExtractor.extractSingleDirectDomain(property, ontology))
        .or { extractDomainOfEquivalentProperty(property, ontology) }
        .or { extractDomainOfSuperProperty(property, ontology) }
        .getResult()
        .map { convertToType(it) }
        .orElse(predefinedTypes.thingTraitListType)
  }

  private Optional<OWLClassExpression> extractDomainOfEquivalentProperty(OWLDataProperty property, OWLOntology ontology) {
    return findEquivalentProperty(property, ontology).flatMap { directDomainExtractor.extractSingleDirectDomain(it, ontology) }
  }

  private static Optional<OWLDataProperty> findEquivalentProperty(OWLDataProperty property, OWLOntology ontology) {
    def equivalentPropertyAxiom =
        ontology.equivalentDataPropertiesAxioms(property).find() as OWLEquivalentDataPropertiesAxiom
    return Optional.ofNullable(equivalentPropertyAxiom?.properties()?.find { !it.is(property) } as OWLDataProperty)
  }

  private Optional<OWLClassExpression> extractDomainOfSuperProperty(OWLDataProperty property, OWLOntology ontology) {
    return findSuperProperty(property, ontology).flatMap { directDomainExtractor.extractSingleDirectDomain(it, ontology) }
  }

  private static Optional<OWLDataProperty> findSuperProperty(OWLDataProperty property, OWLOntology ontology) {
    def subPropertyAxiom = ontology.dataSubPropertyAxiomsForSubProperty(property).find() as OWLSubDataPropertyOfAxiom
    return Optional.ofNullable(subPropertyAxiom?.superProperty as OWLDataProperty)
  }

  private Type convertToType(OWLClass owlClass) {
    String className = owlClass.getIRI().remainder
        .orElseThrow { throw new MissingIRIRemainderException("Missing class name in ontology for: $owlClass") }
    return new ListType(new SimpleType(generatedClassesPackage, className))
  }
}
