package owl2converter.engine.converter.attribute.range

import owl2converter.groovymetamodel.type.Type
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * A class responsible for translation of an OWL property range (data, object) to its equivalent Groovy meta-model type.
 */
@Component
class PropertyRangeExtractor {

  private ObjectPropertyRangeExtractor objectPropertyRangeExtractor
  private DataPropertyRangeExtractor dataPropertyRangeExtractor

  /**
   * Class constructor.
   *
   * @param objectPropertyRangeExtractor an instance of ObjectPropertyRangeExtractor (used for translation of OWL object property's range)
   * @param dataPropertyRangeExtractor an instance of DataPropertyRangeExtractor (used for translation of OWL data property's range)
   */
  @Autowired
  PropertyRangeExtractor(ObjectPropertyRangeExtractor objectPropertyRangeExtractor,
                         DataPropertyRangeExtractor dataPropertyRangeExtractor) {
    this.objectPropertyRangeExtractor = objectPropertyRangeExtractor
    this.dataPropertyRangeExtractor = dataPropertyRangeExtractor
  }

  /**
   * Translates an OWL object property range into its Groovy meta-model type equivalent.
   *
   * @param property an OWL object property which is to be translated
   * @param ontology an OWL ontology in which the property is defined in
   * @return the type from Groovy meta-model being a representation of the range of the OWL object property
   */
  Type extractRange(OWLObjectProperty property, OWLOntology ontology) {
    return objectPropertyRangeExtractor.extractRange(property, ontology)
  }


  /**
   * Translates an OWL data property range into its Groovy meta-model type equivalent.
   *
   * @param property an OWL data property which is to be translated
   * @param ontology an OWL ontology in which the property is defined in
   * @return the type from Groovy meta-model being a representation of the range of the OWL data property
   */
  Type extractRange(OWLDataProperty property, OWLOntology ontology) {
    return dataPropertyRangeExtractor.extractRange(property, ontology)
  }
}
