package owl2converter.engine.generator

import groovy.transform.PackageScope
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.ClassElementVisitor
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.generator.constructor.ConstructorCodeGenerator
import owl2converter.engine.generator.method.MethodCodeGenerator

/**
 * A visitor class used to generate class body ({body}).
 */
@Component
class ClassBodyCodeGenerator implements ClassElementVisitor {

  private ConstructorCodeGenerator constructorCodeGenerator
  private AttributeCodeGenerator attributeCodeGenerator
  private MethodCodeGenerator methodCodeGenerator

  /**
   * Class constructor.
   *
   * @param constructorCodeGenerator
   * @param attributeCodeGenerator
   * @param methodCodeGenerator
   */
  @Autowired
  ClassBodyCodeGenerator(ConstructorCodeGenerator constructorCodeGenerator,
                         AttributeCodeGenerator attributeCodeGenerator, MethodCodeGenerator methodCodeGenerator) {
    this.constructorCodeGenerator = constructorCodeGenerator
    this.attributeCodeGenerator = attributeCodeGenerator
    this.methodCodeGenerator = methodCodeGenerator
  }

  /* they could be divided into static and non-static constructors, attributes, method to improve performance
   * as static elements are analyzed separately to put them before non-static ones
   * but it would decrease readability of the code and the current performance is good enough
   */
  private Map<Constructor, String> constructorCodes = [:]
  private Map<Attribute, String> attributeCodes = [:]
  private Map<Method, String> methodCodes = [:]

  /**
   * Gets the map with source code for each constructor
   *
   * @return the map containing source codes for constructors
   */
  @PackageScope Map<Constructor, String> getConstructorCodes() {
    return constructorCodes
  }

  /**
   * Gets the map with source code for each attribute
   *
   * @return the map containing source codes for attributes
   */
  @PackageScope Map<Attribute, String> getAttributeCodes() {
    return attributeCodes
  }

  /**
   * Gets the map with source code for each method
   *
   * @return the map containing source codes for methods
   */
  @PackageScope Map<Method, String> getMethodCodes() {
    return methodCodes
  }

  /**
   * Generates source code for constructor within the constructor owner class and stores it in the map (returned by {@link ClassBodyCodeGenerator#getConstructorCodes}).
   *
   * @param constructor a constructor for which the code is to be generated
   * @param constructorOwner a class containing the constructor
   */
  @Override
  void visit(Constructor constructor, Class constructorOwner) {
    def sourceCode = constructorCodeGenerator.generateCode(constructor, constructorOwner)
    constructorCodes.put(constructor, sourceCode)
  }

  /**
   * Generates source code for attribute and stores it in the map (returned by {@link ClassBodyCodeGenerator#getAttributeCodes}).
   *
   * @param attribute an attribute for which the code is to be generated
   */
  @Override
  void visit(Attribute attribute) {
    def sourceCode = attributeCodeGenerator.generateCode(attribute)
    attributeCodes.put(attribute, sourceCode)
  }

  /**
   * Generates source code for a method and stores it in the map (returned by {@link ClassBodyCodeGenerator#getMethodCodes})..
   *
   * @param method a method for which the code is to be generated
   */
  @Override
  void visit(Method method) {
    def sourceCode = methodCodeGenerator.generateCode(method)
    methodCodes.put(method, sourceCode)
  }
}
