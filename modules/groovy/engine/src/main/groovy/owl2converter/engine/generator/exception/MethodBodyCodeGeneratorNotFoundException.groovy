package owl2converter.engine.generator.exception

import owl2converter.groovymetamodel.method.Method

class MethodBodyCodeGeneratorNotFoundException extends Exception {

  MethodBodyCodeGeneratorNotFoundException(Method method) {
    super("Cannot generate body code of method $method because there is no code generator that supports this method")
  }
}
