package owl2converter.engine.converter.attribute.range

import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLDatatype
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.converter.attribute.PredefinedTypes
import owl2converter.engine.utils.OptionalTransformer
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.Type

/**
 * A class responsible for translation of a data property range into its Groovy meta-model type.
 */
@Component
class DataPropertyRangeExtractor {

  private PredefinedTypes predefinedTypes
  private PropertyDirectRangeExtractor directRangeExtractor
  private OWLDatatypeToTypeConverter datatypeToTypeConverter

  /**
   * Class constructor.
   *
   * @param predefinedTypes an instance of predefined types class
   * @param directRangeExtractor an instance of PropertyDirectRangeExtractor class
   * @param datatypeToTypeConverter a converter of an OWL data type to Groovy meta-model type
   */
  @Autowired
  DataPropertyRangeExtractor(PredefinedTypes predefinedTypes,
                             PropertyDirectRangeExtractor directRangeExtractor,
                             OWLDatatypeToTypeConverter datatypeToTypeConverter) {
    this.predefinedTypes = predefinedTypes
    this.directRangeExtractor = directRangeExtractor
    this.datatypeToTypeConverter = datatypeToTypeConverter
  }

  /**
   * Returns a type from Groovy meta-model being a representation of OWL data property range.
   * If there is no explicit range declaration, then range is reasoned using equivalent property's range,
   * or super property's range.
   *
   * @param property an OWL data property range of which is to be translated to a Groovy meta-model type
   * @param ontology an OWL ontology in which the property is defined
   * @return the type being a Groovy meta-model type representation of the OWL data property range
   */
  Type extractRange(OWLDataProperty property, OWLOntology ontology) {
    Optional<Type> simpleType = OptionalTransformer.of(directRangeExtractor.extractSingleDirectRange(property, ontology))
        .or { extractRangeOfEquivalentProperty(property, ontology) }
        .or { extractRangeOfSuperProperty(property, ontology) }
        .getResult()
        .map { datatypeToTypeConverter.convert(it) }
    if (hasSingleType(property, ontology)) {
      return simpleType.orElse(predefinedTypes.thingTraitType)
    }
    return simpleType.isPresent() ? new ListType(simpleType.get()) : predefinedTypes.thingTraitListType
  }

  private static boolean hasSingleType(OWLDataProperty property, OWLOntology ontology) {
    return ontology.functionalDataPropertyAxioms(property).count() > 0
  }

  private Optional<OWLDatatype> extractRangeOfEquivalentProperty(OWLDataProperty property, OWLOntology ontology) {
    return findEquivalentProperty(property, ontology).flatMap { directRangeExtractor.extractSingleDirectRange(it, ontology) }
  }

  private static Optional<OWLDataProperty> findEquivalentProperty(OWLDataProperty property, OWLOntology ontology) {
    OWLEquivalentObjectPropertiesAxiom equivalentPropertyAxiom =
        ontology.equivalentDataPropertiesAxioms(property).find() as OWLEquivalentObjectPropertiesAxiom
    return Optional.ofNullable(equivalentPropertyAxiom?.properties()?.find { !it.is(property) } as OWLDataProperty)
  }

  private Optional<OWLDatatype> extractRangeOfSuperProperty(OWLDataProperty property, OWLOntology ontology) {
    return findSuperProperty(property, ontology).flatMap { directRangeExtractor.extractSingleDirectRange(it, ontology) }
  }

  private static Optional<OWLDataProperty> findSuperProperty(OWLDataProperty property, OWLOntology ontology) {
    OWLSubObjectPropertyOfAxiom subPropertyAxiom = ontology.dataSubPropertyAxiomsForSubProperty(property).find() as OWLSubObjectPropertyOfAxiom
    return Optional.ofNullable(subPropertyAxiom?.superProperty as OWLDataProperty)
  }
}
