package owl2converter.engine.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component
import owl2converter.engine.exception.NotSupportedOwlElementException

import static org.semanticweb.owlapi.model.ClassExpressionType.OBJECT_UNION_OF

/**
 * A helper class which checks if a given OWL class has equivalent class in OWL ontology.
 */
@Component
class UnionClassChecker {

  /**
   * Checks whether an OWL class is part of either equivalent class axioms or disjoint union class axioms, i.e.
   * if the class has any equivalent class in OWL ontology
   *
   * @param owlClass an OWL class to be checked
   * @param ontology an OWL ontology in which the axioms are looked for
   * @return true if the {@code owlClass} is part of any equivalent class axiom or any disjoint union class axiom in the {@code ontology}
   * @throws NotSupportedOwlElementException if the class is part of more than one equivalent class axioms
   */
  boolean isUnionClass(OWLClass owlClass, OWLOntology ontology) {
    return isEquivalentToClassesUnion(ontology, owlClass) || isDisjointUnion(ontology, owlClass)
  }

  private static boolean isEquivalentToClassesUnion(OWLOntology ontology, OWLClass owlClass) {
    List<OWLEquivalentClassesAxiom> unionEquivalentClassesAxiom = ontology.equivalentClassesAxioms(owlClass)
        .findAll { isEquivalentOfClassesUnion(it) }
    if (unionEquivalentClassesAxiom.isEmpty()) {
      return false
    }
    if (unionEquivalentClassesAxiom.size() == 1) {
      return true
    }
    throw new NotSupportedOwlElementException('OWL class cannot be equivalent of more than one unions!')
  }

  private static boolean isEquivalentOfClassesUnion(OWLEquivalentClassesAxiom equivalentClassesAxiom) {
    return equivalentClassesAxiom.classExpressions().any { it.classExpressionType == OBJECT_UNION_OF }
  }

  private static boolean isDisjointUnion(OWLOntology ontology, OWLClass owlClass) {
    return ontology.disjointUnionAxioms(owlClass).count() > 0
  }
}
