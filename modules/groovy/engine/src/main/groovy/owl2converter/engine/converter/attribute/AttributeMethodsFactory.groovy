package owl2converter.engine.converter.attribute

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.AddValueMethod
import owl2converter.groovymetamodel.method.DirectSetter
import owl2converter.groovymetamodel.method.Getter
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.method.RemoveValueMethod
import owl2converter.groovymetamodel.method.Setter
import owl2converter.groovymetamodel.type.ListType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2converter.engine.generator.method.common.TransitiveAttributeHelpersGenerator

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE

/**
 * A factory class with a factory method to create all methods for a specific attribute.
 */
@Component
class AttributeMethodsFactory {

  private TransitiveAttributeHelpersGenerator transitiveAttributeHelpersGenerator

  /**
   * Class constructor.
   *
   * @param transitiveAttributeHelpersGenerator a factory class instance to create a helper method for transitive attributes
   */
  @Autowired
  AttributeMethodsFactory(TransitiveAttributeHelpersGenerator transitiveAttributeHelpersGenerator) {
    this.transitiveAttributeHelpersGenerator = transitiveAttributeHelpersGenerator
  }

  /**
   * A factory method to create all the methods connected to a specific attribute.
   *
   * @param attribute an attribute for which the methods are to be created
   * @return the list of methods related to the attribute
   */
  List<Method> createMethods(Attribute attribute) {
    List<Method> methods = []
    methods.with {
      add(createGetter(attribute))
      add(createSetter(attribute))
      if (attribute.valueType instanceof ListType) {
        add(createAddValue(attribute))
        add(createRemoveValue(attribute))
      }
    }
    generateSpecificMethods(attribute, methods)
    return methods
  }

  private void generateSpecificMethods(Attribute attribute, ArrayList<Method> methods) {
    if (TRANSITIVE in attribute.characteristics && attribute.valueType instanceof ListType) {
      methods.add(transitiveAttributeHelpersGenerator.generateGetterHelperMethod(attribute))
    }
    if (attribute.isInverseAttribute()) {
      methods.add(new DirectSetter.Builder(attribute).buildMethod())
    }
  }

  private static Getter createGetter(Attribute attribute) {
    return new Getter.Builder(attribute).buildMethod()
  }

  private static Setter createSetter(Attribute attribute) {
    return new Setter.Builder(attribute).buildMethod()
  }

  private static AddValueMethod createAddValue(Attribute attribute) {
    return new AddValueMethod.Builder(attribute).buildMethod()
  }

  private static RemoveValueMethod createRemoveValue(Attribute attribute) {
    return new RemoveValueMethod.Builder(attribute).buildMethod()
  }
}
