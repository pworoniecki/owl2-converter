package owl2converter.engine.utils

/**
 * Utility class extracting name of subpackage
 */
class SubPackageNameExtractor {

  /**
   * Extracts subpackage name of given class, i.e. unqualified package name of the class.
   * Example: if class is placed in package 'com.example.test', then 'test' is returned by this method.
   *
   * @param clazz a class which subpackage's name is to be extracted
   * @return the subpackage name of the {@code clazz}
   */
  static String getSubPackageName(Class clazz) {
    return org.springframework.util.StringUtils.unqualify(clazz.package.name)
  }
}
