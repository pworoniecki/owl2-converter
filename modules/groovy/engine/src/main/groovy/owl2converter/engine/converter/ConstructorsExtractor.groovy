package owl2converter.engine.converter

import owl2converter.groovymetamodel.Constructor
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.AccessModifier.PRIVATE

/**
 * Class responsible for creation of constructors defined for Groovy class metamodels.
 * Currently it returns the same result for all Groovy class metamodel instances.
 */
@Component
class ConstructorsExtractor {

  /**
   * Currently returns the list (one element) with a default private constructor used in every Groovy class implementation.
   *
   * @return the list of constructors used for each Groovy class.
   */
  // TODO handle enum classes in the future
  List<Constructor> extract() {
    return createConstructorsForNonEnumClass()
  }

  private static List<Constructor> createConstructorsForNonEnumClass() {
    return [new Constructor(accessModifier: PRIVATE, isStatic: false, parameters: [])]
  }
}
