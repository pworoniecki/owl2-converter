package owl2converter.engine.generator

import groovy.transform.PackageScope
import owl2converter.groovymetamodel.TraitElementVisitor
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import owl2converter.engine.generator.method.MethodCodeGenerator

/**
 * A visitor class generating body for a trait.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
class TraitBodyCodeGenerator implements TraitElementVisitor {

  private AttributeCodeGenerator attributeCodeGenerator
  private MethodCodeGenerator methodCodeGenerator

  /**
   * Class constructor.
   *
   * @param attributeCodeGenerator
   * @param methodCodeGenerator
   */
  @Autowired
  TraitBodyCodeGenerator(AttributeCodeGenerator attributeCodeGenerator, MethodCodeGenerator methodCodeGenerator) {
    this.attributeCodeGenerator = attributeCodeGenerator
    this.methodCodeGenerator = methodCodeGenerator
  }

  /* they could be divided into static and non-static constructors, attributes, method to improve performance
   * as static elements are analyzed separately to put them before non-static ones
   * but it would decrease readability of the code and the performance is good enough
   */
  private Map<Attribute, String> attributeCodes = [:]
  private Map<Method, String> methodCodes = [:]

  /**
   * Gets the map of attribute codes
   *
   * @return the map of attribute codes
   */
  @PackageScope
  Map<Attribute, String> getAttributeCodes() {
    return attributeCodes
  }

  /**
   * Gets the map of method codes.
   *
   * @return the map of method codes
   */
  @PackageScope
  Map<Method, String> getMethodCodes() {
    return methodCodes
  }

  /**
   * Generates source code for attribute and stores it in the map (returned by {@link TraitBodyCodeGenerator#getAttributeCodes}).
   *
   * @param attribute an attribute for which the code is to be generated
   */
  @Override
  void visit(Attribute attribute) {
    def sourceCode = attributeCodeGenerator.generateCode(attribute)
    attributeCodes.put(attribute, sourceCode)
  }

  /**
   * Generates the source code of the method and stores it in the map (returned by {@link TraitBodyCodeGenerator#getMethodCodes}).
   *
   * @param method a method for which the code is to be generated
   */
  @Override
  void visit(Method method) {
    def sourceCode = methodCodeGenerator.generateCode(method)
    methodCodes.put(method, sourceCode)
  }
}
