package owl2converter.engine.generator.method.getter

import owl2converter.groovymetamodel.method.Getter

interface GetterBodyCodeGeneratorStrategy {

  boolean supports(Getter getter)

  String generateCode(Getter getter)

  // lower value means higher priority
  int getPriority()
}
