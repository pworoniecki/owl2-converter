package owl2converter.engine.generator.method.addvalue

import owl2converter.groovymetamodel.method.AddValueMethod
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.IRREFLEXIVE

/**
 * A code generation strategy class supporting add value methods of irreflexive attributes.
 */
@Component
class IrreflexiveAttributeAddValueBodyCodeGenerator implements AddValueBodyCodeGeneratorStrategy {

  /**
   * Checks whether the generator can be used for the method.
   *
   * @param method a method for which the code is to be generated
   * @return true if the method is defined for an irreflexive attribute
   */
  @Override
  boolean supports(AddValueMethod method) {
    return IRREFLEXIVE in method.attribute.characteristics
  }

  /**
   * Returns the string representing validation part of code (throws an exception if the method parameter is this)
   *
   * @param method a method for which the code is to be generated
   * @return the string with validation code
   */
  @Override
  Optional<String> generateValidationCode(AddValueMethod method) {
    def methodParameterName = method.parameter.name
    def validationCode = generateCode([
        "if ($methodParameterName == this) {",
        "throw new IrreflexiveAttributeException(\"Cannot add value 'this' - " +
            "it would break irreflexivity of attribute\")",
        '}',
    ])
    return Optional.of(validationCode)
  }

  /**
   * Returns the string representing specific part of method's code
   *
   * @param method a method for which the code is to be generated
   * @return no code (empty {@link Optional})
   */
  @Override
  Optional<String> generateSpecificCode(AddValueMethod method) {
    return Optional.empty()
  }

  /**
   * Returns the relative order of generator within the group
   *
   * @return 2
   */
  @Override
  int getOrderWithinPriorityGroup() {
    return 2
  }
}
