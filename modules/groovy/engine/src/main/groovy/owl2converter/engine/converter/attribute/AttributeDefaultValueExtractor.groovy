package owl2converter.engine.converter.attribute

import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component

/**
 * A class responsible for establishing a default value (in Groovy code) of OWL object property on the basis of its characteristics.
 */
@Component
class AttributeDefaultValueExtractor {

  /**
   * Returns a default value of an OWL object property dependent on the property characteristics.
   *
   * @param property an OWL object property which default value is to be established
   * @param ontology an OWL ontology in which the property is defined
   * @return the default value of object property: {@code this} for single-type reflexive property,
   * {@link Optional#EMPTY} for single-type non-reflexive property, {@code [this]} for list-type reflexive property
   * or {@code []} for list-type non-reflexive property
   */
  Optional<String> extract(OWLObjectProperty property, OWLOntology ontology) {
    def isReflexiveProperty = isReflexive(property, ontology)
    if (isSingleTypeProperty(property, ontology)) {
      return isReflexiveProperty ? Optional.of('this') : Optional.empty()
    }
    return Optional.of(isReflexiveProperty ? '[this]' : '[]')
  }

  private static boolean isSingleTypeProperty(OWLObjectProperty property, OWLOntology ontology) {
    return ontology.functionalObjectPropertyAxioms(property).count() > 0
  }

  /**
   * Returns a default value of an OWL data property dependent on the property characteristics.
   *
   * @param property an OWL object property which default value is to be established
   * @param ontology an OWL ontology in which the property is defined
   * @return the default value of object property: {@link Optional#EMPTY} for single-type property
   * or {@code []} for list-type property
   */
  Optional<String> extract(OWLDataProperty property, OWLOntology ontology) {
    return isSingleTypeProperty(property, ontology) ? Optional.empty() : Optional.of('[]')
  }

  private static boolean isSingleTypeProperty(OWLDataProperty property, OWLOntology ontology) {
    return ontology.functionalDataPropertyAxioms(property).count() > 0
  }

  private static boolean isReflexive(OWLObjectProperty property, OWLOntology ontology) {
    return ontology.reflexiveObjectPropertyAxioms(property).count() > 0
  }
}
