package owl2converter.engine.generator

import owl2converter.groovymetamodel.Trait
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import owl2converter.engine.codebuilder.TraitCodeBuilder
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration

/**
 * Generates the whole code for a trait with the use of visitor pattern.
 * Coordinates the construction process.
 */
@Component
class TraitCodeGenerator {

  private ApplicationContext applicationContext
  private TraitSignatureCodeGenerator traitSignatureCodeGenerator
  private GeneratorConfiguration generatorConfiguration

  /**
   * Class constructor.
   *
   * @param applicationContext
   * @param traitSignatureCodeGenerator
   * @param applicationConfiguration
   */
  @Autowired
  TraitCodeGenerator(ApplicationContext applicationContext, TraitSignatureCodeGenerator traitSignatureCodeGenerator,
                     ApplicationConfiguration applicationConfiguration) {
    this.applicationContext = applicationContext
    this.traitSignatureCodeGenerator = traitSignatureCodeGenerator
    this.generatorConfiguration = applicationConfiguration.generator
  }

  /**
   * Generates the full definition of the trait (starting from package declaration).
   *
   * @param traitModel a trait for which the code is to be generated
   * @return the string with full trait implementation
   */
  String generateCode(Trait traitModel) {
    TraitBodyCodeGenerator traitBodyGenerator = applicationContext.getBean(TraitBodyCodeGenerator)
    traitModel.accept(traitBodyGenerator)

    def codeBuilder = new TraitCodeBuilder(traitModel)
    codeBuilder.with {
      appendPackage()
      appendEmptyLine()
      appendImports(generatorConfiguration)
      appendEmptyLine()
      appendAnnotations(traitModel.annotations)
      appendCode(traitSignatureCodeGenerator.generateTraitSignature(traitModel))
      appendEmptyLine()
      appendStaticAttributes(traitBodyGenerator.attributeCodes)
      appendNonStaticAttributes(traitBodyGenerator.attributeCodes)
      appendStaticMethodsWithEmptyLines(traitBodyGenerator.methodCodes)
      appendNonStaticMethodsWithEmptyLines(traitBodyGenerator.methodCodes)
      appendTraitSignatureEnd()
      appendEmptyLine()
    }

    return codeBuilder.code
  }
}
