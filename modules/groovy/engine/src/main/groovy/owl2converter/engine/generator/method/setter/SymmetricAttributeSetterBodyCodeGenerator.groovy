package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.method.AddValueMethod
import owl2converter.groovymetamodel.method.RemoveValueMethod
import owl2converter.groovymetamodel.method.Setter
import owl2converter.groovymetamodel.type.ListType
import org.springframework.stereotype.Component

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.SYMMETRIC

@Component
class SymmetricAttributeSetterBodyCodeGenerator implements SetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Setter setter) {
    return SYMMETRIC in setter.attribute.characteristics
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificCode(Setter setter) {
    List<String> codeLines = setter.attribute.valueType instanceof ListType ?
        generateListAttributeSettingValueCodeLines(setter) :
        generateNonListAttributeSettingValueCodeLines(setter
    )
    return Optional.of(generateCode(codeLines))
  }

  private static List<String> generateListAttributeSettingValueCodeLines(Setter setter) {
    String attributeName = setter.attribute.name
    String capitalizedAttributeName = attributeName.capitalize()
    return [
        "def old$capitalizedAttributeName = this.$attributeName ?: []",
        "def new$capitalizedAttributeName = ${setter.parameter.name} ?: []",
        "def removed$capitalizedAttributeName = old$capitalizedAttributeName - new$capitalizedAttributeName",
        "def added$capitalizedAttributeName = new$capitalizedAttributeName - old$capitalizedAttributeName",
        "removed${capitalizedAttributeName}.each { ${RemoveValueMethod.getName(setter.attribute)}(it) }",
        "added${capitalizedAttributeName}.each { ${AddValueMethod.getName(setter.attribute)}(it) }"
    ]
  }

  private static List<String> generateNonListAttributeSettingValueCodeLines(Setter setter) {
    String attributeName = setter.attribute.name
    String parameterName = setter.parameter.name
    return [
        "if (this.$attributeName == $parameterName) {",
        'return',
        '}',
        "this.$attributeName = $parameterName",
        "${parameterName}.set${attributeName.capitalize()}(this)"
    ]
  }

  @Override
  int getOrderWithinPriorityGroup() {
    return 7
  }
}
