package owl2converter.engine.converter

import org.semanticweb.owlapi.model.OWLIndividual
import org.semanticweb.owlapi.model.OWLNamedIndividual
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component

@Component
class IndividualsTopologicalSorter {

    List<OWLNamedIndividual> sort(OWLOntology ontology, List<OWLNamedIndividual> individuals) {
        List<IndividualNode> individualNodes = convertToNodes(ontology, individuals)
        List<IndividualNode> sortedNodes = []
        List<IndividualNode> nodesWithoutDependencies = individualNodes.findAll { it.inputIndividuals.isEmpty() }
        while (!nodesWithoutDependencies.isEmpty()) {
            IndividualNode node = nodesWithoutDependencies.pop()
            sortedNodes.add(node)
            node.outputIndividuals.each {
                it.inputIndividuals.remove(node)
                if (it.inputIndividuals.isEmpty()) {
                    nodesWithoutDependencies.add(it)
                }
            }
            node.outputIndividuals.clear()
        }

        if (sortedNodes.size() != individualNodes.size()) {
            throw new IllegalArgumentException("Cycle dependencies between individuals detected. " +
                    "Problem is related to one of the following individuals: ${(individualNodes - sortedNodes)*.individual}")
        }
        return sortedNodes*.individual
    }

    private List<IndividualNode> convertToNodes(OWLOntology ontology, List<OWLNamedIndividual> individuals) {
        List<IndividualNode> nodes = individuals.collect { new IndividualNode(it) }
        return nodes.each { node ->
            ontology.objectPropertyAssertionAxioms(node.individual)*.object.each { OWLIndividual outputIndividual ->
                IndividualNode outputNode = nodes.find { it.individual == outputIndividual }
                node.addInputIndividual(outputNode)
                outputNode.addOutputIndividual(node)
            }
        }
    }

    private class IndividualNode {
        OWLNamedIndividual individual
        Set<IndividualNode> inputIndividuals = []
        Set<IndividualNode> outputIndividuals = []

        IndividualNode(OWLNamedIndividual individual) {
            this.individual = individual
            this.inputIndividuals = inputIndividuals
            this.outputIndividuals = outputIndividuals
        }

        void addInputIndividual(IndividualNode individual) {
            inputIndividuals.add(individual)
        }

        void addOutputIndividual(IndividualNode individual) {
            outputIndividuals.add(individual)
        }
    }
}
