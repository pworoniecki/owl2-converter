package owl2converter.engine.converter.trait

import owl2converter.groovymetamodel.Annotation
import owl2converter.groovymetamodel.type.SimpleType
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.exception.MissingIRIRemainderException

import static org.semanticweb.owlapi.model.AxiomType.DISJOINT_UNION
import static owl2converter.engine.converter.trait.TraitsExtractor.TRAIT_NAME_SUFFIX

/**
 * A class responsible for extraction of OWL axioms mapped to Groovy annotation metamodels
 */
@Component
class AnnotationsExtractor {

  static final String DISJOINT_WITH_ANNOTATION_SUBPACKAGE = 'validator'
  static final String DISJOINT_WITH_ANNOTATION_NAME = 'DisjointWith'

  private String generatedCodeBasePackage

  /**
   * Class constructor.
   *
   * @param configuration an instance of application configuration
   */
  AnnotationsExtractor(ApplicationConfiguration configuration) {
    generatedCodeBasePackage = configuration.generator.generatedClasses.basePackage
  }

  /**
   * Extracts from OWL ontology axioms needed to create Groovy annotation metamodels.
   * For now there is only one annotation - DisjointWith.
   * So currently only disjoint union/class OWL axioms are analyzed for given OWL class (if there are any).
   *
   * @param owlClass an OWL class for which annotations are to be extracted
   * @param ontology an OWL ontology in which axioms are looked for
   * @return the list of Groovy meta-model annotations
   */
  List<Annotation> extract(OWLClass owlClass, OWLOntology ontology) {
    List<Annotation> annotations = []
    createDisjointWithAnnotation(ontology, owlClass).ifPresent { annotations.add(it) }
    return annotations
  }

  private Optional<Annotation> createDisjointWithAnnotation(OWLOntology ontology, OWLClass owlClass) {
    List<OWLClass> disjointClasses = findDisjointClassesByDisjointUnionAxiom(ontology, owlClass) +
        findDisjointClassesByDisjointClassesAnxiom(ontology, owlClass)
    List<String> disjointClassTraits = disjointClasses.collect { extractName(it) + TRAIT_NAME_SUFFIX }
    return disjointClassTraits.isEmpty() ? Optional.empty() : Optional.of(createDisjointWithAnnotation(disjointClassTraits))
  }

  private static String extractName(OWLClass owlClass) {
    return owlClass.getIRI().remainder.orElseThrow {
      throw new MissingIRIRemainderException("Cannot extract name of OWL class $owlClass")
    }
  }

  private Annotation createDisjointWithAnnotation(List<String> disjointWithTraits) {
    return new Annotation(new SimpleType(generatedCodeBasePackage + '.' + DISJOINT_WITH_ANNOTATION_SUBPACKAGE, DISJOINT_WITH_ANNOTATION_NAME), disjointWithTraits)
  }

  private static List<OWLClass> findDisjointClassesByDisjointUnionAxiom(OWLOntology ontology, OWLClass owlClass) {
    List<OWLDisjointUnionAxiom> disjointUnionAxioms = ontology.axioms().findAll { it.axiomType == DISJOINT_UNION }
    List<OWLDisjointUnionAxiom> axiomsRelatedToClass =
        disjointUnionAxioms.findAll { it.classesInSignature().any { it == owlClass } }
    return axiomsRelatedToClass.collect { it.classesInSignature().findAll { it != owlClass } }.flatten() as List<OWLClass>
  }

  private static List<OWLClass> findDisjointClassesByDisjointClassesAnxiom(OWLOntology ontology, OWLClass owlClass) {
    List<OWLDisjointClassesAxiom> disjointClassesAxioms = ontology.disjointClassesAxioms(owlClass).findAll()
    return disjointClassesAxioms.collect { it.classesInSignature().findAll { it != owlClass } }.flatten() as List<OWLClass>
  }
}
