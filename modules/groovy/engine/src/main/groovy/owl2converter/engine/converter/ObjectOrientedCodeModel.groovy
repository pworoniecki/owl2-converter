package owl2converter.engine.converter

import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Trait

/**
 * A wrapper class keeping together class and trait meta-model instances to be converted to groovy code.
 */
class ObjectOrientedCodeModel {

  private List<Class> classes
  private List<Trait> traits
  private Class individualsDataSourceClass

  /**
   * Class constructor.
   *
   * @param classes the list of classes
   * @param traits the list of traits
   */
  ObjectOrientedCodeModel(List<Class> classes, List<Trait> traits, Class individualsDataSourceClass) {
    this.classes = classes
    this.traits = traits
    this.individualsDataSourceClass = individualsDataSourceClass
  }

  /**
   * Returns the list of classes.
   *
   * @return the list of classes
   */
  List<Class> getClasses() {
    return classes
  }

  /**
   * Returns the list of traits.
   *
   * @return the list of traits
   */
  List<Trait> getTraits() {
    return traits
  }

  /***
   * Returns the class containing method returning all objects created from ontology's individuals
   *
   * @return the individuals data source class
   */
  Class getIndividualsDataSourceClass() {
    return individualsDataSourceClass
  }
}
