package owl2converter.engine.codebuilder

import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.groovymetamodel.Trait
import owl2converter.engine.codebuilder.common.CodeBuilder

/**
 * Builder responsible for generation of Groovy trait's source code
 */
class TraitCodeBuilder extends CodeBuilder {

  private Trait traitModel

  /**
   * Class constructor.
   *
   * @param traitModel the trait meta-model to be translated to the source code
   */
  TraitCodeBuilder(Trait traitModel) {
    this.traitModel = traitModel
  }

  /**
   * Appends the package declaration of the trait to be translated.
   */
  void appendPackage() {
    appendCode("package ${traitModel.packageName}")
  }

  /**
   * Appends the "}" to the trait source code.
   */
  void appendTraitSignatureEnd() {
    appendCode('}')
  }

  /**
   * Appends all imports required by trait definition, caused by parent classes, implemented traits, methods, attributes, annotations etc.
   */
  @Override
  void appendSpecificImports(GeneratorConfiguration generatorConfiguration) {
    appendImplementedTraitImports(traitModel.inheritedTraits, traitModel.packageName)
    appendMethodParameterImports(traitModel.methods)
    appendMethodReturnTypeImports(traitModel.methods)
    appendAttributeTypeImports(traitModel.attributes)
    appendAnnotationTypeImports(traitModel.annotations)
  }

  /**
   * Returns the name of the package the trait is defined in.
   *
   * @return the name of the package
   */
  @Override
  String getOwnerPackageName() {
    return traitModel.packageName
  }
}
