package owl2converter.engine.converter.trait

import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.TraitBuilder
import owl2converter.groovymetamodel.UnionTrait
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.stereotype.Component
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.exception.MissingIRIRemainderException

import static owl2converter.engine.converter.trait.TraitsExtractor.TRAIT_NAME_SUFFIX

/**
 * Factory responsible for Trait metamodel creation
 */
@Component
class TraitBuilderFactory {

  private String traitPackage
  private UnionClassChecker unionClassChecker

  /**
   * Class constructor.
   *
   * @param configuration an instance of application configuration
   * @param unionClassChecker an instance of {@link UnionClassChecker}
   */
  TraitBuilderFactory(ApplicationConfiguration configuration, UnionClassChecker unionClassChecker) {
    traitPackage = configuration.generator.generatedClasses.modelFullPackage
    this.unionClassChecker = unionClassChecker
  }

  /**
   * Returns the proper trait builder subclass (either {@link Trait.Builder} or {@link UnionTrait.Builder}) for an OWL class.
   *
   * @param owlClass an OWL class for which the trait builder is to be returned
   * @param ontology an OWL ontology in which the OWL class is defined
   * @return the proper builder needed to create a trait
   */
  TraitBuilder<? extends TraitBuilder, ? extends Trait> createTraitBuilder(OWLClass owlClass, OWLOntology ontology) {
    String traitName = extractName(owlClass) + TRAIT_NAME_SUFFIX
    return unionClassChecker.isUnionClass(owlClass, ontology) ? createUnionTrait(traitName) : createTrait(traitName)
  }

  private static String extractName(OWLClass owlClass) {
    return owlClass.getIRI().remainder.orElseThrow {
      throw new MissingIRIRemainderException("Cannot extract name of OWL class $owlClass")
    }
  }

  private Trait.Builder createTrait(String traitName) {
    return new Trait.Builder(traitPackage, traitName)
  }

  private UnionTrait.Builder createUnionTrait(String traitName) {
    return new UnionTrait.Builder(traitPackage, traitName)
  }
}
