package owl2converter.engine.codebuilder.common

interface ContainingCode {

/**
 * Appends source code.
 *
 * @param code the code to be appended
 */
  abstract void appendCode(String code)

  /**
   * Appends an empty line to the source code.
   */
  abstract void appendEmptyLine()
}
