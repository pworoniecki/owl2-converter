package owl2converter.engine.converter.attribute

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.relation.AttributeRelation
import owl2converter.groovymetamodel.attribute.relation.EquivalentToAttributeRelation
import owl2converter.groovymetamodel.attribute.relation.InverseAttributeRelation
import owl2converter.groovymetamodel.attribute.relation.SuperAttributeOfRelation
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom
import org.springframework.stereotype.Component
import owl2converter.engine.exception.MissingIRIRemainderException
import owl2converter.engine.exception.NotSupportedOwlElementException

/**
 * A class responsible for extraction of OWL relations between properties: data, object (sub-property, equivalent property, inverse property)
 * and their mapping to Groovy meta-model (instances of {@link AttributeRelation} class).
 */
@Component
class AttributeRelationsExtractor {

  /**
   * Extracts relation axioms from OWL ontology an OWL object property is involved in and maps them into instances of {@link AttributeRelation} class.
   *
   * @param property an OWL object property for which relation axioms are to be extracted and mapped
   * @param ontology an OWL ontology the property is defined in
   * @param attributes a mapping between OWL object properties and their Groovy attribute metamodels
   * @return the list of {@link AttributeRelation} instances connected with the {@code property}
   */
  List<AttributeRelation> extract(OWLObjectProperty property, OWLOntology ontology, Map<OWLObjectProperty, Attribute> attributes) {
    List<AttributeRelation> relations = []
    relations.addAll(extractInverseAttributeRelations(ontology, property, attributes))
    relations.addAll(extractSuperAttributeOfRelations(ontology, property, attributes))
    extractEquivalentAttributeRelation(ontology, property, attributes).ifPresent { relations.add(it) }
    return relations
  }

  private static List<InverseAttributeRelation> extractInverseAttributeRelations(OWLOntology ontology, OWLObjectProperty property,
                                                                                 Map<OWLObjectProperty, Attribute> attributes) {
    List<OWLInverseObjectPropertiesAxiom> inversePropertyAxioms = ontology.inverseObjectPropertyAxioms(property).findAll()
    List<OWLObjectProperty> inverseProperties = inversePropertyAxioms.collect {it.properties().findAll()}.flatten()
        .findAll { !it.is(property) } as List<OWLObjectProperty>
    List<Attribute> inverseAttributes = inverseProperties.collect { mapPropertyToAttribute(it, attributes) }
    return inverseAttributes.collect { new InverseAttributeRelation(it) }
  }

  private static List<SuperAttributeOfRelation> extractSuperAttributeOfRelations(OWLOntology ontology, OWLObjectProperty property,
                                                                                 Map<OWLObjectProperty, Attribute> attributes) {
    List<OWLSubObjectPropertyOfAxiom> superPropertyAxioms = ontology.objectSubPropertyAxiomsForSuperProperty(property).findAll()
    return extractSuperAttributeOfRelations(superPropertyAxioms*.subProperty as List<OWLObjectProperty>, attributes)
  }

  private static List<SuperAttributeOfRelation> extractSuperAttributeOfRelations(List<OWLProperty> superProperties,
                                                                                 Map<OWLProperty, Attribute> attributes) {
    List<Attribute> superAttributes = superProperties.collect { mapPropertyToAttribute(it, attributes) }
    return superAttributes.collect { new SuperAttributeOfRelation(it) }
  }

  private static Optional<EquivalentToAttributeRelation> extractEquivalentAttributeRelation(
      OWLOntology ontology, OWLObjectProperty property, Map<OWLObjectProperty, Attribute> attributes) {
    List<OWLEquivalentObjectPropertiesAxiom> equivalentPropertyAxioms =
        ontology.equivalentObjectPropertiesAxioms(property).findAll()
    List<OWLObjectProperty> equivalentProperties = equivalentPropertyAxioms*.properties()*.findAll { !it.is(property) }
        .flatten() as List<OWLObjectProperty>
    return extractEquivalentAttributeRelation(equivalentProperties, attributes, property)
  }

  private static Optional<EquivalentToAttributeRelation> extractEquivalentAttributeRelation(
      List<OWLProperty> equivalentProperties, Map<OWLProperty, Attribute> attributes, OWLProperty property) {
    List<Attribute> equivalentAttributes = equivalentProperties.collect { mapPropertyToAttribute(it, attributes) }
    if (equivalentAttributes.isEmpty()) {
      return Optional.empty()
    }
    assertSingleEquivalentAttribute(equivalentAttributes, property)
    return Optional.of(new EquivalentToAttributeRelation(equivalentAttributes.first()))
  }

  private static void assertSingleEquivalentAttribute(List<Attribute> equivalentAttributes, OWLProperty property) {
    if (equivalentAttributes.size() > 1) {
      throw new NotSupportedOwlElementException("More than 1 property are equivalent to ${property.getIRI()}. " +
          "Only 1 equivalent property is supported. Please adjust ontology to this limitation.")
    }
  }

  private static Attribute mapPropertyToAttribute(OWLProperty property, Map<OWLProperty, Attribute> attributes) {
    String propertyName = extractName(property)
    Attribute attribute = attributes[property]
    if (!attribute) {
      throw new IllegalArgumentException("Missing attribute for property $propertyName")
    }
    return attribute
  }

  private static String extractName(OWLProperty property) {
    return property.getIRI().remainder
        .orElseThrow { new MissingIRIRemainderException("Invalid property's IRI: ${property.getIRI()}") }
  }

  /**
   * Extracts relation axioms from OWL ontology an OWL data property is involved in and maps them into instances of {@link AttributeRelation} class.
   *
   * @param property an OWL data property for which relation axioms are to be extracted and mapped
   * @param ontology an OWL ontology the property is defined in
   * @param attributes a mapping between OWL data properties and their Groovy attribute metamodels
   * @return the list of {@link AttributeRelation} class instances connected with the {@code property}
   */
  List<AttributeRelation> extract(OWLDataProperty property, OWLOntology ontology, Map<OWLDataProperty, Attribute> attributes) {
    List<AttributeRelation> relations = []
    relations.addAll(extractSuperAttributeOfRelations(ontology, property, attributes))
    extractEquivalentAttributeRelation(ontology, property, attributes).ifPresent { relations.add(it) }
    return relations
  }

  private static List<SuperAttributeOfRelation> extractSuperAttributeOfRelations(OWLOntology ontology, OWLDataProperty property,
                                                                                 Map<OWLDataProperty, Attribute> attributes) {
    List<OWLDataProperty> superProperties = ontology.dataSubPropertyAxiomsForSuperProperty(property)*.subProperty
        .findAll { it instanceof OWLDataProperty && !it.is(property) } as List<OWLDataProperty>
    return extractSuperAttributeOfRelations(superProperties, attributes)
  }

  private static Optional<EquivalentToAttributeRelation> extractEquivalentAttributeRelation(
      OWLOntology ontology, OWLDataProperty property, Map<OWLDataProperty, Attribute> attributes) {
    List<OWLEquivalentDataPropertiesAxiom> equivalentPropertyAxioms = ontology.equivalentDataPropertiesAxioms(property).findAll()
    List<OWLDataProperty> equivalentProperties = equivalentPropertyAxioms*.properties()*.findAll { !it.is(property) }
        .flatten() as List<OWLDataProperty>
    return extractEquivalentAttributeRelation(equivalentProperties, attributes, property)
  }
}
