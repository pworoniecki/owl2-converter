package owl2converter.engine.generator.method.getter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Getter
import owl2converter.groovymetamodel.type.ListType
import org.springframework.stereotype.Component
import owl2converter.engine.generator.method.common.TransitiveAttributeHelpersGenerator

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE

@Component
class TransitiveAttributeGetterBodyCodeGenerator implements GetterBodyCodeGeneratorStrategy {

  @Override
  boolean supports(Getter getter) {
    return TRANSITIVE in getter.attribute.characteristics
  }

  @Override
  String generateCode(Getter getter) {
    if (getter.attribute.valueType instanceof ListType) {
      return generateCodeForListAttribute(getter)
    }
    return generateCodeForSingleAttribute(getter)
  }

  private static String generateCodeForListAttribute(Getter getter) {
    Attribute attribute = getter.attribute
    return [
        'def result = []',
        "${TransitiveAttributeHelpersGenerator.getGetterHelperMethodName(attribute)}(result)",
        'return result'
    ].join('\n')
  }

  private static String generateCodeForSingleAttribute(Getter getter) {
    return "return this.${getter.attribute.name}"
  }

  @Override
  int getPriority() {
    return 1
  }
}
