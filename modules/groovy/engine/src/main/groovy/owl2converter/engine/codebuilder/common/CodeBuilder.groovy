package owl2converter.engine.codebuilder.common

import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.groovymetamodel.type.CollectionType
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType

/**
 * An abstract class responsible for building a piece of a source code from small pieces, e.g. imports specification,
 * source code lines (represented by strings) and empty lines.
 */
abstract class CodeBuilder implements ContainingMethods, ContainingAttributes, ContainingImplementedTraits,
    ContainingAnnotations {

  private StringBuilder codeBuilder = new StringBuilder()
  private List<String> imports = []

  /**
   * Appends string to the internal representation of a source code. After builder creation the source code string is empty.
   *
   * @param code the code to be appended
   */
  void appendCode(String code) {
    codeBuilder.append("$code\n")
  }

  /**
   * Appends one empty line to the internal source code representation.
   */
  void appendEmptyLine() {
    appendCode('')
  }

  /**
   * Returns the internal source code representation
   *
   * @return string representing the actual code
   */
  String getCode() {
    return codeBuilder.toString()
  }

  /**
   * Appends imports: both default (always the same) and those required by element content
   *
   * @param generatorConfiguration a generator configuration (wrapper class) for class configuration
   */
  void appendImports(GeneratorConfiguration generatorConfiguration) {
    appendDefaultImports(generatorConfiguration)
    appendSpecificImports(generatorConfiguration)
  }

  /**
   * Appends default imports for exceptions with the use of generator configuration.
   *
   * @param generatorConfiguration a generator configuration (wrapper class) for class configuration
   */
  void appendDefaultImports(GeneratorConfiguration generatorConfiguration) {
    appendPackageImport(generatorConfiguration.generatedClasses.exceptionFullPackage)
  }

  /**
   * Appends a specific import in the following format: 'import packageName.*' to the source code.
   *
   * @param packageName a name of package which entire content should be imported (packageName.*)
   */
  @Override
  void appendPackageImport(String packageName) {
    def importCandidate = "import $packageName.*"
    if (!imports.contains(importCandidate)) {
      appendCode(importCandidate)
      imports.add(importCandidate)
    }
  }

  /**
   * Appends a specific import in the following format: 'import packageName.className' to the source code.
   *
   * @param classFullName a full qualified name of the class to be imported
   */
  @Override
  void appendClassImport(String classFullName) {
    def importCandidate = "import $classFullName"
    if (!imports.contains(importCandidate)) {
      appendCode(importCandidate)
      imports.add(importCandidate)
    }
  }

  /**
   * An abstract method which aims in appending specific imports required by a given element (class or trait).
   */
  abstract void appendSpecificImports(GeneratorConfiguration generatorConfiguration)

  /**
   * Appends a set of import statements in the following format: 'import packageName.className' from the passed list of types
   * for all types that are not an instance of {@link VoidType}, are not imported by default (see: {@link Type#isImportedByDefault})
   * and are not in the same package as {@code ownerPackage}
   *
   * @param types a list of types to be imported (after applying the described filter to avoid unnecessary imports)
   * @param ownerPackage a name of the package of the owner class (that contains appended imports) - types from this package will not be imported
   */
  void appendImports(List<Type> types, String ownerPackage) {
    List<Type> nonVoidTypes = types.findAll { !(it instanceof VoidType) }
    List<Type> flattenTypes = nonVoidTypes.collect { extractSimpleType(it) }
    List<Type> typesToBeImported = flattenTypes.unique().findAll { !it.isImportedByDefault() && it.packageName != ownerPackage }
    typesToBeImported*.fullName.unique().each { appendClassImport(it) }
  }

  /**
   * Extracts a parametrized type of {@link CollectionType} or returns given type itself (unchanged).
   *
   * @param type a type to extract information from
   * @return the parametrized type T for a {@link CollectionType} (e.g. List&lt;T&gt;, Set&lt;T&gt;), type unchanged otherwise
   */
  static Type extractSimpleType(Type type) {
    return type instanceof CollectionType ? type.extractSimpleParameterizedType() : type
  }
}
