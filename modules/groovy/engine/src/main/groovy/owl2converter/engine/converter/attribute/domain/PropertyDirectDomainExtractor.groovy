package owl2converter.engine.converter.attribute.domain

import org.semanticweb.owlapi.model.AxiomType
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.slf4j.Logger

/**
 * A class responsible for finding information about OWL property domain in an OWL ontology using OWL property domain axiom.
 * Information about the domain is returned as an instance of {@link OWLClass}.
 */
@Component
class PropertyDirectDomainExtractor {

  private Logger logger

  /**
   * Class constructor.
   *
   * @param logger a logger class to write warnings about failures in extraction process
   */
  @Autowired
  PropertyDirectDomainExtractor(Logger logger) {
    this.logger = logger
  }

  /**
   * Extracts domain of OWL object property by scanning OWL object property domain axioms and returns it as an instance of {@link OWLClass}.
   * Writes warnings to the log if domain extraction was impossible.
   *
   * @param property an OWL object property the domain of which is to be extracted
   * @param ontology an OWL ontology containing the property
   * @return {@link OWLClass} instance wrapped by {@link Optional} being the representation of property domain if the domain has been defined in the ontology,
   * {@link Optional#EMPTY} otherwise
   */
  // TODO support more than 1 domain declarations - it means intersections of classes
  // TODO support other types of domain declarations, e.g. union of classes
  Optional<OWLClass> extractSingleDirectDomain(OWLObjectProperty property, OWLOntology ontology) {
    def domainDeclarations = ontology.axioms(property).findAll { it.axiomType == AxiomType.OBJECT_PROPERTY_DOMAIN }
        .collect { (it as OWLObjectPropertyDomainAxiom).domain } as List<OWLClassExpression>
    return extractDomain(domainDeclarations, property)
  }

  /**
   * Extracts domain of OWL data property by scanning OWL object data domain axioms and returns it as an instance of {@link OWLClass}.
   * Writes warnings to the log if domain extraction was impossible.
   *
   * @param property an OWL data property the domain of which is to be extracted
   * @param ontology an OWL ontology containing the property
   * @return {@link OWLClass} instance wrapped by {@link Optional} being the representation of property domain if the domain has been defined in the ontology,
   * {@link Optional#EMPTY} otherwise
   */
  Optional<OWLClass> extractSingleDirectDomain(OWLDataProperty property, OWLOntology ontology) {
    def domainDeclarations = ontology.axioms(property).findAll { it.axiomType == AxiomType.DATA_PROPERTY_DOMAIN }
            .collect { (it as OWLDataPropertyDomainAxiom).domain } as List<OWLClassExpression>
    return extractDomain(domainDeclarations, property)
  }

  private Optional<OWLClass> extractDomain(List<OWLClassExpression> domainDeclarations, OWLProperty property) {
    if (domainDeclarations.isEmpty()) {
      return Optional.empty()
    }
    if (domainDeclarations.size() > 1) {
      logger.warn("More than 1 domain declarations found for property $property but only 1 is supported at the time. " +
          "Domain will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found domain declarations: $domainDeclarations")
      return Optional.empty()
    }
    if (domainDeclarations.first() && !(domainDeclarations.first() instanceof OWLClass)) {
      logger.warn("Unsupported type of domain declaration found for property $property. " +
          "Domain will be determined from other axioms or default one (Thing) will be used if it will be impossible. " +
          "Found domain declaration: ${domainDeclarations.first()}")
      return Optional.empty()
    }
    return Optional.ofNullable(domainDeclarations.first() as OWLClass)
  }
}
