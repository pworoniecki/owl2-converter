package owl2converter.engine.generator.method

import owl2converter.groovymetamodel.method.Method

/**
 * Generic trait responsible for method's body source code generation.
 *
 * @param <T> the method class or its subclass that is supported by the generator
 */
trait MethodBodyCodeGenerator<T extends Method> {

  /**
   * Checks whether the specific generator supports type of given {@code method}.
   * Supported type is specified by {@link MethodBodyCodeGenerator#getSupportedClass} method
   *
   * @param method a method which class is to be supported by the specific generator
   * @return true if the generator supports the method's type, false otherwise
   */
  boolean supports(Method method) {
    return method?.class == supportedClass
  }

  /**
   * Generates the source code for the method.
   *
   * @param method a method for which the code is to be generated
   * @return the string with full body of the method
   */
  abstract String generateCode(T method)

  /**
   * Returns supported type for which the generator can generate source code
   *
   * @return the class info presenting the supported class, i.e. Method class or its subclass
   */
  abstract Class<T> getSupportedClass()
}
