package owl2converter.engine.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class NotSupportedOwlElementException extends Exception {
}
