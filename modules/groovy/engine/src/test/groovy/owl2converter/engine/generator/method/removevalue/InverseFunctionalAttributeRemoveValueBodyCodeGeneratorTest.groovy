package owl2converter.engine.generator.method.removevalue

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.RemoveValueMethod
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.INVERSE_FUNCTIONAL
import static owl2converter.engine.test.generator.model.RemoveValueMethodTestFactory.createRemoveValueMethod

class InverseFunctionalAttributeRemoveValueBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private InverseFunctionalAttributeRemoveValueBodyCodeGenerator generator = new InverseFunctionalAttributeRemoveValueBodyCodeGenerator()

  def 'should support remove value method of inverse functional attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withCharacteristics([INVERSE_FUNCTIONAL]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    generator.supports(removeValueMethod)
  }

  def 'should not support remove value method of non inverse functional attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    !generator.supports(removeValueMethod)
  }

  def 'should generate no validation code of remove value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([INVERSE_FUNCTIONAL]).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(removeValueMethod)

    then:
    !generatedCode.isPresent()
  }

  def 'should generate no specific remove value code of remove value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([INVERSE_FUNCTIONAL]).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(removeValueMethod)

    then:
    !generatedCode.isPresent()
  }
}
