package owl2converter.engine.utils

import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import spock.lang.Specification
import spock.lang.Subject

class GroovyCodeFormatterTest extends Specification {

  private static int INDENTATION_STEP = 2
  private static String SINGLE_INDENTATION = ' ' * INDENTATION_STEP

  @Subject
  private GroovyCodeFormatter formatter

  def setup() {
    formatter = new GroovyCodeFormatter(createConfiguration(INDENTATION_STEP))
  }

  def 'should return empty code for empty input code'() {
    expect:
    formatter.format('').isEmpty()
  }

  def 'should increase indentation level when line ends with {'() {
    when:
    def sourceCode = '''class Test {
                       |bodyOfClass {
                       |nestedBody'''.stripMargin()

    then:
    formatter.format(sourceCode) == """class Test {
                                      |${SINGLE_INDENTATION}bodyOfClass {
                                      |${SINGLE_INDENTATION * 2}nestedBody""".stripMargin()

  }

  def 'should not increase indentation level when line contains { but does not end with it'() {
    when:
    def sourceCode = '''expressionOne { expressionTwo
                       |expressionThree'''.stripMargin()

    then:
    formatter.format(sourceCode) == sourceCode
  }

  def 'should decrease indentation level when line ends with } and does not contain {'() {
    when:
    def sourceCode = '''class Test {
                       |bodyOfClass
                       |}'''.stripMargin()

    then:
    formatter.format(sourceCode) == """class Test {
                                      |${SINGLE_INDENTATION}bodyOfClass
                                      |}""".stripMargin()
  }

  def 'should not decrease indentation level when line ends with } but also contains {'() {
    when:
    def sourceCode = '''collection.each { print $it }
                       |print test'''.stripMargin()

    then:
    formatter.format(sourceCode) == sourceCode
  }

  private static ApplicationConfiguration createConfiguration(int indentationStep) {
    def generatedClassConfiguration = new GeneratedClassesConfiguration(indentationStep: indentationStep)
    def generatorConfiguration = new GeneratorConfiguration(generatedClasses: generatedClassConfiguration)
    return new ApplicationConfiguration(generator: generatorConfiguration)
  }
}
