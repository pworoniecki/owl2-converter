package owl2converter.engine.configuration.model

import spock.lang.Specification
import spock.lang.Subject

@Subject(GeneratedClassesConfiguration)
class GeneratedClassesConfigurationTest extends Specification {

  def 'should return full package of generated class model composed of its base package and sub package'() {
    given:
    def basePackage = 'base'
    def subPackage = 'sub'

    when:
    def configuration = new GeneratedClassesConfiguration(basePackage: basePackage, modelSubPackage: subPackage)

    then:
    configuration.modelFullPackage == "$basePackage.$subPackage".toString()
  }

  def 'should return full package of generated exception class composed of its base package and sub package'() {
    given:
    def basePackage = 'base'
    def subPackage = 'sub'

    when:
    def configuration = new GeneratedClassesConfiguration(basePackage: basePackage, exceptionSubPackage: subPackage)

    then:
    configuration.exceptionFullPackage == "$basePackage.$subPackage".toString()
  }
}
