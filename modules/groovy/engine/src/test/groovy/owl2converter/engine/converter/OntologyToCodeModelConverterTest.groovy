package owl2converter.engine.converter

import owl2converter.engine.converter.attribute.AttributesExtractor
import owl2converter.engine.converter.trait.TraitsExtractor
import owl2converter.engine.test.TestOWLOntologyBuilder
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.ClassTestFactory
import owl2converter.engine.test.generator.model.TraitTestFactory
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.attribute.Attribute
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import spock.lang.Specification
import spock.lang.Subject

class OntologyToCodeModelConverterTest extends Specification {

  private static final OWLOntology ONTOLOGY = new TestOWLOntologyBuilder().withObjectPropertyDeclaration('testProperty')
      .withClassDeclaration('TestClass').build()
  private static final Map<OWLProperty, Attribute> ATTRIBUTES = [(getObjectProperty()): AttributeTestFactory.createAttribute()]
  private static final Map<OWLClass, List<Trait>> TRAITS = [(getOwlClass()): [TraitTestFactory.createTrait()]]

  private AttributesExtractor attributesExtractor = Mock()
  private ClassesExtractor classesExtractor = Mock()
  private TraitsExtractor traitsExtractor = Mock()
  private IndividualsDataSourceClassExtractor individualsDataSourceClassExtractor = Mock()

  @Subject
  private OntologyToCodeModelConverter ontologyToCodeConverter = new OntologyToCodeModelConverter(attributesExtractor,
          classesExtractor, traitsExtractor, individualsDataSourceClassExtractor)

  def 'should convert ontology to object oriented code model'() {
    given:
    List<Class> classes = [ClassTestFactory.createClass()]

    when:
    ObjectOrientedCodeModel code = ontologyToCodeConverter.convert(ONTOLOGY)

    then:
    1 * attributesExtractor.extract(ONTOLOGY) >> ATTRIBUTES
    1 * classesExtractor.extract(TRAITS) >> classes
    1 * traitsExtractor.extract(ONTOLOGY, ATTRIBUTES) >> TRAITS
    code.classes == classes
    code.traits == TRAITS.values().flatten().toList()
  }

  private static OWLObjectProperty getObjectProperty() {
    return ONTOLOGY.objectPropertiesInSignature().findFirst().get()
  }

  private static OWLClass getOwlClass() {
    return ONTOLOGY.classesInSignature().findFirst().get()
  }
}
