package owl2converter.engine.generator.method.getter

import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.GetterTestFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class GenericGetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private GenericGetterBodyCodeGenerator generator = new GenericGetterBodyCodeGenerator()

  def 'should support getter of attribute not related to other attributes'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def getter = GetterTestFactory.createMethod(attribute)

    then:
    generator.supports(getter)
  }

  def 'should generate body code of getter'() {
    given:
    def attributeName = 'testAttribute'
    def attribute = AttributeTestFactory.createAttribute(attributeName)
    def getter = GetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateCode(getter)

    then:
    generatedCode == "return this.$attributeName".toString()
  }
}
