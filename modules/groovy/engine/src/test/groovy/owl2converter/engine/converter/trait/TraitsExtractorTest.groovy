package owl2converter.engine.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.converter.TraitInstanceMethodsFactory
import owl2converter.engine.converter.attribute.AttributeMethodsFactory
import owl2converter.engine.converter.attribute.PredefinedTypes
import owl2converter.engine.converter.attribute.domain.PropertyDomainExtractor
import owl2converter.engine.exception.NotSupportedOwlElementException
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.UnionTrait
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SetType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import owl2converter.engine.test.TestOWLOntologyBuilder
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.converter.trait.EquivalentTraitsExtractor.EQUIVALENT_TRAIT_NAME_SUFFIX
import static owl2converter.engine.converter.trait.TraitsExtractor.ALL_INSTANCES_ATTRIBUTE_NAME
import static owl2converter.engine.converter.trait.TraitsExtractor.TRAIT_NAME_SUFFIX
import static owl2converter.groovymetamodel.AccessModifier.PRIVATE
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static owl2converter.engine.test.generator.model.AttributeTestFactory.createAttribute
import static owl2converter.engine.test.generator.model.MethodTestFactory.createMethod

class TraitsExtractorTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(
          generatedClasses: new GeneratedClassesConfiguration(basePackage: 'org', modelSubPackage: 'test'))
  )
  private static final String CLASS_PACKAGE = CONFIGURATION.generator.generatedClasses.modelFullPackage
  private static final String CLASS_1_NAME = 'TestClass1'
  private static final String CLASS_2_NAME = 'TestClass2'
  private static final String CLASS_3_NAME = 'TestClass3'
  private static final String PROPERTY_1_NAME = 'property1'
  private static final String PROPERTY_2_NAME = 'property2'
  private static final Map<OWLProperty, Attribute> NONE_ATTRIBUTES = [:]
  private static final String ATTRIBUTE_CLASS_PACKAGE = 'org.package'
  private static final Type ATTRIBUTE_TYPE = new SimpleType(String)
  private static final List<Method> INSTANCE_RELATED_METHODS =
      [createMethod('instance-method-1', PUBLIC), createMethod('instance-method-2', PUBLIC)]
  private static final List<Method> METHODS_1_LIST = [createMethod('method1-1', PRIVATE), createMethod('method1-2', PRIVATE)]
  private static final List<Method> METHODS_2_LIST = [createMethod('method2-1', PRIVATE), createMethod('method2-2', PRIVATE)]

  private PropertyDomainExtractor propertyDomainExtractor = Mock()
  private AttributeMethodsFactory attributeMethodsFactory = Mock()
  private TraitInstanceMethodsFactory instanceMethodsFactory = Mock()
  private PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

  private ClassPropertyAttributesExtractor classPropertyAttributesExtractor =
      new ClassPropertyAttributesExtractor(propertyDomainExtractor)
  private UnionClassChecker unionClassChecker = new UnionClassChecker()
  private TraitBuilderFactory traitBuilderFactory = new TraitBuilderFactory(CONFIGURATION, unionClassChecker)
  private AnnotationsExtractor annotationsExtractor = new AnnotationsExtractor(CONFIGURATION)
  private SimpleBaseTraitExtractor simpleBaseTraitsExtractor = new SimpleBaseTraitExtractor(attributeMethodsFactory,
      classPropertyAttributesExtractor, traitBuilderFactory, annotationsExtractor)
  private UnionSubclassesExtractor unionSubclassesExtractor = new UnionSubclassesExtractor()
  private BaseTraitsExtractor baseTraitsExtractor = new BaseTraitsExtractor(simpleBaseTraitsExtractor, unionSubclassesExtractor,
      predefinedTypes)
  private EquivalentTraitsExtractor equivalentTraitsExtractor = new EquivalentTraitsExtractor(CONFIGURATION)

  @Subject
  private TraitsExtractor traitsExtractor = new TraitsExtractor(baseTraitsExtractor, instanceMethodsFactory,
      equivalentTraitsExtractor)

  def setup() {
    instanceMethodsFactory.create(*_) >> []
    attributeMethodsFactory.createMethods(_) >> []
  }

  def 'should return only Thing trait if there are no classes declared in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().build()

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    traits == [(getOwlThingClass(ontology)): [predefinedTypes.thingTrait]]
  }

  def 'should return name of single trait related to a class declared in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).build()

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    traits.keySet().toList() == [ontology.classesInSignature().findFirst().get(), getOwlThingClass(ontology)]
    traits.values().flatten()*.name == [CLASS_1_NAME + TRAIT_NAME_SUFFIX, predefinedTypes.thingTrait.name]
  }

  def 'should return names of multiple traits related to multiple classes declared in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .build()

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    traits.keySet().toList() == ontology.classesInSignature().findAll() + getOwlThingClass(ontology)
    traits.values().flatten()*.name.sort() == [CLASS_1_NAME + TRAIT_NAME_SUFFIX, CLASS_2_NAME + TRAIT_NAME_SUFFIX,
                                               predefinedTypes.thingTrait.name].sort()
  }

  def 'should assign attributes to traits'() {
    given:
    def classesCount = 2
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withDataPropertyDeclaration(PROPERTY_1_NAME).withObjectPropertyDeclaration(PROPERTY_2_NAME)
        .withDataPropertyDomain(PROPERTY_1_NAME, CLASS_1_NAME).withObjectPropertyDomain(PROPERTY_2_NAME, CLASS_2_NAME)
        .build()
    OWLProperty property1 = ontology.dataPropertiesInSignature().findFirst().get()
    OWLProperty property2 = ontology.objectPropertiesInSignature().findFirst().get()
    Map<OWLProperty, Attribute> attributes = [
        (property1): createAttribute(PROPERTY_1_NAME, ATTRIBUTE_TYPE, PRIVATE),
        (property2): createAttribute(PROPERTY_2_NAME, ATTRIBUTE_TYPE, PRIVATE)
    ]

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, attributes)

    then:
    classesCount * propertyDomainExtractor.extractDomain(property1, ontology) >>
        new SimpleType(ATTRIBUTE_CLASS_PACKAGE, CLASS_1_NAME)
    classesCount * propertyDomainExtractor.extractDomain(property2, ontology) >>
        new SimpleType(ATTRIBUTE_CLASS_PACKAGE, CLASS_2_NAME)
    findTraitByClassName(CLASS_1_NAME, traits).attributes ==
        [getExpectedAllInstancesAttribute(CLASS_1_NAME)] + attributes[property1]
    findTraitByClassName(CLASS_2_NAME, traits).attributes ==
        [getExpectedAllInstancesAttribute(CLASS_2_NAME)] + attributes[property2]
  }

  def 'should assign list-type attributes to traits using their parametrized types'() {
    given:
    def classesCount = 2
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withDataPropertyDeclaration(PROPERTY_1_NAME).withObjectPropertyDeclaration(PROPERTY_2_NAME)
        .withDataPropertyDomain(PROPERTY_1_NAME, CLASS_1_NAME).withObjectPropertyDomain(PROPERTY_2_NAME, CLASS_2_NAME)
        .build()
    OWLProperty property1 = ontology.dataPropertiesInSignature().findFirst().get()
    OWLProperty property2 = ontology.objectPropertiesInSignature().findFirst().get()
    Map<OWLProperty, Attribute> attributes = [
        (property1): createAttribute(PROPERTY_1_NAME, ATTRIBUTE_TYPE, PRIVATE),
        (property2): createAttribute(PROPERTY_2_NAME, ATTRIBUTE_TYPE, PRIVATE)
    ]

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, attributes)

    then:
    classesCount * propertyDomainExtractor.extractDomain(property1, ontology) >>
        new ListType(new SimpleType(ATTRIBUTE_CLASS_PACKAGE, CLASS_1_NAME))
    classesCount * propertyDomainExtractor.extractDomain(property2, ontology) >>
        new ListType(new SimpleType(ATTRIBUTE_CLASS_PACKAGE, CLASS_2_NAME))
    findTraitByClassName(CLASS_1_NAME, traits).attributes ==
        [getExpectedAllInstancesAttribute(CLASS_1_NAME)] + attributes[property1]
    findTraitByClassName(CLASS_2_NAME, traits).attributes ==
        [getExpectedAllInstancesAttribute(CLASS_2_NAME)] + attributes[property2]
  }

  def 'should generate all class instances attribute even if there are no properties in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).build()

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    List<Attribute> traitAttributes = findTraitByClassName(CLASS_1_NAME, traits).attributes
    traitAttributes.size() == 1
    assertEqual(traitAttributes.first(), getExpectedAllInstancesAttribute(CLASS_1_NAME))
  }

  def 'should generate all class instances attribute for class which is equivalent to another class'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withEquivalentClasses(CLASS_1_NAME, CLASS_2_NAME).build()
    String expectedEquivalentTraitName = CLASS_1_NAME + CLASS_2_NAME + EQUIVALENT_TRAIT_NAME_SUFFIX + TRAIT_NAME_SUFFIX

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    List<Attribute> traitAttributes = findTraitByClassName(CLASS_1_NAME, traits).attributes
    traitAttributes.size() == 1
    with(traitAttributes.first()) {
      name == ALL_INSTANCES_ATTRIBUTE_NAME
      valueType.fullName == new SetType(new SimpleType(CLASS_PACKAGE, expectedEquivalentTraitName)).fullName
    }
  }

  def 'should assign inherited traits to traits'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withSubClassOf(CLASS_2_NAME, CLASS_1_NAME).withSubClassOf(CLASS_2_NAME, CLASS_3_NAME)
        .build()

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    findTraitByClassName(CLASS_1_NAME, traits).inheritedTraits == [predefinedTypes.thingTrait]
    findTraitByClassName(CLASS_2_NAME, traits).inheritedTraits ==
        [findTraitByClassName(CLASS_1_NAME, traits), findTraitByClassName(CLASS_3_NAME, traits)]
    findTraitByClassName(CLASS_3_NAME, traits).inheritedTraits == [predefinedTypes.thingTrait]
  }

  def 'should assign methods to traits'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withDataPropertyDeclaration(PROPERTY_1_NAME).withObjectPropertyDeclaration(PROPERTY_2_NAME)
        .withDataPropertyDomain(PROPERTY_1_NAME, CLASS_1_NAME).withObjectPropertyDomain(PROPERTY_2_NAME, CLASS_2_NAME)
        .build()
    OWLProperty property1 = ontology.dataPropertiesInSignature().findFirst().get()
    OWLProperty property2 = ontology.objectPropertiesInSignature().findFirst().get()
    Map<OWLProperty, Attribute> attributes = [
        (property1): createAttribute(PROPERTY_1_NAME, ATTRIBUTE_TYPE, PRIVATE),
        (property2): createAttribute(PROPERTY_2_NAME, ATTRIBUTE_TYPE, PRIVATE)
    ]
    propertyDomainExtractor.extractDomain(property1, ontology) >> new SimpleType(ATTRIBUTE_CLASS_PACKAGE, CLASS_1_NAME)
    propertyDomainExtractor.extractDomain(property2, ontology) >> new SimpleType(ATTRIBUTE_CLASS_PACKAGE, CLASS_2_NAME)

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, attributes)

    then:
    [CLASS_1_NAME, CLASS_2_NAME, getOwlThingClass(ontology).getIRI().remainder.get()].each {
      1 * instanceMethodsFactory.create(*_) >> INSTANCE_RELATED_METHODS
    }
    1 * attributeMethodsFactory.createMethods(attributes[property1]) >> METHODS_1_LIST
    1 * attributeMethodsFactory.createMethods(attributes[property2]) >> METHODS_2_LIST
    findTraitByClassName(CLASS_1_NAME, traits).methods.sort() == [INSTANCE_RELATED_METHODS, METHODS_1_LIST].flatten().sort()
    findTraitByClassName(CLASS_2_NAME, traits).methods.sort() == [INSTANCE_RELATED_METHODS, METHODS_2_LIST].flatten().sort()
  }

  def 'should assign no annotations to traits'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .build()
    Map<OWLProperty, Attribute> attributes = NONE_ATTRIBUTES

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, attributes)

    then:
    traits.values().flatten()*.annotations.each { assert it.isEmpty() }
  }

  def 'should assign DisjointWith annotation to disjoint traits using disjoint union axiom'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withDisjointUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()
    Map<OWLProperty, Attribute> attributes = NONE_ATTRIBUTES

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, attributes)

    then:
    assertSingleAnnotationInTrait(findTraitByClassName(CLASS_1_NAME, traits), 'DisjointWith',
        [CLASS_2_NAME + TRAIT_NAME_SUFFIX, CLASS_3_NAME + TRAIT_NAME_SUFFIX])
    assertSingleAnnotationInTrait(findTraitByClassName(CLASS_2_NAME, traits), 'DisjointWith',
        [CLASS_1_NAME + TRAIT_NAME_SUFFIX, CLASS_3_NAME + TRAIT_NAME_SUFFIX])
    assertSingleAnnotationInTrait(findTraitByClassName(CLASS_3_NAME, traits), 'DisjointWith',
        [CLASS_1_NAME + TRAIT_NAME_SUFFIX, CLASS_2_NAME + TRAIT_NAME_SUFFIX])
  }

  def 'should assign DisjointWith annotation to disjoint traits using disjoint classes axiom'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withDisjointClasses(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()
    Map<OWLProperty, Attribute> attributes = NONE_ATTRIBUTES

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, attributes)

    then:
    assertSingleAnnotationInTrait(findTraitByClassName(CLASS_1_NAME, traits), 'DisjointWith',
        [CLASS_2_NAME + TRAIT_NAME_SUFFIX, CLASS_3_NAME + TRAIT_NAME_SUFFIX])
    assertSingleAnnotationInTrait(findTraitByClassName(CLASS_2_NAME, traits), 'DisjointWith',
        [CLASS_1_NAME + TRAIT_NAME_SUFFIX, CLASS_3_NAME + TRAIT_NAME_SUFFIX])
    assertSingleAnnotationInTrait(findTraitByClassName(CLASS_3_NAME, traits), 'DisjointWith',
        [CLASS_1_NAME + TRAIT_NAME_SUFFIX, CLASS_2_NAME + TRAIT_NAME_SUFFIX])
  }

  def 'should create one trait for equivalent classes in addition to their traits'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withEquivalentClasses(CLASS_1_NAME, CLASS_2_NAME).build()
    String expectedEquivalentTraitName = CLASS_1_NAME + CLASS_2_NAME + EQUIVALENT_TRAIT_NAME_SUFFIX + TRAIT_NAME_SUFFIX

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    traits.keySet().toList() == ontology.classesInSignature().findAll() + getOwlThingClass(ontology)
    traits.values()[0]*.name.sort() == [CLASS_2_NAME + TRAIT_NAME_SUFFIX, expectedEquivalentTraitName].sort()
    traits.values()[1]*.name.sort() == [CLASS_1_NAME + TRAIT_NAME_SUFFIX, expectedEquivalentTraitName].sort()
    traits.values()[0].find { it.name == expectedEquivalentTraitName }
        .is(traits.values()[1].find { it.name == expectedEquivalentTraitName })
  }

  def 'should create trait for equivalent classes in the same package as them and with inherited traits'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withEquivalentClasses(CLASS_1_NAME, CLASS_2_NAME).build()
    String expectedEquivalentTraitName = CLASS_1_NAME + CLASS_2_NAME + EQUIVALENT_TRAIT_NAME_SUFFIX + TRAIT_NAME_SUFFIX

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    Trait equivalentTrait = traits.values().first().find { it.name == expectedEquivalentTraitName }
    with(equivalentTrait) {
      packageName == CLASS_PACKAGE
      inheritedTraits*.name.sort() == [CLASS_1_NAME, CLASS_2_NAME].sort().collect { it + TRAIT_NAME_SUFFIX }
      annotations.isEmpty()
      attributes.isEmpty()
      methods.isEmpty()
    }
  }

  def 'should throw an exception when more than one class is equivalent to given class'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withEquivalentClasses(CLASS_1_NAME, CLASS_2_NAME)
        .withEquivalentClasses(CLASS_1_NAME, CLASS_3_NAME).build()

    when:
    traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    thrown NotSupportedOwlElementException
  }

  def 'should create union trait and assign it to inherited traits of equivalent union classes'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withClassesEquivalentOfUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    findTraitByClassName(CLASS_1_NAME, traits) instanceof UnionTrait
    findTraitByClassName(CLASS_2_NAME, traits).inheritedTraits*.name == [CLASS_1_NAME + TRAIT_NAME_SUFFIX]
    findTraitByClassName(CLASS_3_NAME, traits).inheritedTraits*.name == [CLASS_1_NAME + TRAIT_NAME_SUFFIX]
  }

  def 'should create two union traits and assign them to inherited traits of equivalent union classes'() {
    given:
    String class4Name = 'Class4'
    String class5Name = 'Class5'
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withClassDeclaration(class4Name).withClassDeclaration(class5Name)
        .withClassesEquivalentOfUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME)
        .withClassesEquivalentOfUnion(class4Name, CLASS_2_NAME, class5Name).build()

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    findTraitByClassName(CLASS_1_NAME, traits) instanceof UnionTrait
    findTraitByClassName(CLASS_2_NAME, traits).inheritedTraits*.name.sort() ==
        [CLASS_1_NAME + TRAIT_NAME_SUFFIX, class4Name + TRAIT_NAME_SUFFIX].sort()
    findTraitByClassName(CLASS_3_NAME, traits).inheritedTraits*.name == [CLASS_1_NAME + TRAIT_NAME_SUFFIX]
    findTraitByClassName(class4Name, traits) instanceof UnionTrait
    findTraitByClassName(class5Name, traits).inheritedTraits*.name == [class4Name + TRAIT_NAME_SUFFIX]
  }

  def 'should create no instance attribute nor instance related methods for union trait'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withClassesEquivalentOfUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    UnionTrait unionTrait = findTraitByClassName(CLASS_1_NAME, traits) as UnionTrait
    unionTrait.attributes.isEmpty()
    unionTrait.methods.isEmpty()
  }

  def 'should create union trait when disjoint union axiom is used'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withDisjointUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()

    when:
    Map<OWLClass, List<Trait>> traits = traitsExtractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    findTraitByClassName(CLASS_1_NAME, traits) instanceof UnionTrait
    findTraitByClassName(CLASS_2_NAME, traits).inheritedTraits*.name == [CLASS_1_NAME + TRAIT_NAME_SUFFIX]
    findTraitByClassName(CLASS_3_NAME, traits).inheritedTraits*.name == [CLASS_1_NAME + TRAIT_NAME_SUFFIX]
  }

  private static Trait findTraitByClassName(String className, Map<OWLClass, List<Trait>> traits) {
    return traits.values().flatten().find { it.name == className + TRAIT_NAME_SUFFIX }
  }

  private static void assertSingleAnnotationInTrait(Trait traitModel, String name, List<String> parameters) {
    assert traitModel.annotations.size() == 1
    traitModel.annotations.first().with {
      assert it.type.simpleName == name
      assert it.parameters == parameters
    }
  }

  private static Attribute getExpectedAllInstancesAttribute(String className) {
    def type = new SetType(new SimpleType(CLASS_PACKAGE, className + TRAIT_NAME_SUFFIX))
    return new Attribute.AttributeBuilder(ALL_INSTANCES_ATTRIBUTE_NAME, type).asStatic().withDefaultValue('[].toSet()')
        .withAccessModifier(PRIVATE).buildAttribute()
  }

  private static void assertEqual(Attribute attribute1, Attribute attribute2) {
    attribute1.with {
      assert name == attribute2.name
      assert valueType == attribute2.valueType
      assert defaultValue == attribute2.defaultValue
      assert isStatic() == attribute2.isStatic()
      assert accessModifier == attribute2.accessModifier
      assert relations == attribute2.relations
      assert characteristics == attribute2.characteristics
    }
  }

  private static OWLClass getOwlThingClass(OWLOntology ontology) {
    return ontology.OWLOntologyManager.OWLDataFactory.OWLThing
  }
}
