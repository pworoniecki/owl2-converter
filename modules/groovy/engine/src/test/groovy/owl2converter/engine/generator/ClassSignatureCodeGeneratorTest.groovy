package owl2converter.engine.generator

import owl2converter.groovymetamodel.Class
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.test.generator.model.ClassTestFactory.createEmptyClass
import static owl2converter.engine.test.generator.model.TraitTestFactory.createTrait

class ClassSignatureCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private ClassSignatureCodeGenerator generator = new ClassSignatureCodeGenerator()

  def 'should generate class signature for class that inherits no class and implements no trait'() {
    when:
    def testClass = createEmptyClass()

    then:
    generator.generateClassSignature(testClass) == "class $testClass.name {".toString()
  }

  def 'should generate class signature for class that inherits some class but implements no trait'() {
    given:
    def inheritedClass = createEmptyClass()

    when:
    def testClass = new Class.Builder('org.test', 'TestClass').withInheritedClass(inheritedClass).buildClass()

    then:
    generator.generateClassSignature(testClass) == "class $testClass.name extends $inheritedClass.name {".toString()
  }

  def 'should generate class signature for class that inherits no class but implements some trait'() {
    given:
    def implementedTrait = createTrait()

    when:
    def testClass = new Class.Builder('org.test', 'TestClass').withImplementedTraits([implementedTrait])
        .buildClass()

    then:
    generator.generateClassSignature(testClass) ==
        "class $testClass.name implements ${implementedTrait.name} {".toString()
  }

  def 'should generate class signature for class that inherits some class and implements some trait'() {
    given:
    def inheritedClass = createEmptyClass()
    def implementedTrait = createTrait()

    when:
    def testClass = new Class.Builder('org.test', 'TestClass').withInheritedClass(inheritedClass)
        .withImplementedTraits([implementedTrait]).buildClass()

    then:
    generator.generateClassSignature(testClass) ==
        "class $testClass.name extends $inheritedClass.name implements ${implementedTrait.name} {".toString()
  }
}
