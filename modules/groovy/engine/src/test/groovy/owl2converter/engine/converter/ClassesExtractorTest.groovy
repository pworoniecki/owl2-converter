package owl2converter.engine.converter

import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.Trait
import owl2converter.engine.test.TestOWLOntologyBuilder
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.AccessModifier.PRIVATE
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static owl2converter.engine.test.generator.model.TraitTestFactory.createTrait

class ClassesExtractorTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(
          generatedClasses: new GeneratedClassesConfiguration(basePackage: 'org', modelSubPackage: 'test'))
  )
  private static final String CLASS_1_NAME = 'TestClass1'
  private static final String CLASS_2_NAME = 'TestClass2'
  private static final String CLASS_3_NAME = 'TestClass3'
  private static final String PROPERTY_1_NAME = 'property1'
  private static final String PROPERTY_2_NAME = 'property2'

  private ConstructorsExtractor constructorsExtractor = Mock()

  @Subject
  private ClassesExtractor classesExtractor = new ClassesExtractor(CONFIGURATION, constructorsExtractor)

  def setup() {
    constructorsExtractor.extract(*_) >> []
  }

  def 'should return no classes if there are classes declared in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().build()

    when:
    List<Class> classes = classesExtractor.extract(createMockTraits(ontology))

    then:
    classes.isEmpty()
  }

  def 'should return name of single class declared in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).build()

    when:
    List<Class> classes = classesExtractor.extract(createMockTraits(ontology))

    then:
    classes*.name == [CLASS_1_NAME]
  }

  def 'should return names of multiple classes declared in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .build()

    when:
    List<Class> classes = classesExtractor.extract(createMockTraits(ontology))

    then:
    classes*.name.sort() == [CLASS_1_NAME, CLASS_2_NAME].sort()
  }

  def 'should assign no attributes to class'() {
    given:
    OWLOntology ontology = createOntologyWithTwoProperties()

    when:
    List<Class> classes = classesExtractor.extract(createMockTraits(ontology))

    then:
    classes*.attributes.flatten().isEmpty()
  }

  def 'should assign no inherited class to class even if there is inheritance defined in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withSubClassOf(CLASS_2_NAME, CLASS_1_NAME).build()

    when:
    List<Class> classes = classesExtractor.extract(createMockTraits(ontology))

    then:
    classes.each { assert !it.inheritedClass.isPresent() }
  }

  def 'should assign no methods to class'() {
    given:
    OWLOntology ontology = createOntologyWithTwoProperties()

    when:
    List<Class> classes = classesExtractor.extract(createMockTraits(ontology))

    then:
    classes*.methods.flatten().isEmpty()
  }

  def 'should assign trait created for class to implemented traits of class'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withSubClassOf(CLASS_2_NAME, CLASS_1_NAME).withSubClassOf(CLASS_2_NAME, CLASS_3_NAME)
        .build()
    OWLClass class1 = ontology.classesInSignature().find { it.getIRI().remainder.get() == CLASS_1_NAME } as OWLClass
    OWLClass class2 = ontology.classesInSignature().find { it.getIRI().remainder.get() == CLASS_2_NAME } as OWLClass
    OWLClass class3 = ontology.classesInSignature().find { it.getIRI().remainder.get() == CLASS_3_NAME } as OWLClass
    Map<OWLClass, List<Trait>> traits = [
        (class1): [createTrait(CLASS_1_NAME)],
        (class2): [createTrait(CLASS_2_NAME)],
        (class3): [createTrait(CLASS_3_NAME)]
    ]

    when:
    List<Class> classes = classesExtractor.extract(traits)

    then:
    findClassByName(CLASS_1_NAME, classes).implementedTraits == traits[class1]
    findClassByName(CLASS_2_NAME, classes).implementedTraits == traits[class2]
    findClassByName(CLASS_3_NAME, classes).implementedTraits == traits[class3]
  }

  def 'should assign whole set of traits to implemented traits of class including those inherited by others'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).build()
    OWLClass class1 = ontology.classesInSignature().find { it.getIRI().remainder.get() == CLASS_1_NAME } as OWLClass
    Trait trait1 = createTrait('Trait1')
    Trait trait2 = createTrait('Trait2')
    Trait trait3 = new Trait.Builder('package', 'Trait3').withInheritedTraits([trait2]).buildTrait()
    Trait trait4 = new Trait.Builder('package', 'Trait4').withInheritedTraits([trait3]).buildTrait()
    Map<OWLClass, List<Trait>> traits = [(class1): [trait1, trait2, trait3, trait4]]

    when:
    List<Class> classes = classesExtractor.extract(traits)

    then:
    findClassByName(CLASS_1_NAME, classes).implementedTraits == traits[class1]
  }

  def 'should assign constructors to class'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .build()
    List<Constructor> constructors = [new Constructor(accessModifier: PUBLIC), new Constructor(accessModifier: PRIVATE),
                                      new Constructor(isStatic: true)]

    when:
    List<Class> classes = classesExtractor.extract(createMockTraits(ontology))

    then:
    2 * constructorsExtractor.extract() >> constructors
    classes.each { assert it.constructors == constructors }
  }

  private static Class findClassByName(String className, List<Class> classes) {
    return classes.find { it.name == className }
  }

  private static OWLOntology createOntologyWithTwoProperties() {
    return new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withDataPropertyDeclaration(PROPERTY_1_NAME).withObjectPropertyDeclaration(PROPERTY_2_NAME)
        .withDataPropertyDomain(PROPERTY_1_NAME, CLASS_1_NAME).withObjectPropertyDomain(PROPERTY_2_NAME, CLASS_2_NAME)
        .build()
  }

  private static Map<OWLClass, List<Trait>> createMockTraits(OWLOntology ontology) {
    List<OWLClass> owlClasses = ontology.classesInSignature().findAll()
    return owlClasses.collectEntries { [(it): [createTrait(it.getIRI().remainder.get())]] }
  }
}
