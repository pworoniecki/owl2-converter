package owl2converter.engine.converter

import owl2converter.groovymetamodel.EquivalentTrait
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.UnionTrait
import owl2converter.groovymetamodel.method.CustomMethod
import owl2converter.groovymetamodel.type.PrimitiveType
import owl2converter.groovymetamodel.type.VoidType
import owl2converter.engine.test.generator.model.TraitTestFactory
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static owl2converter.engine.converter.TraitInstanceMethodsFactory.ADD_INSTANCE_METHOD_NAME
import static owl2converter.engine.converter.TraitInstanceMethodsFactory.CONTAINS_INSTANCE_METHOD_NAME
import static owl2converter.engine.converter.TraitInstanceMethodsFactory.CREATE_INSTANCE_METHOD_NAME
import static owl2converter.engine.converter.TraitInstanceMethodsFactory.GET_INSTANCES_METHOD_NAME
import static owl2converter.engine.converter.TraitInstanceMethodsFactory.INSTANCE_PARAMETER_NAME
import static owl2converter.engine.converter.TraitInstanceMethodsFactory.REMOVE_INSTANCE_METHOD_NAME
import static owl2converter.engine.converter.TraitInstanceMethodsFactory.REMOVE_INSTANCE_RESULT_VARIABLE_NAME
import static owl2converter.engine.converter.trait.TraitsExtractor.ALL_INSTANCES_ATTRIBUTE_NAME
import static owl2converter.engine.converter.trait.TraitsExtractor.TRAIT_NAME_SUFFIX
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC

class TraitInstanceMethodsFactoryTest extends Specification {

  private static final String CLASS_NAME = 'Sample'
  private static final String TRAIT_NAME = CLASS_NAME + TRAIT_NAME_SUFFIX
  private static final String TRAIT_PACKAGE = 'org.test'
  private static final Trait TRAIT = TraitTestFactory.createTrait(TRAIT_NAME, TRAIT_PACKAGE)
  private static final String ANOTHER_CLASS_NAME = 'Another'
  private static final Trait ANOTHER_TRAIT = TraitTestFactory.createTrait(ANOTHER_CLASS_NAME + TRAIT_NAME_SUFFIX, TRAIT_PACKAGE)
  private static final EquivalentTrait EQUIVALENT_TRAIT = new EquivalentTrait.Builder(TRAIT.packageName, 'TestEquivalentTrait')
      .withInheritedTraits([TRAIT, ANOTHER_TRAIT]).buildTrait()
  private static final List<EquivalentTrait> NONE_EQUIVALENT_TRAITS = []
  private static final UnionTrait UNION_TRAIT = createUnionTraitOf(CLASS_NAME, ANOTHER_CLASS_NAME)

  @Subject
  private TraitInstanceMethodsFactory factory = new TraitInstanceMethodsFactory()

  def 'should return create instance method'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == CREATE_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      parameters.isEmpty()
      accessModifier == PUBLIC
      isStatic()
      returnType.fullName == "$TRAIT_PACKAGE.$CLASS_NAME"
      bodyCode == getExpectedCreateInstanceMethodCode()
    }
  }

  def 'should return create instance method for equivalent trait'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, [EQUIVALENT_TRAIT])

    then:
    CustomMethod method = methods.find { it.name == CREATE_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      parameters.isEmpty()
      accessModifier == PUBLIC
      isStatic()
      returnType.fullName == "$TRAIT_PACKAGE.$CLASS_NAME"
      bodyCode == getExpectedCreateInstanceMethodInEquivalentTraitCode(ANOTHER_CLASS_NAME)
    }
  }

  def 'should add created instance to parent classes except union ones in create instance method'() {
    given:
    String class1Name = 'Test1'
    String class2Name = 'Test2'
    String class3Name = 'Test3'
    Trait trait1 = TraitTestFactory.createEmptyTrait(class1Name + TRAIT_NAME_SUFFIX)
    Trait trait2 = TraitTestFactory.createEmptyTrait(class2Name + TRAIT_NAME_SUFFIX)
    UnionTrait unionTrait = new UnionTrait.Builder('org.test', 'UnionTrait').buildTrait()
    Trait trait3 = new Trait.Builder('org.test', class3Name + TRAIT_NAME_SUFFIX)
        .withInheritedTraits([trait1, trait2, unionTrait]).buildTrait()

    when:
    List<CustomMethod> methods = factory.create(trait3, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == CREATE_INSTANCE_METHOD_NAME }
    method.bodyCode == """def classInstance = new $class3Name()
                    |${ALL_INSTANCES_ATTRIBUTE_NAME}.add(classInstance)
                    |${class1Name}.${ADD_INSTANCE_METHOD_NAME}(classInstance)
                    |${class2Name}.${ADD_INSTANCE_METHOD_NAME}(classInstance)
                    |return classInstance""".stripMargin()
  }

  def 'should return non-supported create instance method for union trait'() {
    when:
    List<CustomMethod> methods = factory.create(UNION_TRAIT, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == CREATE_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      accessModifier == PUBLIC
      isStatic()
      returnType.fullName == "$TRAIT_PACKAGE.$CLASS_NAME"
      bodyCode == "throw new UnsupportedOperationException('Method not available in union class')"
    }
  }

  def 'should return add instance method'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == ADD_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      parameters.size() == 1
      parameters.first().name == INSTANCE_PARAMETER_NAME
      parameters.first().type.fullName == "$TRAIT_PACKAGE.$TRAIT_NAME"
      accessModifier == PUBLIC
      isStatic()
      returnType == VoidType.INSTANCE
      bodyCode == getExpectedAddInstanceMethodCode()
    }
  }

  def 'should return add instance method for equivalent trait'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, [EQUIVALENT_TRAIT])

    then:
    CustomMethod method = methods.find { it.name == ADD_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      parameters.size() == 1
      parameters.first().name == INSTANCE_PARAMETER_NAME
      parameters.first().type.fullName == "$TRAIT_PACKAGE.${EQUIVALENT_TRAIT.name}"
      accessModifier == PUBLIC
      isStatic()
      returnType == VoidType.INSTANCE
      bodyCode == getExpectedAddInstanceMethodInEquivalentTraitCode(ANOTHER_CLASS_NAME)
    }
  }

  def 'should return add instance method for union trait'() {
    when:
    List<CustomMethod> methods = factory.create(UNION_TRAIT, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == ADD_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      accessModifier == PUBLIC
      isStatic()
      returnType == VoidType.INSTANCE
      bodyCode == "throw new UnsupportedOperationException('Method not available in union class')"
    }
  }

  def 'should add instance to parent classes except union ones in add instance method'() {
    given:
    String class1Name = 'Test1'
    String class2Name = 'Test2'
    String class3Name = 'Test3'
    Trait trait1 = TraitTestFactory.createEmptyTrait(class1Name + TRAIT_NAME_SUFFIX)
    Trait trait2 = TraitTestFactory.createEmptyTrait(class2Name + TRAIT_NAME_SUFFIX)
    UnionTrait unionTrait = new UnionTrait.Builder('org.test', 'UnionTrait').buildTrait()
    Trait trait3 = new Trait.Builder('org.test', class3Name + TRAIT_NAME_SUFFIX)
        .withInheritedTraits([trait1, trait2, unionTrait]).buildTrait()

    when:
    List<CustomMethod> methods = factory.create(trait3, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == ADD_INSTANCE_METHOD_NAME }
    method.bodyCode == """if(${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)) {
                         |return
                         |}
                         |${ALL_INSTANCES_ATTRIBUTE_NAME}.add($INSTANCE_PARAMETER_NAME)
                         |${class1Name}.${ADD_INSTANCE_METHOD_NAME}($INSTANCE_PARAMETER_NAME)
                         |${class2Name}.${ADD_INSTANCE_METHOD_NAME}($INSTANCE_PARAMETER_NAME)""".stripMargin()
  }

  def 'should return remove instance method'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == REMOVE_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      parameters.size() == 1
      parameters.first().name == INSTANCE_PARAMETER_NAME
      parameters.first().type.fullName == "$TRAIT_PACKAGE.$TRAIT_NAME"
      accessModifier == PUBLIC
      isStatic()
      returnType == PrimitiveType.BOOLEAN
      bodyCode == getExpectedRemoveInstanceMethodCode()
    }
  }

  def 'should return remove instance method for equivalent trait'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, [EQUIVALENT_TRAIT])

    then:
    CustomMethod method = methods.find { it.name == REMOVE_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      parameters.size() == 1
      parameters.first().name == INSTANCE_PARAMETER_NAME
      parameters.first().type.fullName == "$TRAIT_PACKAGE.${EQUIVALENT_TRAIT.name}"
      accessModifier == PUBLIC
      isStatic()
      returnType == PrimitiveType.BOOLEAN
      bodyCode == getExpectedRemoveInstanceMethodInEquivalentTraitCode(ANOTHER_CLASS_NAME)
    }
  }

  def 'should return remove instance method for union trait'() {
    when:
    List<CustomMethod> methods = factory.create(UNION_TRAIT, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == REMOVE_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      accessModifier == PUBLIC
      isStatic()
      returnType == PrimitiveType.BOOLEAN
      bodyCode == getExpectedRemoveInstanceMethodInUnionTraitCode()
    }
  }

  def 'should remove instance from parent classes except union ones in remove instance method'() {
    given:
    String class1Name = 'Test1'
    String class2Name = 'Test2'
    String class3Name = 'Test3'
    Trait trait1 = TraitTestFactory.createEmptyTrait(class1Name + TRAIT_NAME_SUFFIX)
    Trait trait2 = TraitTestFactory.createEmptyTrait(class2Name + TRAIT_NAME_SUFFIX)
    UnionTrait unionTrait = new UnionTrait.Builder('org.test', 'UnionTrait').buildTrait()
    Trait trait3 = new Trait.Builder('org.test', class3Name + TRAIT_NAME_SUFFIX)
        .withInheritedTraits([trait1, trait2, unionTrait]).buildTrait()

    when:
    List<CustomMethod> methods = factory.create(trait3, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == REMOVE_INSTANCE_METHOD_NAME }
    method.bodyCode == """if(!${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)) {
                         |return false
                         |}
                         |${ALL_INSTANCES_ATTRIBUTE_NAME}.remove($INSTANCE_PARAMETER_NAME)
                         |${class1Name}.${REMOVE_INSTANCE_METHOD_NAME}($INSTANCE_PARAMETER_NAME)
                         |${class2Name}.${REMOVE_INSTANCE_METHOD_NAME}($INSTANCE_PARAMETER_NAME)
                         |return true""".stripMargin()
  }

  def 'should return contains instance method'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == CONTAINS_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      parameters.size() == 1
      parameters.first().name == INSTANCE_PARAMETER_NAME
      parameters.first().type.fullName == "$TRAIT_PACKAGE.$TRAIT_NAME"
      accessModifier == PUBLIC
      isStatic()
      returnType == PrimitiveType.BOOLEAN
      bodyCode == "${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)"
    }
  }

  def 'should return contains instance method for equivalent trait'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, [EQUIVALENT_TRAIT])

    then:
    CustomMethod method = methods.find { it.name == CONTAINS_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      parameters.size() == 1
      parameters.first().name == INSTANCE_PARAMETER_NAME
      parameters.first().type.fullName == "$TRAIT_PACKAGE.${EQUIVALENT_TRAIT.name}"
      accessModifier == PUBLIC
      isStatic()
      returnType == PrimitiveType.BOOLEAN
      bodyCode == "${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)"
    }
  }

  def 'should return contains instance method for union trait'() {
    when:
    List<CustomMethod> methods = factory.create(UNION_TRAIT, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == CONTAINS_INSTANCE_METHOD_NAME }
    method != null
    with(method) {
      parameters.size() == 1
      parameters.first().name == INSTANCE_PARAMETER_NAME
      parameters.first().type.fullName == "$TRAIT_PACKAGE.${UNION_TRAIT.name}"
      accessModifier == PUBLIC
      isStatic()
      returnType == PrimitiveType.BOOLEAN
      bodyCode == "return ${CLASS_NAME}.containsInstance($INSTANCE_PARAMETER_NAME as ${CLASS_NAME}${TRAIT_NAME_SUFFIX}) " +
          "|| ${ANOTHER_CLASS_NAME}.containsInstance($INSTANCE_PARAMETER_NAME as ${ANOTHER_CLASS_NAME}${TRAIT_NAME_SUFFIX})"
    }
  }

  def 'should return get instances method'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == GET_INSTANCES_METHOD_NAME }
    method != null
    with(method) {
      parameters.isEmpty()
      accessModifier == PUBLIC
      isStatic()
      returnType.fullName == "$Set.name<$TRAIT_PACKAGE.$TRAIT_NAME>"
      bodyCode == "return new HashSet<>($ALL_INSTANCES_ATTRIBUTE_NAME)"
    }
  }

  def 'should return get instances method for equivalent trait'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, [EQUIVALENT_TRAIT])

    then:
    CustomMethod method = methods.find { it.name == GET_INSTANCES_METHOD_NAME }
    method != null
    with(method) {
      parameters.isEmpty()
      accessModifier == PUBLIC
      isStatic()
      returnType.fullName == "$Set.name<$TRAIT_PACKAGE.${EQUIVALENT_TRAIT.name}>"
      bodyCode == "return new HashSet<>($ALL_INSTANCES_ATTRIBUTE_NAME)"
    }
  }

  def 'should return get instances method for union trait'() {
    given:
    UnionTrait unionTrait = createUnionTraitOf(CLASS_NAME, ANOTHER_CLASS_NAME)

    when:
    List<CustomMethod> methods = factory.create(unionTrait, NONE_EQUIVALENT_TRAITS)

    then:
    CustomMethod method = methods.find { it.name == GET_INSTANCES_METHOD_NAME }
    method != null
    with(method) {
      parameters.isEmpty()
      accessModifier == PUBLIC
      isStatic()
      returnType.fullName == "$Set.name<$TRAIT_PACKAGE.$TRAIT_NAME>"
      bodyCode == "return [] + ${CLASS_NAME}.getInstances() + ${ANOTHER_CLASS_NAME}.getInstances()"
    }
  }

  @Unroll
  def 'should return all instance related methods'() {
    when:
    List<CustomMethod> methods = factory.create(TRAIT, equivalentTraits)

    then:
    methods.size() == [CREATE_INSTANCE_METHOD_NAME, ADD_INSTANCE_METHOD_NAME, REMOVE_INSTANCE_METHOD_NAME,
                       CONTAINS_INSTANCE_METHOD_NAME, GET_INSTANCES_METHOD_NAME].size()

    where:
    equivalentTraits << [NONE_EQUIVALENT_TRAITS, [EQUIVALENT_TRAIT]]
  }

  private static UnionTrait createUnionTraitOf(String class1Name, String class2Name) {
    Trait trait1 = TraitTestFactory.createTrait(class1Name + TRAIT_NAME_SUFFIX)
    Trait trait2 = TraitTestFactory.createTrait(class2Name + TRAIT_NAME_SUFFIX)
    UnionTrait unionTrait = new UnionTrait.Builder(TRAIT_PACKAGE, TRAIT_NAME).buildTrait()
    unionTrait.addUnionOf([trait1, trait2])
    return unionTrait
  }

  private static String getExpectedCreateInstanceMethodCode() {
    assert TRAIT.inheritedTraits*.name == ['Empty' + TRAIT_NAME_SUFFIX]
    return """def classInstance = new $CLASS_NAME()
             |${ALL_INSTANCES_ATTRIBUTE_NAME}.add(classInstance)
             |Empty.addInstance(classInstance)
             |return classInstance""".stripMargin()
  }

  private static String getExpectedCreateInstanceMethodInEquivalentTraitCode(String equivalentClassName) {
    assert TRAIT.inheritedTraits*.name == ['Empty' + TRAIT_NAME_SUFFIX]
    return """def classInstance = new $CLASS_NAME()
             |${ALL_INSTANCES_ATTRIBUTE_NAME}.add(classInstance)
             |Empty.addInstance(classInstance)
             |${equivalentClassName}.addInstance(classInstance)
             |return classInstance""".stripMargin()
  }

  private static String getExpectedAddInstanceMethodCode() {
    assert TRAIT.inheritedTraits*.name == ['Empty' + TRAIT_NAME_SUFFIX]
    return [
        "if(${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)) {",
        'return',
        '}',
        "${ALL_INSTANCES_ATTRIBUTE_NAME}.add($INSTANCE_PARAMETER_NAME)",
        "Empty.addInstance($INSTANCE_PARAMETER_NAME)"
    ].join('\n')
  }

  private static String getExpectedAddInstanceMethodInEquivalentTraitCode(String equivalentClassName) {
    assert TRAIT.inheritedTraits*.name == ['Empty' + TRAIT_NAME_SUFFIX]
    return [
        "if(${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)) {",
        'return',
        '}',
        "${ALL_INSTANCES_ATTRIBUTE_NAME}.add($INSTANCE_PARAMETER_NAME)",
        "Empty.addInstance($INSTANCE_PARAMETER_NAME)",
        "${equivalentClassName}.addInstance($INSTANCE_PARAMETER_NAME)"
    ].join('\n')
  }

  private static String getExpectedRemoveInstanceMethodCode() {
    assert TRAIT.inheritedTraits*.name == ['Empty' + TRAIT_NAME_SUFFIX]
    return [
        "if(!${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)) {",
        'return false',
        '}',
        "${ALL_INSTANCES_ATTRIBUTE_NAME}.remove($INSTANCE_PARAMETER_NAME)",
        "Empty.removeInstance($INSTANCE_PARAMETER_NAME)",
        'return true'
    ].join('\n')
  }

  private static String getExpectedRemoveInstanceMethodInEquivalentTraitCode(String equivalentClassName) {
    assert TRAIT.inheritedTraits*.name == ['Empty' + TRAIT_NAME_SUFFIX]
    return [
        "if(!${ALL_INSTANCES_ATTRIBUTE_NAME}.contains($INSTANCE_PARAMETER_NAME)) {",
        'return false',
        '}',
        "${ALL_INSTANCES_ATTRIBUTE_NAME}.remove($INSTANCE_PARAMETER_NAME)",
        "Empty.removeInstance($INSTANCE_PARAMETER_NAME)",
        "${equivalentClassName}.removeInstance($INSTANCE_PARAMETER_NAME)",
        'return true'
    ].join('\n')
  }

  private static String getExpectedRemoveInstanceMethodInUnionTraitCode() {
    assert UNION_TRAIT.unionOf*.name == [CLASS_NAME, ANOTHER_CLASS_NAME].collect { it + TRAIT_NAME_SUFFIX }
    return [
        "boolean $REMOVE_INSTANCE_RESULT_VARIABLE_NAME",
        "if(${INSTANCE_PARAMETER_NAME}.class == $CLASS_NAME) {",
        "$REMOVE_INSTANCE_RESULT_VARIABLE_NAME = ${CLASS_NAME}.removeInstance($INSTANCE_PARAMETER_NAME as ${CLASS_NAME}${TRAIT_NAME_SUFFIX})",
        '}',
        "else if(${INSTANCE_PARAMETER_NAME}.class == $ANOTHER_CLASS_NAME) {",
        "$REMOVE_INSTANCE_RESULT_VARIABLE_NAME = ${ANOTHER_CLASS_NAME}.removeInstance($INSTANCE_PARAMETER_NAME as ${ANOTHER_CLASS_NAME}${TRAIT_NAME_SUFFIX})",
        '}',
        'else {',
        "throw new IllegalArgumentException('Unsupported instance type')",
        '}',
        "return $REMOVE_INSTANCE_RESULT_VARIABLE_NAME"
    ].join('\n')
  }
}
