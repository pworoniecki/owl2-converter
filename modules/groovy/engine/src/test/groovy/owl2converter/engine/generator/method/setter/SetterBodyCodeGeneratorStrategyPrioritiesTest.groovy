package owl2converter.engine.generator.method.setter

import owl2converter.engine.OWL2ToGroovyEngineConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import spock.lang.Specification
import spock.lang.Subject

@ContextConfiguration(loader = AnnotationConfigContextLoader, classes = OWL2ToGroovyEngineConfiguration)
class SetterBodyCodeGeneratorStrategyPrioritiesTest extends Specification {

  @Autowired
  @Subject
  private List<SetterBodyCodeGeneratorStrategy> generators

  def 'should assert equal priorities of all setter body code generators'() {
    expect:
    generators*.priority.unique().size() == 1
  }
}
