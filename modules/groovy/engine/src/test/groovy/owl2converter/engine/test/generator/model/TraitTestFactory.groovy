package owl2converter.engine.test.generator.model

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.type.SimpleType

import static owl2converter.engine.test.generator.model.AttributeTestFactory.createAttribute
import static owl2converter.engine.test.generator.model.MethodTestFactory.createMethod

class TraitTestFactory {

  static Trait createTrait(String traitName = 'TestTrait', String packageName = 'org.test') {
    return new Trait.Builder(packageName, traitName)
        .withAttributes([createAttribute('test', new SimpleType(String), AccessModifier.PUBLIC)])
        .withInheritedTraits([createEmptyTrait()])
        .withMethods([createMethod('test', AccessModifier.PRIVATE)])
        .buildTrait()
  }

  static Trait createEmptyTrait(String name = 'EmptyTrait') {
    return new Trait.Builder('org.test', name).buildTrait()
  }
}
