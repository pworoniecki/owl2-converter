package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.SetterTestFactory
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE

class ReflexiveAttributeSetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private ReflexiveAttributeSetterBodyCodeGenerator generator = new ReflexiveAttributeSetterBodyCodeGenerator()

  def 'should support setter of reflexive but not functional attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withCharacteristics([REFLEXIVE]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    generator.supports(setter)
  }

  def 'should not support setter of reflexive and functional attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withCharacteristics([REFLEXIVE, FUNCTIONAL]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)
  }

  def 'should not support setter of non reflexive attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)
  }

  def 'should generate validation code of list-type setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([REFLEXIVE]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)
    def expectedExceptionMessage = "Cannot set \$attribute as it does not contain 'this' instance - " +
        'it would break reflexive attribute rule'

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """if (!attribute.contains(this)) {
          |throw new ReflexiveAttributeException("$expectedExceptionMessage")
          |}""".stripMargin()
  }

  def 'should generate validation code of single-type setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([REFLEXIVE]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)
    def expectedExceptionMessage = "Cannot set \$attribute as it is not 'this' instance - it would break reflexive attribute rule"

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """if (attribute != this) {
          |throw new ReflexiveAttributeException("$expectedExceptionMessage")
          |}""".stripMargin()
  }

  def 'should generate no specific setting value code of setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([REFLEXIVE]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    !generatedCode.isPresent()
  }
}
