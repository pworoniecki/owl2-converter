package owl2converter.engine.generator.method

import owl2converter.groovymetamodel.method.Method
import owl2converter.engine.test.generator.model.TestMethod
import groovy.transform.InheritConstructors
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class MethodBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private MethodBodyCodeGenerator<TestMethod> generator = new TestMethodBodyCodeGenerator()

  def 'should support method of provided generic type'() {
    when:
    def method = new TestMethod.Builder('test').buildMethod()

    then:
    generator.supports(method)
  }

  def 'should not support method of type another than provided generic type'() {
    when:
    def method = new AnotherTestMethod.Builder('test').buildMethod()

    then:
    !generator.supports(method)
  }

  private class TestMethodBodyCodeGenerator implements MethodBodyCodeGenerator<TestMethod> {

    @Override
    String generateCode(TestMethod method) {
      return 'test'
    }

    @Override
    Class<TestMethod> getSupportedClass() {
      return TestMethod
    }
  }

  @InheritConstructors
  private static class AnotherTestMethod extends Method {
    @InheritConstructors
    static class Builder extends Method.Builder<AnotherTestMethod, Builder> {
      @Override
      Builder getThis() {
        return this
      }

      @Override
      AnotherTestMethod buildMethod() {
        return new AnotherTestMethod(this)
      }
    }
  }
}
