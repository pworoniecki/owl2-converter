package owl2converter.engine.generator.method.addvalue

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.relation.AttributeRelation
import owl2converter.groovymetamodel.attribute.relation.InverseAttributeRelation
import owl2converter.groovymetamodel.method.AddValueMethod
import owl2converter.groovymetamodel.method.DirectSetter
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.test.generator.model.AddValueMethodTestFactory.createAddValueMethod
import static owl2converter.engine.test.generator.model.AttributeTestFactory.createAttribute

class InverseAttributeAddValueBodyCodeGeneratorTest extends Specification {

  private static final List<Attribute> INVERSE_ATTRIBUTES = [
      createAttribute('attr1', Types.STRING_LIST), createAttribute('attr2', Types.STRING),
      createAttribute('attr3', Types.STRING_LIST)
  ]
  private static final List<AttributeRelation> INVERSE_ATTRIBUTE_RELATIONS =
      INVERSE_ATTRIBUTES.collect { new InverseAttributeRelation(it) }

  @Subject
  @Shared
  private InverseAttributeAddValueBodyCodeGenerator generator = new InverseAttributeAddValueBodyCodeGenerator()

  def 'should support add value method of inverse attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withRelations(INVERSE_ATTRIBUTE_RELATIONS).buildAttribute()

    when:
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    then:
    generator.supports(addValueMethod)
  }

  def 'should not support add value method of non inverse attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    then:
    !generator.supports(addValueMethod)
  }

  def 'should generate no validation code of add value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withRelations(INVERSE_ATTRIBUTE_RELATIONS).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(addValueMethod)

    then:
    !generatedCode.isPresent()
  }

  def 'should generate specific add value code of add value method when inverse attribute is list'() {
    given:
    def attributeName = 'attribute'
    def attribute = new Attribute.AttributeBuilder(attributeName, Types.STRING_LIST)
        .withRelations(INVERSE_ATTRIBUTE_RELATIONS).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(addValueMethod)

    then:
    generatedCode.isPresent()
    generatedCode.get() == [
        "${addValueMethod.parameter.name}.add${INVERSE_ATTRIBUTES[0].name.capitalize()}(this)",
        "${addValueMethod.parameter.name}.${DirectSetter.getName(INVERSE_ATTRIBUTES[1])}(this)",
        "${addValueMethod.parameter.name}.add${INVERSE_ATTRIBUTES[2].name.capitalize()}(this)"
    ].join('\n')
  }
}
