package owl2converter.engine.test.generator.model

import owl2converter.groovymetamodel.Annotation
import owl2converter.groovymetamodel.EnumClass
import owl2converter.groovymetamodel.type.SimpleType

import static AttributeTestFactory.createAttribute
import static ClassTestFactory.createEmptyClass
import static ConstructorTestFactory.createConstructor
import static MethodTestFactory.createMethod

class EnumClassTestFactory {

  static EnumClass createEnumClass(String className = 'TestEnumClass', String packageName = 'org.test',
                                   List<Annotation> annotations = [new Annotation(new SimpleType('test', 'DisjointWith'))]) {
    return new EnumClass.Builder(packageName, className)
        .withAnnotations(annotations)
        .withConstructors([createConstructor()])
        .withAttributes([createAttribute()])
        .withInheritedClass(createEmptyClass())
        .withMethods([createMethod()])
        .buildClass()
  }
}
