package owl2converter.engine.generator

import owl2converter.groovymetamodel.Trait
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.test.generator.model.TraitTestFactory.createEmptyTrait
import static owl2converter.engine.test.generator.model.TraitTestFactory.createTrait

class TraitSignatureCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private TraitSignatureCodeGenerator generator = new TraitSignatureCodeGenerator()

  def 'should generate trait signature for trait that inherits no traits'() {
    when:
    def testTrait = createEmptyTrait()

    then:
    generator.generateTraitSignature(testTrait) == "trait $testTrait.name {".toString()
  }

  def 'should generate trait signature for trait that inherits some traits'() {
    given:
    def inheritedTraitNames = ['Trait1', 'Trait2']
    def inheritedTraits = inheritedTraitNames.collect { createTrait(it) }

    when:
    def testTrait = new Trait.Builder('org.test', 'TestTrait').withInheritedTraits(inheritedTraits).buildTrait()

    then:
    generator.generateTraitSignature(testTrait) ==
        "trait $testTrait.name implements ${inheritedTraitNames.join(', ')} {".toString()
  }
}
