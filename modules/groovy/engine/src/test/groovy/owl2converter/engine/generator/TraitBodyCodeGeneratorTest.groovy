package owl2converter.engine.generator

import owl2converter.engine.generator.method.MethodCodeGenerator
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.MethodTestFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class TraitBodyCodeGeneratorTest extends Specification {

  private static final String ATTRIBUTE_SOURCE_CODE = 'attribute code'
  private static final String METHOD_SOURCE_CODE = 'method code'

  @Shared
  private AttributeCodeGenerator attributeCodeGenerator = Stub(AttributeCodeGenerator)

  @Shared
  private MethodCodeGenerator methodCodeGenerator = Stub(MethodCodeGenerator)

  @Subject
  @Shared
  private TraitBodyCodeGenerator traitBodyCodeGenerator

  def setupSpec() {
    attributeCodeGenerator.generateCode(_ as Attribute) >> ATTRIBUTE_SOURCE_CODE
    methodCodeGenerator.generateCode(_ as Method) >> METHOD_SOURCE_CODE
    traitBodyCodeGenerator = new TraitBodyCodeGenerator(attributeCodeGenerator, methodCodeGenerator)
  }

  def 'should have no source codes generated before visiting any trait element'() {
    when:
    def generator = new TraitBodyCodeGenerator(attributeCodeGenerator, methodCodeGenerator)

    then:
    with(generator) {
      attributeCodes.isEmpty()
      methodCodes.isEmpty()
    }
  }

  def 'should generate source code of attribute'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    traitBodyCodeGenerator.visit(attribute)

    then:
    traitBodyCodeGenerator.attributeCodes == [(attribute): ATTRIBUTE_SOURCE_CODE]
  }

  def 'should generate source code of method'() {
    given:
    def method = MethodTestFactory.createMethod()

    when:
    traitBodyCodeGenerator.visit(method)

    then:
    traitBodyCodeGenerator.methodCodes == [(method): METHOD_SOURCE_CODE]
  }
}
