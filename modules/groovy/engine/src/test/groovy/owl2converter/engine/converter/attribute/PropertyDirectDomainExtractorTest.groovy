package owl2converter.engine.converter.attribute

import owl2converter.engine.converter.attribute.domain.PropertyDirectDomainExtractor
import owl2converter.engine.test.TestOWLOntologyBuilder
import org.slf4j.Logger
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import spock.lang.Specification
import spock.lang.Subject

class PropertyDirectDomainExtractorTest extends Specification {

  static final String PROPERTY_1_NAME = 'testProperty1'
  static final String PROPERTY_1_DOMAIN = 'TestClass1'

  private Logger logger = Mock()

  @Subject
  private PropertyDirectDomainExtractor extractor = new PropertyDirectDomainExtractor(logger)

  def 'should return single domain defined directly for object property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_DOMAIN)
        .withObjectPropertyDeclaration(PROPERTY_1_NAME).withObjectPropertyDomain(PROPERTY_1_NAME, PROPERTY_1_DOMAIN)
        .build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> domain = extractor.extractSingleDirectDomain(property, ontology)

    then:
    domain.get().getIRI().remainder.get() == PROPERTY_1_DOMAIN
    0 * logger.warn(*_)
  }

  def 'should return empty domain when no domain is declared for object property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_DOMAIN)
        .withObjectPropertyDeclaration(PROPERTY_1_NAME).build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> domain = extractor.extractSingleDirectDomain(property, ontology)

    then:
    !domain.isPresent()
    0 * logger.warn(*_)
  }

  def 'should return empty domain and log warning when more than 1 domain is found for object property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_DOMAIN)
        .withObjectPropertyDeclaration(PROPERTY_1_NAME)
        .withObjectPropertyDomain(PROPERTY_1_NAME, 'Class1').withObjectPropertyDomain(PROPERTY_1_NAME, 'Class2').build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> domain = extractor.extractSingleDirectDomain(property, ontology)

    then:
    !domain.isPresent()
    1 * logger.warn(*_)
  }

  def 'should return empty domain and log warning when domain found for object property does not refer to OWL class'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_DOMAIN)
        .withObjectPropertyDeclaration(PROPERTY_1_NAME)
        .withObjectPropertyDomainAsUnion(PROPERTY_1_NAME, 'Class1', 'Class2').build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> domain = extractor.extractSingleDirectDomain(property, ontology)

    then:
    !domain.isPresent()
    1 * logger.warn(*_)
  }

  def 'should return single domain defined directly for data property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_DOMAIN)
        .withDataPropertyDeclaration(PROPERTY_1_NAME).withDataPropertyDomain(PROPERTY_1_NAME, PROPERTY_1_DOMAIN)
        .build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> domain = extractor.extractSingleDirectDomain(property, ontology)

    then:
    domain.get().getIRI().remainder.get() == PROPERTY_1_DOMAIN
    0 * logger.warn(*_)
  }

  def 'should return empty domain when no domain is declared for data property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_DOMAIN)
        .withDataPropertyDeclaration(PROPERTY_1_NAME).build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> domain = extractor.extractSingleDirectDomain(property, ontology)

    then:
    !domain.isPresent()
    0 * logger.warn(*_)
  }

  def 'should return empty domain and log warning when more than 1 domain is found for data property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_DOMAIN)
        .withDataPropertyDeclaration(PROPERTY_1_NAME)
        .withDataPropertyDomain(PROPERTY_1_NAME, 'Class1').withDataPropertyDomain(PROPERTY_1_NAME, 'Class2').build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> domain = extractor.extractSingleDirectDomain(property, ontology)

    then:
    !domain.isPresent()
    1 * logger.warn(*_)
  }

  def 'should return empty domain and log warning when domain found for data property does not refer to OWL class'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_DOMAIN)
        .withDataPropertyDeclaration(PROPERTY_1_NAME)
        .withDataPropertyDomainAsUnion(PROPERTY_1_NAME, 'Class1', 'Class2').build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> domain = extractor.extractSingleDirectDomain(property, ontology)

    then:
    !domain.isPresent()
    1 * logger.warn(*_)
  }
}
