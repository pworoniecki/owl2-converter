package owl2converter.engine.test.generator.model

import owl2converter.groovymetamodel.Annotation
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.type.SimpleType

import static AttributeTestFactory.createAttribute
import static ConstructorTestFactory.createConstructor
import static MethodTestFactory.createMethod

class ClassTestFactory {

  static Class createClass(String className = 'TestClass', String packageName = 'org.test',
                           List<Annotation> annotations = [new Annotation(new SimpleType('test', 'DisjointWith'))]) {
    return new Class.Builder(packageName, className)
        .withAnnotations(annotations)
        .withConstructors([createConstructor()])
        .withAttributes([createAttribute()])
        .withInheritedClass(createEmptyClass())
        .withMethods([createMethod()])
        .buildClass()
  }

  static Class createEmptyClass(String packageName = 'org.test', String className = 'EmptyClass') {
    return new Class.Builder(packageName, className).buildClass()
  }
}
