package owl2converter.engine.generator.constructor

import owl2converter.engine.generator.exception.ConstructorBodyCodeGeneratorNotFoundException
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.engine.test.generator.model.ClassTestFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static owl2converter.groovymetamodel.AccessModifier.PRIVATE
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static owl2converter.engine.test.generator.model.ConstructorTestFactory.createConstructor

class ConstructorCodeGeneratorTest extends Specification {

  private static final SimpleType INTEGER_TYPE = new SimpleType(Integer)
  private static final Parameter PARAMETER_1 = new Parameter(INTEGER_TYPE, 'param1')
  private static final Parameter PARAMETER_2 = new Parameter(new ListType(INTEGER_TYPE), 'param2')

  @Shared
  private Class testClass1 = ClassTestFactory.createClass('TestClass1')

  @Shared
  private Class testClass2 = ClassTestFactory.createClass('TestClass2')

  @Shared
  private List<ConstructorBodyCodeGenerator> bodyCodeGenerators = [
      new TestConstructorBodyCodeGenerator(testClass1),
      new TestConstructorBodyCodeGenerator(testClass2)
  ]

  @Subject
  @Shared
  private ConstructorCodeGenerator generator = new ConstructorCodeGenerator(bodyCodeGenerators)

  @Unroll
  def 'should generate signature of constructor'() {
    expect:
    generator.generateCode(createConstructor(modifier, parameters, isStatic), owner).readLines().first() ==
        signature.toString()

    where:
    modifier | parameters                 | isStatic | owner      | signature
    PRIVATE  | []                         | false    | testClass1 | "private $testClass1.name() {"
    PRIVATE  | [PARAMETER_1]              | false    | testClass1 | "private $testClass1.name(Integer param1) {"
    PRIVATE  | [PARAMETER_1]              | true     | testClass1 | "private static $testClass1.name(Integer param1) {"
    PUBLIC   | [PARAMETER_1, PARAMETER_2] | false    | testClass1 | "$testClass1.name(Integer param1, List<Integer> param2) {"
    PUBLIC   | [PARAMETER_1, PARAMETER_2] | false    | testClass2 | "$testClass2.name(Integer param1, List<Integer> param2) {"
  }

  def 'should generate body of constructor using body code generator that supports owner of the constructor'() {
    when:
    def generatedCodeLines = generator.generateCode(createConstructor(), constructorOwner).readLines()
    def bodyCodeLines = generatedCodeLines[1..generatedCodeLines.size() - 2]

    then:
    bodyCodeLines == getMockCode(constructorOwner).readLines()

    where:
    constructorOwner << [testClass1, testClass2]
  }

  def 'should generate end of constructor definition'() {
    expect:
    generator.generateCode(createConstructor(), testClass1).readLines().last() == '}'
  }

  def 'should throw an exception when no constructor body code generator supports given constructor owner'() {
    when:
    generator.generateCode(createConstructor(), ClassTestFactory.createClass('NotSupportedClass'))
        .readLines().last() == '}'

    then:
    thrown ConstructorBodyCodeGeneratorNotFoundException
  }

  private static class TestConstructorBodyCodeGenerator implements ConstructorBodyCodeGenerator {

    private Class supportedClass

    TestConstructorBodyCodeGenerator(Class supportedClass) { this.supportedClass = supportedClass }

    @Override
    boolean supports(Class ownerClass) { return ownerClass == supportedClass }

    @Override
    String generateCode(Constructor constructor) { return getMockCode(supportedClass) }
  }

  private static String getMockCode(Class constructorOwner) {
    return """Mock first line of constructor body in $constructorOwner.name class
             |Mock second line of constructor body in $constructorOwner.name class""".stripMargin()
  }
}
