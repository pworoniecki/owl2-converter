package owl2converter.engine.test.generator.model

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Setter

import static owl2converter.engine.test.generator.model.AttributeTestFactory.createAttribute

class SetterTestFactory {

  static Setter createMethod(Attribute attribute = createAttribute()) {
    return new Setter.Builder(attribute).withAccessModifier(AccessModifier.PROTECTED).asStatic()
        .buildMethod()
  }
}
