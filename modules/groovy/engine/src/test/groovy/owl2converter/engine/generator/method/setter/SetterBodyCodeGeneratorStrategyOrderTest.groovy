package owl2converter.engine.generator.method.setter

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import owl2converter.engine.OWL2ToGroovyEngineConfiguration
import spock.lang.Specification
import spock.lang.Subject

@ContextConfiguration(loader = AnnotationConfigContextLoader, classes = OWL2ToGroovyEngineConfiguration)
class SetterBodyCodeGeneratorStrategyOrderTest extends Specification {

  @Autowired
  @Subject
  private List<SetterBodyCodeGeneratorStrategy> generators

  def 'should assert correct order of setter body code generators'() {
    expect:
    generators*.priority.unique().size() == 1
    generators.sort { it.orderWithinPriorityGroup }*.class == [
        ReflexiveAttributeSetterBodyCodeGenerator,
        IrreflexiveAttributeSetterBodyCodeGenerator,
        AsymmetricAttributeSetterBodyCodeGenerator,
        TransitiveAttributeSetterBodyCodeGenerator,
        FunctionalAttributeSetterBodyCodeGenerator,
        InverseFunctionalAttributeSetterBodyCodeGenerator,
        SymmetricAttributeSetterBodyCodeGenerator,
        InverseAttributeSetterBodyCodeGenerator,
        FunctionalReflexiveAttributeSetterBodyCodeGenerator
    ]
  }
}
