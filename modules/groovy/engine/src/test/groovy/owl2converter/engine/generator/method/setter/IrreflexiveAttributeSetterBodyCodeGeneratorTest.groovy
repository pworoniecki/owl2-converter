package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.SetterTestFactory
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.IRREFLEXIVE

class IrreflexiveAttributeSetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private IrreflexiveAttributeSetterBodyCodeGenerator generator = new IrreflexiveAttributeSetterBodyCodeGenerator()

  def 'should support setter of irreflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    generator.supports(setter)
  }

  def 'should not support setter of non irreflexive attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)
  }

  def 'should generate validation code of list-type setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)
    def expectedExceptionMessage = "Cannot set \$attribute as it contains 'this' instance - " +
        'it would break irreflexive attribute rule'

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """if (attribute.contains(this)) {
          |throw new IrreflexiveAttributeException("$expectedExceptionMessage")
          |}""".stripMargin()
  }

  def 'should generate validation code of single-type setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)
    def expectedExceptionMessage = "Cannot set 'this' instance - it would break irreflexive attribute rule"

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """if (attribute == this) {
          |throw new IrreflexiveAttributeException("$expectedExceptionMessage")
          |}""".stripMargin()
  }

  def 'should generate no specific setting value code of setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([IRREFLEXIVE]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    !generatedCode.isPresent()
  }
}
