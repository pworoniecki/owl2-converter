package owl2converter.engine.generator.constructor

import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.engine.test.generator.model.ConstructorTestFactory
import owl2converter.engine.test.generator.model.EnumClassTestFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.generator.constructor.EnumClassConstructorBodyCodeGenerator.INDIVIDUAL_NAME_PARAMETER
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC

class EnumClassConstructorBodyCodeGeneratorTest extends Specification {

  private static final SimpleType INTEGER_TYPE = new SimpleType(Integer)
  private static final Parameter PARAMETER_1 = new Parameter(INTEGER_TYPE, 'param1')
  private static final Parameter PARAMETER_2 = new Parameter(new ListType(INTEGER_TYPE), 'param2')

  @Subject
  @Shared
  private EnumClassConstructorBodyCodeGenerator generator = new EnumClassConstructorBodyCodeGenerator()

  def 'should support EnumClass type'() {
    expect:
    generator.supports(EnumClassTestFactory.createEnumClass())
  }

  def 'should generate body code of constructor in enumeration class'() {
    given:
    def constructor = ConstructorTestFactory.createConstructor(PUBLIC, [PARAMETER_1, PARAMETER_2])

    when:
    def constructorBodyCode = generator.generateCode(constructor)

    then:
    constructorBodyCode == """addInstance(this)
                             |IndividualRepository.addIndividual($INDIVIDUAL_NAME_PARAMETER, this)
                             |setParam1(param1)
                             |setParam2(param2)""".stripMargin()
  }
}
