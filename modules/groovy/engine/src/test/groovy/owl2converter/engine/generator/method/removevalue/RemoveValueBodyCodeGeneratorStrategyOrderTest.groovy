package owl2converter.engine.generator.method.removevalue

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import owl2converter.engine.OWL2ToGroovyEngineConfiguration
import spock.lang.Specification
import spock.lang.Subject

@ContextConfiguration(loader = AnnotationConfigContextLoader, classes = OWL2ToGroovyEngineConfiguration)
class RemoveValueBodyCodeGeneratorStrategyOrderTest extends Specification {

  @Autowired
  @Subject
  private List<RemoveValueBodyCodeGeneratorStrategy> generators

  def 'should assert correct order of remove value body code generators'() {
    expect:
    generators*.priority.unique().size() == 1
    generators.sort { it.orderWithinPriorityGroup }*.class == [
        ReflexiveAttributeRemoveValueBodyCodeGenerator,
        IrreflexiveAttributeRemoveValueBodyCodeGenerator,
        AsymmetricAttributeRemoveValueBodyCodeGenerator,
        TransitiveAttributeRemoveValueBodyCodeGenerator,
        InverseFunctionalAttributeRemoveValueBodyCodeGenerator,
        SymmetricAttributeRemoveValueBodyCodeGenerator,
        InverseAttributeRemoveValueBodyCodeGenerator
    ]
  }
}
