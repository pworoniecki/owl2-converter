package owl2converter.engine.converter.attribute

import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.converter.attribute.range.DataPropertyRangeExtractor
import owl2converter.engine.converter.attribute.range.ObjectPropertyRangeExtractor
import owl2converter.engine.converter.attribute.range.PropertyRangeExtractor
import owl2converter.groovymetamodel.type.Type
import owl2converter.engine.test.generator.model.Types
import spock.lang.Specification
import spock.lang.Subject

class PropertyRangeExtractorTest extends Specification {

  private static final Type TYPE = Types.STRING_LIST

  private ObjectPropertyRangeExtractor objectPropertyRangeExtractor = Mock()
  private DataPropertyRangeExtractor dataPropertyRangeExtractor = Mock()

  @Subject
  private PropertyRangeExtractor extractor = new PropertyRangeExtractor(objectPropertyRangeExtractor, dataPropertyRangeExtractor)

  def 'should extract range for object property using proper extractor'() {
    given:
    OWLObjectProperty property = Mock()
    OWLOntology ontology = Mock()

    when:
    Type type = extractor.extractRange(property, ontology)

    then:
    1 * objectPropertyRangeExtractor.extractRange(property, ontology) >> TYPE
    type.is(TYPE)
  }

  def 'should extract range for data property using proper extractor'() {
    given:
    OWLDataProperty property = Mock()
    OWLOntology ontology = Mock()

    when:
    Type type = extractor.extractRange(property, ontology)

    then:
    1 * dataPropertyRangeExtractor.extractRange(property, ontology) >> TYPE
    type.is(TYPE)
  }
}
