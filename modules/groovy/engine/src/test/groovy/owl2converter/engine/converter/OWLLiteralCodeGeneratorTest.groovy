package owl2converter.engine.converter

import org.semanticweb.owlapi.model.IRI
import org.semanticweb.owlapi.model.OWLDatatype
import org.semanticweb.owlapi.model.OWLLiteral
import owl2converter.engine.exception.MissingIRIRemainderException
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

class OWLLiteralCodeGeneratorTest extends Specification {

    static final String XML_SCHEMA_NAMESPACE = 'http://www.w3.org/2001/XMLSchema'

    @Subject
    private OWLLiteralCodeGenerator literalCodeGenerator = new OWLLiteralCodeGenerator()

    @Unroll
    def 'should generate code for OWL literal depending on its type'() {
        given:
        OWLLiteral literal = Mock()
        OWLDatatype datatype = Mock()
        literal.datatype >> datatype
        datatype.getIRI() >> IRI.create(XML_SCHEMA_NAMESPACE, "#$literalType")
        literal.literal >> literalValue

        when:
        String generatedCode = literalCodeGenerator.generateCode(literal)

        then:
        generatedCode == expectedCode

        where:
        literalValue          | literalType          | expectedCode
        '2'                   | 'string'             | "'2'"
        '2'                   | 'normalizedString'   | "new NormalizedString('2')"
        '2'                   | 'token'              | "new Token('2')"
        '2'                   | 'language'           | "new Language('2')"
        '2'                   | 'NMTOKEN'            | "new NMTOKEN('2')"
        '2'                   | 'Name'               | "new Name('2')"
        '2'                   | 'NCName'             | "new NCName('2')"
        'true'                | 'boolean'            | "Boolean.valueOf('true')"
        '2'                   | 'decimal'            | "new BigDecimal('2')"
        '2'                   | 'float'              | "Float.valueOf('2')"
        '2'                   | 'double'             | "Double.valueOf('2')"
        '2'                   | 'integer'            | "new BigInteger('2')"
        '0'                   | 'nonPositiveInteger' | "new NonPositiveInteger(new BigInteger('0'))"
        '-2'                  | 'negativeInteger'    | "new NegativeInteger(new BigInteger('-2'))"
        '2'                   | 'long'               | "Long.valueOf('2')"
        '2'                   | 'int'                | "Integer.valueOf('2')"
        '2'                   | 'short'              | "Short.valueOf('2')"
        '2'                   | 'byte'               | "Byte.valueOf('2')"
        '0'                   | 'nonNegativeInteger' | "new NonNegativeInteger(new BigInteger('0'))"
        '2'                   | 'unsignedLong'       | "new UnsignedLong(new BigInteger('2'))"
        '2'                   | 'unsignedInt'        | "new UnsignedInt(new BigInteger('2'))"
        '2'                   | 'unsignedShort'      | "new UnsignedShort(new BigInteger('2'))"
        '2'                   | 'unsignedByte'       | "new UnsignedByte(new BigInteger('2'))"
        '2'                   | 'positiveInteger'    | "new PositiveInteger(new BigInteger('2'))"
        '2002-05-30T09:00:00' | 'dateTime'           | "DatatypeFactory.newInstance().newXMLGregorianCalendar('2002-05-30T09:00:00')"
        '09:00:00'            | 'time'               | "DatatypeFactory.newInstance().newXMLGregorianCalendar('09:00:00')"
        '2002-05-30'          | 'date'               | "DatatypeFactory.newInstance().newXMLGregorianCalendar('2002-05-30')"
        '2001-10'             | 'gYearMonth'         | "DatatypeFactory.newInstance().newXMLGregorianCalendar('2001-10')"
        '2001'                | 'gYear'              | "DatatypeFactory.newInstance().newXMLGregorianCalendar('2001')"
        '--05-01'             | 'gMonthDay'          | "DatatypeFactory.newInstance().newXMLGregorianCalendar('--05-01')"
        '---01'               | 'gDay'               | "DatatypeFactory.newInstance().newXMLGregorianCalendar('---01')"
        '--05'                | 'gMonth'             | "DatatypeFactory.newInstance().newXMLGregorianCalendar('--05')"
    }

    def 'should throw an exception when literal is not in XML schema namespace'() {
        given:
        OWLLiteral literal = Mock()
        OWLDatatype datatype = Mock()
        literal.datatype >> datatype
        datatype.getIRI() >> IRI.create('invalidNamespace', '#xsd:string')
        literal.literal >> '2'

        when:
        literalCodeGenerator.generateCode(literal)

        then:
        thrown IllegalArgumentException
    }

    def 'should throw an exception when literal has no name'() {
        given:
        OWLLiteral literal = Mock()
        OWLDatatype datatype = Mock()
        IRI literalIRI = Mock()
        literal.datatype >> datatype
        datatype.getIRI() >> literalIRI
        literalIRI.namespace >> "$XML_SCHEMA_NAMESPACE#"
        literalIRI.remainder >> Optional.empty()
        literal.literal >> '2'

        when:
        literalCodeGenerator.generateCode(literal)

        then:
        thrown MissingIRIRemainderException
    }
}
