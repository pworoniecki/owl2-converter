package owl2converter.engine.converter.attribute

import org.semanticweb.owlapi.model.OWLAnnotationProperty
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.converter.attribute.domain.DataPropertyDomainExtractor
import owl2converter.engine.converter.attribute.domain.ObjectPropertyDomainExtractor
import owl2converter.engine.converter.attribute.domain.PropertyDomainExtractor
import owl2converter.groovymetamodel.type.Type
import owl2converter.engine.test.generator.model.Types
import spock.lang.Specification
import spock.lang.Subject

class PropertyDomainExtractorTest extends Specification {

  private static final Type TYPE = Types.STRING_LIST

  private ObjectPropertyDomainExtractor objectPropertyDomainExtractor = Mock()
  private DataPropertyDomainExtractor dataPropertyDomainExtractor = Mock()

  @Subject
  private PropertyDomainExtractor extractor =
      new PropertyDomainExtractor(objectPropertyDomainExtractor, dataPropertyDomainExtractor)

  def 'should extract domain for object property using proper extractor'() {
    given:
    OWLObjectProperty property = Mock()
    OWLOntology ontology = Mock()

    when:
    Type type = extractor.extractDomain(property, ontology)

    then:
    1 * objectPropertyDomainExtractor.extract(property, ontology) >> TYPE
    type.is(TYPE)
  }

  def 'should extract domain for data property using proper extractor'() {
    given:
    OWLDataProperty property = Mock()
    OWLOntology ontology = Mock()

    when:
    Type type = extractor.extractDomain(property, ontology)

    then:
    1 * dataPropertyDomainExtractor.extract(property, ontology) >> TYPE
    type.is(TYPE)
  }

  def 'should throw an exception when trying to extract domain of unsupported OWL property type'() {
    given:
    OWLOntology ontology = Mock()
    OWLAnnotationProperty property = Mock()

    when:
    extractor.extractDomain(property, ontology)

    then:
    thrown IllegalArgumentException
  }
}
