package owl2converter.engine.generator.method.common

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.CustomMethod
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.VoidType
import owl2converter.engine.test.generator.model.Types
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE

class TransitiveAttributeHelpersGeneratorTest extends Specification {

  private static final String FILL_TRANSITIVE_PARAMETER_NAME = 'result'

  @Subject
  private TransitiveAttributeHelpersGenerator generator = new TransitiveAttributeHelpersGenerator()

  def 'should generate fillTransitive method for transitive list attribute'() {
    given:
    ListType attributeType = Types.STRING_LIST
    def transitiveAttribute = new Attribute.AttributeBuilder('test', attributeType).withCharacteristics([TRANSITIVE])
        .buildAttribute()

    when:
    CustomMethod method = generator.generateGetterHelperMethod(transitiveAttribute)

    then:
    with(method) {
      returnType == VoidType.INSTANCE
      name == getExpectedFillTransitiveMethodName(transitiveAttribute)
      parameters.size() == 1
      parameters.first().name == FILL_TRANSITIVE_PARAMETER_NAME
      parameters.first().type == attributeType
      bodyCode == getExpectedFillTransitiveCode(transitiveAttribute)
      accessModifier == PUBLIC
    }
  }

  def 'should throw an exception when provided attribute is not transitive'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).buildAttribute()

    when:
    generator.generateGetterHelperMethod(attribute)

    then:
    thrown IllegalArgumentException
  }

  def 'should throw an exception when provided attribute is not list'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING).withCharacteristics([TRANSITIVE]).buildAttribute()

    when:
    generator.generateGetterHelperMethod(attribute)

    then:
    thrown IllegalArgumentException
  }

  private static String getExpectedFillTransitiveMethodName(Attribute attribute) {
    return "_fillTransitive${attribute.name.capitalize()}".toString()
  }

  private static String getExpectedFillTransitiveCode(Attribute attribute) {
    String attributeName = attribute.name
    String parameterName = attributeName
    return [
        "(this.${attribute.name} - $FILL_TRANSITIVE_PARAMETER_NAME).each {",
        "if (!${FILL_TRANSITIVE_PARAMETER_NAME}.contains(it)) {",
        "${FILL_TRANSITIVE_PARAMETER_NAME}.add(it)",
        "it.${getExpectedFillTransitiveMethodName(attribute)}($FILL_TRANSITIVE_PARAMETER_NAME)",
        '}',
        '}'
    ].join('\n')
  }
}
