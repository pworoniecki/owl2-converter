package owl2converter.engine.generator

import owl2converter.engine.generator.constructor.ConstructorCodeGenerator
import owl2converter.engine.generator.method.MethodCodeGenerator
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.ClassTestFactory
import owl2converter.engine.test.generator.model.ConstructorTestFactory
import owl2converter.engine.test.generator.model.MethodTestFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class ClassBodyCodeGeneratorTest extends Specification {

  private static final Class OWNER_CLASS = ClassTestFactory.createClass()
  private static final String CONSTRUCTOR_SOURCE_CODE = 'constructor code'
  private static final String ATTRIBUTE_SOURCE_CODE = 'attribute code'
  private static final String METHOD_SOURCE_CODE = 'method code'

  @Shared
  private ConstructorCodeGenerator constructorCodeGenerator = Stub(ConstructorCodeGenerator)

  @Shared
  private AttributeCodeGenerator attributeCodeGenerator = Stub(AttributeCodeGenerator)

  @Shared
  private MethodCodeGenerator methodCodeGenerator = Stub(MethodCodeGenerator)

  @Subject
  @Shared
  private ClassBodyCodeGenerator classBodyCodeGenerator

  def setupSpec() {
    constructorCodeGenerator.generateCode(_ as Constructor, OWNER_CLASS) >> CONSTRUCTOR_SOURCE_CODE
    attributeCodeGenerator.generateCode(_ as Attribute) >> ATTRIBUTE_SOURCE_CODE
    methodCodeGenerator.generateCode(_ as Method) >> METHOD_SOURCE_CODE
    classBodyCodeGenerator =
        new ClassBodyCodeGenerator(constructorCodeGenerator, attributeCodeGenerator, methodCodeGenerator)
  }

  def 'should have no source codes generated before visiting any class element'() {
    when:
    def generator = new ClassBodyCodeGenerator(constructorCodeGenerator, attributeCodeGenerator, methodCodeGenerator)

    then:
    with(generator) {
      constructorCodes.isEmpty()
      attributeCodes.isEmpty()
      methodCodes.isEmpty()
    }
  }

  def 'should generate source code of constructor'() {
    given:
    def constructor = ConstructorTestFactory.createConstructor()

    when:
    classBodyCodeGenerator.visit(constructor, OWNER_CLASS)

    then:
    classBodyCodeGenerator.constructorCodes == [(constructor): CONSTRUCTOR_SOURCE_CODE]
  }

  def 'should generate source code of attribute'() {
    given:
    def attribute = AttributeTestFactory.createAttribute()

    when:
    classBodyCodeGenerator.visit(attribute)

    then:
    classBodyCodeGenerator.attributeCodes == [(attribute): ATTRIBUTE_SOURCE_CODE]
  }

  def 'should generate source code of method'() {
    given:
    def method = MethodTestFactory.createMethod()

    when:
    classBodyCodeGenerator.visit(method)

    then:
    classBodyCodeGenerator.methodCodes == [(method): METHOD_SOURCE_CODE]
  }
}
