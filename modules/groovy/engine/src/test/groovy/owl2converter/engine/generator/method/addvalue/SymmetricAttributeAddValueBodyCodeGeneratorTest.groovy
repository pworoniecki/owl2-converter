package owl2converter.engine.generator.method.addvalue

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.AddValueMethod
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.SYMMETRIC
import static owl2converter.engine.test.generator.model.AddValueMethodTestFactory.createAddValueMethod

class SymmetricAttributeAddValueBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private SymmetricAttributeAddValueBodyCodeGenerator generator = new SymmetricAttributeAddValueBodyCodeGenerator()

  def 'should support add value method of symmetric attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withCharacteristics([SYMMETRIC]).buildAttribute()

    when:
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    then:
    generator.supports(addValueMethod)
  }

  def 'should not support add value method of non symmetric attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    then:
    !generator.supports(addValueMethod)
  }

  def 'should generate no validation code of add value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([SYMMETRIC]).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(addValueMethod)

    then:
    !generatedCode.isPresent()
  }

  def 'should generate add value code of add value method'() {
    given:
    def attributeName = 'attribute'
    def attribute = new Attribute.AttributeBuilder(attributeName, Types.STRING_LIST)
        .withCharacteristics([SYMMETRIC]).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(addValueMethod)

    then:
    generatedCode.isPresent()
    generatedCode.get() == "${addValueMethod.parameter.name}.add${attributeName.capitalize()}(this)"
  }
}
