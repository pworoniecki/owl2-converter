package owl2converter.engine.test.generator.model

import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType

class Types {

  static final SimpleType STRING = new SimpleType(String)
  static final ListType STRING_LIST = new ListType(STRING)
}
