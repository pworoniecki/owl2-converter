package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.SetterTestFactory
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.converter.trait.TraitsExtractor.ALL_INSTANCES_ATTRIBUTE_NAME
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.INVERSE_FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE

class InverseFunctionalAttributeSetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private InverseFunctionalAttributeSetterBodyCodeGenerator generator = new InverseFunctionalAttributeSetterBodyCodeGenerator()

  def 'should support setter of inverse functional attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withCharacteristics([INVERSE_FUNCTIONAL]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    generator.supports(setter)
  }

  def 'should not support setter of non inverse functional attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)
  }

  def 'should generate validation code of list-type setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([INVERSE_FUNCTIONAL]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() == getExpectedStandardValidationCode()
  }

  def 'should generate additional validation code of list-type setter when attribute is also transitive'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([INVERSE_FUNCTIONAL, TRANSITIVE]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() == getExpectedStandardValidationCode() + '\n' +
        """if (${setter.parameter.name}.any { !it.${attribute.name}.isEmpty() }) {
          |throw new InverseFunctionalAttributeException("Cannot set \$commonValues because it would break inverse functional attribute rule - some values would belong to more than one class instance due to transitivity.")
          |}""".stripMargin()
  }

  def 'should generate validation code of single-type setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([INVERSE_FUNCTIONAL]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)
    def expectedExceptionMessage = 'Cannot set $attribute because it would break inverse functional attribute rule - ' +
        'this value is already set in other class instance.'

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """def attributeOfOtherInstances = (${ALL_INSTANCES_ATTRIBUTE_NAME} - this)*.attribute.flatten()
          |if (attribute in attributeOfOtherInstances) {
          |throw new InverseFunctionalAttributeException("$expectedExceptionMessage")
          |}""".stripMargin()
  }

  def 'should generate no specific setting value code of setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([INVERSE_FUNCTIONAL]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    !generatedCode.isPresent()
  }

  private static String getExpectedStandardValidationCode() {
    def expectedExceptionMessage = 'Cannot set $commonValues because it would break inverse functional attribute rule - ' +
        'these values are already set in other class instances.'
    return """def attributeOfOtherInstances = (${ALL_INSTANCES_ATTRIBUTE_NAME} - this)*.attribute.flatten()
          |def commonValues = attribute.intersect(attributeOfOtherInstances)
          |if (!commonValues.isEmpty()) {
          |throw new InverseFunctionalAttributeException("$expectedExceptionMessage")
          |}""".stripMargin()
  }
}
