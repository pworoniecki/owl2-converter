package owl2converter.engine.converter

import org.semanticweb.owlapi.model.OWLNamedIndividual
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.test.TestOWLOntologyBuilder
import spock.lang.Specification

class IndividualsTopologicalSorterTest extends Specification {

    private IndividualsTopologicalSorter sorter = new IndividualsTopologicalSorter()

    def 'should return empty list of individuals for empty list to sort'() {
        given:
        OWLOntology ontology = new TestOWLOntologyBuilder().build()

        when:
        List<OWLNamedIndividual> individuals = sorter.sort(ontology, ontology.individualsInSignature().findAll())

        then:
        individuals.isEmpty()
    }

    def 'should return individuals topologically - by dependencies between them'() {
        given:
        String class1Name = 'Class1'
        String class2Name = 'Class2'
        String class3Name = 'Class3'
        OWLOntology ontology = new TestOWLOntologyBuilder()
                .withClassDeclarations([class1Name, class2Name, class3Name])
                .withObjectPropertyDeclaration('property1')
                .withObjectPropertyDomain('property1', class1Name)
                .withObjectPropertyDeclaration('property2')
                .withObjectPropertyDomain('property2', class2Name)
                .withObjectPropertyDeclaration('property3')
                .withObjectPropertyDomain('property3', class3Name)
                .withObjectPropertyDeclaration('property4')
                .withObjectPropertyDomain('property4', class1Name)
                .withIndividual('individual1', class1Name)
                .withIndividual('individual2', class2Name)
                .withIndividual('individual3', class3Name)
                .withIndividual('individual4', class1Name)
                .withIndividual('individual5', class2Name)
                .withIndividual('individual6', class3Name)
                .withObjectPropertyAssertion('property1', 'individual1', 'individual2')
                .withObjectPropertyAssertion('property2', 'individual1', 'individual3')
                .withObjectPropertyAssertion('property2', 'individual4', 'individual5')
                .build()
        List<OWLNamedIndividual> unsortedIndividuals = ontology.individualsInSignature().findAll()

        when:
        List<OWLNamedIndividual> individuals = sorter.sort(ontology, unsortedIndividuals)

        then:
        individuals == ['individual6', 'individual2', 'individual3', 'individual1', 'individual5', 'individual4']
                .collect { individualName -> unsortedIndividuals.find { it.getIRI().remainder.get() == individualName }
        }
    }

    def 'should throw an exception when trying to sort individuals with cyclic dependencies'() {
        given:
        String className = 'Class1'
        OWLOntology ontology = new TestOWLOntologyBuilder()
                .withClassDeclarations([className])
                .withObjectPropertyDeclaration('property1')
                .withObjectPropertyDomain('property1', className)
                .withObjectPropertyDeclaration('property2')
                .withObjectPropertyDomain('property2', className)
                .withIndividual('individual1', className)
                .withIndividual('individual2', className)
                .withObjectPropertyAssertion('property1', 'individual1', 'individual2')
                .withObjectPropertyAssertion('property2', 'individual2', 'individual1')
                .build()
        List<OWLNamedIndividual> unsortedIndividuals = ontology.individualsInSignature().findAll()

        when:
        sorter.sort(ontology, unsortedIndividuals)

        then:
        thrown IllegalArgumentException
    }
}
