package owl2converter.engine.converter.attribute

import owl2converter.engine.converter.attribute.range.PropertyRangeExtractor
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.AttributeCharacteristic
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import owl2converter.engine.test.TestOWLOntologyBuilder
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.AccessModifier.PRIVATE

class PropertyToAttributeConverterTest extends Specification {

  private static final String PROPERTY_NAME = 'sample'
  private static final Type PROPERTY_TYPE = new SimpleType(String)
  private static final List<AttributeCharacteristic> PROPERTY_CHARACTERISTICS =AttributeCharacteristic.values()
  private static final String PROPERTY_DEFAULT_VALUE = 'test'

  private PropertyRangeExtractor propertyRangeExtractor = Mock()
  private AttributeCharacteristicsExtractor characteristicsExtractor = Mock()
  private AttributeDefaultValueExtractor defaultValueExtractor = Mock()

  @Subject
  private PropertyToAttributeConverter propertyToAttributeConverter =
      new PropertyToAttributeConverter(propertyRangeExtractor, characteristicsExtractor, defaultValueExtractor)

  def 'should convert object property to attribute ignoring its relations to other attributes'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withObjectPropertyDeclaration(PROPERTY_NAME).build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findAny().get()

    when:
    Attribute attribute = propertyToAttributeConverter.convertWithoutRelations(property, ontology)

    then:
    1 * propertyRangeExtractor.extractRange(property, ontology) >> PROPERTY_TYPE
    1 * characteristicsExtractor.extract(property, ontology) >> PROPERTY_CHARACTERISTICS
    1 * defaultValueExtractor.extract(property, ontology) >> Optional.of(PROPERTY_DEFAULT_VALUE)
    with(attribute) {
      name == PROPERTY_NAME
      accessModifier == PRIVATE
      !isStatic()
      valueType == PROPERTY_TYPE
      defaultValue == PROPERTY_DEFAULT_VALUE
      characteristics == PROPERTY_CHARACTERISTICS
      relations.isEmpty()
    }
  }

  def 'should convert object property to attribute without default value'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withObjectPropertyDeclaration(PROPERTY_NAME).build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findAny().get()

    when:
    Attribute attribute = propertyToAttributeConverter.convertWithoutRelations(property, ontology)

    then:
    1 * propertyRangeExtractor.extractRange(property, ontology) >> PROPERTY_TYPE
    1 * characteristicsExtractor.extract(property, ontology) >> PROPERTY_CHARACTERISTICS
    1 * defaultValueExtractor.extract(property, ontology) >> Optional.empty()
    with(attribute) {
      name == PROPERTY_NAME
      accessModifier == PRIVATE
      !isStatic()
      valueType == PROPERTY_TYPE
      defaultValue == null
      characteristics == PROPERTY_CHARACTERISTICS
      relations.isEmpty()
    }
  }

  def 'should convert data property to attribute ignoring its relations to other attributes'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withDataPropertyDeclaration(PROPERTY_NAME).build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findAny().get()

    when:
    Attribute attribute = propertyToAttributeConverter.convertWithoutRelations(property, ontology)

    then:
    1 * propertyRangeExtractor.extractRange(property, ontology) >> PROPERTY_TYPE
    1 * characteristicsExtractor.extract(property, ontology) >> PROPERTY_CHARACTERISTICS
    1 * defaultValueExtractor.extract(property, ontology) >> Optional.of(PROPERTY_DEFAULT_VALUE)
    with(attribute) {
      name == PROPERTY_NAME
      accessModifier == PRIVATE
      !isStatic()
      valueType == PROPERTY_TYPE
      defaultValue == PROPERTY_DEFAULT_VALUE
      characteristics == PROPERTY_CHARACTERISTICS
      relations.isEmpty()
    }
  }

  def 'should convert data property to attribute without default value'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withDataPropertyDeclaration(PROPERTY_NAME).build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findAny().get()

    when:
    Attribute attribute = propertyToAttributeConverter.convertWithoutRelations(property, ontology)

    then:
    1 * propertyRangeExtractor.extractRange(property, ontology) >> PROPERTY_TYPE
    1 * characteristicsExtractor.extract(property, ontology) >> PROPERTY_CHARACTERISTICS
    1 * defaultValueExtractor.extract(property, ontology) >> Optional.empty()
    with(attribute) {
      name == PROPERTY_NAME
      accessModifier == PRIVATE
      !isStatic()
      valueType == PROPERTY_TYPE
      defaultValue == null
      characteristics == PROPERTY_CHARACTERISTICS
      relations.isEmpty()
    }
  }
}
