package owl2converter.engine.generator.method.addvalue

import owl2converter.engine.OWL2ToGroovyEngineConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import spock.lang.Specification
import spock.lang.Subject

@ContextConfiguration(loader = AnnotationConfigContextLoader, classes = OWL2ToGroovyEngineConfiguration)
class AddValueBodyCodeGeneratorStrategyPrioritiesTest extends Specification {

  @Autowired
  @Subject
  private List<AddValueBodyCodeGeneratorStrategy> generators

  def 'should assert equal priorities of all add value body code generators'() {
    expect:
    generators*.priority.unique().size() == 1
  }
}
