package owl2converter.engine.converter.attribute

import owl2converter.engine.test.TestOWLOntologyBuilder
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import spock.lang.Specification
import spock.lang.Subject

class AttributeDefaultValueExtractorTest extends Specification {

  private static final String OBJECT_PROPERTY_NAME = 'testObjectProperty'
  private static final String OBJECT_PROPERTY_2_NAME = 'testObjectProperty2'
  private static final String DATA_PROPERTY_NAME = 'testDataProperty'

  @Subject
  private AttributeDefaultValueExtractor extractor = new AttributeDefaultValueExtractor()

  def 'should return no default value for functional object property'() {
    given:
    OWLOntology ontology = ontologyBuilder().withFunctionalObjectProperty(OBJECT_PROPERTY_NAME).build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<String> defaultPropertyValue = extractor.extract(property, ontology)

    then:
    !defaultPropertyValue.isPresent()
  }

  def 'should return empty list as default value for non-reflexive object property'() {
    given:
    OWLOntology ontology = ontologyBuilder().build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<String> defaultPropertyValue = extractor.extract(property, ontology)

    then:
    defaultPropertyValue.get() == '[]'
  }

  def 'should return list with single element "this" as default value for reflexive object property'() {
    given:
    OWLOntology ontology = ontologyBuilder().withReflexiveObjectProperty(OBJECT_PROPERTY_NAME).build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<String> defaultPropertyValue = extractor.extract(property, ontology)

    then:
    defaultPropertyValue.get() == '[this]'
  }

  def 'should return "this" as default value for reflexive and functional object property'() {
    given:
    OWLOntology ontology = ontologyBuilder().withReflexiveObjectProperty(OBJECT_PROPERTY_NAME)
        .withFunctionalObjectProperty(OBJECT_PROPERTY_NAME).build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<String> defaultPropertyValue = extractor.extract(property, ontology)

    then:
    defaultPropertyValue.get() == 'this'
  }

  def 'should return no default value for functional data property'() {
    given:
    OWLOntology ontology = ontologyBuilder().withFunctionalDataProperty(DATA_PROPERTY_NAME).build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Optional<String> defaultPropertyValue = extractor.extract(property, ontology)

    then:
    !defaultPropertyValue.isPresent()
  }

  def 'should return empty list as default value for data property'() {
    given:
    OWLOntology ontology = ontologyBuilder().build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Optional<String> defaultPropertyValue = extractor.extract(property, ontology)

    then:
    defaultPropertyValue.get() == '[]'
  }

  private static TestOWLOntologyBuilder ontologyBuilder() {
    return new TestOWLOntologyBuilder().withObjectPropertyDeclaration(OBJECT_PROPERTY_NAME)
        .withDataPropertyDeclaration(DATA_PROPERTY_NAME)
  }
}
