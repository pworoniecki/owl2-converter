package owl2converter.engine.generator.constructor

import owl2converter.engine.test.generator.model.ClassTestFactory
import owl2converter.engine.test.generator.model.ConstructorTestFactory
import owl2converter.engine.test.generator.model.EnumClassTestFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class GenericClassConstructorBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private GenericClassConstructorBodyCodeGenerator generator = new GenericClassConstructorBodyCodeGenerator()

  def 'should support Class type'() {
    expect:
    generator.supports(ClassTestFactory.createClass())
  }

  def 'should not support subtype of Class'() {
    expect:
    !generator.supports(EnumClassTestFactory.createEnumClass())
  }

  def 'should generate empty code'() {
    expect:
    generator.generateCode(ConstructorTestFactory.createConstructor()) == ''
  }
}
