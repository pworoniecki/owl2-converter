package owl2converter.engine.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.converter.attribute.PredefinedTypes
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.UnionTrait
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.engine.test.TestOWLOntologyBuilder
import owl2converter.engine.test.generator.model.TraitTestFactory
import spock.lang.Specification
import spock.lang.Subject

class BaseTraitsExtractorTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(
          generatedClasses: new GeneratedClassesConfiguration(basePackage: 'org', modelSubPackage: 'test'))
  )

  private static final String CLASS_1_NAME = 'TestClass1'
  private static final String CLASS_2_NAME = 'TestClass2'
  private static final String CLASS_3_NAME = 'TestClass3'
  private static final Map<OWLProperty, Attribute> NONE_ATTRIBUTES = [:]
  private Trait trait1
  private Trait trait2
  private Trait trait3

  private SimpleBaseTraitExtractor simpleBaseTraitsExtractor = Mock()
  private UnionSubclassesExtractor unionSubclassesExtractor = Mock()
  private PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

  @Subject
  private BaseTraitsExtractor extractor = 
      new BaseTraitsExtractor(simpleBaseTraitsExtractor, unionSubclassesExtractor, predefinedTypes)

  def setup() {
    trait1 = TraitTestFactory.createEmptyTrait('Trait1')
    trait2 = TraitTestFactory.createEmptyTrait('Trait2')
    trait3 = TraitTestFactory.createEmptyTrait('Trait3')
  }

  def 'should return only Thing trait if there are no classes declared in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().build()

    when:
    Map<OWLClass, Trait> traits = extractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    0 * simpleBaseTraitsExtractor.extract(*_)
    traits == [(getOwlThingClass(ontology)): predefinedTypes.thingTrait]
  }

  def 'should return name of single trait related to a class declared in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).build()
    OWLClass owlClass = ontology.classesInSignature().findFirst().get()

    when:
    Map<OWLClass, Trait> traits = extractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass, NONE_ATTRIBUTES) >> trait1
    traits == [(owlClass): trait1, (getOwlThingClass(ontology)): predefinedTypes.thingTrait]
  }

  def 'should return names of traits related to multiple classes declared in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .build()
    OWLClass owlClass1 = findClassByName(ontology, CLASS_1_NAME)
    OWLClass owlClass2 = findClassByName(ontology, CLASS_2_NAME)

    when:
    Map<OWLClass, Trait> traits = extractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass1, NONE_ATTRIBUTES) >> trait1
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass2, NONE_ATTRIBUTES) >> trait2
    traits == [(owlClass1): trait1, (owlClass2): trait2, (getOwlThingClass(ontology)): predefinedTypes.thingTrait]
  }

  def 'should assign inherited traits to traits'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withSubClassOf(CLASS_2_NAME, CLASS_1_NAME).withSubClassOf(CLASS_2_NAME, CLASS_3_NAME)
        .build()
    OWLClass owlClass1 = findClassByName(ontology, CLASS_1_NAME)
    OWLClass owlClass2 = findClassByName(ontology, CLASS_2_NAME)
    OWLClass owlClass3 = findClassByName(ontology, CLASS_3_NAME)

    when:
    Map<OWLClass, Trait> traits = extractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass1, NONE_ATTRIBUTES) >> trait1
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass2, NONE_ATTRIBUTES) >> trait2
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass3, NONE_ATTRIBUTES) >> trait3
    traits.values().sort() == [trait1, trait2, trait3, predefinedTypes.thingTrait].sort()

    and:
    trait1.inheritedTraits == [predefinedTypes.thingTrait]
    trait2.inheritedTraits == [trait1, trait3]
    trait3.inheritedTraits == [predefinedTypes.thingTrait]
  }

  def 'should assign union traits to inherited traits of equivalent union classes'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withClassesEquivalentOfUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()
    UnionTrait unionTrait = new UnionTrait.Builder('test.package', 'UnionTrait').buildTrait()
    OWLClass owlClass1 = findClassByName(ontology, CLASS_1_NAME)
    OWLClass owlClass2 = findClassByName(ontology, CLASS_2_NAME)
    OWLClass owlClass3 = findClassByName(ontology, CLASS_3_NAME)

    when:
    Map<OWLClass, Trait> traits = extractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass1, NONE_ATTRIBUTES) >> unionTrait
    1 * unionSubclassesExtractor.extract(ontology, owlClass1) >> [owlClass2, owlClass3]
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass2, NONE_ATTRIBUTES) >> trait2
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass3, NONE_ATTRIBUTES) >> trait3
    traits.values().sort() == [unionTrait, trait2, trait3, predefinedTypes.thingTrait].sort()

    and:
    unionTrait.inheritedTraits == [predefinedTypes.thingTrait]
    trait2.inheritedTraits == [unionTrait]
    trait3.inheritedTraits == [unionTrait]
  }

  def 'should assign fill union trait with information about traits related to the union'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withClassesEquivalentOfUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()
    UnionTrait unionTrait = new UnionTrait.Builder('test.package', 'UnionTrait').buildTrait()
    OWLClass owlClass1 = findClassByName(ontology, CLASS_1_NAME)
    OWLClass owlClass2 = findClassByName(ontology, CLASS_2_NAME)
    OWLClass owlClass3 = findClassByName(ontology, CLASS_3_NAME)

    when:
    extractor.extract(ontology, NONE_ATTRIBUTES)

    then:
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass1, NONE_ATTRIBUTES) >> unionTrait
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass2, NONE_ATTRIBUTES) >> trait2
    1 * simpleBaseTraitsExtractor.extract(ontology, owlClass3, NONE_ATTRIBUTES) >> trait3
    1 * unionSubclassesExtractor.extract(ontology, owlClass1) >> [owlClass2, owlClass3]

    and:
    with(unionTrait) {
      unionTrait.inheritedTraits == [predefinedTypes.thingTrait]
      unionTrait.isUnionOf(trait2)
      unionTrait.isUnionOf(trait3)
    }
  }

  private static OWLClass getOwlThingClass(OWLOntology ontology) {
    return ontology.OWLOntologyManager.OWLDataFactory.OWLThing
  }

  private static OWLClass findClassByName(OWLOntology ontology, String name) {
    return ontology.classesInSignature().find { it.getIRI().remainder.get() == name } as OWLClass
  }
}
