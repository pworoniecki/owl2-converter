package owl2converter.engine.converter.attribute

import com.google.common.collect.HashBasedTable
import com.google.common.collect.Table
import com.google.common.collect.Table.Cell
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.relation.AttributeRelation
import owl2converter.groovymetamodel.attribute.relation.EquivalentToAttributeRelation
import owl2converter.groovymetamodel.attribute.relation.InverseAttributeRelation
import owl2converter.groovymetamodel.attribute.relation.SuperAttributeOfRelation
import owl2converter.engine.test.TestOWLOntologyBuilder
import owl2converter.engine.test.generator.model.AttributeTestFactory
import org.slf4j.Logger
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import spock.lang.Specification
import spock.lang.Subject

class AttributesExtractorTest extends Specification {

  private static final Attribute ATTRIBUTE = AttributeTestFactory.createAttribute()
  private static final List<AttributeRelation> ATTRIBUTE_RELATIONS_1 = [
      new InverseAttributeRelation(ATTRIBUTE), new EquivalentToAttributeRelation(ATTRIBUTE)
  ]
  private static final List<AttributeRelation> ATTRIBUTE_RELATIONS_2 = [
      new SuperAttributeOfRelation(ATTRIBUTE), new EquivalentToAttributeRelation(ATTRIBUTE)
  ]

  private Attribute attribute1
  private Attribute attribute2

  private Logger logger = Mock()
  private PropertyToAttributeConverter propertyToAttributeConverter = Mock()
  private AttributeRelationsExtractor relationsExtractor = Mock()

  @Subject
  private AttributesExtractor extractor = new AttributesExtractor(logger, propertyToAttributeConverter, relationsExtractor)

  def setup() {
    attribute1 = AttributeTestFactory.createAttribute('attribute1')
    attribute2 = AttributeTestFactory.createAttribute('attribute2')
    relationsExtractor.extract(_ as OWLObjectProperty, _ as OWLOntology, _ as Map<OWLObjectProperty, Attribute>) >> ATTRIBUTE_RELATIONS_1
    relationsExtractor.extract(_ as OWLDataProperty, _ as OWLOntology, _ as Map<OWLDataProperty, Attribute>) >> ATTRIBUTE_RELATIONS_2
  }

  def 'should return empty list of attributes when ontology contains no attributes'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().build()

    when:
    Map<OWLProperty, Attribute> attributes = extractor.extract(ontology)

    then:
    0 * propertyToAttributeConverter.convertWithoutRelations(_)
    attributes.isEmpty()
  }

  def 'should extract object properties from ontology and convert them into attributes'() {
    given:
    def classNames = ['Class1', 'Class2']
    Table<String, String, String> objectPropertyToDomainAndRange = HashBasedTable.create()
    objectPropertyToDomainAndRange.put('objectProperty1', 'Class1', 'Class2')
    objectPropertyToDomainAndRange.put('objectProperty2', 'Class2', 'Class1')
    OWLOntology ontology = createSampleOntologyWithObjectProperties(classNames, objectPropertyToDomainAndRange)
    List<OWLObjectProperty> properties = ontology.objectPropertiesInSignature().findAll()

    when:
    Map<OWLProperty, Attribute> attributes = extractor.extract(ontology)

    then:
    1 * propertyToAttributeConverter.convertWithoutRelations(properties.first(), ontology) >> attribute1
    1 * relationsExtractor.extract(properties.first(), ontology, _ as Map<OWLObjectProperty, Attribute>) >> ATTRIBUTE_RELATIONS_1
    1 * propertyToAttributeConverter.convertWithoutRelations(properties.last(), ontology) >> attribute2
    1 * relationsExtractor.extract(properties.last(), ontology, _ as Map<OWLObjectProperty, Attribute>) >> ATTRIBUTE_RELATIONS_2
    with(attributes.entrySet().first()) { Map.Entry<OWLObjectProperty, Attribute> propertyToAttribute ->
      propertyToAttribute.key == properties.first()
      propertyToAttribute.value == attribute1
      propertyToAttribute.value.relations == ATTRIBUTE_RELATIONS_1
    }
    with(attributes.entrySet().last()) { Map.Entry<OWLObjectProperty, Attribute> propertyToAttribute ->
      propertyToAttribute.key == properties.last()
      propertyToAttribute.value == attribute2
      propertyToAttribute.value.relations == ATTRIBUTE_RELATIONS_2
    }
  }

  def 'should extract data properties from ontology and convert them into attributes'() {
    given:
    def classNames = ['Class1', 'Class2']
    Table<String, String, String> dataPropertyToDomainAndRange = HashBasedTable.create()
    dataPropertyToDomainAndRange.put('dataProperty1', 'Class1', 'int')
    dataPropertyToDomainAndRange.put('dataProperty2', 'Class2', 'int')
    OWLOntology ontology = createSampleOntologyWithDataProperties(classNames, dataPropertyToDomainAndRange)
    List<OWLDataProperty> properties = ontology.dataPropertiesInSignature().findAll()

    when:
    Map<OWLProperty, Attribute> attributes = extractor.extract(ontology)

    then:
    1 * propertyToAttributeConverter.convertWithoutRelations(properties.first(), ontology) >> attribute1
    1 * relationsExtractor.extract(properties.first(), ontology, _ as Map<OWLDataProperty, Attribute>) >> ATTRIBUTE_RELATIONS_1
    1 * propertyToAttributeConverter.convertWithoutRelations(properties.last(), ontology) >> attribute2
    1 * relationsExtractor.extract(properties.last(), ontology, _ as Map<OWLDataProperty, Attribute>) >> ATTRIBUTE_RELATIONS_2
    with(attributes.entrySet().first()) { Map.Entry<OWLDataProperty, Attribute> propertyToAttribute ->
      propertyToAttribute.key == properties.first()
      propertyToAttribute.value == attribute1
      propertyToAttribute.value.relations == ATTRIBUTE_RELATIONS_1
    }
    with(attributes.entrySet().last()) { Map.Entry<OWLDataProperty, Attribute> propertyToAttribute ->
      propertyToAttribute.key == properties.last()
      propertyToAttribute.value == attribute2
      propertyToAttribute.value.relations == ATTRIBUTE_RELATIONS_2
    }
  }

  def 'should not extract properties from other ontologies'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder()
        .withObjectPropertyDeclaration('ownObjectProperty')
        .withObjectPropertyDeclaration('anotherOwnObjectProperty')
        .withObjectPropertyDeclaration('externalObjectProperty', 'anotherOntology')
        .withDataPropertyDeclaration('ownDataProperty')
        .withDataPropertyDeclaration('externalDataProperty', 'yetAnotherOntology')
        .build()

    when:
    Map<OWLProperty, Attribute> attributes = extractor.extract(ontology)

    then:
    3 * propertyToAttributeConverter.convertWithoutRelations(*_) >> attribute1
    attributes.size() == 3
    2 * logger.warn(*_)
  }

  private static OWLOntology createSampleOntologyWithObjectProperties(List<String> classNames,
                                                                      Table<String, String, String> propertyToDomainAndRange) {
    TestOWLOntologyBuilder ontologyBuilder = new TestOWLOntologyBuilder().withClassDeclarations(classNames)
    propertyToDomainAndRange.cellSet().each { Cell<String, String, String> propertyNameToDomainAndRange ->
      String propertyName = propertyNameToDomainAndRange.rowKey
      String domain = propertyNameToDomainAndRange.columnKey
      String range = propertyNameToDomainAndRange.value
      ontologyBuilder = ontologyBuilder.withObjectPropertyDeclaration(propertyName)
          .withObjectPropertyDomain(propertyName, domain).withObjectPropertyRange(propertyName, range)
    }
    return ontologyBuilder.build()
  }

  private static OWLOntology createSampleOntologyWithDataProperties(List<String> classNames,
                                                                    Table<String, String, String> propertyToDomainAndRange) {
    TestOWLOntologyBuilder ontologyBuilder = new TestOWLOntologyBuilder().withClassDeclarations(classNames)
    propertyToDomainAndRange.cellSet().each { Cell<String, String, String> propertyNameToDomainAndRange ->
      String propertyName = propertyNameToDomainAndRange.rowKey
      String domain = propertyNameToDomainAndRange.columnKey
      String range = propertyNameToDomainAndRange.value
      ontologyBuilder = ontologyBuilder.withDataPropertyDeclaration(propertyName)
          .withDataPropertyDomain(propertyName, domain).withDataPropertyRange(propertyName, range)
    }
    return ontologyBuilder.build()
  }
}
