package owl2converter.engine.converter.attribute

import owl2converter.groovymetamodel.attribute.AttributeCharacteristic
import owl2converter.engine.test.TestOWLOntologyBuilder
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.ASYMMETRIC
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.INVERSE_FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.IRREFLEXIVE
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.SYMMETRIC
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE

class AttributeCharacteristicsExtractorTest extends Specification {

  private static final String OBJECT_PROPERTY_NAME = 'testObjectProperty'
  private static final String DATA_PROPERTY_NAME = 'testDataProperty'

  @Subject
  private AttributeCharacteristicsExtractor extractor = new AttributeCharacteristicsExtractor()

  @Unroll
  def 'should extract attribute characteristics for given object property'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    List<AttributeCharacteristic> characteristics = extractor.extract(property, ontology)

    then:
    characteristics == [expectedCharacteristic]

    where:
    ontology                                                                            | expectedCharacteristic
    ontologyBuilder().withSymmetricObjectProperty(OBJECT_PROPERTY_NAME).build()         | SYMMETRIC
    ontologyBuilder().withAsymmetricObjectProperty(OBJECT_PROPERTY_NAME).build()        | ASYMMETRIC
    ontologyBuilder().withTransitiveObjectProperty(OBJECT_PROPERTY_NAME).build()        | TRANSITIVE
    ontologyBuilder().withFunctionalObjectProperty(OBJECT_PROPERTY_NAME).build()        | FUNCTIONAL
    ontologyBuilder().withInverseFunctionalObjectProperty(OBJECT_PROPERTY_NAME).build() | INVERSE_FUNCTIONAL
    ontologyBuilder().withReflexiveObjectProperty(OBJECT_PROPERTY_NAME).build()         | REFLEXIVE
    ontologyBuilder().withIrreflexiveObjectProperty(OBJECT_PROPERTY_NAME).build()       | IRREFLEXIVE
  }

  def 'should extract all possible attribute characteristics'() {
    given:
    OWLOntology ontology = ontologyBuilder().withSymmetricObjectProperty(OBJECT_PROPERTY_NAME)
        .withAsymmetricObjectProperty(OBJECT_PROPERTY_NAME).withTransitiveObjectProperty(OBJECT_PROPERTY_NAME)
        .withFunctionalObjectProperty(OBJECT_PROPERTY_NAME).withInverseFunctionalObjectProperty(OBJECT_PROPERTY_NAME)
        .withReflexiveObjectProperty(OBJECT_PROPERTY_NAME).withIrreflexiveObjectProperty(OBJECT_PROPERTY_NAME).build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    List<AttributeCharacteristic> characteristics = extractor.extract(property, ontology)

    then:
    characteristics.sort() == AttributeCharacteristic.values().sort()
  }

  def 'should extract no characteristics of data property'() {
    given:
    OWLOntology ontology = ontologyBuilder().build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    List<AttributeCharacteristic> characteristics = extractor.extract(property, ontology)

    then:
    characteristics.isEmpty()
  }

  def 'should extract functional characteristic of data property'() {
    given:
    OWLOntology ontology = ontologyBuilder().withFunctionalDataProperty(DATA_PROPERTY_NAME).build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    List<AttributeCharacteristic> characteristics = extractor.extract(property, ontology)

    then:
    characteristics == [FUNCTIONAL]
  }

  private static TestOWLOntologyBuilder ontologyBuilder() {
    return new TestOWLOntologyBuilder().withObjectPropertyDeclaration(OBJECT_PROPERTY_NAME)
        .withDataPropertyDeclaration(DATA_PROPERTY_NAME)
  }
}
