package owl2converter.engine.generator.method.getter

import owl2converter.engine.OWL2ToGroovyEngineConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import javax.annotation.PostConstruct

@ContextConfiguration(loader = AnnotationConfigContextLoader, classes = OWL2ToGroovyEngineConfiguration)
class GetterBodyCodeGeneratorStrategyPrioritiesTest extends Specification {

  @Autowired
  @Subject
  private List<GetterBodyCodeGeneratorStrategy> generators

  @Shared
  private GetterBodyCodeGeneratorStrategy genericGenerator

  @Shared
  private List<GetterBodyCodeGeneratorStrategy> nonGenericGenerators

  @PostConstruct
  def setupGenerators() {
    genericGenerator = generators.find { it instanceof GenericGetterBodyCodeGenerator }
    nonGenericGenerators = generators - genericGenerator
  }

  def 'should assert equal priorities of all getter body code generators except generic one'() {
    expect:
    nonGenericGenerators*.priority.unique().size() == 1
  }

  def 'should assert less priority of generic generator than all other generators'() {
    expect:
    genericGenerator.priority < nonGenericGenerator.priority

    where:
    nonGenericGenerator << nonGenericGenerators
  }
}
