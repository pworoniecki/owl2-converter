package owl2converter.engine.converter.attribute

import owl2converter.engine.converter.attribute.range.PropertyDirectRangeExtractor
import owl2converter.engine.test.TestOWLOntologyBuilder
import org.slf4j.Logger
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLDatatype
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import spock.lang.Specification
import spock.lang.Subject

class PropertyDirectRangeExtractorTest extends Specification {

  static final String PROPERTY_1_NAME = 'testProperty1'
  static final String PROPERTY_1_CLASS_RANGE = 'TestClass1'
  static final String PROPERTY_1_DATATYPE_RANGE = 'int'

  private Logger logger = Mock()

  @Subject
  private PropertyDirectRangeExtractor extractor = new PropertyDirectRangeExtractor(logger)

  def 'should return single range defined directly for object property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_CLASS_RANGE)
        .withObjectPropertyDeclaration(PROPERTY_1_NAME).withObjectPropertyRange(PROPERTY_1_NAME, PROPERTY_1_CLASS_RANGE)
        .build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> range = extractor.extractSingleDirectRange(property, ontology)

    then:
    range.get().getIRI().remainder.get() == PROPERTY_1_CLASS_RANGE
    0 * logger.warn(*_)
  }

  def 'should return empty range when no range is declared for object property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_CLASS_RANGE)
        .withObjectPropertyDeclaration(PROPERTY_1_NAME).build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> range = extractor.extractSingleDirectRange(property, ontology)

    then:
    !range.isPresent()
    0 * logger.warn(*_)
  }

  def 'should return empty range and log warning when more than 1 range is found for object property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_CLASS_RANGE)
        .withObjectPropertyDeclaration(PROPERTY_1_NAME)
        .withObjectPropertyRange(PROPERTY_1_NAME, 'Class1').withObjectPropertyRange(PROPERTY_1_NAME, 'Class2').build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> range = extractor.extractSingleDirectRange(property, ontology)

    then:
    !range.isPresent()
    1 * logger.warn(*_)
  }

  def 'should return empty range and log warning when range found for object property does not refer to OWL class'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(PROPERTY_1_CLASS_RANGE)
        .withObjectPropertyDeclaration(PROPERTY_1_NAME)
        .withObjectPropertyRangeAsUnion(PROPERTY_1_NAME, 'Class1', 'Class2').build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLClass> range = extractor.extractSingleDirectRange(property, ontology)

    then:
    !range.isPresent()
    1 * logger.warn(*_)
  }

  def 'should return single range defined directly for data property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withDataPropertyDeclaration(PROPERTY_1_NAME)
        .withDataPropertyRange(PROPERTY_1_NAME, PROPERTY_1_DATATYPE_RANGE).build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLDatatype> range = extractor.extractSingleDirectRange(property, ontology)

    then:
    range.get().getIRI().remainder.get() == PROPERTY_1_DATATYPE_RANGE
    0 * logger.warn(*_)
  }

  def 'should return empty range when no range is declared for data property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withDataPropertyDeclaration(PROPERTY_1_NAME).build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLDatatype> range = extractor.extractSingleDirectRange(property, ontology)

    then:
    !range.isPresent()
    0 * logger.warn(*_)
  }

  def 'should return empty range and log warning when more than 1 range is found for data property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withObjectPropertyDeclaration(PROPERTY_1_NAME)
        .withDataPropertyRange(PROPERTY_1_NAME, 'byte').withDataPropertyRange(PROPERTY_1_NAME, 'int').build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLDatatype> range = extractor.extractSingleDirectRange(property, ontology)

    then:
    !range.isPresent()
    1 * logger.warn(*_)
  }

  def 'should return empty range and log warning when range found for data property does not refer to OWL datatype'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withDataPropertyDeclaration(PROPERTY_1_NAME)
        .withDataPropertyRangeAsUnion(PROPERTY_1_NAME, 'int', 'short').build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Optional<OWLDatatype> range = extractor.extractSingleDirectRange(property, ontology)

    then:
    !range.isPresent()
    1 * logger.warn(*_)
  }
}
