package owl2converter.engine.utils

import spock.lang.Specification

class StringUtilsTest extends Specification {
  
  def 'should return empty string if input string was empty'() {
    expect:
    StringUtils.emptyOrAddSpace('') == ''
  }

  def 'should return string with added single space if input string was not empty'() {
    expect:
    StringUtils.emptyOrAddSpace('test') == 'test '
  }
}
