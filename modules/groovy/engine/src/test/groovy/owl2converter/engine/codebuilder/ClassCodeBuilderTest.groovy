package owl2converter.engine.codebuilder

import owl2converter.groovymetamodel.*
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import java.util.concurrent.*
import java.util.concurrent.locks.Lock

import static owl2converter.groovymetamodel.AccessModifier.PRIVATE
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static owl2converter.engine.test.generator.model.AttributeTestFactory.createEmptyNonStaticAttribute
import static owl2converter.engine.test.generator.model.AttributeTestFactory.createEmptyStaticAttribute
import static owl2converter.engine.test.generator.model.ClassTestFactory.createEmptyClass
import static owl2converter.engine.test.generator.model.MethodTestFactory.createNonStaticMethod
import static owl2converter.engine.test.generator.model.MethodTestFactory.createStaticMethod
import static owl2converter.engine.test.generator.model.TraitTestFactory.createTrait

class ClassCodeBuilderTest extends Specification {

  private static final Type ANNOTATION_TYPE = new SimpleType('test', 'DisjointWith')
  private static final CONSTRUCTOR_TYPES_TO_BE_IMPORTED = [ConcurrentLinkedQueue]
  private static final ATTRIBUTE_TYPES_TO_BE_IMPORTED = [BlockingQueue, ConcurrentHashMap]
  private static final METHOD_TYPES_TO_BE_IMPORTED = [Semaphore, BlockingDeque, Lock]

  @Shared
  private Annotation annotationWithoutParameter = new Annotation(ANNOTATION_TYPE)

  @Shared
  private Annotation annotationWithParameters = new Annotation(ANNOTATION_TYPE, ['One', 'Two'])

  @Shared
  private Class classModel = createTestClass()

  @Subject
  private ClassCodeBuilder builder

  def setup() {
    builder = new ClassCodeBuilder(classModel)
  }

  def 'should append package'() {
    when:
    builder.appendPackage()

    then:
    builder.code == "package ${classModel.packageName}\n".toString()
  }

  def 'should append class annotations'() {
    when:
    builder.appendAnnotations(classModel.annotations)

    then:
    builder.code ==
        """@$annotationWithoutParameter.type.simpleName
          |@${annotationWithParameters.type.simpleName}([${annotationWithParameters.parameters.join(', ')}])\n""".stripMargin()
  }

  def 'should append class signature end character'() {
    when:
    builder.appendClassSignatureEnd()

    then:
    builder.code == "}\n"
  }

  def 'should append imports for attributes, constructors, methods, implemented traits and inherited class'() {
    when:
    builder.appendSpecificImports()

    then:
    def expectedImports = [
        classModel.inheritedClass.get().fullName,
        classModel.implementedTraits[0].fullName,
        classModel.implementedTraits[1].fullName,
    ] + CONSTRUCTOR_TYPES_TO_BE_IMPORTED*.name + METHOD_TYPES_TO_BE_IMPORTED*.name +
        ATTRIBUTE_TYPES_TO_BE_IMPORTED*.name + ANNOTATION_TYPE.fullName
    builder.code == expectedImports.collect { "import $it\n" }.join('')
  }

  def 'should append static constructors with empty lines after them'() {
    given:
    Closure<String> getCodeForConstructor = { "${it.parameters}${it.accessModifier}${it.isStatic()}" }
    Map<Constructor, String> constructorsToCode =
        classModel.constructors.collectEntries { [(it): getCodeForConstructor(it)] }

    when:
    builder.appendStaticConstructorsWithEmptyLines(constructorsToCode)

    then:
    builder.code == classModel.constructors.findAll { it.isStatic() }.collect { getCodeForConstructor(it) + '\n\n' }
        .join('')
  }

  def 'should append non-static constructors with empty lines after them'() {
    given:
    Closure<String> getCodeForConstructor = { "${it.parameters}${it.accessModifier}${it.isStatic()}" }
    Map<Constructor, String> constructorsToCode =
        classModel.constructors.collectEntries { [(it): getCodeForConstructor(it)] }

    when:
    builder.appendNonStaticConstructorsWithEmptyLines(constructorsToCode)

    then:
    builder.code == classModel.constructors.findAll { !it.isStatic() }.collect { getCodeForConstructor(it) + '\n\n' }
        .join('')
  }

  private Class createTestClass() {
    return new Class.Builder('org.test', 'TestClass')
        .withAnnotations([annotationWithoutParameter, annotationWithParameters])
        .withInheritedClass(createEmptyClass('org.anotherpackage'))
        .withImplementedTraits(createTestTraits())
        .withAttributes(createTestAttributes())
        .withConstructors(createTestConstructors())
        .withMethods(createTestMethods())
        .buildClass()
  }

  private static List<Trait> createTestTraits() {
    return [createTrait('Trait1', 'com.traits'), createTrait('Trait2', 'com.traits')]
  }

  private static List<Attribute> createTestAttributes() {
    return [
        createEmptyStaticAttribute('firstStaticAttribute', new SimpleType(ATTRIBUTE_TYPES_TO_BE_IMPORTED[0])),
        createEmptyStaticAttribute('secondStaticAttribute', new SimpleType(String)),
        createEmptyNonStaticAttribute('firstNonStaticAttribute', new SimpleType(ATTRIBUTE_TYPES_TO_BE_IMPORTED[1])),
        createEmptyNonStaticAttribute('secondNonStaticAttribute', new SimpleType(Integer))
    ]
  }

  private static List<Constructor> createTestConstructors() {
    return [
        new Constructor(isStatic: true, accessModifier: PUBLIC),
        new Constructor(isStatic: true, accessModifier: PRIVATE,
            parameters: [new Parameter(new SimpleType(CONSTRUCTOR_TYPES_TO_BE_IMPORTED[0]), 'testParameter')]),
        new Constructor(isStatic: false, accessModifier: PUBLIC),
        new Constructor(isStatic: false, accessModifier: PRIVATE)
    ]
  }

  private static List<Method> createTestMethods() {
    return [
        createStaticMethod('firstStaticMethod', new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[0])),
        createStaticMethod('secondStaticMethod'),
        createNonStaticMethod('firstNonStaticMethod', new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[1])),
        createNonStaticMethod('secondNonStaticMethod', new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[2]),
            [new Parameter(new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[0]), 'testParameter')]),
    ]
  }
}
