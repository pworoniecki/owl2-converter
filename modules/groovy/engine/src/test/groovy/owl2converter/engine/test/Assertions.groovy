package owl2converter.engine.test

class Assertions {

  static void assertEqualIgnoreOrder(Collection<?> collection1, Collection<?> collection2) {
    if (collection1 == null || collection2 == null) {
      assert collection1 == collection2
      return
    }
    assert collection1.size() == collection2.size()
    collection1.each { assert collection2.contains(it) }
  }
}
