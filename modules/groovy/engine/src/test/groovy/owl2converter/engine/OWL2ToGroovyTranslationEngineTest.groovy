package owl2converter.engine

import commoncodemetamodel.Element
import commoncodemetamodel.Folder
import commoncodemetamodel.TextFile
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.converter.ObjectOrientedCodeModel
import owl2converter.engine.converter.OntologyToCodeModelConverter
import owl2converter.engine.generator.ClassCodeGenerator
import owl2converter.engine.generator.TraitCodeGenerator
import owl2converter.engine.test.generator.model.ClassTestFactory
import owl2converter.engine.utils.GroovyCodeFormatter
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Trait
import org.semanticweb.owlapi.model.OWLOntology
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.test.generator.model.ClassTestFactory.createClass
import static owl2converter.engine.test.generator.model.TraitTestFactory.createTrait

class OWL2ToGroovyTranslationEngineTest extends Specification {

  private OWLOntology ontology = Mock()
  private Class individualsDataSourceClass = createClass('IndividualsDataSource')
  private List<Class> classes = [createClass('Class1'), createClass('Class2')]
  private List<Trait> traits = [createTrait('Trait1'), createTrait('Trait2')]

  private OntologyToCodeModelConverter ontologyToCodeModelConverter = Mock()
  private ClassCodeGenerator classCodeGenerator = Mock()
  private TraitCodeGenerator traitCodeGenerator = Mock()
  private GroovyCodeFormatter codeFormatter = Mock()
  private ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(
          generatedClasses: new GeneratedClassesConfiguration(basePackage: 'com.app.test', modelSubPackage: 'model')
      )
  )
  private ResultCodeBaseMetamodelExtractor resultCodeBaseMetamodelExtractor = Mock()
  private Folder resultCodeBaseFolder = new Folder(
      'src', [
      new Folder('main', [
          new Folder('groovy', [
              new Folder('com', [
                  new Folder('app', [
                      new Folder('test', [
                          new Folder('model', [].toSet())
                          ].toSet()
                      )].toSet()
                  )].toSet()
              )].toSet()
          )].toSet()
      )].toSet()
  )

  @Subject
  private OWL2ToGroovyTranslationEngine translationEngine = new OWL2ToGroovyTranslationEngine(ontologyToCodeModelConverter,
      classCodeGenerator, traitCodeGenerator, codeFormatter, applicationConfiguration, resultCodeBaseMetamodelExtractor)

  def setup() {
    ontologyToCodeModelConverter.convert(ontology) >> new ObjectOrientedCodeModel(classes, traits, individualsDataSourceClass)
    classCodeGenerator.generateCode(_ as Class) >> { generateCode(it) }
    traitCodeGenerator.generateCode(_ as Trait) >> { generateCode(it) }
    codeFormatter.format(_) >> { args -> args[0] }
    resultCodeBaseMetamodelExtractor.extractResultCodeMetamodel() >> [resultCodeBaseFolder].toSet()
  }

  def 'should create all necessary directories to place generated files'() {
    given:
    String[] rootFolderMiddleFolderNames = applicationConfiguration.generator.generatedClasses.basePackage.split('\\.')
    String modelSubPackage = applicationConfiguration.generator.generatedClasses.modelSubPackage

    when:
    Set<Element> codeMetamodel = translationEngine.translateOWL2Ontology(ontology)

    then:
    codeMetamodel.size() == 1
    Folder folder = codeMetamodel.first() as Folder
    folder.name == 'src'
    folder.subFolderNames == ['main'].toSet()
    folder.subFolders.first().subFolderNames == ['groovy'].toSet()
    folder.subFolders.first().subFolders.first().subFolderNames == [rootFolderMiddleFolderNames[0]].toSet()
    folder.subFolders.first().subFolders.first().subFolders.first().subFolderNames == [rootFolderMiddleFolderNames[1]].toSet()
    folder.subFolders.first().subFolders.first().subFolders.first().subFolders.first().subFolderNames == [rootFolderMiddleFolderNames[2]].toSet()
    folder.subFolders.first().subFolders.first().subFolders.first().subFolders.first().subFolders.first().subFolderNames == [modelSubPackage].toSet()
    folder.subFolders.first().subFolders.first().subFolders.first().subFolders.first().subFolders.first().subFolders.first().subFolders.isEmpty()
  }

  def 'should place generated classes and subclasses in root folder from configuration and its model subpackage'() {
    when:
    Set<Element> codeMetamodel = translationEngine.translateOWL2Ontology(ontology)

    then:
    codeMetamodel.size() == 1
    Folder folder = codeMetamodel.first() as Folder
    Folder modelFolder = folder.subFolders.first().subFolders.first().subFolders.first().subFolders.first().subFolders.first().subFolders.first()
    modelFolder.subFolders.isEmpty()
    modelFolder.files.size() == classes.size() + traits.size()
    with(modelFolder.files.sort {it.name}) {
      it.each { assert it instanceof TextFile }
      it*.name == ['Class1', 'Class2', 'Trait1', 'Trait2']
      (it as List<TextFile>)*.content == classes.collect {generateCode(it)} + traits.collect {generateCode(it)}
   }
  }

  private static String generateCode(Class classModel) {
    return "Code for $classModel.name"
  }

  private static String generateCode(Trait traitModel) {
    return "Code for $traitModel.name"
  }
}
