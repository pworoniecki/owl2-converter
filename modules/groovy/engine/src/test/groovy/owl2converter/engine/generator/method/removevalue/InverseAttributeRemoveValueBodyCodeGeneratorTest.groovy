package owl2converter.engine.generator.method.removevalue

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.relation.AttributeRelation
import owl2converter.groovymetamodel.attribute.relation.InverseAttributeRelation
import owl2converter.groovymetamodel.method.DirectSetter
import owl2converter.groovymetamodel.method.RemoveValueMethod
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.test.generator.model.AttributeTestFactory.createAttribute
import static owl2converter.engine.test.generator.model.RemoveValueMethodTestFactory.createRemoveValueMethod

class InverseAttributeRemoveValueBodyCodeGeneratorTest extends Specification {

  private static final List<Attribute> INVERSE_ATTRIBUTES = [
      createAttribute('attr1', Types.STRING_LIST), createAttribute('attr2', Types.STRING),
      createAttribute('attr3', Types.STRING_LIST)
  ]
  private static final List<AttributeRelation> INVERSE_ATTRIBUTE_RELATIONS =
      INVERSE_ATTRIBUTES.collect { new InverseAttributeRelation(it) }

  @Subject
  @Shared
  private InverseAttributeRemoveValueBodyCodeGenerator generator = new InverseAttributeRemoveValueBodyCodeGenerator()

  def 'should support remove value method of inverse attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withRelations(INVERSE_ATTRIBUTE_RELATIONS).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    generator.supports(removeValueMethod)
  }

  def 'should not support remove value method of non inverse attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    !generator.supports(removeValueMethod)
  }

  def 'should generate no validation code of remove value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withRelations(INVERSE_ATTRIBUTE_RELATIONS).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(removeValueMethod)

    then:
    !generatedCode.isPresent()
  }

  def 'should generate specific remove value code of remove value method'() {
    given:
    def attributeName = 'attribute'
    def attribute = new Attribute.AttributeBuilder(attributeName, Types.STRING_LIST)
        .withRelations(INVERSE_ATTRIBUTE_RELATIONS).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(removeValueMethod)

    then:
    generatedCode.isPresent()
    generatedCode.get() == [
        "${removeValueMethod.parameter.name}.remove${INVERSE_ATTRIBUTES[0].name.capitalize()}(this)",
        "${removeValueMethod.parameter.name}.${DirectSetter.getName(INVERSE_ATTRIBUTES[1])}(null)",
        "${removeValueMethod.parameter.name}.remove${INVERSE_ATTRIBUTES[2].name.capitalize()}(this)"
    ].join('\n')
  }
}
