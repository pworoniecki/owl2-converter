package owl2converter.engine.codebuilder

import owl2converter.groovymetamodel.Annotation
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import java.util.concurrent.BlockingDeque
import java.util.concurrent.BlockingQueue
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Semaphore
import java.util.concurrent.locks.Lock

import static owl2converter.engine.test.generator.model.AttributeTestFactory.createEmptyNonStaticAttribute
import static owl2converter.engine.test.generator.model.AttributeTestFactory.createEmptyStaticAttribute
import static owl2converter.engine.test.generator.model.MethodTestFactory.createNonStaticMethod
import static owl2converter.engine.test.generator.model.MethodTestFactory.createStaticMethod
import static owl2converter.engine.test.generator.model.TraitTestFactory.createTrait

class TraitCodeBuilderTest extends Specification {

  private static final Type ANNOTATION_TYPE = new SimpleType('test', 'DisjointWith')
  private static final ATTRIBUTE_TYPES_TO_BE_IMPORTED = [BlockingQueue, ConcurrentHashMap]
  private static final METHOD_TYPES_TO_BE_IMPORTED = [Semaphore, BlockingDeque, Lock]

  @Shared
  private Annotation annotationWithoutParameter = new Annotation(ANNOTATION_TYPE)

  @Shared
  private Annotation annotationWithParameters = new Annotation(ANNOTATION_TYPE, ['One', 'Two'])

  @Shared
  private Trait traitModel = createTestTrait()

  @Subject
  private TraitCodeBuilder builder

  def setup() {
    builder = new TraitCodeBuilder(traitModel)
  }

  def 'should append package'() {
    when:
    builder.appendPackage()

    then:
    builder.code == "package ${traitModel.packageName}\n".toString()
  }

  def 'should append trait annotations'() {
    when:
    builder.appendAnnotations(traitModel.annotations)

    then:
    builder.code ==
        """@$annotationWithoutParameter.type.simpleName
          |@${annotationWithParameters.type.simpleName}([${annotationWithParameters.parameters.join(', ')}])\n""".stripMargin()
  }

  def 'should append class signature end character'() {
    when:
    builder.appendTraitSignatureEnd()

    then:
    builder.code == "}\n"
  }

  def 'should append imports for attributes, constructors, methods, implemented traits and inherited class'() {
    when:
    builder.appendSpecificImports()

    then:
    def expectedImports = [
        traitModel.inheritedTraits[0].fullName,
        traitModel.inheritedTraits[1].fullName,
    ] + METHOD_TYPES_TO_BE_IMPORTED*.name + ATTRIBUTE_TYPES_TO_BE_IMPORTED*.name + ANNOTATION_TYPE.fullName
    builder.code == expectedImports.collect { "import $it\n" }.join('')
  }

  private Trait createTestTrait() {
    return new Trait.Builder('org.test', 'TestTrait')
        .withAnnotations([annotationWithoutParameter, annotationWithParameters])
        .withInheritedTraits(createTestTraits())
        .withAttributes(createTestAttributes())
        .withMethods(createTestMethods())
        .buildTrait()
  }

  private static List<Trait> createTestTraits() {
    return [createTrait('Trait1', 'com.traits'), createTrait('Trait2', 'com.traits')]
  }

  private static List<Attribute> createTestAttributes() {
    return [
        createEmptyStaticAttribute('firstStaticAttribute', new SimpleType(ATTRIBUTE_TYPES_TO_BE_IMPORTED[0])),
        createEmptyStaticAttribute('secondStaticAttribute', new SimpleType(String)),
        createEmptyNonStaticAttribute('firstNonStaticAttribute', new SimpleType(ATTRIBUTE_TYPES_TO_BE_IMPORTED[1])),
        createEmptyNonStaticAttribute('secondNonStaticAttribute', new SimpleType(Integer))
    ]
  }

  private static List<Method> createTestMethods() {
    return [
        createStaticMethod('firstStaticMethod', new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[0])),
        createStaticMethod('secondStaticMethod'),
        createNonStaticMethod('firstNonStaticMethod', new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[1])),
        createNonStaticMethod('secondNonStaticMethod', new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[2]),
            [new Parameter(new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[0]), 'testParameter')]),
    ]
  }
}
