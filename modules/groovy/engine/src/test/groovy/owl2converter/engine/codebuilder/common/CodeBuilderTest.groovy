package owl2converter.engine.codebuilder.common

import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.test.generator.model.TraitTestFactory
import spock.lang.Specification
import spock.lang.Subject

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

import static owl2converter.engine.test.generator.model.AttributeTestFactory.createEmptyNonStaticAttribute
import static owl2converter.engine.test.generator.model.AttributeTestFactory.createEmptyStaticAttribute
import static owl2converter.engine.test.generator.model.MethodTestFactory.createNonStaticMethod
import static owl2converter.engine.test.generator.model.MethodTestFactory.createStaticMethod

class CodeBuilderTest extends Specification {

  @Subject
  private CodeBuilder builder

  def setup() {
    builder = new TestCodeBuilder()
  }

  def 'should append code with new line'() {
    given:
    def code = 'testCode'

    when:
    builder.appendCode(code)

    then:
    builder.code == code + '\n'
  }

  def 'should append empty line'() {
    when:
    builder.appendEmptyLine()

    then:
    builder.code == '\n'
  }

  def 'should append default imports based on generator configuration and specific ones'() {
    given:
    GeneratorConfiguration configuration = new GeneratorConfiguration(
        generatedClasses: new GeneratedClassesConfiguration(basePackage: 'base', modelSubPackage: 'sub',
            exceptionSubPackage: 'exception'),
    )

    when:
    builder.appendImports(configuration)

    then:
    builder.code == """import ${configuration.generatedClasses.exceptionFullPackage}.*
                      |${TestCodeBuilder.SPECIFIC_IMPORT}""".stripMargin() + '\n'
  }

  def 'should append package import'() {
    given:
    def packageName = 'test.package'

    when:
    builder.appendPackageImport(packageName)

    then:
    builder.code == "import ${packageName}.*" + '\n'
  }

  def 'should append class import'() {
    given:
    def className = 'TestClass'

    when:
    builder.appendClassImport(className)

    then:
    builder.code == "import $className" + '\n'
  }

  def 'should append imports for given types, including nested ones'() {
    given:
    def types = [new SimpleType(Lock), new ListType(new ListType(new SimpleType(ConcurrentHashMap)))]

    when:
    builder.appendImports(types, 'another.package')

    then:
    builder.code == """import ${Lock.name}
                      |import ${ConcurrentHashMap.name}""".stripMargin() + '\n'
  }

  def 'should append imports for types of attributes'() {
    given:
    def attributes = [
        createEmptyStaticAttribute('test', new SimpleType(Lock)),
        createEmptyNonStaticAttribute('test', new ListType(new SimpleType(ConcurrentHashMap))),
    ]

    when:
    builder.appendAttributeTypeImports(attributes)

    then:
    builder.code == """import ${Lock.name}
                      |import ${ConcurrentHashMap.name}""".stripMargin() + '\n'
  }

  def 'should append no static attributes and no empty line'() {
    given:
    def attributesToCode = [
        (createEmptyNonStaticAttribute('second')): 'attr2',
        (createEmptyNonStaticAttribute('fourth')): 'attr4'
    ]

    when:
    builder.appendStaticAttributes(attributesToCode)

    then:
    builder.code.isEmpty()
  }

  def 'should append static attributes with empty line'() {
    given:
    def attributesToCode = [
        (createEmptyStaticAttribute('first'))    : 'attr1',
        (createEmptyNonStaticAttribute('second')): 'attr2',
        (createEmptyStaticAttribute('third'))    : 'attr3',
        (createEmptyNonStaticAttribute('fourth')): 'attr4'
    ]

    when:
    builder.appendStaticAttributes(attributesToCode)

    then:
    builder.code == 'attr1\nattr3\n\n'
  }

  def 'should append no non-static attributes and no empty line'() {
    given:
    def attributesToCode = [
        (createEmptyStaticAttribute('first')): 'attr1',
        (createEmptyStaticAttribute('third')): 'attr3',
    ]

    when:
    builder.appendNonStaticAttributes(attributesToCode)

    then:
    builder.code.isEmpty()
  }

  def 'should append non-static attributes with empty line'() {
    given:
    def attributesToCode = [
        (createEmptyStaticAttribute('first'))    : 'attr1',
        (createEmptyNonStaticAttribute('second')): 'attr2',
        (createEmptyStaticAttribute('third'))    : 'attr3',
        (createEmptyNonStaticAttribute('fourth')): 'attr4'
    ]

    when:
    builder.appendNonStaticAttributes(attributesToCode)

    then:
    builder.code == 'attr2\nattr4\n\n'
  }

  def 'should append imports for types of parameters of methods'() {
    given:
    def returnType1 = new SimpleType(Lock)
    def returnType2 = new SimpleType(ReentrantLock)
    def parameter1 = new Parameter<>(new SimpleType(ConcurrentHashMap), 'param1')
    Parameter<ListType> parameter2 = new Parameter<>(new ListType(new SimpleType(ConcurrentLinkedDeque)), 'param2')
    def parameter3 = new Parameter<>(new SimpleType(ConcurrentLinkedQueue), 'param3')
    def methods = [
        createNonStaticMethod('method1', returnType1, [parameter1, parameter2]),
        createStaticMethod('method2', returnType2, [parameter3])
    ]

    when:
    builder.appendMethodParameterImports(methods)

    then:
    builder.code == """import $parameter1.type.fullName
                      |import $parameter2.type.parameterizedType.fullName
                      |import $parameter3.type.fullName""".stripMargin() + '\n'
  }

  def 'should append imports for return types of parameters of methods'() {
    given:
    def returnType1 = new SimpleType(Lock)
    def returnType2 = new ListType(new SimpleType(ReentrantLock))
    def parameter1 = new Parameter<>(new SimpleType(ConcurrentHashMap), 'param1')
    def parameter2 = new Parameter<>(new SimpleType(ConcurrentLinkedQueue), 'param2')
    def methods = [
        createNonStaticMethod('method1', returnType1, [parameter1]),
        createStaticMethod('method2', returnType2, [parameter2])
    ]

    when:
    builder.appendMethodReturnTypeImports(methods)

    then:
    builder.code == """import $returnType1.fullName
                      |import $returnType2.parameterizedType.fullName""".stripMargin() + '\n'
  }

  def 'should append non-static methods'() {
    given:
    def methodsToCodes = [
        (createNonStaticMethod('method1')): 'method1',
        (createStaticMethod('method2'))   : 'method2',
        (createNonStaticMethod('method3')): 'method3',
        (createStaticMethod('method4'))   : 'method4',
    ]

    when:
    builder.appendNonStaticMethodsWithEmptyLines(methodsToCodes)

    then:
    builder.code == "method1\n\nmethod3\n\n"
  }

  def 'should append static methods'() {
    given:
    def methodsToCodes = [
        (createNonStaticMethod('method1')): 'method1',
        (createStaticMethod('method2'))   : 'method2',
        (createNonStaticMethod('method3')): 'method3',
        (createStaticMethod('method4'))   : 'method4',
    ]

    when:
    builder.appendStaticMethodsWithEmptyLines(methodsToCodes)

    then:
    builder.code == "method2\n\nmethod4\n\n"
  }

  def 'should append imports for implemented traits '() {
    given:
    def testPackage = 'org.package2'
    def trait1 = TraitTestFactory.createTrait('Trait1', 'org.package1')
    def trait2 = TraitTestFactory.createTrait('Trait2', testPackage)
    def trait3 = TraitTestFactory.createTrait('Trait3', 'org.package3')
    def trait4 = TraitTestFactory.createTrait('Trait4', testPackage)

    when:
    builder.appendImplementedTraitImports([trait1, trait2, trait3, trait4], testPackage)

    then:
    builder.code == """import $trait1.fullName
                      |import $trait3.fullName""".stripMargin() + '\n'
  }

  def 'should not append import if its owner is in the same package as given type'() {
    given:
    def packageName = 'test.package'
    def types = [new SimpleType(packageName, 'Test'), new SimpleType(packageName, 'Test2')]

    when:
    builder.appendImports(types, packageName)

    then:
    builder.code.isEmpty()
  }

  private class TestCodeBuilder extends CodeBuilder {

    private static final String SPECIFIC_IMPORT = 'import specific.packages'

    @Override
    void appendSpecificImports(GeneratorConfiguration generatorConfiguration) {
      appendCode(SPECIFIC_IMPORT)
    }

    @Override
    String getOwnerPackageName() {
      return 'another.package'
    }
  }
}
