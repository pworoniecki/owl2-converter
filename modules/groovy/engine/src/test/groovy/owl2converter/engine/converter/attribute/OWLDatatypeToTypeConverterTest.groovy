package owl2converter.engine.converter.attribute

import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.converter.attribute.range.OWLDatatypeToTypeConverter
import owl2converter.engine.exception.MissingIRIRemainderException
import owl2converter.groovymetamodel.type.Type
import org.semanticweb.owlapi.model.IRI
import org.semanticweb.owlapi.model.OWLDatatype
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import javax.xml.datatype.XMLGregorianCalendar

class OWLDatatypeToTypeConverterTest extends Specification {

  private static final String GENERATED_CODE_BASE_PACKAGE = 'org.test'
  private static final String OWL_CUSTOM_TYPE_SUBPACKAGE = 'owlSimpleType'
  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(generatedClasses: new GeneratedClassesConfiguration(basePackage: GENERATED_CODE_BASE_PACKAGE)))

  @Subject
  private OWLDatatypeToTypeConverter datatypeToTypeConverter = new OWLDatatypeToTypeConverter(CONFIGURATION)

  private static final String XML_SCHEMA_NAMESPACE = 'http://www.w3.org/2001/XMLSchema'

  @Unroll
  def 'should convert OWL datatype to Groovy native type'() {
    given:
    OWLDatatype datatype = createDatatype(XML_SCHEMA_NAMESPACE, name)

    when:
    Type type = datatypeToTypeConverter.convert(datatype)

    then:
    type.fullName == typeClass.name

    where:
    name                 | typeClass
    'string'             | String
    'boolean'            | Boolean
    'decimal'            | BigDecimal
    'float'              | Float
    'double'             | Double
    'integer'            | BigInteger
    'long'               | Long
    'int'                | Integer
    'short'              | Short
    'byte'               | Byte
    'dateTime'           | XMLGregorianCalendar
    'time'               | XMLGregorianCalendar
    'date'               | XMLGregorianCalendar
    'gYearMonth'         | XMLGregorianCalendar
    'gYear'              | XMLGregorianCalendar
    'gMonthDay'          | XMLGregorianCalendar
    'gDay'               | XMLGregorianCalendar
    'gMonth'             | XMLGregorianCalendar
  }

  @Unroll
  def 'should convert OWL datatype to custom type'() {
    given:
    OWLDatatype datatype = createDatatype(XML_SCHEMA_NAMESPACE, name)
    String expectedTypePackage = GENERATED_CODE_BASE_PACKAGE + '.' + OWL_CUSTOM_TYPE_SUBPACKAGE

    when:
    Type type = datatypeToTypeConverter.convert(datatype)

    then:
    type.fullName == "${expectedTypePackage}.$typeClassName"

    where:
    name                 | typeClassName
    'normalizedString'   | 'NormalizedString'
    'token'              | 'Token'
    'language'           | 'Language'
    'NMTOKEN'            | 'NMTOKEN'
    'Name'               | 'Name'
    'NCName'             | 'NCName'
    'nonPositiveInteger' | 'NonPositiveInteger'
    'negativeInteger'    | 'NegativeInteger'
    'nonNegativeInteger' | 'NonNegativeInteger'
    'unsignedLong'       | 'UnsignedLong'
    'unsignedInt'        | 'UnsignedInt'
    'unsignedShort'      | 'UnsignedShort'
    'unsignedByte'       | 'UnsignedByte'
    'positiveInteger'    | 'PositiveInteger'
  }

  def 'should throw an exception when trying to convert datatype which is not defined within XML Schema namespace'() {
    given:
    OWLDatatype datatype = createDatatype('invalidNamespace', 'int')

    when:
    datatypeToTypeConverter.convert(datatype)

    then:
    thrown IllegalArgumentException
  }

  def 'should throw an exception when name of datatype is empty'() {
    given:
    OWLDatatype datatype = createDatatype(XML_SCHEMA_NAMESPACE, '')

    when:
    datatypeToTypeConverter.convert(datatype)

    then:
    thrown MissingIRIRemainderException
  }

  private OWLDatatype createDatatype(String namespace, String name) {
    OWLDatatype datatype = Mock()
    datatype.getIRI() >> IRI.create("$namespace#$name")
    return datatype
  }
}
