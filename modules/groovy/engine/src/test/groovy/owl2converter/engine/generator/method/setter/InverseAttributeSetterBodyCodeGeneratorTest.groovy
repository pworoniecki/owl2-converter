package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.relation.InverseAttributeRelation
import owl2converter.groovymetamodel.method.DirectSetter
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.SetterTestFactory
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class InverseAttributeSetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private InverseAttributeSetterBodyCodeGenerator generator = new InverseAttributeSetterBodyCodeGenerator()

  def 'should support setter of inverse attribute'() {
    given:
    def inverseAttribute = AttributeTestFactory.createAttribute()
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withRelations([new InverseAttributeRelation(inverseAttribute)]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    generator.supports(setter)
  }

  def 'should not support setter of non inverse attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)
  }

  def 'should generate no validation code of setter'() {
    given:
    def inverseAttribute1 = AttributeTestFactory.createAttribute('inverseAttribute1')
    def inverseAttribute2 = AttributeTestFactory.createAttribute('inverseAttribute2')
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withRelations([inverseAttribute1, inverseAttribute2].collect { new InverseAttributeRelation(it) })
        .buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    !generatedCode.isPresent()
  }

  def 'should generate specific setting value code of list-type setter'() {
    given:
    def inverseAttribute1 = AttributeTestFactory.createAttribute('inverseAttribute1', Types.STRING)
    def inverseAttribute2 = AttributeTestFactory.createAttribute('inverseAttribute2', Types.STRING_LIST)
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withRelations([inverseAttribute1, inverseAttribute2].collect { new InverseAttributeRelation(it) })
        .buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """def oldAttribute = this.attribute ?: []
          |def newAttribute = attribute ?: []
          |def removedAttribute = oldAttribute - newAttribute
          |def addedAttribute = newAttribute - oldAttribute
          |removedAttribute.each {
          |it.${inverseAttribute2.name}.remove(this)
          |}
          |addedAttribute.each {
          |it.${DirectSetter.getName(inverseAttribute1)}(this)
          |it.${DirectSetter.getName(inverseAttribute2)}(it.${inverseAttribute2.name} ? it.${inverseAttribute2.name} + this : [this])
          |}
          |this.attribute = attribute""".stripMargin()
  }

  def 'should generate specific setting value code of list-type setter when none inverse attributes are lists'() {
    given:
    def inverseAttribute1 = AttributeTestFactory.createAttribute('inverseAttribute1', Types.STRING)
    def inverseAttribute2 = AttributeTestFactory.createAttribute('inverseAttribute2', Types.STRING)
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withRelations([inverseAttribute1, inverseAttribute2].collect { new InverseAttributeRelation(it) })
        .buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """def oldAttribute = this.attribute ?: []
          |def newAttribute = attribute ?: []
          |def removedAttribute = oldAttribute - newAttribute
          |def addedAttribute = newAttribute - oldAttribute
          |addedAttribute.each {
          |it.${DirectSetter.getName(inverseAttribute1)}(this)
          |it.${DirectSetter.getName(inverseAttribute2)}(this)
          |}
          |this.attribute = attribute""".stripMargin()
  }

  def 'should generate specific setting value code of single-type setter'() {
    given:
    def inverseAttribute1 = AttributeTestFactory.createAttribute('inverseAttribute1', Types.STRING_LIST)
    def inverseAttribute2 = AttributeTestFactory.createAttribute('inverseAttribute2', Types.STRING)
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withRelations([inverseAttribute1, inverseAttribute2].collect { new InverseAttributeRelation(it) })
        .buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)
    def parameterName = setter.parameter.name

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """${parameterName}.${DirectSetter.getName(inverseAttribute1)}([this])
          |${parameterName}.${DirectSetter.getName(inverseAttribute2)}(this)
          |this.attribute = $parameterName""".stripMargin()
  }
}
