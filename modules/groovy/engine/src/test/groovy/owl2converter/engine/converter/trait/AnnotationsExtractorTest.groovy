package owl2converter.engine.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.groovymetamodel.Annotation
import owl2converter.engine.test.TestOWLOntologyBuilder
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static TraitsExtractor.TRAIT_NAME_SUFFIX

class AnnotationsExtractorTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(
          generatedClasses: new GeneratedClassesConfiguration(basePackage: 'org', modelSubPackage: 'test'))
  )
  private static final String CLASS_1_NAME = 'TestClass1'
  private static final String CLASS_2_NAME = 'TestClass2'
  private static final String CLASS_3_NAME = 'TestClass3'

  @Subject
  private AnnotationsExtractor annotationsExtractor = new AnnotationsExtractor(CONFIGURATION)

  def 'should return no annotations'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).build()
    OWLClass owlClass = ontology.classesInSignature().findFirst().get()

    when:
    List<Annotation> annotations = annotationsExtractor.extract(owlClass, ontology)

    then:
    annotations.isEmpty()
  }

  @Unroll
  def 'should return DisjointWith annotation for disjoint traits defined by disjoint union axiom'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withDisjointUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()
    OWLClass owlClass = findClassByName(ontology, className)

    when:
    List<Annotation> annotations = annotationsExtractor.extract(owlClass, ontology)

    then:
    annotations.size() == 1
    annotations.first().type.simpleName == 'DisjointWith'
    annotations.first().parameters == disjointWithClasses.collect { it + TRAIT_NAME_SUFFIX }

    where:
    className    | disjointWithClasses
    CLASS_1_NAME | [CLASS_2_NAME, CLASS_3_NAME]
    CLASS_2_NAME | [CLASS_1_NAME, CLASS_3_NAME]
    CLASS_3_NAME | [CLASS_1_NAME, CLASS_2_NAME]
  }

  @Unroll
  def 'should return DisjointWith annotation for disjoint traits defined by disjoint classes axiom'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withDisjointClasses(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()
    OWLClass owlClass = findClassByName(ontology, className)

    when:
    List<Annotation> annotations = annotationsExtractor.extract(owlClass, ontology)

    then:
    annotations.size() == 1
    annotations.first().type.simpleName == 'DisjointWith'
    annotations.first().parameters == disjointWithClasses.collect { it + TRAIT_NAME_SUFFIX }

    where:
    className    | disjointWithClasses
    CLASS_1_NAME | [CLASS_2_NAME, CLASS_3_NAME]
    CLASS_2_NAME | [CLASS_1_NAME, CLASS_3_NAME]
    CLASS_3_NAME | [CLASS_1_NAME, CLASS_2_NAME]
  }

  private static OWLClass findClassByName(OWLOntology ontology, String name) {
    return ontology.classesInSignature().find { it.getIRI().remainder.get() == name } as OWLClass
  }
}
