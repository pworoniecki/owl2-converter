package owl2converter.engine.test

import org.semanticweb.owlapi.model.OWLOntology
import owl2metamodel.OntologyParser

import java.nio.file.Files
import java.nio.file.Path

class TestOWLOntologyBuilder {

  static final String XML_SCHEMA_PREFIX = 'xsd'

  private static final String ONTOLOGY_END_CHARACTER = ')'
  private static final String XML_SCHEMA_ADDRESS = 'http://www.w3.org/2001/XMLSchema'

  private OntologyParser ontologyParser = new OntologyParser()
  private StringBuilder owlOntologyBuilder = new StringBuilder()
  private String ontologyAddress

  TestOWLOntologyBuilder(String ontologyAddress = 'http://test.ontology.com') {
    this.ontologyAddress = ontologyAddress
    appendOntologyLine("Prefix(:=<$ontologyAddress#>)")
    appendOntologyLine("Prefix($XML_SCHEMA_PREFIX:=<$XML_SCHEMA_ADDRESS#>)")
    appendOntologyLine("Ontology( <$ontologyAddress>")
  }

  private void appendOntologyLine(String line) {
    owlOntologyBuilder.append(line + '\n')
  }

  TestOWLOntologyBuilder withClassDeclarations(List<String> classNames) {
    classNames.each { appendClassDeclaration(it) }
    return this
  }

  private StringBuilder appendClassDeclaration(String className) {
    return appendOntologyLine("Declaration(Class(<$ontologyAddress#$className>))")
  }

  TestOWLOntologyBuilder withClassDeclaration(String className) {
    appendClassDeclaration(className)
    return this
  }

  TestOWLOntologyBuilder withObjectPropertyDeclarations(List<String> propertyNames,
                                                        String sourceOntologyAddress = ontologyAddress) {
    propertyNames.each { appendObjectPropertyDeclaration(sourceOntologyAddress, it) }
    return this
  }

  private appendObjectPropertyDeclaration(String sourceOntologyAddress, String propertyName) {
    appendOntologyLine("Declaration(ObjectProperty(<$sourceOntologyAddress#$propertyName>))")
  }

  TestOWLOntologyBuilder withObjectPropertyDeclaration(String propertyName,
                                                       String sourceOntologyAddress = ontologyAddress) {
    appendObjectPropertyDeclaration(sourceOntologyAddress, propertyName)
    return this
  }

  TestOWLOntologyBuilder withObjectPropertyDomain(String propertyName, String domain) {
    appendOntologyLine("ObjectPropertyDomain(<$ontologyAddress#$propertyName> <$ontologyAddress#$domain>)")
    return this
  }

  TestOWLOntologyBuilder withObjectPropertyDomainAsUnion(String propertyName, String domainUnionClass1, String domainUnionClass2) {
    String domain = "ObjectUnionOf(<$ontologyAddress#$domainUnionClass1> <$ontologyAddress#$domainUnionClass2>)"
    appendOntologyLine("ObjectPropertyDomain(<$ontologyAddress#$propertyName> $domain)")
    return this
  }

  TestOWLOntologyBuilder withDataPropertyDomainAsUnion(String propertyName, String domainUnionClass1, String domainUnionClass2) {
    String domain = "ObjectUnionOf(<$ontologyAddress#$domainUnionClass1> <$ontologyAddress#$domainUnionClass2>)"
    appendOntologyLine("DataPropertyDomain(<$ontologyAddress#$propertyName> $domain)")
    return this
  }

  TestOWLOntologyBuilder withObjectPropertyRange(String propertyName, String range) {
    appendOntologyLine("ObjectPropertyRange(<$ontologyAddress#$propertyName> <$ontologyAddress#$range>)")
    return this
  }

  TestOWLOntologyBuilder withObjectPropertyRangeAsUnion(String propertyName, String domainUnionClass1, String domainUnionClass2) {
    String domain = "ObjectUnionOf(<$ontologyAddress#$domainUnionClass1> <$ontologyAddress#$domainUnionClass2>)"
    appendOntologyLine("ObjectPropertyRange(<$ontologyAddress#$propertyName> $domain)")
    return this
  }

  TestOWLOntologyBuilder withDataPropertyRangeAsUnion(String propertyName, String dataType1, String dataType2) {
    String domain = "DataUnionOf(<$ontologyAddress#$XML_SCHEMA_PREFIX:$dataType1> <$ontologyAddress#$XML_SCHEMA_PREFIX:$dataType2>)"
    appendOntologyLine("DataPropertyRange(<$ontologyAddress#$propertyName> $domain)")
    return this
  }


  TestOWLOntologyBuilder withDataPropertyDeclaration(String propertyName,
                                                     String sourceOntologyAddress = ontologyAddress) {
    appendDataPropertyDeclaration(sourceOntologyAddress, propertyName)
    return this
  }

  private appendDataPropertyDeclaration(String sourceOntologyAddress, String propertyName) {
    appendOntologyLine("Declaration(DataProperty(<$sourceOntologyAddress#$propertyName>))")
  }

  TestOWLOntologyBuilder withDataPropertyDeclarations(List<String> propertyNames,
                                                      String sourceOntologyAddress = ontologyAddress) {
    propertyNames.each { appendDataPropertyDeclaration(sourceOntologyAddress, it) }
    return this
  }

  TestOWLOntologyBuilder withDataPropertyDomain(String propertyName, String domain) {
    appendOntologyLine("DataPropertyDomain(<$ontologyAddress#$propertyName> <$ontologyAddress#$domain>)")
    return this
  }

  TestOWLOntologyBuilder withDataPropertyRange(String propertyName, String range) {
    appendOntologyLine("DataPropertyRange(<$ontologyAddress#$propertyName> <$ontologyAddress#$XML_SCHEMA_PREFIX:$range>)")
    return this
  }

  TestOWLOntologyBuilder withEquivalentObjectProperties(String property1, String property2) {
    appendOntologyLine("EquivalentObjectProperties(<$ontologyAddress#$property1> <$ontologyAddress#$property2>)")
    return this
  }

  TestOWLOntologyBuilder withEquivalentDataProperties(String property1, String property2) {
    appendOntologyLine("EquivalentDataProperties(<$ontologyAddress#$property1> <$ontologyAddress#$property2>)")
    return this
  }

  TestOWLOntologyBuilder withInverseObjectProperties(String property1, String property2) {
    appendOntologyLine("InverseObjectProperties(<$ontologyAddress#$property1> <$ontologyAddress#$property2>)")
    return this
  }

  TestOWLOntologyBuilder withSubObjectPropertyOf(String subProperty, String superProperty) {
    appendOntologyLine("SubObjectPropertyOf(<$ontologyAddress#$subProperty> <$ontologyAddress#$superProperty>)")
    return this
  }

  TestOWLOntologyBuilder withSubDataPropertyOf(String subProperty, String superProperty) {
    appendOntologyLine("SubDataPropertyOf(<$ontologyAddress#$subProperty> <$ontologyAddress#$superProperty>)")
    return this
  }

  TestOWLOntologyBuilder withSymmetricObjectProperty(String propertyName) {
    appendOntologyLine("SymmetricObjectProperty(<$ontologyAddress#$propertyName>)")
    return this
  }

  TestOWLOntologyBuilder withAsymmetricObjectProperty(String propertyName) {
    appendOntologyLine("AsymmetricObjectProperty(<$ontologyAddress#$propertyName>)")
    return this
  }

  TestOWLOntologyBuilder withTransitiveObjectProperty(String propertyName) {
    appendOntologyLine("TransitiveObjectProperty(<$ontologyAddress#$propertyName>)")
    return this
  }

  TestOWLOntologyBuilder withFunctionalObjectProperty(String propertyName) {
    appendOntologyLine("FunctionalObjectProperty(<$ontologyAddress#$propertyName>)")
    return this
  }

  TestOWLOntologyBuilder withInverseFunctionalObjectProperty(String propertyName) {
    appendOntologyLine("InverseFunctionalObjectProperty(<$ontologyAddress#$propertyName>)")
    return this
  }

  TestOWLOntologyBuilder withReflexiveObjectProperty(String propertyName) {
    appendOntologyLine("ReflexiveObjectProperty(<$ontologyAddress#$propertyName>)")
    return this
  }

  TestOWLOntologyBuilder withIrreflexiveObjectProperty(String propertyName) {
    appendOntologyLine("IrreflexiveObjectProperty(<$ontologyAddress#$propertyName>)")
    return this
  }

  TestOWLOntologyBuilder withFunctionalDataProperty(String propertyName) {
    appendOntologyLine("FunctionalDataProperty(<$ontologyAddress#$propertyName>)")
    return this
  }

  TestOWLOntologyBuilder withSubClassOf(String subClass, String owlClass) {
    appendOntologyLine("SubClassOf(<$ontologyAddress#$subClass> <$ontologyAddress#$owlClass>)")
    return this
  }

  TestOWLOntologyBuilder withEquivalentClasses(String class1, String class2) {
    appendOntologyLine("EquivalentClasses(<$ontologyAddress#$class1> <$ontologyAddress#$class2>)")
    return this
  }

  TestOWLOntologyBuilder withClassesEquivalentOfUnion(String className, String unionClass1, String unionClass2) {
    appendOntologyLine("EquivalentClasses(<$ontologyAddress#$className> ObjectUnionOf(<$ontologyAddress#$unionClass1> " +
        "<$ontologyAddress#$unionClass2>))")
    return this
  }

  TestOWLOntologyBuilder withDisjointUnion(String... classes) {
    String axiomParameters = classes.collect { "<$ontologyAddress#$it>" }.join(' ')
    appendOntologyLine("DisjointUnion($axiomParameters)")
    return this
  }

  TestOWLOntologyBuilder withDisjointClasses(String... classes) {
    String axiomParameters = classes.collect { "<$ontologyAddress#$it>" }.join(' ')
    appendOntologyLine("DisjointClasses($axiomParameters)")
    return this
  }

  TestOWLOntologyBuilder withIndividual(String individualName, String className) {
    appendOntologyLine("Declaration(NamedIndividual(<$ontologyAddress#$individualName>))")
    appendOntologyLine("ClassAssertion(<$ontologyAddress#$className> <$ontologyAddress#$individualName>)")
    return this
  }

  TestOWLOntologyBuilder withObjectPropertyAssertion(String propertyName, String individualName, String secondIndividualName) {
    appendOntologyLine("ObjectPropertyAssertion(<$ontologyAddress#$propertyName> <$ontologyAddress#$individualName> <$ontologyAddress#$secondIndividualName>)")
    return this
  }

  TestOWLOntologyBuilder withDataPropertyAssertion(String propertyName, String individualName, String literal,
                                                   String literalType) {
    appendOntologyLine("DataPropertyAssertion(<$ontologyAddress#$propertyName> <$ontologyAddress#$individualName> \"$literal\"^^$literalType)")
    return this
  }

  OWLOntology build() {
    Path ontologyFilePath = Files.createTempFile('ontology', '.owl')
    Files.write(ontologyFilePath, buildOntologyAsBytes())
    OWLOntology ontology = ontologyParser.parse(ontologyFilePath.toFile())
    Files.delete(ontologyFilePath)
    return ontology
  }

  byte[] buildOntologyAsBytes() {
    String ontologyContent = owlOntologyBuilder.toString() + ONTOLOGY_END_CHARACTER
    return ontologyContent.bytes
  }
}
