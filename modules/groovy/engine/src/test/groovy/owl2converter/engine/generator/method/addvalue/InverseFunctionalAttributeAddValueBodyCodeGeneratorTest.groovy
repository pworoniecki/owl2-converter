package owl2converter.engine.generator.method.addvalue

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.AddValueMethod
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.converter.trait.TraitsExtractor.getALL_INSTANCES_ATTRIBUTE_NAME
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.INVERSE_FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE
import static owl2converter.engine.test.generator.model.AddValueMethodTestFactory.createAddValueMethod

class InverseFunctionalAttributeAddValueBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private InverseFunctionalAttributeAddValueBodyCodeGenerator generator = new InverseFunctionalAttributeAddValueBodyCodeGenerator()

  def 'should support add value method of inverse functional attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withCharacteristics([INVERSE_FUNCTIONAL]).buildAttribute()

    when:
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    then:
    generator.supports(addValueMethod)
  }

  def 'should not support add value method of non inverse functional attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    then:
    !generator.supports(addValueMethod)
  }

  def 'should generate validation code of add value method'() {
    given:
    def attributeName = 'attribute'
    def attribute = new Attribute.AttributeBuilder(attributeName, Types.STRING_LIST)
        .withCharacteristics([INVERSE_FUNCTIONAL]).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)
    String parameterName = addValueMethod.parameter.name

    when:
    def generatedCode = generator.generateValidationCode(addValueMethod)

    then:
    generatedCode.isPresent()
    generatedCode.get() == getExpectedStandardCode(attributeName, parameterName)
  }

  def 'should generate additional validation code of add value method when it is also transitive'() {
    given:
    def attributeName = 'attribute'
    def attribute = new Attribute.AttributeBuilder(attributeName, Types.STRING_LIST)
        .withCharacteristics([INVERSE_FUNCTIONAL, TRANSITIVE]).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)
    String parameterName = addValueMethod.parameter.name

    when:
    def generatedCode = generator.generateValidationCode(addValueMethod)

    then:
    generatedCode.isPresent()
    generatedCode.get() == getExpectedStandardCode(attributeName, parameterName) + '\n' +
        """if (!${parameterName}.${attributeName}.isEmpty()) {
          |throw new InverseFunctionalAttributeException("Cannot add value \$${parameterName} because it would break inverse functional attribute rule - some values would belong to more than one class instance due to transitivity.")
          |}""".stripMargin()
  }

  def 'should generate no specific add value code of add value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([INVERSE_FUNCTIONAL]).buildAttribute()
    AddValueMethod addValueMethod = createAddValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(addValueMethod)

    then:
    !generatedCode.isPresent()
  }

  private static String getExpectedStandardCode(String attributeName, String parameterName) {
    return "def ${attributeName}OfOtherInstances = (${ALL_INSTANCES_ATTRIBUTE_NAME} - this)*.${attributeName}.flatten()\n" +
        "if ($attributeName in ${attributeName}OfOtherInstances) {\n" +
        "throw new InverseFunctionalAttributeException(\"Cannot add value '\$$parameterName' - " +
        "it would break inverse functional attribute rule\")\n" +
        '}'
  }
}
