package owl2converter.engine.generator

import org.springframework.context.ApplicationContext
import owl2converter.engine.configuration.ApplicationConfigurationReader
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.groovymetamodel.Annotation
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static owl2converter.engine.test.Assertions.assertEqualIgnoreOrder
import static owl2converter.engine.test.generator.model.AttributeTestFactory.createEmptyNonStaticAttribute
import static owl2converter.engine.test.generator.model.AttributeTestFactory.createEmptyStaticAttribute
import static owl2converter.engine.test.generator.model.MethodTestFactory.createNonStaticMethod
import static owl2converter.engine.test.generator.model.MethodTestFactory.createStaticMethod
import static owl2converter.engine.test.generator.model.TraitTestFactory.createTrait

class TraitCodeGeneratorTest extends Specification {

  private static final Type ANNOTATION_TYPE = new SimpleType('test', 'DisjointWith')
  private static final String APPLICATION_CONFIGURATION_FILE_NAME = 'configuration.yml'
  private static final ApplicationConfiguration APPLICATION_CONFIGURATION =
      ApplicationConfigurationReader.readConfiguration(APPLICATION_CONFIGURATION_FILE_NAME)
  private static final GeneratorConfiguration GENERATOR_CONFIGURATION = APPLICATION_CONFIGURATION.generator

  @Shared
  private Annotation annotationWithoutParameter = new Annotation(ANNOTATION_TYPE)

  @Shared
  private Annotation annotationWithParameters = new Annotation(ANNOTATION_TYPE, ['One', 'Two'])

  @Shared
  private TraitBodyCodeGenerator traitBodyCodeGenerator = createMockTraitBodyCodeGenerator()

  @Shared
  private ApplicationContext applicationContext = Mock()

  @Shared
  private TraitSignatureCodeGenerator traitSignatureCodeGenerator = Mock()

  @Shared
  private Trait testTrait = createTestTrait()

  @Subject
  @Shared
  private TraitCodeGenerator codeGenerator

  @Shared
  private List<String> generatedCodeLines

  def setupSpec() {
    traitBodyCodeGenerator.getAttributeCodes() >>
        testTrait.attributes.collectEntries { attribute -> [(attribute): getMockCode(attribute)] }
    traitBodyCodeGenerator.getMethodCodes() >>
        testTrait.methods.collectEntries { method -> [(method): getMockCode(method)] }
    applicationContext.getBean(TraitBodyCodeGenerator) >> traitBodyCodeGenerator
    traitSignatureCodeGenerator.generateTraitSignature(_ as Trait) >> { traitModel -> getMockTraitSignature(traitModel) }
    codeGenerator = new TraitCodeGenerator(applicationContext, traitSignatureCodeGenerator, APPLICATION_CONFIGURATION)
    generatedCodeLines = codeGenerator.generateCode(testTrait).readLines()
  }

  def 'should append package to generated source code at the beginning and empty line afterwards'() {
    expect:
    generatedCodeLines[0] == "package ${testTrait.packageName}"
    generatedCodeLines[1].isEmpty()
  }

  @Unroll
  def 'should append imports to generated source code after package and empty line afterwards'() {
    expect:
    testTrait.inheritedTraits.size() == 2 // for simplicity in further tests
    assertEqualIgnoreOrder(generatedCodeLines[2..5], expectedImports.collect { "import $it".toString() })
    generatedCodeLines[6].isEmpty()

    where:
    expectedImports = [
        "${GENERATOR_CONFIGURATION.generatedClasses.exceptionFullPackage}.*",
        testTrait.inheritedTraits[0].fullName,
        testTrait.inheritedTraits[1].fullName,
        ANNOTATION_TYPE.fullName
    ]
  }

  def 'should append trait annotations to generated source code after imports'() {
    expect:
    assertEqualIgnoreOrder(generatedCodeLines[7..8], expectedAnnotations*.toString())

    where:
    expectedAnnotations = [
        "@$annotationWithoutParameter.type.simpleName",
        "@${annotationWithParameters.type.simpleName}([${annotationWithParameters.parameters.join(', ')}])"
    ]
  }

  def 'should append trait signature to generated source code after annotations and empty line afterwards'() {
    expect:
    generatedCodeLines[9] == getMockTraitSignature(testTrait)
    generatedCodeLines[10].isEmpty()
  }

  def 'should append static attributes to generated source code after trait signature and empty line afterwards'() {
    expect:
    attributes.size() == 2 // for simplicity in further tests
    generatedCodeLines[11] == getMockCode(attributes[0])
    generatedCodeLines[12] == getMockCode(attributes[1])
    generatedCodeLines[13].isEmpty()

    where:
    attributes = testTrait.attributes.findAll { it.isStatic() }
  }

  def 'should append non-static attributes to generated source code and empty line afterwards'() {
    expect:
    attributes.size() == 2 // for simplicity in further tests
    generatedCodeLines[14] == getMockCode(attributes[0])
    generatedCodeLines[15] == getMockCode(attributes[1])
    generatedCodeLines[16].isEmpty()

    where:
    attributes = testTrait.attributes.findAll { !it.isStatic() }
  }

  def 'should append static methods to generated source code and empty lines after them'() {
    expect:
    methods.size() == 2 // for simplicity in further tests
    generatedCodeLines[17] == getMockCode(methods[0])
    generatedCodeLines[18].isEmpty()
    generatedCodeLines[19] == getMockCode(methods[1])
    generatedCodeLines[20].isEmpty()

    where:
    methods = testTrait.methods.findAll { it.isStatic() }
  }

  def 'should append non-static methods to generated source code and empty lines after them'() {
    expect:
    methods.size() == 2 // for simplicity in further tests
    generatedCodeLines[21] == getMockCode(methods[0])
    generatedCodeLines[22].isEmpty()
    generatedCodeLines[23] == getMockCode(methods[1])
    generatedCodeLines[24].isEmpty()

    where:
    methods = testTrait.methods.findAll { !it.isStatic() }
  }

  def 'should append end of trait signature to generated source code'() {
    expect:
    generatedCodeLines[25] == '}'
  }

  private TraitBodyCodeGenerator createMockTraitBodyCodeGenerator() {
    def attributeCodeGenerator = null
    def methodCodeGenerator = null
    return Mock(TraitBodyCodeGenerator, constructorArgs: [attributeCodeGenerator, methodCodeGenerator])
  }

  private Trait createTestTrait() {
    return new Trait.Builder('org.test', 'TestTrait')
        .withAnnotations([annotationWithoutParameter, annotationWithParameters])
        .withInheritedTraits(createTestTraits())
        .withAttributes(createTestAttributes())
        .withMethods(createTestMethods())
        .buildTrait()
  }

  private static List<Trait> createTestTraits() {
    return [createTrait('Trait1', 'com.traits'), createTrait('Trait2', 'com.traits')]
  }

  private static List<Attribute> createTestAttributes() {
    return [
        createEmptyStaticAttribute('firstStaticAttribute'),
        createEmptyStaticAttribute('secondStaticAttribute'),
        createEmptyNonStaticAttribute('firstNonStaticAttribute'),
        createEmptyNonStaticAttribute('secondNonStaticAttribute')
    ]
  }

  private static List<Method> createTestMethods() {
    return [
        createStaticMethod('firstStaticMethod'),
        createStaticMethod('secondStaticMethod'),
        createNonStaticMethod('firstNonStaticMethod'),
        createNonStaticMethod('secondNonStaticMethod'),
    ]
  }

  private static String getMockCode(Attribute attribute) {
    return "full code of ${attribute.name} attribute - static: ${attribute.isStatic()}"
  }

  private static String getMockCode(Method method) {
    return "full code of ${method.name} method - static: ${method.isStatic()}"
  }

  private static String getMockTraitSignature(Trait traitModel) {
    return "trait $traitModel.name {"
  }
}
