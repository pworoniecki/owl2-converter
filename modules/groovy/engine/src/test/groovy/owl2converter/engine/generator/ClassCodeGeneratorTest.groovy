package owl2converter.engine.generator

import owl2converter.engine.configuration.ApplicationConfigurationReader
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.groovymetamodel.Annotation
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import java.util.concurrent.BlockingDeque
import java.util.concurrent.BlockingQueue
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.Semaphore
import java.util.concurrent.locks.Lock

import static owl2converter.groovymetamodel.AccessModifier.PRIVATE
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static owl2converter.engine.test.Assertions.assertEqualIgnoreOrder
import static owl2converter.engine.test.generator.model.AttributeTestFactory.createEmptyNonStaticAttribute
import static owl2converter.engine.test.generator.model.AttributeTestFactory.createEmptyStaticAttribute
import static owl2converter.engine.test.generator.model.ClassTestFactory.createEmptyClass
import static owl2converter.engine.test.generator.model.MethodTestFactory.createNonStaticMethod
import static owl2converter.engine.test.generator.model.MethodTestFactory.createStaticMethod
import static owl2converter.engine.test.generator.model.TraitTestFactory.createTrait

class ClassCodeGeneratorTest extends Specification {

  private static final Type ANNOTATION_TYPE = new SimpleType('test', 'DisjointWith')
  private static final String APPLICATION_CONFIGURATION_FILE_NAME = 'configuration.yml'
  private static final ApplicationConfiguration APPLICATION_CONFIGURATION =
      ApplicationConfigurationReader.readConfiguration(APPLICATION_CONFIGURATION_FILE_NAME)
  private static final GeneratorConfiguration GENERATOR_CONFIGURATION = APPLICATION_CONFIGURATION.generator
  private static final CONSTRUCTOR_TYPES_TO_BE_IMPORTED = [ConcurrentLinkedQueue]
  private static final ATTRIBUTE_TYPES_TO_BE_IMPORTED = [BlockingQueue, ConcurrentHashMap]
  private static final METHOD_TYPES_TO_BE_IMPORTED = [Semaphore, BlockingDeque, Lock]

  @Shared
  private Annotation annotationWithoutParameter = new Annotation(ANNOTATION_TYPE)

  @Shared
  private Annotation annotationWithParameters = new Annotation(ANNOTATION_TYPE, ['One', 'Two'])

  @Shared
  private ClassBodyCodeGenerator classBodyCodeGenerator = createMockClassBodyCodeGenerator()

  @Shared
  private ClassSignatureCodeGenerator classSignatureCodeGenerator = Mock()

  @Shared
  private Class testClass = createTestClass()

  @Subject
  @Shared
  private ClassCodeGenerator codeGenerator

  @Shared
  private List<String> generatedCodeLines

  def setupSpec() {
    classBodyCodeGenerator.getAttributeCodes() >>
        testClass.attributes.collectEntries { attribute -> [(attribute): getMockCode(attribute)] }
    classBodyCodeGenerator.getConstructorCodes() >>
        testClass.constructors.collectEntries { constructor -> [(constructor): getMockCode(constructor)] }
    classBodyCodeGenerator.getMethodCodes() >>
        testClass.methods.collectEntries { method -> [(method): getMockCode(method)] }
    classSignatureCodeGenerator.generateClassSignature(_ as Class) >> { classModel -> getMockClassSignature(classModel) }
    codeGenerator = new ClassCodeGenerator(classBodyCodeGenerator, classSignatureCodeGenerator, APPLICATION_CONFIGURATION)
    generatedCodeLines = codeGenerator.generateCode(testClass).readLines()
  }

  def 'should append package to generated source code at the beginning and empty line afterwards'() {
    expect:
    generatedCodeLines[0] == "package ${testClass.packageName}"
    generatedCodeLines[1].isEmpty()
  }

  @Unroll
  def 'should append imports to generated source code after package and empty line afterwards'() {
    expect:
    testClass.implementedTraits.size() == 2 // for simplicity in further tests
    assertEqualIgnoreOrder(generatedCodeLines[2..12], expectedImports.collect { "import $it".toString() })
    generatedCodeLines[13].isEmpty()

    where:
    expectedImports = [
        "${GENERATOR_CONFIGURATION.generatedClasses.exceptionFullPackage}.*",
        testClass.inheritedClass.get().fullName,
        testClass.implementedTraits[0].fullName,
        testClass.implementedTraits[1].fullName,
    ] + CONSTRUCTOR_TYPES_TO_BE_IMPORTED*.name + METHOD_TYPES_TO_BE_IMPORTED*.name +
        ATTRIBUTE_TYPES_TO_BE_IMPORTED*.name + ANNOTATION_TYPE.fullName
  }

  def 'should append class annotations to generated source code after imports'() {
    expect:
    assertEqualIgnoreOrder(generatedCodeLines[14..15], expectedAnnotations*.toString())

    where:
    expectedAnnotations = [
        "@$annotationWithoutParameter.type.simpleName",
        "@${annotationWithParameters.type.simpleName}([${annotationWithParameters.parameters.join(', ')}])"
    ]
  }

  def 'should append class signature to generated source code after annotations and empty line afterwards'() {
    expect:
    generatedCodeLines[16] == getMockClassSignature(testClass)
    generatedCodeLines[17].isEmpty()
  }

  def 'should append static attributes to generated source code after class signature and empty line afterwards'() {
    expect:
    attributes.size() == 2 // for simplicity in further tests
    generatedCodeLines[18] == getMockCode(attributes[0])
    generatedCodeLines[19] == getMockCode(attributes[1])
    generatedCodeLines[20].isEmpty()

    where:
    attributes = testClass.attributes.findAll { it.isStatic() }
  }

  def 'should append static constructors to generated source code after static attributes and empty lines after them'() {
    expect:
    constructors.size() == 2 // for simplicity in further tests
    generatedCodeLines[21] == getMockCode(constructors[0])
    generatedCodeLines[22].isEmpty()
    generatedCodeLines[23] == getMockCode(constructors[1])
    generatedCodeLines[24].isEmpty()

    where:
    constructors = testClass.constructors.findAll { it.isStatic() }
  }

  def 'should append non-static attributes to generated source code and empty line afterwards'() {
    expect:
    attributes.size() == 2 // for simplicity in further tests
    generatedCodeLines[25] == getMockCode(attributes[0])
    generatedCodeLines[26] == getMockCode(attributes[1])
    generatedCodeLines[27].isEmpty()

    where:
    attributes = testClass.attributes.findAll { !it.isStatic() }
  }

  def 'should append non-static constructors to generated source code and empty lines after them'() {
    expect:
    constructors.size() == 2 // for simplicity in further tests
    generatedCodeLines[28] == getMockCode(constructors[0])
    generatedCodeLines[29].isEmpty()
    generatedCodeLines[30] == getMockCode(constructors[1])
    generatedCodeLines[31].isEmpty()

    where:
    constructors = testClass.constructors.findAll { !it.isStatic() }
  }

  def 'should append static methods to generated source code and empty lines after them'() {
    expect:
    methods.size() == 2 // for simplicity in further tests
    generatedCodeLines[32] == getMockCode(methods[0])
    generatedCodeLines[33].isEmpty()
    generatedCodeLines[34] == getMockCode(methods[1])
    generatedCodeLines[35].isEmpty()

    where:
    methods = testClass.methods.findAll { it.isStatic() }
  }

  def 'should append non-static methods to generated source code and empty lines after them'() {
    expect:
    methods.size() == 2 // for simplicity in further tests
    generatedCodeLines[36] == getMockCode(methods[0])
    generatedCodeLines[37].isEmpty()
    generatedCodeLines[38] == getMockCode(methods[1])
    generatedCodeLines[39].isEmpty()

    where:
    methods = testClass.methods.findAll { !it.isStatic() }
  }

  def 'should append end of class signature to generated source code'() {
    expect:
    generatedCodeLines[40] == '}'
  }

  private ClassBodyCodeGenerator createMockClassBodyCodeGenerator() {
    def constructorCodeGenerator = null
    def attributeCodeGenerator = null
    def methodCodeGenerator = null
    return Mock(ClassBodyCodeGenerator,
        constructorArgs: [constructorCodeGenerator, attributeCodeGenerator, methodCodeGenerator])
  }

  private Class createTestClass() {
    return new Class.Builder('org.test', 'TestClass')
        .withAnnotations([annotationWithoutParameter, annotationWithParameters])
        .withInheritedClass(createEmptyClass('org.anotherpackage'))
        .withImplementedTraits(createTestTraits())
        .withAttributes(createTestAttributes())
        .withConstructors(createTestConstructors())
        .withMethods(createTestMethods())
        .buildClass()
  }

  private static List<Trait> createTestTraits() {
    return [createTrait('Trait1', 'com.traits'), createTrait('Trait2', 'com.traits')]
  }

  private static List<Attribute> createTestAttributes() {
    return [
        createEmptyStaticAttribute('firstStaticAttribute', new SimpleType(ATTRIBUTE_TYPES_TO_BE_IMPORTED[0])),
        createEmptyStaticAttribute('secondStaticAttribute', new SimpleType(String)),
        createEmptyNonStaticAttribute('firstNonStaticAttribute', new SimpleType(ATTRIBUTE_TYPES_TO_BE_IMPORTED[1])),
        createEmptyNonStaticAttribute('secondNonStaticAttribute', new SimpleType(Integer))
    ]
  }

  private static List<Constructor> createTestConstructors() {
    return [
        new Constructor(isStatic: true, accessModifier: PUBLIC),
        new Constructor(isStatic: true, accessModifier: PRIVATE,
            parameters: [new Parameter(new SimpleType(CONSTRUCTOR_TYPES_TO_BE_IMPORTED[0]), 'testParameter')]),
        new Constructor(isStatic: false, accessModifier: PUBLIC),
        new Constructor(isStatic: false, accessModifier: PRIVATE)
    ]
  }

  private static List<Method> createTestMethods() {
    return [
        createStaticMethod('firstStaticMethod', new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[0])),
        createStaticMethod('secondStaticMethod'),
        createNonStaticMethod('firstNonStaticMethod', new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[0])),
        createNonStaticMethod('secondNonStaticMethod', new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[1]),
            [new Parameter(new SimpleType(METHOD_TYPES_TO_BE_IMPORTED[2]), 'testParameter')]),
    ]
  }

  private static String getMockCode(Attribute attribute) {
    return "full code of ${attribute.name} attribute - static: ${attribute.isStatic()}"
  }

  private static String getMockCode(Constructor constructor) {
    return "full code of constructor - static: ${constructor.isStatic()}, parameters: ${constructor.parameters.size()}"
  }

  private static String getMockCode(Method method) {
    return "full code of ${method.name} method - static: ${method.isStatic()}"
  }

  private static String getMockClassSignature(Class classModel) {
    return "class $classModel.name {"
  }
}
