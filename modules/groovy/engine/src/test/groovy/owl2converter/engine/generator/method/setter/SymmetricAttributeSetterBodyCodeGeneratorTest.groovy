package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.SetterTestFactory
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.SYMMETRIC

class SymmetricAttributeSetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private SymmetricAttributeSetterBodyCodeGenerator generator = new SymmetricAttributeSetterBodyCodeGenerator()

  def 'should support setter of symmetric attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withCharacteristics([SYMMETRIC]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    generator.supports(setter)
  }

  def 'should not support setter of non symmetric attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)
  }

  def 'should generate no validation code of setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([SYMMETRIC]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    !generatedCode.isPresent()
  }

  def 'should generate specific setting value code of list-type setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([SYMMETRIC]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """def oldAttribute = this.attribute ?: []
          |def newAttribute = attribute ?: []
          |def removedAttribute = oldAttribute - newAttribute
          |def addedAttribute = newAttribute - oldAttribute
          |removedAttribute.each { removeAttribute(it) }
          |addedAttribute.each { addAttribute(it) }""".stripMargin()
  }

  def 'should generate specific setting value code of single-type setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([SYMMETRIC]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """if (this.attribute == attribute) {
          |return
          |}
          |this.attribute = attribute
          |attribute.setAttribute(this)""".stripMargin()
  }
}
