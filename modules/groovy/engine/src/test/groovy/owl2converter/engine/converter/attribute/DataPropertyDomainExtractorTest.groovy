package owl2converter.engine.converter.attribute

import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.converter.attribute.domain.DataPropertyDomainExtractor
import owl2converter.engine.converter.attribute.domain.PropertyDirectDomainExtractor
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.Type
import owl2converter.engine.test.TestOWLOntologyBuilder
import org.slf4j.Logger
import spock.lang.Specification
import spock.lang.Subject

class DataPropertyDomainExtractorTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(generatedClasses: new GeneratedClassesConfiguration(
          basePackage: 'test', modelSubPackage: 'subpackage')))
  private static final String PACKAGE_NAME = CONFIGURATION.generator.generatedClasses.modelFullPackage
  static final String PROPERTY_1_NAME = 'testProperty1'
  static final String PROPERTY_1_DOMAIN = 'TestClass1'
  static final String PROPERTY_1_RANGE = 'TestClass2'
  static final String PROPERTY_2_NAME = 'testProperty2'
  static final String PROPERTY_3_NAME = 'testProperty3'
  static final String PROPERTY_4_NAME = 'testProperty4'
  static final String PROPERTY_5_NAME = 'testProperty5'

  private Logger logger = Mock()
  private PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

  @Subject
  private DataPropertyDomainExtractor extractor =
      new DataPropertyDomainExtractor(CONFIGURATION, predefinedTypes, new PropertyDirectDomainExtractor(logger))

  def 'should return domain defined directly for data property'() {
    given:
    OWLOntology ontology = createOntologyWithDataProperties()
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_1_NAME } as OWLDataProperty

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain instanceof ListType
    def parameterizedType = (domain as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == PROPERTY_1_DOMAIN
  }

  def 'should return domain defined for equivalent data property when directly one is not available'() {
    given:
    OWLOntology ontology = createOntologyWithDataProperties()
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_2_NAME } as OWLDataProperty

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain instanceof ListType
    def parameterizedType = (domain as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == PROPERTY_1_DOMAIN
  }

  def 'should return domain defined for super data property when directly one is not available'() {
    given:
    OWLOntology ontology = createOntologyWithDataProperties()
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_3_NAME } as OWLDataProperty

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain instanceof ListType
    def parameterizedType = (domain as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == PROPERTY_1_DOMAIN
  }

  def 'should return default Thing type when cannot determine domain of data property'() {
    given:
    OWLOntology ontology = createOntologyWithDataProperties()
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_5_NAME } as OWLDataProperty

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain == predefinedTypes.thingTraitListType
  }

  def 'should return default Thing type when more than 1 domain is defined for data property and log warning about it'() {
    given:
    def propertyName = 'testProperty'
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclarations(['Class1', 'Class2'])
        .withDataPropertyDeclaration(propertyName)
        .withDataPropertyDomain(propertyName, 'Class1').withDataPropertyDomain(propertyName, 'Class2')
        .build()
    OWLDataProperty property = ontology.dataPropertiesInSignature().findFirst().get()

    when:
    Type domain = extractor.extract(property, ontology)

    then:
    domain == predefinedTypes.thingTraitListType
    1 * logger.warn(*_)
  }

  private static OWLOntology createOntologyWithDataProperties() {
    return new TestOWLOntologyBuilder()
        .withClassDeclarations([PROPERTY_1_DOMAIN, PROPERTY_1_RANGE])
        .withDataPropertyDeclarations([PROPERTY_1_NAME, PROPERTY_2_NAME, PROPERTY_3_NAME, PROPERTY_4_NAME, PROPERTY_5_NAME])
        .withDataPropertyDomain(PROPERTY_1_NAME, PROPERTY_1_DOMAIN)
        .withDataPropertyRange(PROPERTY_1_NAME, PROPERTY_1_RANGE)
        .withEquivalentDataProperties(PROPERTY_2_NAME, PROPERTY_1_NAME)
        .withSubDataPropertyOf(PROPERTY_3_NAME, PROPERTY_1_NAME)
        .build()
  }
}
