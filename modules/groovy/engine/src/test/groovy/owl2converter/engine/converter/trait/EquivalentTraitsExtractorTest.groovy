package owl2converter.engine.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.exception.NotSupportedOwlElementException
import owl2converter.groovymetamodel.EquivalentTrait
import owl2converter.groovymetamodel.Trait
import owl2converter.engine.test.TestOWLOntologyBuilder
import owl2converter.engine.test.generator.model.TraitTestFactory
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.converter.trait.EquivalentTraitsExtractor.EQUIVALENT_TRAIT_NAME_SUFFIX
import static owl2converter.engine.converter.trait.TraitsExtractor.TRAIT_NAME_SUFFIX

class EquivalentTraitsExtractorTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(
          generatedClasses: new GeneratedClassesConfiguration(basePackage: 'org', modelSubPackage: 'test'))
  )
  private static final String CLASS_1_NAME = 'TestClass1'
  private static final String CLASS_2_NAME = 'TestClass2'
  private static final String CLASS_3_NAME = 'TestClass3'
  private static final Trait TRAIT_1 = TraitTestFactory.createTrait('Trait1')
  private static final Trait TRAIT_2 = TraitTestFactory.createTrait('Trait2')
  private static final Trait TRAIT_3 = TraitTestFactory.createTrait('Trait3')

  @Subject
  private EquivalentTraitsExtractor extractor = new EquivalentTraitsExtractor(CONFIGURATION)

  def 'should return no equivalent classes'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclarations([CLASS_1_NAME, CLASS_2_NAME]).build()
    OWLClass owlClass1 = findClassByName(CLASS_1_NAME, ontology)
    OWLClass owlClass2 = findClassByName(CLASS_2_NAME, ontology)
    Map<OWLClass, Trait> baseTraits = [(owlClass1): TRAIT_1, (owlClass2): TRAIT_2]

    when:
    Map<OWLClass, Optional<EquivalentTrait>> equivalentTraits = extractor.extract(baseTraits, ontology)

    then:
    equivalentTraits.values().each { assert !it.isPresent() }
  }

  def 'should return equivalent classes'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclarations([CLASS_1_NAME, CLASS_2_NAME])
        .withEquivalentClasses(CLASS_1_NAME, CLASS_2_NAME).build()
    OWLClass owlClass1 = findClassByName(CLASS_1_NAME, ontology)
    OWLClass owlClass2 = findClassByName(CLASS_2_NAME, ontology)
    Map<OWLClass, Trait> baseTraits = [(owlClass1): TRAIT_1, (owlClass2): TRAIT_2]

    when:
    Map<OWLClass, Optional<EquivalentTrait>> equivalentTraits = extractor.extract(baseTraits, ontology)

    then:
    equivalentTraits[owlClass1].get().is(equivalentTraits[owlClass2].get())
    EquivalentTrait equivalentTrait = equivalentTraits[owlClass1].get()
    equivalentTrait.name == CLASS_1_NAME + CLASS_2_NAME + EQUIVALENT_TRAIT_NAME_SUFFIX + TRAIT_NAME_SUFFIX
    equivalentTrait.inheritedTraits.sort() == [TRAIT_1, TRAIT_2].sort()
  }

  def 'should throw an exception when more than one class is equivalent to given class'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclarations([CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME])
        .withEquivalentClasses(CLASS_1_NAME, CLASS_2_NAME).withEquivalentClasses(CLASS_1_NAME, CLASS_3_NAME).build()
    OWLClass owlClass1 = findClassByName(CLASS_1_NAME, ontology)
    OWLClass owlClass2 = findClassByName(CLASS_2_NAME, ontology)
    OWLClass owlClass3 = findClassByName(CLASS_3_NAME, ontology)
    Map<OWLClass, Trait> baseTraits = [(owlClass1): TRAIT_1, (owlClass2): TRAIT_2, (owlClass3): TRAIT_3]

    when:
    extractor.extract(baseTraits, ontology)

    then:
    thrown NotSupportedOwlElementException
  }

  private static OWLClass findClassByName(String name, OWLOntology ontology) {
    return ontology.classesInSignature().find { it.getIRI().remainder.get() == name } as OWLClass
  }
}
