package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.engine.test.generator.model.SetterTestFactory
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE

class FunctionalReflexiveAttributeSetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private FunctionalReflexiveAttributeSetterBodyCodeGenerator generator = new FunctionalReflexiveAttributeSetterBodyCodeGenerator()

  def 'should support setter of reflexive and functional attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withCharacteristics([REFLEXIVE, FUNCTIONAL]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    generator.supports(setter)
  }

  def 'should not support setter of attribute that is not both reflexive and functional'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withCharacteristics(characteristics).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)

    where:
    characteristics << [[REFLEXIVE], [FUNCTIONAL], []]
  }

  def 'should generate no validation code of setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([REFLEXIVE, FUNCTIONAL]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    !generatedCode.isPresent()
  }

  def 'should throw an exception when attribute handled by setter has list type'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([REFLEXIVE, FUNCTIONAL]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    generator.generateSpecificCode(setter)

    then:
    thrown IllegalStateException
  }

  def 'should generate specific setting value code of setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([REFLEXIVE, FUNCTIONAL]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)
    def expectedExceptionMessage = "Cannot set \$attribute as it is not 'this' instance - it would break reflexive attribute rule"

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() == "throw new ReflexiveAttributeException(\"$expectedExceptionMessage\")"
  }
}
