package owl2converter.engine.test.generator.model

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.Accessor
import owl2converter.groovymetamodel.type.SimpleType
import groovy.transform.InheritConstructors

import static owl2converter.engine.test.generator.model.AttributeTestFactory.createAttribute

class AccessorTestFactory {

  static Accessor createAccessor(String name = 'testAccessor', Attribute attribute = createAttribute()) {
    return new TestAccessor.Builder(name, attribute)
        .withAccessModifier(AccessModifier.PROTECTED)
        .withParameters([new Parameter(new SimpleType(String), 'testParameter')])
        .withReturnType(new SimpleType(Integer))
        .asStatic()
        .buildMethod()
  }

  @InheritConstructors
  static class TestAccessor extends Accessor {

    @InheritConstructors
    static class Builder extends Accessor.Builder<TestAccessor, Builder> {
      @Override
      Builder getThis() {
        return this
      }

      @Override
      TestAccessor buildMethod() {
        return new TestAccessor(this)
      }
    }
  }
}
