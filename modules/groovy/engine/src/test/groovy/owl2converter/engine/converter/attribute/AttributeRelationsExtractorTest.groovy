package owl2converter.engine.converter.attribute

import owl2converter.engine.exception.NotSupportedOwlElementException
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.relation.AttributeRelation
import owl2converter.groovymetamodel.attribute.relation.EquivalentToAttributeRelation
import owl2converter.groovymetamodel.attribute.relation.InverseAttributeRelation
import owl2converter.groovymetamodel.attribute.relation.SuperAttributeOfRelation
import owl2converter.engine.test.TestOWLOntologyBuilder
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.engine.test.generator.model.AttributeTestFactory.createAttribute

class AttributeRelationsExtractorTest extends Specification {

  private static final String PROPERTY_1_NAME = 'property1'
  private static final String PROPERTY_2_NAME = 'property2'
  private static final String PROPERTY_3_NAME = 'property3'
  private static final List<Attribute> ATTRIBUTES =
      [PROPERTY_1_NAME, PROPERTY_2_NAME, PROPERTY_3_NAME].collect { createAttribute(it) }

  @Subject
  private AttributeRelationsExtractor extractor = new AttributeRelationsExtractor()

  def 'should extract relations between object properties'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder()
        .withObjectPropertyDeclarations([PROPERTY_1_NAME, PROPERTY_2_NAME])
        .withInverseObjectProperties(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withInverseObjectProperties(PROPERTY_1_NAME, PROPERTY_3_NAME)
        .withEquivalentObjectProperties(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withSubObjectPropertyOf(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withSubObjectPropertyOf(PROPERTY_3_NAME, PROPERTY_1_NAME)
        .build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_1_NAME } as OWLObjectProperty
    Map<OWLObjectProperty, Attribute> propertyToAttributeMap = [
        (getObjectProperty(ontology, PROPERTY_1_NAME)): getAttribute(PROPERTY_1_NAME),
        (getObjectProperty(ontology, PROPERTY_2_NAME)): getAttribute(PROPERTY_2_NAME),
        (getObjectProperty(ontology, PROPERTY_3_NAME)): getAttribute(PROPERTY_3_NAME),
    ]

    when:
    List<AttributeRelation> relations = extractor.extract(property, ontology, propertyToAttributeMap)

    then:
    relations.size() == 4
    relations.findAll { it instanceof InverseAttributeRelation }*.relatedAttribute == ATTRIBUTES[1..2]
    relations.findAll { it instanceof EquivalentToAttributeRelation }*.relatedAttribute == [ATTRIBUTES[1]]
    relations.findAll { it instanceof SuperAttributeOfRelation }*.relatedAttribute == [ATTRIBUTES[2]]
  }

  def 'should extract relations between data properties'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder()
        .withDataPropertyDeclarations([PROPERTY_1_NAME, PROPERTY_2_NAME])
        .withEquivalentDataProperties(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withSubDataPropertyOf(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withSubDataPropertyOf(PROPERTY_3_NAME, PROPERTY_1_NAME)
        .build()
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_1_NAME } as OWLDataProperty
    Map<OWLDataProperty, Attribute> propertyToAttributeMap = [
        (getDataProperty(ontology, PROPERTY_1_NAME)): getAttribute(PROPERTY_1_NAME),
        (getDataProperty(ontology, PROPERTY_2_NAME)): getAttribute(PROPERTY_2_NAME),
        (getDataProperty(ontology, PROPERTY_3_NAME)): getAttribute(PROPERTY_3_NAME),
    ]

    when:
    List<AttributeRelation> relations = extractor.extract(property, ontology, propertyToAttributeMap)

    then:
    relations.size() == 2
    relations.find { it instanceof EquivalentToAttributeRelation }.relatedAttribute == ATTRIBUTES[1]
    relations.find { it instanceof SuperAttributeOfRelation }.relatedAttribute == ATTRIBUTES[2]
  }

  def 'should throw an exception when a property has more than one equivalent property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder()
        .withDataPropertyDeclarations([PROPERTY_1_NAME, PROPERTY_2_NAME])
        .withEquivalentDataProperties(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withEquivalentDataProperties(PROPERTY_1_NAME, PROPERTY_3_NAME)
        .build()
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_1_NAME } as OWLDataProperty
    Map<OWLDataProperty, Attribute> propertyToAttributeMap = [
        (getDataProperty(ontology, PROPERTY_1_NAME)): getAttribute(PROPERTY_1_NAME),
        (getDataProperty(ontology, PROPERTY_2_NAME)): getAttribute(PROPERTY_2_NAME),
        (getDataProperty(ontology, PROPERTY_3_NAME)): getAttribute(PROPERTY_3_NAME),
    ]

    when:
    extractor.extract(property, ontology, propertyToAttributeMap)

    then:
    thrown NotSupportedOwlElementException
  }

  def 'should throw an exception when there is no attribute in passed attributes list for related property'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder()
        .withDataPropertyDeclarations([PROPERTY_1_NAME, PROPERTY_2_NAME])
        .withEquivalentDataProperties(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .build()
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_1_NAME } as OWLDataProperty
    Map<OWLDataProperty, Attribute> propertyToAttributeMap = [:]

    when:
    extractor.extract(property, ontology, propertyToAttributeMap)

    then:
    thrown IllegalArgumentException
  }

  def 'should not include equivalent attributes relation when there is no proper declaration in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder()
        .withObjectPropertyDeclarations([PROPERTY_1_NAME, PROPERTY_2_NAME])
        .withInverseObjectProperties(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withInverseObjectProperties(PROPERTY_1_NAME, PROPERTY_3_NAME)
        .withSubObjectPropertyOf(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withSubObjectPropertyOf(PROPERTY_3_NAME, PROPERTY_1_NAME)
        .build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_1_NAME } as OWLObjectProperty
    Map<OWLObjectProperty, Attribute> propertyToAttributeMap = [
        (getObjectProperty(ontology, PROPERTY_1_NAME)): getAttribute(PROPERTY_1_NAME),
        (getObjectProperty(ontology, PROPERTY_2_NAME)): getAttribute(PROPERTY_2_NAME),
        (getObjectProperty(ontology, PROPERTY_3_NAME)): getAttribute(PROPERTY_3_NAME),
    ]

    when:
    List<AttributeRelation> relations = extractor.extract(property, ontology, propertyToAttributeMap)

    then:
    relations.every { !(it instanceof EquivalentToAttributeRelation) }
  }

  def 'should not include inverse attributes relation when there is no proper declaration in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder()
        .withObjectPropertyDeclarations([PROPERTY_1_NAME, PROPERTY_2_NAME])
        .withEquivalentObjectProperties(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withSubObjectPropertyOf(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withSubObjectPropertyOf(PROPERTY_3_NAME, PROPERTY_1_NAME)
        .build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_1_NAME } as OWLObjectProperty
    Map<OWLObjectProperty, Attribute> propertyToAttributeMap = [
        (getObjectProperty(ontology, PROPERTY_1_NAME)): getAttribute(PROPERTY_1_NAME),
        (getObjectProperty(ontology, PROPERTY_2_NAME)): getAttribute(PROPERTY_2_NAME),
        (getObjectProperty(ontology, PROPERTY_3_NAME)): getAttribute(PROPERTY_3_NAME),
    ]

    when:
    List<AttributeRelation> relations = extractor.extract(property, ontology, propertyToAttributeMap)

    then:
    relations.every { !(it instanceof InverseAttributeRelation) }
  }

  def 'should not include super attributes relation when there is no proper declaration in ontology'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder()
        .withObjectPropertyDeclarations([PROPERTY_1_NAME, PROPERTY_2_NAME])
        .withEquivalentObjectProperties(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .withInverseObjectProperties(PROPERTY_1_NAME, PROPERTY_2_NAME)
        .build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_1_NAME } as OWLObjectProperty
    Map<OWLObjectProperty, Attribute> propertyToAttributeMap = [
        (getObjectProperty(ontology, PROPERTY_1_NAME)): getAttribute(PROPERTY_1_NAME),
        (getObjectProperty(ontology, PROPERTY_2_NAME)): getAttribute(PROPERTY_2_NAME),
    ]

    when:
    List<AttributeRelation> relations = extractor.extract(property, ontology, propertyToAttributeMap)

    then:
    relations.every { !(it instanceof SuperAttributeOfRelation) }
  }

  def 'should return no relations'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withObjectPropertyDeclaration(PROPERTY_1_NAME).build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == PROPERTY_1_NAME } as OWLObjectProperty
    Map<OWLObjectProperty, Attribute> propertyToAttributeMap = [
        (getObjectProperty(ontology, PROPERTY_1_NAME)): getAttribute(PROPERTY_1_NAME),
    ]

    when:
    List<AttributeRelation> relations = extractor.extract(property, ontology, propertyToAttributeMap)

    then:
    relations.isEmpty()
  }

  private static Attribute getAttribute(String name) {
    return ATTRIBUTES.find { it.name == name }
  }

  private static OWLObjectProperty getObjectProperty(OWLOntology ontology, String name) {
    return ontology.objectPropertiesInSignature().find { it.getIRI().remainder.get() == name } as OWLObjectProperty
  }

  private static OWLDataProperty getDataProperty(OWLOntology ontology, String name) {
    return ontology.dataPropertiesInSignature().find { it.getIRI().remainder.get() == name } as OWLDataProperty
  }
}
