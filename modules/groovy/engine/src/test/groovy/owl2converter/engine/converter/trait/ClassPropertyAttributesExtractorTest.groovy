package owl2converter.engine.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLProperty
import owl2converter.engine.converter.attribute.domain.PropertyDomainExtractor
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.engine.test.TestOWLOntologyBuilder
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.ClassTestFactory
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

class ClassPropertyAttributesExtractorTest extends Specification {

  private static final String CLASS_NAME = 'TestClass'
  private static final String PROPERTY_1_NAME = 'Property1'
  private static final String PROPERTY_2_NAME = 'Property2'
  private static final String PROPERTY_3_NAME = 'Property3'
  private static final OWLOntology ONTOLOGY = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_NAME)
      .withObjectPropertyDeclarations([PROPERTY_1_NAME, PROPERTY_2_NAME, PROPERTY_3_NAME]).build()
  private static final OWLClass OWL_CLASS = ONTOLOGY.classesInSignature().findFirst().get()
  private static final Class CLASS = ClassTestFactory.createClass(CLASS_NAME)
  private static final OWLProperty PROPERTY_1 = findPropertyByName(PROPERTY_1_NAME)
  private static final OWLProperty PROPERTY_2 = findPropertyByName(PROPERTY_2_NAME)
  private static final OWLProperty PROPERTY_3 = findPropertyByName(PROPERTY_3_NAME)
  private static final Attribute ATTRIBUTE_1 = AttributeTestFactory.createAttribute(PROPERTY_1_NAME)
  private static final Attribute ATTRIBUTE_2 = AttributeTestFactory.createAttribute(PROPERTY_2_NAME)
  private static final Attribute ATTRIBUTE_3 = AttributeTestFactory.createAttribute(PROPERTY_3_NAME)
  private static final Map<OWLProperty, Attribute> ATTRIBUTES = [(PROPERTY_1): ATTRIBUTE_1, (PROPERTY_2): ATTRIBUTE_2,
                                                                 (PROPERTY_3): ATTRIBUTE_3]

  private PropertyDomainExtractor propertyDomainExtractor = Mock()

  @Subject
  private ClassPropertyAttributesExtractor extractor = new ClassPropertyAttributesExtractor(propertyDomainExtractor)

  @Unroll
  def 'should extract attributes belonging to given class when they have simple types'() {
    when:
    List<Attribute> attributes = extractor.extractClassAttributes(ONTOLOGY, OWL_CLASS, ATTRIBUTES)

    then:
    1 * propertyDomainExtractor.extractDomain(PROPERTY_1, ONTOLOGY) >> classType
    1 * propertyDomainExtractor.extractDomain(PROPERTY_2, ONTOLOGY) >> new SimpleType(ClassTestFactory.createClass('Another'))
    1 * propertyDomainExtractor.extractDomain(PROPERTY_3, ONTOLOGY) >> classType
    attributes == [ATTRIBUTE_1, ATTRIBUTE_3]

    where:
    classType << [new SimpleType(CLASS),
                  new ListType(new SimpleType(CLASS)),
                  new ListType(new ListType(new SimpleType(CLASS)))]
  }

  private static OWLProperty findPropertyByName(String name) {
    return ONTOLOGY.objectPropertiesInSignature().find { it.getIRI().remainder.get() == name } as OWLProperty
  }
}
