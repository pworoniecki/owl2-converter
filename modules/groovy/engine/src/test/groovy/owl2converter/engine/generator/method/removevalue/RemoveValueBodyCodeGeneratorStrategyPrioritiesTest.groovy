package owl2converter.engine.generator.method.removevalue

import owl2converter.engine.OWL2ToGroovyEngineConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import spock.lang.Specification
import spock.lang.Subject

@ContextConfiguration(loader = AnnotationConfigContextLoader, classes = OWL2ToGroovyEngineConfiguration)
class RemoveValueBodyCodeGeneratorStrategyPrioritiesTest extends Specification {

  @Autowired
  @Subject
  private List<RemoveValueBodyCodeGeneratorStrategy> generators

  def 'should assert equal priorities of all remove value body code generators'() {
    expect:
    generators*.priority.unique().size() == 1
  }
}
