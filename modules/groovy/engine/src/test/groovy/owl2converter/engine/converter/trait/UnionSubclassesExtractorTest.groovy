package owl2converter.engine.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.test.TestOWLOntologyBuilder
import spock.lang.Specification
import spock.lang.Subject

class UnionSubclassesExtractorTest extends Specification {

  private static final String CLASS_1_NAME = 'TestClass1'
  private static final String CLASS_2_NAME = 'TestClass2'
  private static final String CLASS_3_NAME = 'TestClass3'
  private static final String CLASS_4_NAME = 'TestClass4'
  private static final String CLASS_5_NAME = 'TestClass5'

  @Subject
  private UnionSubclassesExtractor extractor = new UnionSubclassesExtractor()

  def 'should return classes that are part of union which is equivalent to given class'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withClassesEquivalentOfUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()
    OWLClass unionClass = findClassByName(ontology, CLASS_1_NAME)

    when:
    List<OWLClass> owlClasses = extractor.extract(ontology, unionClass)

    then:
    owlClasses.sort() == [findClassByName(ontology, CLASS_2_NAME), findClassByName(ontology, CLASS_3_NAME)].sort()
  }

  def 'should return classes that are part of disjoint union'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withDisjointUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME).build()
    OWLClass unionClass = findClassByName(ontology, CLASS_1_NAME)

    when:
    List<OWLClass> owlClasses = extractor.extract(ontology, unionClass)

    then:
    owlClasses.sort() == [findClassByName(ontology, CLASS_2_NAME), findClassByName(ontology, CLASS_3_NAME)].sort()
  }

  def 'should prioritize equivalent to union of classes axiom over disjoint union axiom'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1_NAME).withClassDeclaration(CLASS_2_NAME)
        .withClassDeclaration(CLASS_3_NAME).withClassesEquivalentOfUnion(CLASS_1_NAME, CLASS_2_NAME, CLASS_3_NAME)
        .withClassesEquivalentOfUnion(CLASS_1_NAME, CLASS_4_NAME, CLASS_5_NAME).build()
    OWLClass unionClass = findClassByName(ontology, CLASS_1_NAME)

    when:
    List<OWLClass> owlClasses = extractor.extract(ontology, unionClass)

    then:
    owlClasses.sort() == [findClassByName(ontology, CLASS_2_NAME), findClassByName(ontology, CLASS_3_NAME)].sort()
  }

  private static OWLClass findClassByName(OWLOntology ontology, String name) {
    return ontology.classesInSignature().find { it.getIRI().remainder.get() == name } as OWLClass
  }
}
