package owl2converter.engine.generator.method

import owl2converter.engine.generator.exception.MethodBodyCodeGeneratorNotFoundException
import owl2converter.groovymetamodel.Parameter
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.VoidType
import owl2converter.engine.test.generator.model.MethodTestFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static owl2converter.groovymetamodel.AccessModifier.PRIVATE
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC

class MethodCodeGeneratorTest extends Specification {

  private static final SimpleType INTEGER_TYPE = new SimpleType(Integer)
  private static final Parameter PARAMETER_1 = new Parameter(INTEGER_TYPE, 'param1')
  private static final Parameter PARAMETER_2 = new Parameter(new ListType(INTEGER_TYPE), 'param2')
  private static final String METHOD_1_NAME = 'method1'
  private static final String METHOD_2_NAME = 'method2'

  @Shared
  private List<MethodBodyCodeGenerator> bodyCodeGenerators = [
      new TestMethodBodyCodeGenerator(METHOD_1_NAME),
      new TestMethodBodyCodeGenerator(METHOD_2_NAME),
      new TestMethodBodyCodeGenerator('test')
  ]

  @Subject
  @Shared
  private MethodCodeGenerator generator = new MethodCodeGenerator(bodyCodeGenerators)

  @Unroll
  def 'should generate signature of method'() {
    expect:
    generator.generateCode(MethodTestFactory.createMethod(name, modifier, isStatic, returnType, parameters))
        .readLines().first() == signature.toString()

    where:
    name          | modifier | parameters                 | isStatic | returnType             | signature
    METHOD_1_NAME | PRIVATE  | []                         | false    | VoidType.INSTANCE      | "private void method1() {"
    METHOD_2_NAME | PRIVATE  | [PARAMETER_1]              | false    | VoidType.INSTANCE      | "private void method2(Integer param1) {"
    METHOD_1_NAME | PRIVATE  | [PARAMETER_1]              | true     | VoidType.INSTANCE      | "private static void method1(Integer param1) {"
    METHOD_1_NAME | PUBLIC   | [PARAMETER_1, PARAMETER_2] | false    | VoidType.INSTANCE      | "void method1(Integer param1, List<Integer> param2) {"
    METHOD_1_NAME | PUBLIC   | []                         | false    | new SimpleType(String) | "String method1() {"
  }

  def 'should generate body of method using body code generator that supports given method'() {
    when:
    def generatedCodeLines = generator.generateCode(method).readLines()
    def bodyCodeLines = generatedCodeLines[1..generatedCodeLines.size() - 2]

    then:
    bodyCodeLines == getMockCode(method).readLines()

    where:
    method << [
        MethodTestFactory.createMethod(METHOD_1_NAME),
        MethodTestFactory.createMethod(METHOD_2_NAME)
    ]
  }

  def 'should generate end of method definition'() {
    expect:
    generator.generateCode(MethodTestFactory.createMethod(METHOD_1_NAME)).readLines().last() == '}'
  }

  def 'should throw an exception when no method body code generator supports given method'() {
    given:
    def method = MethodTestFactory.createMethod('nonSupportedMethod')

    when:
    generator.generateCode(method)

    then:
    thrown MethodBodyCodeGeneratorNotFoundException
  }

  private static class TestMethodBodyCodeGenerator implements MethodBodyCodeGenerator<Method> {

    private String supportedMethodName

    TestMethodBodyCodeGenerator(String supportedMethodName) {
      this.supportedMethodName = supportedMethodName
    }

    @Override
    boolean supports(Method method) {
      return method.name == supportedMethodName
    }

    @Override
    String generateCode(Method method) {
      return getMockCode(method)
    }

    @Override
    Class<Method> getSupportedClass() {
      return Method
    }
  }

  private static String getMockCode(Method method) {
    return """Mock first line of method body. Method's name is: $method.name
             |Mock second line of $method.name method body.""".stripMargin()
  }
}
