package owl2converter.engine.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.exception.NotSupportedOwlElementException
import owl2converter.engine.test.TestOWLOntologyBuilder
import spock.lang.Specification
import spock.lang.Subject

class UnionClassCheckerTest extends Specification {

  private static final String CLASS_1 = 'TestClass1'
  private static final String CLASS_2 = 'TestClass2'
  private static final String CLASS_3 = 'TestClass3'
  private static final String CLASS_4 = 'TestClass4'
  private static final String CLASS_5 = 'TestClass5'

  @Subject
  private UnionClassChecker checker = new UnionClassChecker()

  def 'should indicate that given class is no union class when it is not a part of any union classes axiom'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_1).build()
    OWLClass owlClass = findClassByName(CLASS_1, ontology)

    when:
    boolean isUnionClass = checker.isUnionClass(owlClass, ontology)

    then:
    !isUnionClass
  }

  def 'should indicate that given class is union class when it is equivalent to union of classes'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclarations([CLASS_1, CLASS_2, CLASS_3])
        .withClassesEquivalentOfUnion(CLASS_1, CLASS_2, CLASS_3).build()
    OWLClass owlClass = findClassByName(CLASS_1, ontology)

    when:
    boolean isUnionClass = checker.isUnionClass(owlClass, ontology)

    then:
    isUnionClass
  }

  def 'should indicate that given class is union class when it is part of union of classes defined by disjoint union axiom'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclarations([CLASS_1, CLASS_2, CLASS_3])
        .withDisjointUnion(CLASS_1, CLASS_2, CLASS_3).build()
    OWLClass owlClass = findClassByName(CLASS_1, ontology)

    when:
    boolean isUnionClass = checker.isUnionClass(owlClass, ontology)

    then:
    isUnionClass
  }

  def 'should throw an exception when given class is equivalent of more than one union class'() {
    given:
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclarations([CLASS_1, CLASS_2, CLASS_3, CLASS_4, CLASS_5])
        .withClassesEquivalentOfUnion(CLASS_1, CLASS_2, CLASS_3).withClassesEquivalentOfUnion(CLASS_1, CLASS_4, CLASS_5)
        .build()
    OWLClass owlClass = findClassByName(CLASS_1, ontology)

    when:
    checker.isUnionClass(owlClass, ontology)

    then:
    thrown NotSupportedOwlElementException
  }

  private static OWLClass findClassByName(String name, OWLOntology ontology) {
    return ontology.classesInSignature().find { it.getIRI().remainder.get() == name } as OWLClass
  }
}
