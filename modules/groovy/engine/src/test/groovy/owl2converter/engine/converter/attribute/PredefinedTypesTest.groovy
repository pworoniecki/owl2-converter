package owl2converter.engine.converter.attribute

import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.converter.trait.TraitsExtractor
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import spock.lang.Specification

class PredefinedTypesTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(generator: new GeneratorConfiguration(
      generatedClasses: new GeneratedClassesConfiguration(basePackage: 'test', modelSubPackage: 'subpackage')
  ))
  private static final String PACKAGE_NAME = CONFIGURATION.generator.generatedClasses.modelFullPackage
  static final String THING_CLASS_NAME = 'Thing'

  def 'should return Thing class instance'() {
    when:
    PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

    then:
    predefinedTypes.thingClass.name == THING_CLASS_NAME
    predefinedTypes.thingClass.packageName == PACKAGE_NAME
  }

  def 'should return Thing trait instance'() {
    when:
    PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

    then:
    predefinedTypes.thingTrait.name == THING_CLASS_NAME + TraitsExtractor.TRAIT_NAME_SUFFIX
    predefinedTypes.thingTrait.packageName == PACKAGE_NAME
  }

  def 'should return single Thing trait type'() {
    when:
    PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

    then:
    predefinedTypes.thingTraitType.fullName == new SimpleType(predefinedTypes.thingTrait).fullName
  }

  def 'should return list of Thing trait type'() {
    when:
    PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

    then:
    predefinedTypes.thingTraitListType.fullName == new ListType(new SimpleType(predefinedTypes.thingTrait)).fullName
  }
}
