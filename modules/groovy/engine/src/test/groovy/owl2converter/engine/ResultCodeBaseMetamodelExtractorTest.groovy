package owl2converter.engine

import commoncodemetamodel.Element
import commoncodemetamodel.File
import commoncodemetamodel.Folder
import commoncodemetamodel.TextFile
import commoncodemetamodel.tools.CommonCodeMetamodelReader
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import spock.lang.Specification
import spock.lang.Subject

class ResultCodeBaseMetamodelExtractorTest extends Specification {

  private static final int EXPECTED_RESULT_CODE_BASE_FILES = 38
  private static final int EXPECTED_RESULT_CODE_BASE_FOLDERS = 11
  private static final String[] BASE_PACKAGE_HIERARCHY = ['com', 'app', 'test']
  private static final String BASE_PACKAGE = BASE_PACKAGE_HIERARCHY.join('.')

  private ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(
          generatedClasses: new GeneratedClassesConfiguration(basePackage: BASE_PACKAGE, modelSubPackage: 'model')
      )
  )

  private CommonCodeMetamodelReader codeMetamodelReader = new CommonCodeMetamodelReader()

  @Subject
  private ResultCodeBaseMetamodelExtractor extractor = new ResultCodeBaseMetamodelExtractor(applicationConfiguration, codeMetamodelReader)

  def 'should return files and subdirectories of generated code base'() {
    when:
    Set<Element> elements = extractor.extractResultCodeMetamodel()

    then:
    countFilesRecurisvely(elements) == EXPECTED_RESULT_CODE_BASE_FILES
    countFoldersRecursively(elements) == EXPECTED_RESULT_CODE_BASE_FOLDERS
  }

  def 'should replace package placeholder in files with actual package of generated classes from configuration'() {
    when:
    Set<Element> elements = extractor.extractResultCodeMetamodel()

    then:
    TextFile buildGradleFile = elements.find { it instanceof TextFile && it.name == 'build' && it.extension == 'gradle' } as TextFile
    buildGradleFile.content.contains("'Main-Class': '${BASE_PACKAGE}.ApplicationInitializer'")
    findAllFilesRecurisvely(elements).findAll { it.extension == 'groovy' }.each {
      assert it instanceof TextFile
      assert it.content.startsWith("package $BASE_PACKAGE")
    }
  }

  def 'should replace folder with name of package placeholder with folders hierarchy composed of actual package name of generated classes'() {
    when:
    Set<Element> elements = extractor.extractResultCodeMetamodel()

    then:
    Folder srcFolder = elements.find { it instanceof Folder && it.name == 'src' } as Folder
    Folder groovySrcFolder = srcFolder.getSubfolder('main').get().getSubfolder('groovy').get()
    !groovySrcFolder.getSubfolder('[PACKAGE_PLACEHOLDER]').isPresent()
    BASE_PACKAGE_HIERARCHY.size() == 3 // to make sure that further assertions are correct
    groovySrcFolder.getSubfolder(BASE_PACKAGE_HIERARCHY[0]).isPresent()
    groovySrcFolder.getSubfolder(BASE_PACKAGE_HIERARCHY[0]).get().getSubfolder(BASE_PACKAGE_HIERARCHY[1]).isPresent()
    groovySrcFolder.getSubfolder(BASE_PACKAGE_HIERARCHY[0]).get().getSubfolder(BASE_PACKAGE_HIERARCHY[1]).get().getSubfolder(BASE_PACKAGE_HIERARCHY[2]).isPresent()
  }

  private static int countFilesRecurisvely(Set<Element> elements) {
    return findAllFilesRecurisvely(elements).size()
  }

  private static List<File> findAllFilesRecurisvely(Set<Element> elements) {
    def folders = elements.findAll { it instanceof Folder } as Set<Folder>
    return (elements.findAll { it instanceof File } as List<File>) +
        (folders.collect { findAllFilesRecurisvely((it as Folder).elements) }.flatten() as List<File>)
  }

  private static int countFoldersRecursively(Set<Element> elements) {
    def folders = elements.findAll { it instanceof Folder } as Set<Folder>
    return folders.size() + (folders.isEmpty() ? 0 : folders.sum { countFoldersRecursively((it as Folder).elements) } as int)
  }
}
