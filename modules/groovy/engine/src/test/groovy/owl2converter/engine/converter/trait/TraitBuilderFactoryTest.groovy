package owl2converter.engine.converter.trait

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.TraitBuilder
import owl2converter.groovymetamodel.UnionTrait
import owl2converter.engine.test.TestOWLOntologyBuilder
import spock.lang.Specification
import spock.lang.Subject

import static TraitsExtractor.TRAIT_NAME_SUFFIX

class TraitBuilderFactoryTest extends Specification {

  private static final String CLASS_NAME = 'TestClass'
  private static final OWLOntology ONTOLOGY = new TestOWLOntologyBuilder().withClassDeclaration(CLASS_NAME).build()
  private static final OWLClass CLASS = ONTOLOGY.classesInSignature().findFirst().get()
  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(
          generatedClasses: new GeneratedClassesConfiguration(basePackage: 'org', modelSubPackage: 'test'))
  )

  private UnionClassChecker unionClassChecker = Mock()

  @Subject
  private TraitBuilderFactory factory = new TraitBuilderFactory(CONFIGURATION, unionClassChecker)

  def 'should return default trait builder'() {
    when:
    TraitBuilder builder = factory.createTraitBuilder(CLASS, ONTOLOGY)

    then:
    1 * unionClassChecker.isUnionClass(CLASS, ONTOLOGY) >> false
    builder instanceof Trait.Builder
    Trait traitModel = builder.buildTrait()
    traitModel.packageName == CONFIGURATION.generator.generatedClasses.modelFullPackage
    traitModel.name == CLASS_NAME + TRAIT_NAME_SUFFIX
  }

  def 'should return union trait builder'() {
    when:
    TraitBuilder builder = factory.createTraitBuilder(CLASS, ONTOLOGY)

    then:
    1 * unionClassChecker.isUnionClass(CLASS, ONTOLOGY) >> true
    builder instanceof UnionTrait.Builder
    Trait traitModel = builder.buildTrait()
    traitModel.packageName == CONFIGURATION.generator.generatedClasses.modelFullPackage
    traitModel.name == CLASS_NAME + TRAIT_NAME_SUFFIX
  }
}
