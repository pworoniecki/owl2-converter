package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.SetterTestFactory
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.ASYMMETRIC

class AsymmetricAttributeSetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private AsymmetricAttributeSetterBodyCodeGenerator generator = new AsymmetricAttributeSetterBodyCodeGenerator()

  def 'should support setter of asymmetric attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withCharacteristics([ASYMMETRIC]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    generator.supports(setter)
  }

  def 'should not support setter of non asymmetric attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)
  }

  def 'should generate validation code of setter defined for single type attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([ASYMMETRIC]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """if (attribute.attribute == this) {
          |throw new AsymmetricAttributeException(\"Cannot set value '\$attribute' - it would break asymmetric attribute rule\")
          |}""".stripMargin()
  }

  def 'should generate validation code of setter defined for list type attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([ASYMMETRIC]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    generatedCode.isPresent()
    generatedCode.get() ==
        """def forbiddenValues = attribute.findAll { it.attribute.contains(this) }
          |if (forbiddenValues) {
          |throw new AsymmetricAttributeException(\"Cannot set values '\$forbiddenValues' - it would break asymmetric attribute rule\")
          |}""".stripMargin()
  }

  def 'should generate no specific setting value code of setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING)
        .withCharacteristics([ASYMMETRIC]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    !generatedCode.isPresent()
  }
}
