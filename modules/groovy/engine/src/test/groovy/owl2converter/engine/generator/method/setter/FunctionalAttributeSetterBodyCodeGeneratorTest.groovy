package owl2converter.engine.generator.method.setter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.SetterTestFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.FUNCTIONAL
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.REFLEXIVE

class FunctionalAttributeSetterBodyCodeGeneratorTest extends Specification {

  private static String FUNCTIONAL_RULE_EXCEPTION_MESSAGE_FORMAT =
      'List $%s does not contain exactly one unique value so it cannot be set as value of functional attribute'

  @Subject
  @Shared
  private FunctionalAttributeSetterBodyCodeGenerator generator = new FunctionalAttributeSetterBodyCodeGenerator()

  def 'should support setter of functional but not reflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', new SimpleType(String))
        .withCharacteristics([FUNCTIONAL]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    generator.supports(setter)
  }

  def 'should not support setter of functional and reflexive attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', new SimpleType(String))
        .withCharacteristics([FUNCTIONAL, REFLEXIVE]).buildAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)
  }

  def 'should not support setter of non functional attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def setter = SetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(setter)
  }

  def 'should generate no validation code of setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', new SimpleType(String))
        .withCharacteristics([FUNCTIONAL]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(setter)

    then:
    !generatedCode.isPresent()
  }

  def 'should generate no specific setting value code of setter'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', new SimpleType(String))
        .withCharacteristics([FUNCTIONAL]).buildAttribute()
    def setter = SetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(setter)

    then:
    !generatedCode.isPresent()
  }
}
