package owl2converter.engine.utils

import spock.lang.Specification

class SubPackageNameExtractorTest extends Specification {

  def 'should return name of the deepest subpackage of given class'() {
    expect:
    SubPackageNameExtractor.getSubPackageName(java.lang.String) == 'lang'
  }
}
