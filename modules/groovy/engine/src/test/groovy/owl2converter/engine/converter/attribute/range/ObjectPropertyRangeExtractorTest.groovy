package owl2converter.engine.converter.attribute.range

import org.semanticweb.owlapi.model.IRI
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.converter.trait.TraitsExtractor
import owl2converter.engine.converter.attribute.PredefinedTypes
import owl2converter.engine.converter.attribute.domain.PropertyDirectDomainExtractor
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import owl2converter.engine.test.TestOWLOntologyBuilder
import org.slf4j.Logger
import spock.lang.Specification
import spock.lang.Subject

import javax.annotation.Nullable

class ObjectPropertyRangeExtractorTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(generatedClasses: new GeneratedClassesConfiguration(
          basePackage: 'test', modelSubPackage: 'subpackage')))
  private static final String PACKAGE_NAME = CONFIGURATION.generator.generatedClasses.modelFullPackage
  private static final String OBJECT_PROPERTY_1_NAME = 'objectProperty1'
  private static final String OBJECT_PROPERTY_1_DOMAIN = 'TestClass1'
  private static final String OBJECT_PROPERTY_1_RANGE = 'TestClass2'
  private static final String FUNCTIONAL_OBJECT_PROPERTY_1_NAME = 'functionalObjectProperty1'
  private static final String FUNCTIONAL_OBJECT_PROPERTY_1_DOMAIN = 'FunctionalTestClass1'
  private static final String FUNCTIONAL_OBJECT_PROPERTY_1_RANGE = 'FunctionalTestClass2'
  private static final String OBJECT_PROPERTY_2_NAME = 'objectProperty2'
  private static final String FUNCTIONAL_OBJECT_PROPERTY_2_NAME = 'functionalObjectProperty2'
  private static final String OBJECT_PROPERTY_3_NAME = 'objectProperty3'
  private static final String FUNCTIONAL_OBJECT_PROPERTY_3_NAME = 'functionalObjectProperty3'
  private static final String OBJECT_PROPERTY_4_NAME = 'objectProperty4'
  private static final String FUNCTIONAL_OBJECT_PROPERTY_4_NAME = 'functionalObjectProperty4'
  private static final String OBJECT_PROPERTY_5_NAME = 'objectProperty5'
  private static final String FUNCTIONAL_OBJECT_PROPERTY_5_NAME = 'functionalObjectProperty5'
  private static final OWLOntology ontology = createOntology()

  private Logger logger = Mock()
  private PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)

  @Subject
  private ObjectPropertyRangeExtractor extractor = new ObjectPropertyRangeExtractor(CONFIGURATION,
      predefinedTypes, new PropertyDirectDomainExtractor(logger), new PropertyDirectRangeExtractor(logger))

  def 'should return range defined directly for object property'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == OBJECT_PROPERTY_1_NAME } as OWLObjectProperty

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range instanceof ListType
    def parameterizedType = (range as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == OBJECT_PROPERTY_1_RANGE + TraitsExtractor.TRAIT_NAME_SUFFIX
  }

  def 'should return single-type range defined directly for object property'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == FUNCTIONAL_OBJECT_PROPERTY_1_NAME } as OWLObjectProperty

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range instanceof SimpleType
    range.packageName == PACKAGE_NAME
    range.simpleName == FUNCTIONAL_OBJECT_PROPERTY_1_RANGE + TraitsExtractor.TRAIT_NAME_SUFFIX
  }

  def 'should return range defined for equivalent object property when directly one is not available'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == OBJECT_PROPERTY_2_NAME } as OWLObjectProperty

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range instanceof ListType
    def parameterizedType = (range as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == OBJECT_PROPERTY_1_RANGE + TraitsExtractor.TRAIT_NAME_SUFFIX
  }

  def 'should return single-type range defined for equivalent object property when directly one is not available'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == FUNCTIONAL_OBJECT_PROPERTY_2_NAME } as OWLObjectProperty

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range instanceof SimpleType
    range.packageName == PACKAGE_NAME
    range.simpleName == FUNCTIONAL_OBJECT_PROPERTY_1_RANGE + TraitsExtractor.TRAIT_NAME_SUFFIX
  }

  def 'should return range defined for super object property when directly one is not available'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == OBJECT_PROPERTY_3_NAME } as OWLObjectProperty

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range instanceof ListType
    def parameterizedType = (range as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == OBJECT_PROPERTY_1_RANGE + TraitsExtractor.TRAIT_NAME_SUFFIX
  }

  def 'should return single-type range defined for super object property when directly one is not available'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == FUNCTIONAL_OBJECT_PROPERTY_3_NAME } as OWLObjectProperty

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range instanceof SimpleType
    range.packageName == PACKAGE_NAME
    range.simpleName == FUNCTIONAL_OBJECT_PROPERTY_1_RANGE + TraitsExtractor.TRAIT_NAME_SUFFIX
  }

  def 'should return domain defined for inverse property as own range when directly one is not available'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == OBJECT_PROPERTY_4_NAME } as OWLObjectProperty

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range instanceof ListType
    def parameterizedType = (range as ListType).parameterizedType
    parameterizedType.packageName == PACKAGE_NAME
    parameterizedType.simpleName == OBJECT_PROPERTY_1_DOMAIN + TraitsExtractor.TRAIT_NAME_SUFFIX
  }

  def 'should return domain defined for inverse property as own single-type range when directly one is not available'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == FUNCTIONAL_OBJECT_PROPERTY_4_NAME } as OWLObjectProperty

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range instanceof SimpleType
    range.packageName == PACKAGE_NAME
    range.simpleName == FUNCTIONAL_OBJECT_PROPERTY_1_DOMAIN + TraitsExtractor.TRAIT_NAME_SUFFIX
  }

  def 'should return default Thing list type when cannot determine range of property'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == OBJECT_PROPERTY_5_NAME } as OWLObjectProperty

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range == predefinedTypes.thingTraitListType
  }

  def 'should return default Thing single type when cannot determine range of functional property'() {
    given:
    OWLObjectProperty property = ontology.objectPropertiesInSignature()
        .find { it.getIRI().remainder.get() == FUNCTIONAL_OBJECT_PROPERTY_5_NAME } as OWLObjectProperty

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range == predefinedTypes.thingTraitType
  }

  def 'should return default Thing list type when more than 1 range is defined for property and log warning about it'() {
    given:
    def propertyName = 'testProperty'
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclarations(['Class1', 'Class2'])
        .withObjectPropertyDeclaration(propertyName)
        .withObjectPropertyRange(propertyName, 'Class1').withObjectPropertyRange(propertyName, 'Class2')
        .build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range == predefinedTypes.thingTraitListType
    1 * logger.warn(*_)
  }

  def 'should return default Thing single type when more than 1 range is defined for functional property and log warning about it'() {
    given:
    def propertyName = 'testProperty'
    OWLOntology ontology = new TestOWLOntologyBuilder().withClassDeclarations(['Class1', 'Class2'])
        .withObjectPropertyDeclaration(propertyName)
        .withObjectPropertyRange(propertyName, 'Class1').withObjectPropertyRange(propertyName, 'Class2')
        .withFunctionalObjectProperty(propertyName)
        .build()
    OWLObjectProperty property = ontology.objectPropertiesInSignature().findFirst().get()

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    range == predefinedTypes.thingTraitType
    1 * logger.warn(*_)
  }

  private static OWLOntology createOntology() {
    return new TestOWLOntologyBuilder()
        .withClassDeclarations([OBJECT_PROPERTY_1_DOMAIN, OBJECT_PROPERTY_1_RANGE])
        .withObjectPropertyDeclarations([OBJECT_PROPERTY_1_NAME, OBJECT_PROPERTY_2_NAME, OBJECT_PROPERTY_3_NAME,
                                         OBJECT_PROPERTY_4_NAME, OBJECT_PROPERTY_5_NAME, FUNCTIONAL_OBJECT_PROPERTY_1_NAME,
                                         FUNCTIONAL_OBJECT_PROPERTY_2_NAME, FUNCTIONAL_OBJECT_PROPERTY_3_NAME,
                                         FUNCTIONAL_OBJECT_PROPERTY_4_NAME, FUNCTIONAL_OBJECT_PROPERTY_5_NAME])
        .withObjectPropertyDomain(OBJECT_PROPERTY_1_NAME, OBJECT_PROPERTY_1_DOMAIN)
        .withObjectPropertyRange(OBJECT_PROPERTY_1_NAME, OBJECT_PROPERTY_1_RANGE)
        .withObjectPropertyDomain(FUNCTIONAL_OBJECT_PROPERTY_1_NAME, FUNCTIONAL_OBJECT_PROPERTY_1_DOMAIN)
        .withObjectPropertyRange(FUNCTIONAL_OBJECT_PROPERTY_1_NAME, FUNCTIONAL_OBJECT_PROPERTY_1_RANGE)
        .withEquivalentObjectProperties(OBJECT_PROPERTY_2_NAME, OBJECT_PROPERTY_1_NAME)
        .withEquivalentObjectProperties(FUNCTIONAL_OBJECT_PROPERTY_2_NAME, FUNCTIONAL_OBJECT_PROPERTY_1_NAME)
        .withSubObjectPropertyOf(OBJECT_PROPERTY_3_NAME, OBJECT_PROPERTY_1_NAME)
        .withSubObjectPropertyOf(FUNCTIONAL_OBJECT_PROPERTY_3_NAME, FUNCTIONAL_OBJECT_PROPERTY_1_NAME)
        .withInverseObjectProperties(OBJECT_PROPERTY_4_NAME, OBJECT_PROPERTY_1_NAME)
        .withInverseObjectProperties(FUNCTIONAL_OBJECT_PROPERTY_4_NAME, FUNCTIONAL_OBJECT_PROPERTY_1_NAME)
        .withFunctionalObjectProperty(FUNCTIONAL_OBJECT_PROPERTY_1_NAME)
        .withFunctionalObjectProperty(FUNCTIONAL_OBJECT_PROPERTY_2_NAME)
        .withFunctionalObjectProperty(FUNCTIONAL_OBJECT_PROPERTY_3_NAME)
        .withFunctionalObjectProperty(FUNCTIONAL_OBJECT_PROPERTY_4_NAME)
        .withFunctionalObjectProperty(FUNCTIONAL_OBJECT_PROPERTY_5_NAME)
        .build()
  }

  private static class OntologyIRI extends IRI {
    protected OntologyIRI(String prefix, @Nullable String suffix) {
      super(prefix, suffix)
    }
  }
}
