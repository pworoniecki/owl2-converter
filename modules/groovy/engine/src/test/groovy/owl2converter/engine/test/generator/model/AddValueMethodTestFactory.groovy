package owl2converter.engine.test.generator.model

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.AddValueMethod

import static owl2converter.engine.test.generator.model.AttributeTestFactory.createAttribute

class AddValueMethodTestFactory {

  static AddValueMethod createAddValueMethod(Attribute attribute) {
    return new AddValueMethod.Builder(attribute).buildMethod()
  }

  static AddValueMethod createAddValueMethod(String attributeName = 'sample') {
    return new AddValueMethod.Builder(createAttribute(attributeName, Types.STRING_LIST)).buildMethod()
  }
}
