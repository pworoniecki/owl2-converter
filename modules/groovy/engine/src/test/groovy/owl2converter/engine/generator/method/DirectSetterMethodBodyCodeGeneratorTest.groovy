package owl2converter.engine.generator.method

import owl2converter.groovymetamodel.AccessModifier
import owl2converter.groovymetamodel.method.DirectSetter
import owl2converter.engine.test.generator.model.AttributeTestFactory
import spock.lang.Specification
import spock.lang.Subject

class DirectSetterMethodBodyCodeGeneratorTest extends Specification {

  @Subject
  private DirectSetterMethodBodyCodeGenerator generator = new DirectSetterMethodBodyCodeGenerator()

  def 'should generate code of direct setter'() {
    given:
    def attributeName = 'attribute'
    def setter = new DirectSetter.Builder(AttributeTestFactory.createAttribute(attributeName))
        .withAccessModifier(AccessModifier.PROTECTED).asStatic().buildMethod()

    when:
    def generatedCode = generator.generateCode(setter)

    then:
    generatedCode == "this.$attributeName = $attributeName".toString()
  }

  def 'should support direct setter'() {
    expect:
    generator.supportedClass == DirectSetter
  }
}
