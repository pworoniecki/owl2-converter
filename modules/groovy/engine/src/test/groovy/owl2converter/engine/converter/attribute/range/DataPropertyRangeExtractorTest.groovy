package owl2converter.engine.converter.attribute.range

import org.semanticweb.owlapi.model.AxiomType
import org.semanticweb.owlapi.model.IRI
import org.semanticweb.owlapi.model.OWLDataProperty
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom
import org.semanticweb.owlapi.model.OWLDatatype
import org.semanticweb.owlapi.model.OWLOntology
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.converter.attribute.PredefinedTypes
import owl2converter.engine.test.TestOWLOntologyBuilder
import org.slf4j.Logger
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import spock.lang.Specification
import spock.lang.Subject

import javax.annotation.Nullable

class DataPropertyRangeExtractorTest extends Specification {

  private static final ApplicationConfiguration CONFIGURATION = new ApplicationConfiguration(
      generator: new GeneratorConfiguration(generatedClasses: new GeneratedClassesConfiguration(
          basePackage: 'test', modelSubPackage: 'subpackage')))
  private static final Type DATA_PROPERTY_TYPE = new ListType(new SimpleType(Integer))
  private static final String DATA_PROPERTY_1_NAME = 'dataProperty1'
  private static final String DATA_PROPERTY_1_DOMAIN = 'TestClass1'
  private static final String DATA_PROPERTY_1_RANGE = 'int'
  private static final String FUNCTIONAL_DATA_PROPERTY_1_NAME = 'functionalDataProperty'
  private static final String FUNCTIONAL_DATA_PROPERTY_1_DOMAIN = 'FunctionalTestClass1'
  private static final String FUNCTIONAL_DATA_PROPERTY_1_RANGE = 'int'
  private static final String DATA_PROPERTY_2_NAME = 'dataProperty2'
  private static final String FUNCTIONAL_DATA_PROPERTY_2_NAME = 'functionalDataProperty2'
  private static final String DATA_PROPERTY_3_NAME = 'dataProperty3'
  private static final String FUNCTIONAL_DATA_PROPERTY_3_NAME = 'functionalDataProperty3'
  private static final OWLOntology ontology = createOntology()

  private Logger logger = Mock()
  private PredefinedTypes predefinedTypes = new PredefinedTypes(CONFIGURATION)
  private OWLDatatypeToTypeConverter datatypeToTypeConverter = Mock()

  @Subject
  private DataPropertyRangeExtractor extractor = new DataPropertyRangeExtractor(predefinedTypes,
      new PropertyDirectRangeExtractor(logger), datatypeToTypeConverter)

  def 'should return range defined directly for data property'() {
    given:
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == DATA_PROPERTY_1_NAME } as OWLDataProperty
    OWLDatatype propertyDataType = extractRange(ontology, property)

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    1 * datatypeToTypeConverter.convert(propertyDataType) >> DATA_PROPERTY_TYPE
    range == new ListType(DATA_PROPERTY_TYPE)
  }

  def 'should return single-type range defined directly for data property'() {
    given:
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == FUNCTIONAL_DATA_PROPERTY_1_NAME } as OWLDataProperty
    OWLDatatype propertyDataType = extractRange(ontology, property)

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    1 * datatypeToTypeConverter.convert(propertyDataType) >> DATA_PROPERTY_TYPE
    range == DATA_PROPERTY_TYPE
  }

  def 'should return range defined for equivalent data property when directly one is not available'() {
    given:
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == DATA_PROPERTY_2_NAME } as OWLDataProperty
    OWLDataProperty equivalentProperty = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == DATA_PROPERTY_1_NAME } as OWLDataProperty
    OWLDatatype equivalentPropertyDataType = extractRange(ontology, equivalentProperty)

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    1 * datatypeToTypeConverter.convert(equivalentPropertyDataType) >> DATA_PROPERTY_TYPE
    range == new ListType(DATA_PROPERTY_TYPE)
  }

  def 'should return single-type range defined for equivalent data property when directly one is not available'() {
    given:
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == FUNCTIONAL_DATA_PROPERTY_2_NAME } as OWLDataProperty
    OWLDataProperty equivalentProperty = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == FUNCTIONAL_DATA_PROPERTY_1_NAME } as OWLDataProperty
    OWLDatatype equivalentPropertyDataType = extractRange(ontology, equivalentProperty)

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    1 * datatypeToTypeConverter.convert(equivalentPropertyDataType) >> DATA_PROPERTY_TYPE
    range == DATA_PROPERTY_TYPE
  }

  def 'should return range defined for super data property when directly one is not available'() {
    given:
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == DATA_PROPERTY_3_NAME } as OWLDataProperty
    OWLDataProperty superProperty = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == DATA_PROPERTY_1_NAME } as OWLDataProperty
    OWLDatatype superPropertyDataType = extractRange(ontology, superProperty)

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    1 * datatypeToTypeConverter.convert(superPropertyDataType) >> DATA_PROPERTY_TYPE
    range == new ListType(DATA_PROPERTY_TYPE)
  }

  def 'should return single-type range defined for super data property when directly one is not available'() {
    given:
    OWLDataProperty property = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == FUNCTIONAL_DATA_PROPERTY_3_NAME } as OWLDataProperty
    OWLDataProperty superProperty = ontology.dataPropertiesInSignature()
        .find { it.getIRI().remainder.get() == FUNCTIONAL_DATA_PROPERTY_1_NAME } as OWLDataProperty
    OWLDatatype superPropertyDataType = extractRange(ontology, superProperty)

    when:
    Type range = extractor.extractRange(property, ontology)

    then:
    1 * datatypeToTypeConverter.convert(superPropertyDataType) >> DATA_PROPERTY_TYPE
    range == DATA_PROPERTY_TYPE
  }

  private static OWLOntology createOntology() {
    return new TestOWLOntologyBuilder()
        .withDataPropertyDeclarations([DATA_PROPERTY_1_NAME, DATA_PROPERTY_2_NAME, DATA_PROPERTY_3_NAME,
                                       FUNCTIONAL_DATA_PROPERTY_1_NAME, FUNCTIONAL_DATA_PROPERTY_2_NAME,
                                       FUNCTIONAL_DATA_PROPERTY_3_NAME])
        .withDataPropertyDomain(DATA_PROPERTY_1_NAME, DATA_PROPERTY_1_DOMAIN)
        .withDataPropertyRange(DATA_PROPERTY_1_NAME, DATA_PROPERTY_1_RANGE)
        .withDataPropertyDomain(FUNCTIONAL_DATA_PROPERTY_1_NAME, FUNCTIONAL_DATA_PROPERTY_1_DOMAIN)
        .withDataPropertyRange(FUNCTIONAL_DATA_PROPERTY_1_NAME, FUNCTIONAL_DATA_PROPERTY_1_RANGE)
        .withEquivalentDataProperties(DATA_PROPERTY_1_NAME, DATA_PROPERTY_2_NAME)
        .withEquivalentDataProperties(FUNCTIONAL_DATA_PROPERTY_1_NAME, FUNCTIONAL_DATA_PROPERTY_2_NAME)
        .withSubDataPropertyOf(DATA_PROPERTY_3_NAME, DATA_PROPERTY_1_NAME)
        .withSubDataPropertyOf(FUNCTIONAL_DATA_PROPERTY_3_NAME, FUNCTIONAL_DATA_PROPERTY_1_NAME)
        .withFunctionalDataProperty(FUNCTIONAL_DATA_PROPERTY_1_NAME)
        .withFunctionalDataProperty(FUNCTIONAL_DATA_PROPERTY_2_NAME)
        .withFunctionalDataProperty(FUNCTIONAL_DATA_PROPERTY_3_NAME)
        .build()
  }

  private static OWLDatatype extractRange(OWLOntology ontology, OWLDataProperty property) {
    OWLDataPropertyRangeAxiom rangeAxiom = ontology.axioms(property)
        .find { it.axiomType == AxiomType.DATA_PROPERTY_RANGE } as OWLDataPropertyRangeAxiom
    return rangeAxiom.range.asOWLDatatype()
  }

  private static class OntologyIRI extends IRI {
    protected OntologyIRI(String prefix, @Nullable String suffix) {
      super(prefix, suffix)
    }
  }
}
