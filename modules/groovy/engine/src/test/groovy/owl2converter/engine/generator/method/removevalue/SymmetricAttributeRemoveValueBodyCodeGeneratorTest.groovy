package owl2converter.engine.generator.method.removevalue

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.RemoveValueMethod
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.SYMMETRIC
import static owl2converter.engine.test.generator.model.RemoveValueMethodTestFactory.createRemoveValueMethod

class SymmetricAttributeRemoveValueBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private SymmetricAttributeRemoveValueBodyCodeGenerator generator = new SymmetricAttributeRemoveValueBodyCodeGenerator()

  def 'should support remove value method of symmetric attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withCharacteristics([SYMMETRIC]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    generator.supports(removeValueMethod)
  }

  def 'should not support remove value method of non symmetric attribute'() {
    given:
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST).withCharacteristics([]).buildAttribute()

    when:
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    then:
    !generator.supports(removeValueMethod)
  }

  def 'should generate no validation code of remove value method'() {
    given:
    def attribute = new Attribute.AttributeBuilder('attribute', Types.STRING_LIST)
        .withCharacteristics([SYMMETRIC]).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    def generatedCode = generator.generateValidationCode(removeValueMethod)

    then:
    !generatedCode.isPresent()
  }

  def 'should generate remove value code of remove value method'() {
    given:
    def attributeName = 'attribute'
    def attribute = new Attribute.AttributeBuilder(attributeName, Types.STRING_LIST)
        .withCharacteristics([SYMMETRIC]).buildAttribute()
    RemoveValueMethod removeValueMethod = createRemoveValueMethod(attribute)

    when:
    def generatedCode = generator.generateSpecificCode(removeValueMethod)

    then:
    generatedCode.isPresent()
    generatedCode.get() == "${removeValueMethod.parameter.name}.remove${attributeName.capitalize()}(this)"
  }
}
