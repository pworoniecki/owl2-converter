package owl2converter.engine.converter.attribute

import owl2converter.engine.generator.method.common.TransitiveAttributeHelpersGenerator
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.AttributeCharacteristic
import owl2converter.groovymetamodel.attribute.relation.InverseAttributeRelation
import owl2converter.groovymetamodel.method.AddValueMethod
import owl2converter.groovymetamodel.method.DirectSetter
import owl2converter.groovymetamodel.method.CustomMethod
import owl2converter.groovymetamodel.method.Getter
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.method.RemoveValueMethod
import owl2converter.groovymetamodel.method.Setter
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType
import owl2converter.engine.test.generator.model.Types
import spock.lang.Specification
import spock.lang.Subject

import static owl2converter.groovymetamodel.AccessModifier.PUBLIC
import static owl2converter.groovymetamodel.attribute.AttributeCharacteristic.TRANSITIVE

class AttributeMethodsFactoryTest extends Specification {

  private static final String ATTRIBUTE_NAME = 'attr'
  private static final ListType LIST_ATTRIBUTE_TYPE = Types.STRING_LIST
  private static final SimpleType SINGLE_ATTRIBUTE_TYPE = Types.STRING

  private TransitiveAttributeHelpersGenerator transitiveAttributeHelpersGenerator = new TransitiveAttributeHelpersGenerator()

  @Subject
  private AttributeMethodsFactory methodsFactory = new AttributeMethodsFactory(transitiveAttributeHelpersGenerator)

  def 'should create only getter, setter, add/remove value methods for non-transitive list attribute'() {
    given:
    Attribute attribute = createListAttributeWithCharacteristics(characteristic)

    when:
    List<Method> methods = methodsFactory.createMethods(attribute)

    then:
    methods.size() == 4
    assertGetterCreatedForAttribute(methods, attribute, LIST_ATTRIBUTE_TYPE)
    assertSetterCreatedForAttribute(methods, attribute, LIST_ATTRIBUTE_TYPE)
    assertAddValueCreatedForAttribute(methods, attribute)
    assertRemoveValueCreatedForAttribute(methods, attribute)

    where:
    characteristic << AttributeCharacteristic.values() - TRANSITIVE
  }

  def 'should create only getter and setter for non-transitive single attribute'() {
    given:
    Attribute attribute = createSingleAttributeWithCharacteristics(characteristic)

    when:
    List<Method> methods = methodsFactory.createMethods(attribute)

    then:
    methods.size() == 2
    assertGetterCreatedForAttribute(methods, attribute, SINGLE_ATTRIBUTE_TYPE)
    assertSetterCreatedForAttribute(methods, attribute, SINGLE_ATTRIBUTE_TYPE)

    where:
    characteristic << AttributeCharacteristic.values() - TRANSITIVE
  }

  def 'should create accessors, getter and removalResult methods for transitive list attribute'() {
    given:
    Attribute attribute = createListAttributeWithCharacteristics(TRANSITIVE)

    when:
    List<Method> methods = methodsFactory.createMethods(attribute)

    then:
    methods.size() == 5
    assertGetterCreatedForAttribute(methods, attribute, LIST_ATTRIBUTE_TYPE)
    assertSetterCreatedForAttribute(methods, attribute, LIST_ATTRIBUTE_TYPE)
    assertAddValueCreatedForAttribute(methods, attribute)
    assertRemoveValueCreatedForAttribute(methods, attribute)
    assertFillTransitiveCreatedForAttribute(methods, attribute)
  }

  def 'should create only getter and setter methods for transitive single attribute'() {
    given:
    Attribute attribute = createSingleAttributeWithCharacteristics(TRANSITIVE)

    when:
    List<Method> methods = methodsFactory.createMethods(attribute)

    then:
    methods.size() == 2
    assertGetterCreatedForAttribute(methods, attribute, SINGLE_ATTRIBUTE_TYPE)
    assertSetterCreatedForAttribute(methods, attribute, SINGLE_ATTRIBUTE_TYPE)
  }

  def 'should create accessors, getter and direct setter methods for inverse attribute'() {
    given:
    Attribute attribute1 = createListAttributeWithCharacteristics()
    Attribute attribute2 = createListAttributeWithCharacteristics()
    attribute1.relations.add(new InverseAttributeRelation(attribute2))

    when:
    List<Method> methods = methodsFactory.createMethods(attribute1)

    then:
    methods.size() == 5
    assertGetterCreatedForAttribute(methods, attribute1, LIST_ATTRIBUTE_TYPE)
    assertSetterCreatedForAttribute(methods, attribute1, LIST_ATTRIBUTE_TYPE)
    assertAddValueCreatedForAttribute(methods, attribute1)
    assertRemoveValueCreatedForAttribute(methods, attribute1)
    assertDirectSetterCreatedForAttribute(methods, attribute1)
  }

  def 'should create getter, setter and direct setter methods for inverse attribute'() {
    given:
    Attribute attribute1 = createSingleAttributeWithCharacteristics()
    Attribute attribute2 = createSingleAttributeWithCharacteristics()
    attribute1.relations.add(new InverseAttributeRelation(attribute2))

    when:
    List<Method> methods = methodsFactory.createMethods(attribute1)

    then:
    methods.size() == 3
    assertGetterCreatedForAttribute(methods, attribute1, SINGLE_ATTRIBUTE_TYPE)
    assertSetterCreatedForAttribute(methods, attribute1, SINGLE_ATTRIBUTE_TYPE)
    assertDirectSetterCreatedForAttribute(methods, attribute1)
  }

  private static Attribute createListAttributeWithCharacteristics(AttributeCharacteristic... characteristics) {
    return new Attribute.AttributeBuilder(ATTRIBUTE_NAME, LIST_ATTRIBUTE_TYPE).withCharacteristics(characteristics.toList())
        .buildAttribute()
  }

  private static Attribute createSingleAttributeWithCharacteristics(AttributeCharacteristic... characteristics) {
    return new Attribute.AttributeBuilder(ATTRIBUTE_NAME, SINGLE_ATTRIBUTE_TYPE).withCharacteristics(characteristics.toList())
        .buildAttribute()
  }

  private static void assertGetterCreatedForAttribute(List<Method> methods, Attribute attribute, Type expectedType) {
    Getter getter = methods.find { it instanceof Getter } as Getter
    getter.with {
      assert it.attribute == attribute
      assert accessModifier == PUBLIC
      assert returnType == expectedType
    }
  }

  private static void assertSetterCreatedForAttribute(List<Method> methods, Attribute attribute, Type expectedType) {
    Setter setter = methods.find { it instanceof Setter } as Setter
    setter.with {
      assert it.attribute == attribute
      assert accessModifier == PUBLIC
      assert parameter.type == expectedType
    }
  }

  private static void assertAddValueCreatedForAttribute(List<Method> methods, Attribute attribute) {
    AddValueMethod addValueMethod = methods.find { it instanceof AddValueMethod } as AddValueMethod
    addValueMethod.with {
      assert it.attribute == attribute
      assert accessModifier == PUBLIC
      assert parameter.type == LIST_ATTRIBUTE_TYPE.extractSimpleParameterizedType()
    }
  }

  private static void assertRemoveValueCreatedForAttribute(List<Method> methods, Attribute attribute) {
    RemoveValueMethod removeValueMethod = methods.find { it instanceof RemoveValueMethod } as RemoveValueMethod
    removeValueMethod.with {
      assert it.attribute == attribute
      assert accessModifier == PUBLIC
      assert parameter.type == LIST_ATTRIBUTE_TYPE.extractSimpleParameterizedType()
    }
  }

  private void assertFillTransitiveCreatedForAttribute(List<Method> methods, Attribute attribute) {
    CustomMethod expectedMethod = transitiveAttributeHelpersGenerator.generateGetterHelperMethod(attribute)
    CustomMethod customMethod = methods.find { it.name == expectedMethod.name } as CustomMethod
    customMethod.with {
      assert !it.static
      assert it.returnType == VoidType.INSTANCE
      assert it.accessModifier == PUBLIC
      assert it.parameters*.type == [attribute.valueType]
    }
  }

  private static void assertDirectSetterCreatedForAttribute(List<Method> methods, Attribute attribute) {
    DirectSetter directSetter = methods.find { it.name == DirectSetter.getName(attribute) } as DirectSetter
    directSetter.with {
      assert it.returnType == VoidType.INSTANCE
      assert it.accessModifier == PUBLIC
      assert it.parameters*.type == [attribute.valueType]
    }
  }
}
