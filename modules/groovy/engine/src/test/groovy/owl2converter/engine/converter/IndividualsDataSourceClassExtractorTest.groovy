package owl2converter.engine.converter

import org.semanticweb.owlapi.model.OWLNamedIndividual
import org.semanticweb.owlapi.model.OWLOntology
import org.slf4j.Logger
import owl2converter.engine.configuration.model.ApplicationConfiguration
import owl2converter.engine.configuration.model.GeneratedClassesConfiguration
import owl2converter.engine.configuration.model.GeneratorConfiguration
import owl2converter.engine.exception.NotSupportedOwlElementException
import owl2converter.engine.test.TestOWLOntologyBuilder
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.groovymetamodel.Class
import owl2converter.groovymetamodel.Constructor
import owl2converter.groovymetamodel.Trait
import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.method.CustomMethod
import owl2converter.groovymetamodel.method.Method
import owl2converter.groovymetamodel.type.ListType
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.groovymetamodel.type.Type
import owl2converter.groovymetamodel.type.VoidType
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static owl2converter.groovymetamodel.AccessModifier.PRIVATE
import static owl2converter.groovymetamodel.AccessModifier.PUBLIC

class IndividualsDataSourceClassExtractorTest extends Specification {

    private static final String CLASS_PACKAGE = PACKAGE_NAME
    public static final String PACKAGE_NAME = 'org.test'

    private Logger logger = Mock()
    private ApplicationConfiguration configuration = new ApplicationConfiguration(generator: new GeneratorConfiguration(
            generatedClasses: new GeneratedClassesConfiguration(basePackage: CLASS_PACKAGE)
    ))
    private OWLLiteralCodeGenerator owlLiteralCodeGenerator = new OWLLiteralCodeGenerator()
    private IndividualsTopologicalSorter individualsTopologicalSorter = Mock()

    @Subject
    private IndividualsDataSourceClassExtractor extractor = new IndividualsDataSourceClassExtractor(logger,
            configuration, owlLiteralCodeGenerator, individualsTopologicalSorter)

    def 'should create IndividualsDataSource class with empty individuals initializing method'() {
        given:
        OWLOntology ontology = Mock()
        List<Class> classes = []

        when:
        Class individualsDataSourceClass = extractor.extract(ontology, classes)

        then:
        with(individualsDataSourceClass) {
            name == 'IndividualsDataSource'
            packageName == CLASS_PACKAGE
            constructors == [new Constructor(accessModifier: PRIVATE, isStatic: false, parameters: [])]
            methods.size() == 1
            methods.first() instanceof CustomMethod
        }
        with(individualsDataSourceClass.methods.first() as CustomMethod) {
            it.class == CustomMethod
            name == 'initializeIndividualsRepository'
            accessModifier == PUBLIC
            isStatic()
            returnType == VoidType.INSTANCE
        }
    }

    def 'should create IndividualsDataSource class with individual not related to any property'() {
        given:
        Class clazz = createClass(PACKAGE_NAME, 'SampleClass', 'SampleTrait')
        List<Class> classes = [clazz]
        OWLOntology ontology = new TestOWLOntologyBuilder()
                .withClassDeclaration(clazz.name)
                .withIndividual('individual1', clazz.name)
                .build()

        List<OWLNamedIndividual> individuals = ontology.individualsInSignature().findAll()
        individualsTopologicalSorter.sort(ontology, _ as List<OWLNamedIndividual>) >> individuals

        when:
        Class individualsDataSourceClass = extractor.extract(ontology, classes)

        then:
        individualsDataSourceClass.methods*.name == ['createIndividual1', 'initializeIndividualsRepository']
        assertMethodCode(individualsDataSourceClass.methods, 'createIndividual1', [
                'SampleClass individualInstance = SampleClass.createInstance()',
                "return new IndividualWrapper('individual1', individualInstance)"
        ].join('\n'))

        assertMethodCode(individualsDataSourceClass.methods, 'initializeIndividualsRepository',
                'IndividualRepository.addIndividual(createIndividual1())')
    }

    def 'should create IndividualsDataSource class with individual related to object property'() {
        given:
        Type class2Type = new SimpleType(PACKAGE_NAME, 'Class2')
        Attribute attribute1 = AttributeTestFactory.createAttribute('property1', class2Type, PRIVATE)

        Class class1 = createClass(PACKAGE_NAME, 'Class1', 'Trait1', attribute1)
        Class class2 = createClass(class2Type.packageName, class2Type.simpleName, 'Trait2')
        List<Class> classes = [class1, class2]
        OWLOntology ontology = new TestOWLOntologyBuilder()
                .withClassDeclarations(classes*.name)
                .withObjectPropertyDeclaration('property1')
                .withObjectPropertyDomain('property1', class1.name)
                .withIndividual('individual1', class1.name)
                .withIndividual('individual2', class2.name)
                .withObjectPropertyAssertion('property1', 'individual1', 'individual2')
                .build()

        List<OWLNamedIndividual> individuals = ontology.individualsInSignature().findAll()
        individualsTopologicalSorter.sort(ontology, _ as List<OWLNamedIndividual>) >> individuals.sort { it.getIRI().remainder.get() }

        when:
        Class individualsDataSourceClass = extractor.extract(ontology, classes)

        then:
        individualsDataSourceClass.methods*.name == ['createIndividual1', 'createIndividual2', 'initializeIndividualsRepository']
        assertMethodCode(individualsDataSourceClass.methods, 'createIndividual1', [
                'Class1 individualInstance = Class1.createInstance()',
                'individualInstance.with {',
                "setProperty1(IndividualRepository.getIndividual('individual2').individualInstance)",
                '}',
                "return new IndividualWrapper('individual1', individualInstance)"
        ].join('\n'))

        assertMethodCode(individualsDataSourceClass.methods, 'createIndividual2', [
                'Class2 individualInstance = Class2.createInstance()',
                "return new IndividualWrapper('individual2', individualInstance)"
        ].join('\n'))

        assertMethodCode(individualsDataSourceClass.methods, 'initializeIndividualsRepository', individuals.collect {
            "IndividualRepository.addIndividual(create${it.getIRI().remainder.get().capitalize()}())"
        }.join('\n'))
    }

    @Unroll
    def 'should create IndividualsDataSource class with individual related to data property'() {
        given:
        Type class2Type = new SimpleType(PACKAGE_NAME, 'Class2')
        Attribute attribute1 = AttributeTestFactory.createAttribute('property1', class2Type, PRIVATE)

        Class sampleClass = createClass(PACKAGE_NAME, 'Class1', 'Trait1', attribute1)
        List<Class> classes = [sampleClass]
        OWLOntology ontology = new TestOWLOntologyBuilder()
                .withClassDeclarations(classes*.name)
                .withDataPropertyDeclaration('property1')
                .withDataPropertyDomain('property1', sampleClass.name)
                .withIndividual('individual', sampleClass.name)
                .withDataPropertyAssertion('property1', 'individual', literal, literalType)
                .build()

        List<OWLNamedIndividual> individuals = ontology.individualsInSignature().findAll()
        individualsTopologicalSorter.sort(ontology, _ as List<OWLNamedIndividual>) >> individuals.sort { it.getIRI().remainder.get() }

        when:
        Class individualsDataSourceClass = extractor.extract(ontology, classes)

        then:
        individualsDataSourceClass.methods*.name == ['createIndividual', 'initializeIndividualsRepository']
        assertMethodCode(individualsDataSourceClass.methods, 'createIndividual', [
                'Class1 individualInstance = Class1.createInstance()',
                'individualInstance.with {',
                "setProperty1($expectedLiteralCode)",
                '}',
                "return new IndividualWrapper('individual', individualInstance)"
        ].join('\n'))

        assertMethodCode(individualsDataSourceClass.methods, 'initializeIndividualsRepository', individuals.collect {
            "IndividualRepository.addIndividual(create${it.getIRI().remainder.get().capitalize()}())"
        }.join('\n'))

        where:
        literal | literalType  | expectedLiteralCode
        'test'  | 'xsd:string' | "'test'"
        '2'     | 'xsd:int'    | "Integer.valueOf('2')"
    }

    def 'should create IndividualsDataSource class with multiple individuals'() {
        given:
        Type class2Type = new SimpleType(PACKAGE_NAME, 'Class2')
        Attribute attribute1 = AttributeTestFactory.createAttribute('property1', class2Type, PRIVATE)
        Attribute attribute2 = AttributeTestFactory.createAttribute('property2', new ListType(new SimpleType(String)), PRIVATE)

        Class class1 = createClass(PACKAGE_NAME, 'Class1', 'Trait1', attribute1)
        Class class2 = createClass(class2Type.packageName, class2Type.simpleName, 'Trait2', attribute2)
        List<Class> classes = [class1, class2]
        String individual2StringLiteral = 'test'
        OWLOntology ontology = new TestOWLOntologyBuilder()
                .withClassDeclarations(classes*.name)
                .withObjectPropertyDeclaration('property1')
                .withObjectPropertyDomain('property1', class1.name)
                .withObjectPropertyDeclaration('property2')
                .withObjectPropertyDomain('property2', class2.name)
                .withIndividual('individual1', class1.name)
                .withIndividual('individual2', class2.name)
                .withIndividual('individual3', class1.name)
                .withObjectPropertyAssertion('property1', 'individual1', 'individual2')
                .withDataPropertyAssertion('property2', 'individual2', individual2StringLiteral, 'xsd:string')
                .build()

        List<OWLNamedIndividual> individuals = ontology.individualsInSignature().findAll()
        List<OWLNamedIndividual> topologicallySortedIndividuals = individuals.reverse()
        individualsTopologicalSorter.sort(ontology, individuals) >> topologicallySortedIndividuals

        when:
        Class individualsDataSourceClass = extractor.extract(ontology, classes)

        then:
        individualsDataSourceClass.methods*.name == ['createIndividual1', 'createIndividual2', 'createIndividual3',
                                                     'initializeIndividualsRepository']
        assertMethodCode(individualsDataSourceClass.methods, 'createIndividual1', [
                'Class1 individualInstance = Class1.createInstance()',
                'individualInstance.with {',
                "setProperty1(IndividualRepository.getIndividual('individual2').individualInstance)",
                '}',
                "return new IndividualWrapper('individual1', individualInstance)"
        ].join('\n'))

        assertMethodCode(individualsDataSourceClass.methods, 'createIndividual2', [
                'Class2 individualInstance = Class2.createInstance()',
                'individualInstance.with {',
                "setProperty2(['$individual2StringLiteral'])",
                '}',
                "return new IndividualWrapper('individual2', individualInstance)"
        ].join('\n'))

        assertMethodCode(individualsDataSourceClass.methods, 'createIndividual3', [
                'Class1 individualInstance = Class1.createInstance()',
                "return new IndividualWrapper('individual3', individualInstance)"
        ].join('\n'))

        assertMethodCode(individualsDataSourceClass.methods, 'initializeIndividualsRepository', topologicallySortedIndividuals.collect {
            "IndividualRepository.addIndividual(create${it.getIRI().remainder.get().capitalize()}())"
        }.join('\n'))
    }

    def 'should throw an exception when individual is related to property lacking attribute metamodel equivalent'() {
        given:
        Class sampleClass = createClass(PACKAGE_NAME, 'Class1', 'Trait1')
        List<Class> classes = [sampleClass]
        OWLOntology ontology = new TestOWLOntologyBuilder()
                .withClassDeclarations(classes*.name)
                .withDataPropertyDeclaration('property1')
                .withDataPropertyDomain('property1', sampleClass.name)
                .withIndividual('individual', sampleClass.name)
                .withDataPropertyAssertion('property1', 'individual', 'testLiteral', 'xsd:string')
                .build()

        List<OWLNamedIndividual> individuals = ontology.individualsInSignature().findAll()
        individualsTopologicalSorter.sort(ontology, _ as List<OWLNamedIndividual>) >> individuals

        when:
        extractor.extract(ontology, classes)

        then:
        thrown NotSupportedOwlElementException
    }

    private static Class createClass(String packageName, String className, String traitName, Attribute attribute) {
        return createClass(packageName, className, traitName, [attribute])
    }

    private static Class createClass(String packageName, String className, String traitName,
                                     List<Attribute> attributes = []) {
        Trait implementedTrait = new Trait.Builder(packageName, traitName).withAttributes(attributes).buildTrait()
        return new Class.Builder(packageName, className).withImplementedTraits([implementedTrait]).buildClass()
    }

    private static void assertMethodCode(List<Method> methods, String methodName, String bodyCode) {
        CustomMethod method = methods.find { it.name == methodName } as CustomMethod
        assert method.bodyCode == bodyCode
    }
}
