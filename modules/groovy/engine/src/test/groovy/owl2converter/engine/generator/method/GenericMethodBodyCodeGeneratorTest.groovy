package owl2converter.engine.generator.method

import owl2converter.groovymetamodel.method.CustomMethod
import spock.lang.Specification
import spock.lang.Subject

class GenericMethodBodyCodeGeneratorTest extends Specification {

  @Subject
  private GenericMethodBodyCodeGenerator generator = new GenericMethodBodyCodeGenerator()

  def 'should generate code of custom method using code stored in it'() {
    given:
    def methodBodyCode = 'test body code'
    def customMethod = new CustomMethod.Builder('testMethod', methodBodyCode).buildMethod()

    when:
    def generatedCode = generator.generateCode(customMethod)

    then:
    generatedCode == customMethod.bodyCode
  }

  def 'should support custom method'() {
    expect:
    generator.supportedClass == CustomMethod
  }
}
