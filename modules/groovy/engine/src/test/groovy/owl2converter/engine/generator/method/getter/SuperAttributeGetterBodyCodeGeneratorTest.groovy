package owl2converter.engine.generator.method.getter

import owl2converter.groovymetamodel.attribute.Attribute
import owl2converter.groovymetamodel.attribute.relation.SuperAttributeOfRelation
import owl2converter.groovymetamodel.type.SimpleType
import owl2converter.engine.test.generator.model.AttributeTestFactory
import owl2converter.engine.test.generator.model.GetterTestFactory
import owl2converter.engine.test.generator.model.Types
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class SuperAttributeGetterBodyCodeGeneratorTest extends Specification {

  @Subject
  @Shared
  private SuperAttributeGetterBodyCodeGenerator generator = new SuperAttributeGetterBodyCodeGenerator()

  def 'should support getter of super attribute'() {
    given:
    def subAttribute = AttributeTestFactory.createAttribute()
    def attribute = new Attribute.AttributeBuilder('test', new SimpleType(String))
        .withRelations([new SuperAttributeOfRelation(subAttribute)]).buildAttribute()

    when:
    def getter = GetterTestFactory.createMethod(attribute)

    then:
    generator.supports(getter)
  }

  def 'should not support getter of non super attribute'() {
    given:
    def attribute = AttributeTestFactory.createEmptyNonStaticAttribute()

    when:
    def getter = GetterTestFactory.createMethod(attribute)

    then:
    !generator.supports(getter)
  }

  def 'should generate body code of list-type getter including all subattributes'() {
    given:
    def subAttributes = [
        AttributeTestFactory.createAttribute('first'),
        AttributeTestFactory.createAttribute('second')
    ]
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING_LIST)
        .withRelations(subAttributes.collect { new SuperAttributeOfRelation(it) })
        .buildAttribute()
    def getter = GetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateCode(getter)

    then:
    generatedCode == 'return this.test + this.first + this.second'
  }

  def 'should generate body code of single-type getter including all subattributes'() {
    given:
    def subAttributes = [
        AttributeTestFactory.createAttribute('first'),
        AttributeTestFactory.createAttribute('second')
    ]
    def attribute = new Attribute.AttributeBuilder('test', Types.STRING)
        .withRelations(subAttributes.collect { new SuperAttributeOfRelation(it) })
        .buildAttribute()
    def getter = GetterTestFactory.createMethod(attribute)

    when:
    def generatedCode = generator.generateCode(getter)

    then:
    generatedCode == [
        'def values = [this.test, this.first, this.second]',
        'if (values.count { it != null } == 0) {',
        'return null',
        '}',
        'if (values.count { it != null } == 1) {',
        'return values.find { it != null }',
        '}',
        "throw new IllegalValuesException('More than one value is set across subattributes and current attribute')"
    ].join('\n')
  }
}
