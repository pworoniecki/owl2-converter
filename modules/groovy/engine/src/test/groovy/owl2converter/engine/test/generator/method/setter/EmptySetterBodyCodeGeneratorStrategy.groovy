package owl2converter.engine.test.generator.method.setter

import owl2converter.engine.generator.method.setter.SetterBodyCodeGeneratorStrategy
import owl2converter.groovymetamodel.method.Setter

class EmptySetterBodyCodeGeneratorStrategy implements SetterBodyCodeGeneratorStrategy {

  private static final int DEFAULT_PRIORITY = 1
  private static final int DEFAULT_ORDER_WITHIN_PRIORITY_GROUP = 1

  private Setter supportedSetter
  private int priority
  private int orderWithinPriorityGroup

  EmptySetterBodyCodeGeneratorStrategy(Setter supportedSetter, int priority = DEFAULT_PRIORITY,
                                       int orderWithinPriorityGroup = DEFAULT_ORDER_WITHIN_PRIORITY_GROUP) {
    this.supportedSetter = supportedSetter
    this.priority = priority
    this.orderWithinPriorityGroup = orderWithinPriorityGroup
  }

  @Override
  boolean supports(Setter setter) {
    return supportedSetter.is(setter)
  }

  @Override
  Optional<String> generateValidationCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  Optional<String> generateSpecificCode(Setter setter) {
    return Optional.empty()
  }

  @Override
  int getPriority() {
    return priority
  }

  int getOrderWithinPriorityGroup() {
    return orderWithinPriorityGroup
  }
}
