package [PACKAGE_PLACEHOLDER]

import [PACKAGE_PLACEHOLDER].validator.DisjointClassesValidator

class ApplicationInitializer {

  static void main(String... args) {
    initialize()
  }

  static void initialize() {
    DisjointClassesValidator.checkDisjointClasses(ApplicationInitializer.package.name)
    IndividualsDataSource.initializeIndividualsRepository()
  }
}
