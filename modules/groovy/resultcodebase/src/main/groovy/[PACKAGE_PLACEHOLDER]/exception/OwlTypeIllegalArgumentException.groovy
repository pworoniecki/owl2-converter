package [PACKAGE_PLACEHOLDER].exception

import [PACKAGE_PLACEHOLDER].owlSimpleType.OwlType
import groovy.transform.InheritConstructors

@InheritConstructors
class OwlTypeIllegalArgumentException extends Exception {

  OwlTypeIllegalArgumentException(Object invalidValue, OwlType owlType, String reason) {
    super("Provided value '$invalidValue' is invalid value of type 'xsd:${owlType.xsdType}'. Reason: $reason")
  }
}
