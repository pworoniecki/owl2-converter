package [PACKAGE_PLACEHOLDER].exception

import groovy.transform.InheritConstructors

@InheritConstructors
class IrreflexiveAttributeException extends Exception {
}
