package [PACKAGE_PLACEHOLDER].exception

import groovy.transform.InheritConstructors

@InheritConstructors
class IllegalValuesException extends Exception {
}
