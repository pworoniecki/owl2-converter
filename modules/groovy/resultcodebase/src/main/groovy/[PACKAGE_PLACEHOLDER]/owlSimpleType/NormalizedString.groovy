package [PACKAGE_PLACEHOLDER].owlSimpleType

class NormalizedString extends StringBasedOwlType {

  private static final String REGEXP = /[^<&]*/
  private static final String XSD_TYPE = 'normalizedString'

  NormalizedString(String value, String xsdType = XSD_TYPE) {
    super(value, xsdType)
  }

  @Override
  protected String getRegexp() {
    return REGEXP
  }
}
