package [PACKAGE_PLACEHOLDER].owlSimpleType

class UnsignedLong extends UnsignedNumberOwlType {

  private static final String XSD_TYPE = 'unsignedLong'

  UnsignedLong(BigInteger value) {
    super(value, XSD_TYPE)
  }

  @Override
  protected BigIntegerRange getAllowedRange() {
    return new BigIntegerRange(from: 0, to: 2**64 - 1)
  }
}
