package [PACKAGE_PLACEHOLDER].owlSimpleType

class UnsignedInt extends UnsignedNumberOwlType {

  private static final String XSD_TYPE = 'unsignedInt'

  UnsignedInt(BigInteger value) {
    super(value, XSD_TYPE)
  }

  @Override
  protected BigIntegerRange getAllowedRange() {
    return new BigIntegerRange(from: 0, to: 2**32 - 1)
  }
}
