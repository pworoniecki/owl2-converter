package [PACKAGE_PLACEHOLDER].owlSimpleType

abstract class OwlType<T> {

  private T value
  private String xsdType

  OwlType(T value, String xsdType) {
    validate(value)
    this.value = value
    this.xsdType = xsdType
  }

  abstract void validate(T value)

  String getXsdType() {
    return xsdType
  }

  Object asType(Class clazz) {
    if (clazz == this.value.class) {
      return this.value
    }
    return super.asType(clazz)
  }

  @Override
  String toString() {
    return this.value?.toString()
  }

  @Override
  boolean equals(o) {
    if (this.is(o)) return true
    if (getClass() != o.class) return false

    OwlType owlType = (OwlType) o
    return value == owlType.value && xsdType == owlType.xsdType
  }

  @Override
  int hashCode() {
    int result
    result = (value != null ? value.hashCode() : 0)
    result = 31 * result + xsdType.hashCode()
    return result
  }

  T getValue() {
    return value
  }
}
