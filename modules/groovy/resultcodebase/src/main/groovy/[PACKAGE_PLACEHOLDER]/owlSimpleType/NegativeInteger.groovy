package [PACKAGE_PLACEHOLDER].owlSimpleType

import [PACKAGE_PLACEHOLDER].exception.OwlTypeIllegalArgumentException

class NegativeInteger extends OwlType<BigInteger> {

  private static final String XSD_TYPE = 'negativeInteger'

  NegativeInteger(BigInteger value) {
    super(value, XSD_TYPE)
  }

  @Override
  void validate(BigInteger value) {
    if (value >= 0) {
      throw new OwlTypeIllegalArgumentException(value, this, 'Value is non-negative')
    }
  }
}
