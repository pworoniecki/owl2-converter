package [PACKAGE_PLACEHOLDER].owlSimpleType

class Language extends StringBasedOwlType {

  private static final String REGEXP = /[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*/
  private static final String XSD_TYPE = 'language'

  Language(String value) {
    super(value, XSD_TYPE)
  }

  @Override
  protected String getRegexp() {
    return REGEXP
  }
}
