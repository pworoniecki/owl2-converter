package [PACKAGE_PLACEHOLDER].owlSimpleType

class NCName extends StringBasedOwlType {

  private static final String REGEXP = /[a-zA-Z_][a-zA-Z0-9.\-_]*/
  private static final String XSD_TYPE = 'NCName'

  NCName(String value) {
    super(value, XSD_TYPE)
  }

  @Override
  protected String getRegexp() {
    return REGEXP
  }
}
