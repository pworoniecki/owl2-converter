package [PACKAGE_PLACEHOLDER].owlSimpleType

class UnsignedByte extends UnsignedNumberOwlType {

  private static final String XSD_TYPE = 'unsignedByte'

  UnsignedByte(BigInteger value) {
    super(value, XSD_TYPE)
  }

  @Override
  protected BigIntegerRange getAllowedRange() {
    return new BigIntegerRange(from: 0, to: 2**8 - 1)
  }
}
