package [PACKAGE_PLACEHOLDER].owlSimpleType

class NMTOKEN extends StringBasedOwlType {

  private static final String REGEXP = /[ ]*[a-zA-Z0-9.\-_:]*[ ]*/
  private static final String XSD_TYPE = 'NMTOKEN'

  NMTOKEN(String value) {
    super(value, XSD_TYPE)
  }

  @Override
  protected String getRegexp() {
    return REGEXP
  }
}
