package [PACKAGE_PLACEHOLDER].owlSimpleType

class UnsignedShort extends UnsignedNumberOwlType {

  private static final String XSD_TYPE = 'unsignedShort'

  UnsignedShort(BigInteger value) {
    super(value, XSD_TYPE)
  }

  @Override
  protected BigIntegerRange getAllowedRange() {
    return new BigIntegerRange(from: 0, to: 2**16 - 1)
  }
}
