package [PACKAGE_PLACEHOLDER].owlSimpleType

import groovy.transform.InheritConstructors
import [PACKAGE_PLACEHOLDER].exception.OwlTypeIllegalArgumentException

@InheritConstructors
abstract class StringBasedOwlType extends OwlType<String> {

  @Override
  void validate(String value) {
    String regexp = getRegexp()
    if (value !=~ regexp) {
      throw new OwlTypeIllegalArgumentException(value, this, "Value doesn't match regular expression '$regexp'")
    }
  }

  protected abstract String getRegexp()
}
