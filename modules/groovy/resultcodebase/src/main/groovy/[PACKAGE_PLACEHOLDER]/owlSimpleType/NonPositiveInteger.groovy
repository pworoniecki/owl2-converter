package [PACKAGE_PLACEHOLDER].owlSimpleType

import [PACKAGE_PLACEHOLDER].exception.OwlTypeIllegalArgumentException

class NonPositiveInteger extends OwlType<BigInteger> {

  private static final String XSD_TYPE = 'nonPositiveInteger'

  NonPositiveInteger(BigInteger value) {
    super(value, XSD_TYPE)
  }

  @Override
  void validate(BigInteger value) {
    if (value > 0) {
      throw new OwlTypeIllegalArgumentException(value, this, 'Value is positive')
    }
  }
}
