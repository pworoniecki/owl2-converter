package [PACKAGE_PLACEHOLDER].individual

import [PACKAGE_PLACEHOLDER].exception.InvalidIndividualException

class IndividualWrapper {
  private String name
  private Object individualInstance

  IndividualWrapper() {}

  IndividualWrapper(String name, Object individualInstance) {
    this.name = name
    this.individualInstance = individualInstance
  }

  String getName() {
    return this.name
  }

  Object getIndividualInstance() {
    return individualInstance
  }

  public <T> T getIndividualInstance(Class<T> type) {
    if (individualInstance.class.isAssignableFrom(type)) {
      return (T) individualInstance
    }
    throw new IllegalArgumentException("Individual instance type '${individualInstance.class}' cannot be cast to type '$type'")
  }
}
