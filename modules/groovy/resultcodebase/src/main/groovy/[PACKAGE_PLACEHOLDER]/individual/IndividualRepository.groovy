package [PACKAGE_PLACEHOLDER].individual

import [PACKAGE_PLACEHOLDER].exception.IndividualDuplicationException

class IndividualRepository {
  private static Map<String, IndividualWrapper> individuals = [:]

  private IndividualRepository() {}

  static IndividualWrapper getIndividual(String name) {
    return individuals[name]
  }

  static Set<String> getIndividualNames() {
    return individuals.keySet()
  }

  static void addIndividual(IndividualWrapper individual) throws IndividualDuplicationException {
    if (individuals[individual.name]) {
      throw new IndividualDuplicationException()
    }
    individuals += [(individual.name): individual]
  }

  static void addIndividual(String name, Object individualClassInstance) throws IndividualDuplicationException {
    if (individuals[name]) {
      throw new IndividualDuplicationException()
    }
    individuals += [(name): new IndividualWrapper(name, individualClassInstance)]
  }
}
