package [PACKAGE_PLACEHOLDER].validator

import [PACKAGE_PLACEHOLDER].exception.IllegalValuesException
import [PACKAGE_PLACEHOLDER].exception.InvalidArgumentException

class SomeOfValuesValidator {

  static void validateValue(Object value, List<Object> someOfValues) {
    validateValues([value], someOfValues)
  }

  static void validateValues(List<Object> values, List<Object> someOfValues) {
    if (!someOfValues) {
      throw new InvalidArgumentException('Missing someOfValues list')
    }
    values.each {
      if (someOfValues.contains(it)) {
        return
      }
    }
    throw new IllegalValuesException("None value from someOfValues list is present in validated values list")
  }
}
