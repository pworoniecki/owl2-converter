package [PACKAGE_PLACEHOLDER].validator

import org.reflections.Reflections
import [PACKAGE_PLACEHOLDER].exception.DisjointClassViolationException
import [PACKAGE_PLACEHOLDER].model.ThingTrait

class DisjointClassesValidator {

  static void checkDisjointClasses(String classesPackage) {
    Reflections reflections = new Reflections(classesPackage)
    Set<Class> classes = reflections.getSubTypesOf(ThingTrait)
    classes.each {
      List<Class> implementedInterfaces = it.interfaces.toList()
      assertNoDisjointInterfaces(implementedInterfaces, it)
    }
  }

  private static void assertNoDisjointInterfaces(List<Class> implementedInterfaces, Class superClass) {
    implementedInterfaces.each { interface1 ->
      implementedInterfaces.each { interface2 ->
        if (areClassesDisjoint(interface1, interface2)) {
          String exceptionMessage = "Class/Trait '${superClass.name}' implements: '$interface1.name' and " +
              "'$interface2.name' but they are disjoint and cannot be implemented by the same class"
          throw new DisjointClassViolationException(exceptionMessage)
        }
      }
    }
  }

  private static boolean areClassesDisjoint(Class class1, Class class2) {
    return isClassDisjointWith(class1, class2) || isClassDisjointWith(class2, class1)
  }

  private static boolean isClassDisjointWith(Class analyzedClass, Class disjointClass) {
    return analyzedClass.getDeclaredAnnotation(DisjointWith)?.value()?.contains(disjointClass)
  }
}
