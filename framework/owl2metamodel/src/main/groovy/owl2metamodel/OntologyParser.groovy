package owl2metamodel

import org.semanticweb.owlapi.apibinding.OWLManager
import org.semanticweb.owlapi.functional.parser.OWLFunctionalSyntaxOWLParserFactory
import org.semanticweb.owlapi.model.OWLException
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLOntologyManager
import org.semanticweb.owlapi.owlxml.parser.OWLXMLParserFactory
import org.semanticweb.owlapi.rdf.rdfxml.parser.RDFXMLParserFactory
import org.springframework.stereotype.Component
import owl2metamodel.exception.OntologyFileReadException
import owl2metamodel.exception.OntologyParseException

@Component
class OntologyParser {

  /**
   * Translates a file with OWL2 ontology into an OWL ontology metamodel.<br>
   * Following formats of ontology representations are supported: functional, RDF, OWLXML.
   * @param ontologyFile a file pointing to the OWL2 ontology
   * @return ontology metamodel
   * @throws OntologyFileReadException if any error during file reading occurs
   * @throws OntologyParseException if syntax error in ontology files exists
   */
  OWLOntology parse(File ontologyFile) throws OntologyFileReadException, OntologyParseException {
    InputStream fileStream = null
    try {
      fileStream = ontologyFile.newInputStream()
      return parseOntology(fileStream)
    } catch (IOException e) {
      throw new OntologyFileReadException(ontologyFile, e)
    } catch (OWLException e) {
      throw new OntologyParseException(ontologyFile, e)
    } finally {
      if (fileStream != null) {
        fileStream.close()
      }
    }
  }

  private static OWLOntology parseOntology(InputStream ontologyContent) {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager()
    manager.ontologyParsers = [new RDFXMLParserFactory(), new OWLXMLParserFactory(),
                               new OWLFunctionalSyntaxOWLParserFactory()].toSet()
    return manager.loadOntologyFromOntologyDocument(ontologyContent)
  }
}
