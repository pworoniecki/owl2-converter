package owl2metamodel

import org.semanticweb.owlapi.apibinding.OWLManager
import org.semanticweb.owlapi.formats.FunctionalSyntaxDocumentFormat
import org.semanticweb.owlapi.model.OWLDocumentFormat
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLOntologyManager
import org.springframework.stereotype.Component
import owl2metamodel.exception.OntologyFileWriteException

@Component
class OntologyWriter {

  /**
   * Writes an OWL2 ontology to the place represented by an ontology file using the specified ontology format.<br>
   * @param ontology an ontology to be saved
   * @param ontologyFormat syntax to be used for ontology representation, for supported values check: {@link OwlOntologyFormat}
   * @param ontologyFile a file object with information where to store the ontology
   * @throws OntologyFileWriteException if any problem occurs during the writing process
   */
  void write(OWLOntology ontology, OwlOntologyFormat ontologyFormat, File ontologyFile) throws OntologyFileWriteException {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager()
    try {
      OutputStream outputFileStream = ontologyFile.newOutputStream()
      manager.saveOntology(ontology, ontologyFormat.value, outputFileStream)
    } catch (Exception e) {
      throw new OntologyFileWriteException(ontologyFile, e)
    }
  }
}
