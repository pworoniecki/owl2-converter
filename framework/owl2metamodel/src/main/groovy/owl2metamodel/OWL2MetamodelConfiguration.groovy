package owl2metamodel

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = OWL2MetamodelConfiguration)
class OWL2MetamodelConfiguration {
}
