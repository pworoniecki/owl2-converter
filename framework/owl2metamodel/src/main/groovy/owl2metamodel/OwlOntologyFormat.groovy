package owl2metamodel

import org.semanticweb.owlapi.formats.FunctionalSyntaxDocumentFormat
import org.semanticweb.owlapi.formats.RDFXMLDocumentFormat
import org.semanticweb.owlapi.model.OWLDocumentFormat

enum OwlOntologyFormat {

  FUNCTIONAL(new FunctionalSyntaxDocumentFormat()), RDF_XML(new RDFXMLDocumentFormat())

  private OWLDocumentFormat value

  OwlOntologyFormat(OWLDocumentFormat value) {
    this.value = value
  }

  OWLDocumentFormat getValue() {
    return value
  }
}
