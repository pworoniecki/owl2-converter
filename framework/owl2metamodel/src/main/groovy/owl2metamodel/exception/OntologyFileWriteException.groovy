package owl2metamodel.exception

class OntologyFileWriteException extends Exception {

  OntologyFileWriteException(File ontologyFile, Exception exceptionCause) {
    super("Unable to save ontology in file: ${ontologyFile.absolutePath}", exceptionCause)
  }
}
