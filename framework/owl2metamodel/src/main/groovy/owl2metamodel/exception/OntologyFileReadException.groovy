package owl2metamodel.exception

class OntologyFileReadException extends Exception {

  OntologyFileReadException(File ontologyFile, Exception exceptionCause) {
    super("Unable to read ontology file: ${ontologyFile.absolutePath}", exceptionCause)
  }
}
