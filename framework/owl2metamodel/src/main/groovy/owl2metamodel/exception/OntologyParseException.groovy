package owl2metamodel.exception

class OntologyParseException extends Exception {

  OntologyParseException(File ontologyFile, Exception exceptionCause) {
    super("Unable to parse ontology from file: ${ontologyFile.absolutePath}", exceptionCause)
  }
}
