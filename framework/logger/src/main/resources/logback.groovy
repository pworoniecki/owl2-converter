scan('30 seconds')

appender('Console-Appender', ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = '%msg%n'
  }
}
appender('File-Appender', FileAppender) {
  file = 'owl2converter.log'
  encoder(PatternLayoutEncoder) {
    pattern = '%msg%n'
  }
}

root(INFO, ['Console-Appender', 'File-Appender'])
