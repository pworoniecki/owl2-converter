package logger

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InjectionPoint
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope

@Configuration
class LoggerConfiguration {

  @Bean
  @Scope(BeanDefinition.SCOPE_PROTOTYPE)
  Logger logger(InjectionPoint injectionPoint){
    Class injectingClass = injectionPoint.methodParameter ? injectionPoint.methodParameter.containingClass
        : injectionPoint.field.declaringClass
    return LoggerFactory.getLogger(injectingClass)
  }
}
