package fromowl2builderapi;

import commoncodemetamodel.Element;
import org.semanticweb.owlapi.model.OWLOntology;
import translatorapi.TranslatorAPI;

import java.util.Set;

public abstract class FromOWL2Translator extends TranslatorAPI {

  /**
   * Returns common code metamodel, i.e. root files and directories being ontology translation result.
   * @param ontology an OWL2 ontology to be translated
   * @return a set of common code metamodel elements being a result of ontology OWL2 translation
   */
  public abstract Set<Element> translateOWL2Ontology(OWLOntology ontology);

  @Override
  public String getSourceLanguageName() {
    return "OWL2";
  }
}
