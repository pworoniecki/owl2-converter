package codetoowl2

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import owl2metamodel.OWL2MetamodelConfiguration

@Configuration
@ComponentScan(basePackageClasses = CodeToOWL2TranslationEngine)
@Import(OWL2MetamodelConfiguration)
class CodeToOWL2Configuration {
}
