package codetoowl2

import commoncodemetamodel.Element
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import toowl2builderapi.ToOWL2Translator

@Component
class CodeToOWL2TranslationEngine {

  private List<ToOWL2Translator> translators

  @Autowired
  CodeToOWL2TranslationEngine(Optional<List<ToOWL2Translator>> translators) {
    this.translators = translators.orElseGet { new ArrayList<>() }
  }

  /**
   * Translates set of elements from common code metamodel to a set of OWL2 ontologies for each registered code->owl2 translator.
   * Returned ontologies may be written to files using {@link owl2metamodel.OntologyWriter} class.
   * @param commonCodeMetamodel the set of root files and directories being part of common code metamodel
   * @return a list of created OWL ontologies, one per each registered translator
   */

  List<OWLOntology> translate(Set<Element> commonCodeMetamodel) {
    return translators.findAll { it.isEnabled() }.collect { it.translateCodeToOntology(commonCodeMetamodel) }
  }
}
