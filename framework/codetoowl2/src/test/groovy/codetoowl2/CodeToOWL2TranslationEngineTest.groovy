package codetoowl2

import commoncodemetamodel.Element
import org.semanticweb.owlapi.apibinding.OWLManager
import org.semanticweb.owlapi.model.IRI
import org.semanticweb.owlapi.model.OWLOntology
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll
import toowl2builderapi.ToOWL2Translator

class CodeToOWL2TranslationEngineTest extends Specification {

  private static Set<Element> CODE_METAMODEL = [].toSet()
  private static OWLOntology ONTOLOGY_1 = OWLManager.createOWLOntologyManager().createOntology(IRI.create('ontology1'))
  private static OWLOntology ONTOLOGY_2 = OWLManager.createOWLOntologyManager().createOntology(IRI.create('ontology2'))

  private ToOWL2Translator translator1 = Mock()
  private ToOWL2Translator translator2 = Mock()

  @Subject
  private CodeToOWL2TranslationEngine engine = new CodeToOWL2TranslationEngine(Optional.of([translator1, translator2]))

  def 'should translate OWL2 ontology to common code metamodel for each registered translator'() {
    given:
    translator1.isEnabled() >> true
    translator2.isEnabled() >> true

    when:
    List<OWLOntology> ontologies = engine.translate(CODE_METAMODEL)

    then:
    1 * translator1.translateCodeToOntology(CODE_METAMODEL) >> ONTOLOGY_1
    1 * translator2.translateCodeToOntology(CODE_METAMODEL) >> ONTOLOGY_2
    ontologies == [ONTOLOGY_1, ONTOLOGY_2]
  }

  @Unroll
  def 'should use only enabled translators'() {
    given:
    translator1.translateCodeToOntology(CODE_METAMODEL) >> ONTOLOGY_1
    translator2.translateCodeToOntology(CODE_METAMODEL) >> ONTOLOGY_2

    when:
    List<OWLOntology> ontologies = engine.translate(CODE_METAMODEL)

    then:
    1 * translator1.isEnabled() >> translator1Enabled
    1 * translator2.isEnabled() >> translator2Enabled
    ontologies == expectedOntologies

    where:
    translator1Enabled | translator2Enabled | expectedOntologies
    true               | true               | [ONTOLOGY_1, ONTOLOGY_2]
    true               | false              | [ONTOLOGY_1]
    false              | true               | [ONTOLOGY_2]
    false              | false              | []
  }
}
