package toowl2builderapi;

import commoncodemetamodel.Element;
import org.semanticweb.owlapi.model.OWLOntology;
import translatorapi.TranslatorAPI;

import java.util.Set;

public abstract class ToOWL2Translator extends TranslatorAPI {

  /**
   * Converts a common code metamodel to OWL2 ontology
   * @param elements common code metamodel root files and directories
   * @return OWL2 ontology representation being a result of common code metamodel translation
   */
  public abstract OWLOntology translateCodeToOntology(Set<Element> elements);

  @Override
  public String getTargetLanguageName() {
    return "OWL2";
  }
}
