package commoncodemetamodel

import spock.lang.Specification
import spock.lang.Subject

import java.nio.charset.StandardCharsets

@Subject(TextFile)
class TextFileTest extends Specification {

  def 'should return byte representation of text content using proper encoding'() {
    given:
    def fileContent = 'test'
    def fileEncoding = StandardCharsets.UTF_8
    def file = new TextFile('filename', 'txt', fileContent, fileEncoding)

    when:
    byte[] fileContentBytes = file.getContentAsBytes()

    then:
    fileContentBytes == fileContent.getBytes(fileEncoding)
  }
}
