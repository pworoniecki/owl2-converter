package commoncodemetamodel

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import java.nio.charset.StandardCharsets

class FolderTest extends Specification {

  private static final String FILE_1_NAME = 'file1'
  private static final String FILE_2_NAME = 'file2'
  private static final String FILE_3_NAME = 'file2'
  private static final String FILE_CONTENT = 'content'
  private static final String FILE_EXTENSION = 'txt'
  private static final String FOLDER_1_NAME = 'folder1'
  private static final String FOLDER_2_NAME = 'folder2'

  @Shared
  private Set<File> files = [new TextFile(FILE_1_NAME, FILE_EXTENSION, FILE_CONTENT, StandardCharsets.UTF_8),
                             new BinaryFile(FILE_2_NAME, FILE_EXTENSION, FILE_CONTENT.bytes),
                             new TextFile(FILE_3_NAME, null, FILE_CONTENT, StandardCharsets.UTF_8)
  ]

  @Shared
  private Set<Folder> folders = [new Folder(FOLDER_1_NAME, [].toSet()), new Folder(FOLDER_2_NAME, [].toSet())]

  @Subject
  private Folder testFolder = new Folder('testFolder', ([] + files + folders).toSet())


  def 'should return subfolders'() {
    expect:
    testFolder.subFolders == folders
  }

  def 'should return subfolder names'() {
    expect:
    testFolder.subFolderNames == [FOLDER_1_NAME, FOLDER_2_NAME].toSet()
  }

  def 'should return files'() {
    expect:
    testFolder.files == files
  }

  def 'should return file names with extensions if they are present'() {
    expect:
    testFolder.fileNames == [FILE_1_NAME + '.' + FILE_EXTENSION, FILE_2_NAME + '.' + FILE_EXTENSION, FILE_3_NAME].toSet()
  }

  def 'should return number of elements'() {
    expect:
    testFolder.elementsSize() == files.size() + folders.size()
  }

  def 'should add elements to folder'() {
    given:
    Folder folder = new Folder(testFolder.name, ([] + files + folders).toSet())
    File file1 = new BinaryFile('binaryFile', 'bin', [] as byte[])
    File file2 = new BinaryFile('binaryFile2', 'bin', [] as byte[])

    when:
    folder.addElements([file1, file2].toSet())

    then:
    folder.files == files + file1 + file2
    folder.subFolders == folders
  }

  def 'should return subfolder with given name'() {
    expect:
    testFolder.getSubfolder(folder.name).get() == folder

    where:
    folder << folders
  }

  def 'should return no subfolder when there is no subfolder with given name'() {
    expect:
    !testFolder.getSubfolder('nonExistingSubfolder').isPresent()
  }
}
