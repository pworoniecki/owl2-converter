package commoncodemetamodel.tools

import commoncodemetamodel.BinaryFile
import commoncodemetamodel.Folder
import commoncodemetamodel.TextFile
import commoncodemetamodel.tools.exception.FileWriteException
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Requires
import spock.lang.Specification
import spock.lang.Subject

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class CommonCodeMetamodelWriterTest extends Specification {

  @Subject
  private CommonCodeMetamodelWriter writer = new CommonCodeMetamodelWriter()

  @Rule
  private TemporaryFolder temporaryFolder = new TemporaryFolder()

  def 'should create root folder and several elements in it'() {
    given:
    Folder folder = new Folder('testFolder', [].toSet())
    BinaryFile file1 = new BinaryFile('file', 'bin', [] as byte[])
    TextFile file2 = new TextFile('file', 'txt', 'content', StandardCharsets.UTF_8)
    Path rootFolderPath = Paths.get(temporaryFolder.root.absolutePath, 'test', 'test2')

    when:
    writer.write(rootFolderPath, [folder, file1, file2].toSet())

    then:
    Files.exists(rootFolderPath)
    Files.exists(rootFolderPath.resolve(folder.name))
    Files.exists(rootFolderPath.resolve("$file1.name.$file1.extension"))
    Files.exists(rootFolderPath.resolve("$file2.name.$file2.extension"))
  }

  def 'should create common code metal files in given root folder'() {
    def textFileContent = 'fileContent'
    byte[] byteFileContent = [1, 3, 5]
    given:
    Folder folder = new Folder('src', [
        new Folder('main', [
            new TextFile('testTextFile', 'txt', textFileContent, Charset.defaultCharset()),
            new BinaryFile('testBinaryFile', 'bin', byteFileContent)
        ].toSet())
    ].toSet())
    Path rootFolderPath = Paths.get(temporaryFolder.root.absolutePath, 'test')

    when:
    writer.write(rootFolderPath, [folder].toSet())

    then:
    Files.exists(rootFolderPath)
    Path srcFolderPath = rootFolderPath.resolve('src')
    Path mainFolderPath = srcFolderPath.resolve('main')
    Path textFilePath = mainFolderPath.resolve('testTextFile.txt')
    Path binaryFilePath = mainFolderPath.resolve('testBinaryFile.bin')
    Files.exists(srcFolderPath)
    Files.exists(mainFolderPath)
    Files.exists(textFilePath)
    Files.exists(binaryFilePath)
    textFilePath.readLines() == [textFileContent]
    binaryFilePath.readBytes() == byteFileContent
  }

  @Requires({ os.linux })
  def 'should throw an exception when not able to create root folder'() {
    given:
    temporaryFolder.root.setWritable(false)
    Path rootFolderPath = Paths.get(temporaryFolder.root.absolutePath, 'test')

    when:
    writer.write(rootFolderPath, [].toSet())

    then:
    thrown FileWriteException
  }

  @Requires({ os.linux })
  def 'should throw an exception when not able to create folder in root folder'() {
    given:
    temporaryFolder.root.setWritable(false)
    Path rootFolderPath = Paths.get(temporaryFolder.root.absolutePath)
    Folder folder = new Folder('test', [].toSet())

    when:
    writer.write(rootFolderPath, [folder].toSet())

    then:
    thrown FileWriteException
  }

  @Requires({ os.linux })
  def 'should throw an exception when not able to create file in folder'() {
    given:
    Path rootFolderPath = Paths.get(temporaryFolder.root.absolutePath)
    Folder folder = new Folder('test', [new BinaryFile('testFile', 'bin', [] as byte[])].toSet())
    Path folderPath = rootFolderPath.resolve(folder.name)
    Files.createDirectory(folderPath)
    folderPath.toFile().setWritable(false)

    when:
    writer.write(rootFolderPath, [folder].toSet())

    then:
    thrown FileWriteException
  }
}
