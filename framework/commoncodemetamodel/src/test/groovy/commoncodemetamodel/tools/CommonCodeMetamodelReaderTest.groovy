package commoncodemetamodel.tools

import commoncodemetamodel.BinaryFile
import commoncodemetamodel.Folder
import commoncodemetamodel.TextFile
import commoncodemetamodel.tools.exception.FileReadException
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Requires
import spock.lang.Specification
import spock.lang.Subject

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class CommonCodeMetamodelReaderTest extends Specification {

  private static final Charset CUSTOM_ENCODING = StandardCharsets.ISO_8859_1
  private static final String ROOT_CHILD_DIRECTORY_1_NAME = 'test'
  private static final String ROOT_CHILD_DIRECTORY_2_NAME = 'test2'
  private static final String TEXT_FILE_NAME = 'file'
  private static final String TEXT_FILE_EXTENSION = 'txt'
  private static final String FULL_TEXT_FILE_NAME = TEXT_FILE_NAME + '.' + TEXT_FILE_EXTENSION
  private static final String BINARY_FILE_NAME = 'file'
  private static final String BINARY_FILE_EXTENSION = 'bin'
  private static final String FULL_BINARY_FILE_NAME = BINARY_FILE_NAME + '.' + BINARY_FILE_EXTENSION
  private static final String XML_FILE_NAME = 'textfile'
  private static final String XML_FILE_EXTENSION = 'xml'
  private static final String FULL_XML_FILE_NAME = XML_FILE_NAME + '.' + XML_FILE_EXTENSION
  private static final String TEXT_FILE_CONTENT = 'content'
  private static final byte[] BINARY_FILE_CONTENT = [1,3,5]

  @Subject
  private CommonCodeMetamodelReader reader = new CommonCodeMetamodelReader()

  @Rule
  private TemporaryFolder temporaryFolder = new TemporaryFolder()

  private Path temporaryFolderPath

  def setup() {
    temporaryFolderPath = temporaryFolder.root.toPath()
    Path testDirectoryPath = Files.createDirectory(temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME))
    Path test2DirectoryPath = Files.createDirectory(temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_2_NAME))
    Files.write(temporaryFolderPath.resolve(FULL_BINARY_FILE_NAME), BINARY_FILE_CONTENT)
    Files.write(testDirectoryPath.resolve(FULL_TEXT_FILE_NAME), TEXT_FILE_CONTENT.bytes)
    Files.write(testDirectoryPath.resolve(FULL_BINARY_FILE_NAME), BINARY_FILE_CONTENT)
    Files.write(test2DirectoryPath.resolve(FULL_XML_FILE_NAME), TEXT_FILE_CONTENT.bytes)
  }

  def 'should parse text file using default UTF8 encoding'() {
    given:
    Path textFilePath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME).resolve(FULL_TEXT_FILE_NAME)

    when:
    TextFile parsedFile = reader.parseTextFile(textFilePath)

    then:
    assertCorrectTextFile(parsedFile, StandardCharsets.UTF_8)
  }

  def 'should parse text file using custom encoding'() {
    given:
    Path textFilePath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME).resolve(FULL_TEXT_FILE_NAME)

    when:
    TextFile parsedFile = reader.parseTextFile(textFilePath, CUSTOM_ENCODING)

    then:
    assertCorrectTextFile(parsedFile, CUSTOM_ENCODING)
  }

  def 'should parse binary file'() {
    given:
    Path binaryFilePath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME).resolve(FULL_BINARY_FILE_NAME)

    when:
    BinaryFile parsedFile = reader.parseBinaryFile(binaryFilePath)

    then:
    assertCorrectBinaryFile(parsedFile)
  }

  def 'should parse folder skipping its subdirectories and files'() {
    given:
    Path folderPath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME)

    when:
    Folder parsedFolder = reader.parseFolderWithoutNestedElements(folderPath)

    then:
    with(parsedFolder) {
      name == ROOT_CHILD_DIRECTORY_1_NAME
      elementsSize() == 0
    }
  }

  def 'should parse folder with its subdirectories and files basing on mime types of files and using UTF8 encoding for text files'() {
    when:
    Folder parsedFolder = reader.parseFolder(temporaryFolderPath)

    then:
    with(parsedFolder) {
      name == temporaryFolderPath.fileName.toString()
      subFolderNames == [ROOT_CHILD_DIRECTORY_1_NAME, ROOT_CHILD_DIRECTORY_2_NAME].toSet()
      fileNames == [FULL_BINARY_FILE_NAME].toSet()
    }
    assertCorrectBinaryFile(parsedFolder.files.first() as BinaryFile)
    with(parsedFolder.getSubfolder(ROOT_CHILD_DIRECTORY_1_NAME).get()) {
      subFolders.isEmpty()
      fileNames == [FULL_TEXT_FILE_NAME, FULL_BINARY_FILE_NAME].toSet()
    }
    assertCorrectTextFile(parsedFolder.getSubfolder(ROOT_CHILD_DIRECTORY_1_NAME).get().files
        .find { it.name == TEXT_FILE_NAME && it.extension == TEXT_FILE_EXTENSION } as TextFile, StandardCharsets.UTF_8)
    assertCorrectBinaryFile(parsedFolder.getSubfolder(ROOT_CHILD_DIRECTORY_1_NAME).get().files
        .find { it.name == BINARY_FILE_NAME && it.extension == BINARY_FILE_EXTENSION } as BinaryFile)
    with(parsedFolder.getSubfolder(ROOT_CHILD_DIRECTORY_2_NAME).get()) {
      subFolders.isEmpty()
      fileNames == [FULL_XML_FILE_NAME].toSet()
      files.first() instanceof TextFile
    }
    with(parsedFolder.getSubfolder(ROOT_CHILD_DIRECTORY_2_NAME).get().files.first() as TextFile) {
      name == XML_FILE_NAME
      extension == XML_FILE_EXTENSION
      content == TEXT_FILE_CONTENT
      encoding == StandardCharsets.UTF_8
    }
  }

  def 'should determine types of files in folder basing on given extensions'() {
    given:
    Set<String> textFileExtensions = [BINARY_FILE_EXTENSION]
    Set<String> binaryFileExtensions = [TEXT_FILE_EXTENSION]

    when:
    Folder parsedFolder = reader.parseFolder(temporaryFolderPath, textFileExtensions, binaryFileExtensions)

    then:
    parsedFolder.getSubfolder(ROOT_CHILD_DIRECTORY_1_NAME).get().files
        .find { it.extension == TEXT_FILE_EXTENSION } instanceof BinaryFile
    parsedFolder.getSubfolder(ROOT_CHILD_DIRECTORY_1_NAME).get().files
        .find { it.extension == BINARY_FILE_EXTENSION } instanceof TextFile
  }

  def 'should throw an exception when trying to parse non-existing file as text file'() {
    when:
    reader.parseTextFile(Paths.get('nonExistingPath'))

    then:
    thrown FileReadException
  }

  @Requires({ os.linux })
  def 'should throw an exception when trying to parse non-readable file as text file'() {
    given:
    Path textFilePath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME).resolve(FULL_TEXT_FILE_NAME)
    textFilePath.toFile().setReadable(false, false)

    when:
    reader.parseTextFile(textFilePath)

    then:
    thrown FileReadException
  }

  def 'should throw an exception when trying to parse directory as text file'() {
    given:
    Path folderPath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME)

    when:
    reader.parseTextFile(folderPath)

    then:
    thrown FileReadException
  }

  def 'should throw an exception when trying to parse non-existing file as binary file'() {
    when:
    reader.parseBinaryFile(Paths.get('nonExistingPath'))

    then:
    thrown FileReadException
  }

  @Requires({ os.linux })
  def 'should throw an exception when trying to parse non-readable file as binary file'() {
    given:
    Path textFilePath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME).resolve(FULL_TEXT_FILE_NAME)
    textFilePath.toFile().setReadable(false, false)

    when:
    reader.parseBinaryFile(textFilePath)

    then:
    thrown FileReadException
  }

  def 'should throw an exception when trying to parse directory as binary file'() {
    given:
    Path folderPath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME)

    when:
    reader.parseBinaryFile(folderPath)

    then:
    thrown FileReadException
  }

  def 'should throw an exception when trying to parse non-existing folder'() {
    when:
    reader.parseFolder(Paths.get('nonExistingPath'))

    then:
    thrown FileReadException
  }

  @Requires({ os.linux })
  def 'should throw an exception when trying to parse non-readable folder'() {
    given:
    Path textFilePath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME)
    textFilePath.toFile().setReadable(false, false)

    when:
    reader.parseFolder(textFilePath)

    then:
    thrown FileReadException
  }

  def 'should throw an exception when trying to parse file as folder'() {
    given:
    Path textFilePath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME).resolve(FULL_TEXT_FILE_NAME)

    when:
    reader.parseFolder(textFilePath)

    then:
    thrown FileReadException
  }

  def 'should throw an exception when trying to parse non-existing folder without its nested elements'() {
    when:
    reader.parseFolderWithoutNestedElements(Paths.get('nonExistingPath'))

    then:
    thrown FileReadException
  }

  @Requires({ os.linux })
  def 'should throw an exception when trying to parse non-readable folder without its nested elements'() {
    given:
    Path textFilePath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME)
    textFilePath.toFile().setReadable(false, false)

    when:
    reader.parseFolderWithoutNestedElements(textFilePath)

    then:
    thrown FileReadException
  }

  def 'should throw an exception when trying to parse file as folder without its nested elements'() {
    given:
    Path textFilePath = temporaryFolderPath.resolve(ROOT_CHILD_DIRECTORY_1_NAME).resolve(FULL_TEXT_FILE_NAME)

    when:
    reader.parseFolderWithoutNestedElements(textFilePath)

    then:
    thrown FileReadException
  }

  private static void assertCorrectTextFile(TextFile file, Charset encoding) {
    file.with {
      assert name == TEXT_FILE_NAME
      assert extension == TEXT_FILE_EXTENSION
      assert content == TEXT_FILE_CONTENT
      assert encoding == encoding
    }
  }

  private static void assertCorrectBinaryFile(BinaryFile file) {
    file.with {
      assert name == BINARY_FILE_NAME
      assert extension == BINARY_FILE_EXTENSION
      assert content == BINARY_FILE_CONTENT
    }
  }
}
