package commoncodemetamodel

class BinaryFile extends File<byte[]> {

  /**
   * Class constructor specifying binary file's details.
   * @param name the name of binary file
   * @param extension the extension of binary file
   * @param content the array of bytes being the file content
   */
  BinaryFile(String name, String extension, byte[] content) {
    super(name, extension, content)
  }

  /**
   * Returns the file content as an array of bytes.
   * @return an array of bytes constituting the file content
   */
  @Override
  byte[] getContentAsBytes() {
    return getContent()
  }
}
