package commoncodemetamodel

import java.nio.charset.Charset

class TextFile extends File<String> {

  private Charset encoding

  /**
   * Class constructor specifying all file details.
   * @param name a name of the text file
   * @param extension a text file extension without dot symbol
   * @param content a text file content
   * @param encoding a content encoding
   */
  TextFile(String name, String extension, String content, Charset encoding) {
    super(name, extension, content)
    this.encoding = encoding
  }

  /**
   * Returns the text file encoding
   * @return the text file encoding
   */
  Charset getEncoding() {
    return encoding
  }

  /**
   * Returns the text file content as an array of bytes
   * @return the text file content as a byte array
   */
  @Override
  byte[] getContentAsBytes() {
    return content.getBytes(encoding)
  }
}
