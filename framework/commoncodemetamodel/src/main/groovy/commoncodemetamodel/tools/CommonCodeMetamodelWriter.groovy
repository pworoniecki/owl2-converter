package commoncodemetamodel.tools

import commoncodemetamodel.Element
import commoncodemetamodel.File
import commoncodemetamodel.Folder
import commoncodemetamodel.tools.exception.FileWriteException
import org.springframework.stereotype.Component

import java.nio.file.Files
import java.nio.file.Path

@Component
class CommonCodeMetamodelWriter {

  /**
   * Saves set of elements (folder, files) in the path. In case of folders their content is saved recursively.
   * @param path a directory in which the elements will be stored
   * @param elementsToSave elements to be stored in the given path
   * @throws FileWriteException when any folder or file cannot be created/written
   */
  void write(Path path, Set<Element>... elementsToSave) throws FileWriteException {
    createRootPath(path)
    elementsToSave.each {
      def folders = it.findAll { it instanceof Folder } as Set<Folder>
      def files = it.findAll { it instanceof File } as Set<File>
      folders.each { saveFolderWithFiles(path, it) }
      files.each { createFile(path, it) }
    }
  }

  private static void createRootPath(Path path) {
    try {
      Files.createDirectories(path)
    } catch (Exception e) {
      throw new FileWriteException(getFolderCreationErrorMessage(path), e)
    }

    if (!Files.exists(path)) {
      throw new FileWriteException(getFolderCreationErrorMessage(path))
    }
  }

  private static String getFolderCreationErrorMessage(Path path) {
    return "Unable to create folder $path"
  }

  private static void saveFolderWithFiles(Path parent, Folder folder) {
    Path createdFolderPath = createFolder(parent, folder)
    folder.files.each { createFile(createdFolderPath, it) }
    folder.subFolders.each { saveFolderWithFiles(createdFolderPath, it) }
  }

  private static Path createFolder(Path parent, Folder folder) throws FileWriteException {
    Path folderPath = parent.resolve(folder.name)
    try {
      if (!Files.exists(folderPath)) {
        Files.createDirectory(folderPath)
      }
    } catch (Exception e) {
      throw new FileWriteException(getFolderCreationErrorMessage(parent), e)
    }
    return folderPath
  }

  private static void createFile(Path path, File file) throws FileWriteException {
    Path filePath = path.resolve("${file.name}.${file.extension}")
    try {
      Files.write(filePath, file.contentAsBytes)
    } catch (Exception e) {
      throw new FileWriteException("Unable to create file $file", e)
    }
  }
}
