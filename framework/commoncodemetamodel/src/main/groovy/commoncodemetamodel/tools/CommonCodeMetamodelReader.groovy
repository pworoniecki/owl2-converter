package commoncodemetamodel.tools

import commoncodemetamodel.BinaryFile
import commoncodemetamodel.Element
import commoncodemetamodel.Folder
import commoncodemetamodel.TextFile
import commoncodemetamodel.tools.exception.FileReadException
import org.springframework.stereotype.Component

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path

@Component
class CommonCodeMetamodelReader {

  private static final String FILENAME_EXTENSION_SEPARATOR = '.'
  private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8
  private static final Set<String> TEXT_CONTENT_TYPES = ['application/json', 'application/xml']

  /**
   * Reads text file to get its common code metamodel representation. It assumes UTF8 encoding.
   * @param path path to the file
   * @return text file representation in common code metamodel
   * @throws FileReadException when there is no file under given path or it is not readable or it is not a file (but rather directory)
   */
  TextFile parseTextFile(Path path) throws FileReadException {
    return parseTextFile(path, DEFAULT_CHARSET)
  }

  /**
   * Reads text file in specific encoding to get its common code metamodel representation.
   * @param path path to the file
   * @param encoding encoding used to read file's content
   * @return text file representation in common code metamodel
   * @throws FileReadException when there is no file under given path or it is not readable or it is not a file (but rather directory)
   */
  TextFile parseTextFile(Path path, Charset encoding) throws FileReadException {
    assertCorrectFile(path)
    String content
    try {
      content = Files.readAllLines(path, encoding).join('\n')
    } catch (Exception e) {
      throw new FileReadException("Unable to read text file: $path", e)
    }
    Filename filename = new Filename(path.fileName.toString())
    return new TextFile(filename.name, filename.extension, content, encoding)
  }

  private static void assertCorrectFile(Path path) {
    assertCorrectFilesystemElement(path)
    if (!Files.isRegularFile(path)) {
      throw new FileReadException("$path is not a file")
    }
  }

  private static void assertCorrectFilesystemElement(Path path) {
    if (!Files.exists(path)) {
      throw new FileReadException("File $path does not exist")
    }
    if (!Files.isReadable(path)) {
      throw new FileReadException("File $path is not readable")
    }
  }

  /**
   * Reads binary file to get its common code metamodel representation.
   * @param path path to the file
   * @return binary file representation in common code metamodel
   * @throws FileReadException when there is no file under given path or it is not readable or it is not a file (but rather directory)
   */
  BinaryFile parseBinaryFile(Path path) throws FileReadException {
    assertCorrectFile(path)
    byte[] content
    try {
      content = Files.readAllBytes(path)
    } catch (Exception e) {
      throw new FileReadException("Unable to read binary file: $path", e)
    }
    Filename filename = new Filename(path.fileName.toString())
    return new BinaryFile(filename.name, filename.extension, content)
  }

  /**
   * Reads directory to get its common code metamodel representation. It does not read neither nested subdirectories nor files.
   * @param path a path to the directory
   * @return directory representation in common code metamodel without nested elements
   * @throws FileReadException when there is no directory under given path or it is not readable or it is not a directory (but rather file)
   */
  Folder parseFolderWithoutNestedElements(Path path) throws FileReadException {
    assertCorrectFolder(path)
    return new Folder(path.fileName.toString(), [].toSet())
  }

  private static void assertCorrectFolder(Path path) {
    assertCorrectFilesystemElement(path)
    if (!Files.isDirectory(path)) {
      throw new FileReadException("$path is not a directory")
    }
  }

  /**
   * Reads directory to get its common code metamodel representation. It reads nested subdirectories and files.<br>
   * File is always treated as textual if its extension is contained in textFileExtensions list.<br>
   * File is always treated as binary if its extension is contained in binaryFileExtensions list.<br>
   * If file's extension is not contained in any of lists, then its type is determined basing on MIME.<br>
   * Text files have MIME type starting with "text" (e.g. text/plain) or one of the following: application/json, application/xml.<br>
   * In case of other MIME type the file will be treated as binary.<br>
   * UTF8 encoding will be used to read content of text files.<br>
   * <br>
   * If more accurate behaviour is needed, then parse folder without nested elements using
   * {@link CommonCodeMetamodelReader#parseFolderWithoutNestedElements} method and add elements to it manually using {@link Folder#addElements} method.
   *
   * @param path a path to directory that will be read
   * @param textFileExtensions a list of file extensions that will always be parsed as text files
   * @param binaryFileExtensions a list of file extensions that will always be parsed as binary files
   * @return directory representation in common code metamodel with all nested elements
   * @throws FileReadException when there is no directory under given path or it is not readable or it is not a directory (but rather file)
   */
  Folder parseFolder(Path path, Set<String> textFileExtensions = [], Set<String> binaryFileExtensions = []) throws FileReadException {
    assertCorrectFolder(path)
    Set<Element> nestedElements = path.toFile().listFiles().collect { parse(it, textFileExtensions, binaryFileExtensions) }
    return new Folder(path.fileName.toString(), nestedElements)
  }

  private Element parse(File element, Set<String> textFileExtensions = [], Set<String> binaryFileExtensions = []) {
    if (element.isDirectory()) {
      return parseFolder(element.toPath(), textFileExtensions, binaryFileExtensions)
    } else {
      return isTextFile(element, textFileExtensions, binaryFileExtensions) ?
          parseTextFile(element.toPath()) : parseBinaryFile(element.toPath())
    }
  }

  private static boolean isTextFile(File file, Set<String> textFileExtensions = [], Set<String> binaryFileExtensions = []) {
    String fileExtension = new Filename(file.name).extension
    if (fileExtension in textFileExtensions) {
      return true
    }
    if (fileExtension in binaryFileExtensions) {
      return false
    }
    String contentType = Files.probeContentType(file.toPath())
    return contentType != null && (contentType.startsWith('text') || contentType in TEXT_CONTENT_TYPES)
  }

  private static class Filename {
    String name
    String extension

    Filename(String filenameWithExtension) {
      int filenameExtensionSeparatorIndex = filenameWithExtension.lastIndexOf(FILENAME_EXTENSION_SEPARATOR)
      if (filenameExtensionSeparatorIndex == -1) {
        name = filenameWithExtension
        extension = null
      } else {
        name = filenameWithExtension.take(filenameExtensionSeparatorIndex)
        extension = filenameWithExtension.substring(filenameExtensionSeparatorIndex + 1)
      }
    }
  }
}
