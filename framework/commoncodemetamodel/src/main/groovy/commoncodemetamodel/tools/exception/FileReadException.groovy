package commoncodemetamodel.tools.exception

class FileReadException extends Exception {

  FileReadException(String message) {
    super(message)
  }

  FileReadException(String message, Exception exceptionCause) {
    super(message, exceptionCause)
  }
}
