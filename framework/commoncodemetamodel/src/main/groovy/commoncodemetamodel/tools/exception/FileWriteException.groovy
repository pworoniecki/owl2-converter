package commoncodemetamodel.tools.exception

class FileWriteException extends Exception {

  FileWriteException(String message) {
    super(message)
  }

  FileWriteException(String message, Exception exceptionCause) {
    super(message, exceptionCause)
  }
}
