package commoncodemetamodel

class Folder extends Element {

  private  static final String FILENAME_EXTENSION_SEPARATOR = '.'

  private Set<Element> elements

  /**
   * Class constructor specifying folder details.
   *
   * @param name a folder name
   * @param elements a set of elements (files and folders) contained by the folder
   */
  Folder(String name, Set<Element> elements) {
    super(name)
    this.elements = new HashSet<>(elements)
  }

  /**
   * Returns subfolders contained directly in the folder.
   *
   * @return the set of subfolders contained in the folder
   */
  Set<Folder> getSubFolders() {
    return elements.findAll { it instanceof Folder }.toSet() as Set<Folder>
  }

  /**
   * Returns names of subfolders contained directly in the folder.
   *
   * @return the set of names of subfolders contained in the folder
   */
  Set<String> getSubFolderNames() {
    return getSubFolders()*.name
  }

  /**
   * Returns files contained directly in the folder.
   *
   * @return the set of files contained in the folder
   */
  Set<File> getFiles() {
    return elements.findAll { it instanceof File }.toSet() as Set<File>
  }

  /**
   * Returns names of files (with extensions) contained directly in the folder.
   *
   * @return the set of names with extensions of files in the folder
   */
  Set<String> getFileNames() {
    return files.collect { it.extension == null ? it.name : it.name + FILENAME_EXTENSION_SEPARATOR + it.extension }
  }

  /**
   * Returns copy of elements (folders and files) contained directly in the folder.
   *
   * @return the set of elements contained in the folder
   */
  Set<Element> getElements() {
    return new HashSet<>(elements)
  }

  /**
   * Returns the number of elements contained directly in the folder.
   *
   * @return the number of elements contained in the folder
   */
  int elementsSize() {
    return elements.size()
  }

  /**
   * Adds a set of elements to the folder. Skips existing elements.
   *
   * @param elements a set of elements to be added
   */
  void addElements(Set<Element> elements) {
    this.elements.addAll(elements)
  }

/**
 * Returns a subfolder identified by the name contained in the folder.
 *
 * @param name a name of subfolder
 * @return the subfolder identified by name if the subfolder exists; empty Optional otherwise
 */
 Optional<Folder> getSubfolder(String name) {
    return Optional.ofNullable(subFolders.find { it.name == name })
  }
}
