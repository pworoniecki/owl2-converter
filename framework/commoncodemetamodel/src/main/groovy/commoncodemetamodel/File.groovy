package commoncodemetamodel

abstract class File<T> extends Element {

  private String extension
  private T content

  /**
   * Class constructor specifying file details.
   * @param name a name of the file
   * @param extension an extension of the file without dot symbol
   * @param content a content of the file
   */
  File(String name, String extension, T content) {
    super(name)
    this.extension = extension
    this.content = content
  }

  /**
   * Returns the file extension.
   * @return the file extension
   */
  String getExtension() {
    return extension
  }

  /**
   * Returns the file content.
   * @return the file content
   */
  T getContent() {
    return content
  }

  abstract byte[] getContentAsBytes()
}
