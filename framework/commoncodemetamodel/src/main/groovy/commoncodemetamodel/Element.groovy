package commoncodemetamodel

abstract class Element {

  private String name

  /**
   * Class constructor specifying element name.
   * @param name the name of element (file or folder).
   */
  Element(String name) {
    this.name = name
  }

  /**
   * Returns the name of the element.
   * @return the name of the element (file or folder).
   */
  String getName() {
    return name
  }
}
