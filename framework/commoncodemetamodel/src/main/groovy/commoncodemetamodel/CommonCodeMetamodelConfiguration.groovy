package commoncodemetamodel

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = CommonCodeMetamodelConfiguration)
class CommonCodeMetamodelConfiguration {
}
