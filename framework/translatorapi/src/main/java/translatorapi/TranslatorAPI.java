package translatorapi;

public abstract class TranslatorAPI {

  private boolean enabled = true;

  /**
   * Sets translator to disabled
   */
  public void disable() {
    enabled = false;
  }

  /**
   * Sets translator to enabled
   */
  public void enable() {
    enabled = true;
  }

  /**
   * Indicates if translator is enabled
   * @return translator status (enabled/disabled)
   */
  public boolean isEnabled() {
    return enabled;
  }

  /**
   * Returns supported source language name
   * @return supported source language name
   */
  public abstract String getSourceLanguageName();

  /**
   * Returns supported target language name
   * @return supported target language name
   */
  public abstract String getTargetLanguageName();
}
