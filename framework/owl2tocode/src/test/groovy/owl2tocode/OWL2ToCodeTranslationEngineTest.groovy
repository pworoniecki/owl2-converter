package owl2tocode

import commoncodemetamodel.BinaryFile
import commoncodemetamodel.Element
import commoncodemetamodel.Folder
import commoncodemetamodel.TextFile
import fromowl2builderapi.FromOWL2Translator
import org.semanticweb.owlapi.model.OWLOntology
import owl2metamodel.OntologyParser
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import java.nio.charset.StandardCharsets

class OWL2ToCodeTranslationEngineTest extends Specification {

  private static Folder FOLDER1 = new Folder('folder1', [].toSet())
  private static Folder FOLDER2 = new Folder('folder2', [].toSet())

  private File ontologyFile = Mock()
  private OWLOntology ontology = Mock()
  private OntologyParser parser = Mock()
  private FromOWL2Translator translator1 = Mock()
  private FromOWL2Translator translator2 = Mock()

  @Subject
  private OWL2ToCodeTranslationEngine engine = new OWL2ToCodeTranslationEngine(parser, Optional.of([translator1, translator2]))

  def 'should translate OWL2 ontology to common code metamodel for each registered translator'() {
    given:
    TextFile file1 = new TextFile('file', 'txt', 'content', StandardCharsets.UTF_8)
    BinaryFile file2 = new BinaryFile('file', 'bin', [] as byte[])
    translator1.isEnabled() >> true
    translator2.isEnabled() >> true

    when:
    List<Set<Element>> codeMetamodels = engine.translate(ontologyFile)

    then:
    1 * parser.parse(ontologyFile) >> ontology
    1 * translator1.translateOWL2Ontology(ontology) >> [file1, FOLDER1].toSet()
    1 * translator2.translateOWL2Ontology(ontology) >> [FOLDER2, file2].toSet()
    codeMetamodels == [[file1, FOLDER1].toSet(), [FOLDER2, file2].toSet()]
  }

  @Unroll
  def 'should use only enabled translators'() {
    given:
    parser.parse(ontologyFile) >> ontology
    translator1.translateOWL2Ontology(ontology) >> [FOLDER1].toSet()
    translator2.translateOWL2Ontology(ontology) >> [FOLDER2].toSet()

    when:
    List<Set<Element>> codeMetamodels = engine.translate(ontologyFile)

    then:
    1 * translator1.isEnabled() >> translator1Enabled
    1 * translator2.isEnabled() >> translator2Enabled
    codeMetamodels == expectedMetamodels

    where:
    translator1Enabled | translator2Enabled | expectedMetamodels
    true               | true               | [[FOLDER1].toSet(), [FOLDER2].toSet()]
    true               | false              | [[FOLDER1].toSet()]
    false              | true               | [[FOLDER2].toSet()]
    false              | false              | []
  }
}
