package owl2tocode

import commoncodemetamodel.Element
import fromowl2builderapi.FromOWL2Translator
import org.semanticweb.owlapi.model.OWLOntology
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import owl2metamodel.OntologyParser
import owl2metamodel.exception.OntologyFileReadException
import owl2metamodel.exception.OntologyParseException

@Component
class OWL2ToCodeTranslationEngine {

  private OntologyParser ontologyParser
  private List<FromOWL2Translator> translators

  @Autowired
  OWL2ToCodeTranslationEngine(OntologyParser ontologyParser, Optional<List<FromOWL2Translator>> translators) {
    this.ontologyParser = ontologyParser
    this.translators = translators.orElseGet{ new ArrayList<>() }
  }

  /**
   * Translates ontology to files structure (common code metamodel) for each registered owl2->code translator
   * @param ontologyFile a file containing OWL2 ontology to translate
   * @return a list of common code metamodels, i.e. root files and directories being the result of ontology to code translation;
   * one metamodel per registered translator
   */
  List<Set<Element>> translate(File ontologyFile) throws OntologyFileReadException, OntologyParseException {
    OWLOntology ontology = ontologyParser.parse(ontologyFile)
    return translators.findAll { it.isEnabled() }.collect { it.translateOWL2Ontology(ontology) }
  }
}
