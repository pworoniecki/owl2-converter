package owl2tocode

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import owl2metamodel.OWL2MetamodelConfiguration

@Configuration
@ComponentScan(basePackageClasses = OWL2ToCodeTranslationEngine)
@Import(OWL2MetamodelConfiguration)
class OWL2ToCodeConfiguration {
}
